# FlexEngine
###### Lumnus Entertainment

FlexEngine its a game engine/framework written in JAVA SE (1.8+) by Lumnus Entertainment/Luann Athayde under the BSB license (see LICENSE).
Under no circumstance this software should be advertised under that company name or holder without explicit written delivered by the owner.

**Current development stage:**
Alpha 1.0.0

### Disclaimers
All games included here are only for demonstration, such offer show insights over the main framework functionality and components.
For more information about the components and the project, see the framework structure bellow.
All assets currently in use by the framework (current repository) has GNU/GPL v2 license or free licenses, including sounds, textures/images and scripts files. We do not recommend the use of those files in your game.

### Project Structure

Folders:

    resources -> contains all resources for the games/engine.
    
    resources/fonts -> contains all fonts available for the game/engine.
    
    resources/languages -> contains all languages available for the game/engine.
    
    resources/textures -> contains all textures available for the game/engine.
    
    resources/sounds/effects -> contains all sound effects available for the game/engine.
    
    resources/sounds/musics -> contains all musics available for the game/engine.
    
Packages:

    Main package: net.flexengine
    
    controller:
        All controller components available in the engine. Such as Input, Scenes, Textures, Server, Sound, etc.
        
    model:
        components:
            All 2D components available for the engine, such as Buttons, Labels, Panels, ect.
        
        config:
        
        cursor:
        
        database:
        
        game:
        
        language:
        
        map:
        
        math:
        
        scene:
        
        sound:
        
        time:
        
    utils:
    
    view:
    
    Optionals:
        FlexEngineBox
    
    
#### Build
The project uses Apache Maven (https://maven.apache.org/).

**To build:**
    
    mvn clean package
    
**To run FlexEngineBox:**
    
    java -jar ./FlexEngineMultiCore/target/FlexEngineMultiCore-pkg.jar
    
### Contatcs:

**Onwer/creator:** `luann.athayde@gmail.com`

**Company: Lumnus** `Entertainment (contact@lumnusentertainment.com)`