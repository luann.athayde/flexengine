/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.flexengine.inertiagame.scene;

import net.flexengine.controller.TextureController;
import net.flexengine.model.scene.GameScene;
import net.flexengine.model.scene.SceneBundle;
import net.flexengine.inertiagame.components.SpaceCraft;

import java.awt.*;

/**
 * @author Luann Athayde
 */
public class InertiaGameScene extends GameScene {

    private SpaceCraft spaceCraft;

    public InertiaGameScene() {
        super("InertiaGameScene");
    }

    @Override
    public void loadResources() {
        TextureController.getInstance().loadTextures();
    }

    @Override
    public void init(SceneBundle bundle) {
        super.init(bundle);
        spaceCraft = new SpaceCraft();
        add(spaceCraft);
    }

    @Override
    public void update() {
        super.update();
    }

    @Override
    public void render(Graphics2D g) {
        super.render(g);
    }

}
