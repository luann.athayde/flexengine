/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.flexengine.inertiagame.components;

import java.awt.Graphics2D;
import net.flexengine.controller.TextureController;
import net.flexengine.model.components.GameComponent;
import net.flexengine.model.scene.SceneBundle;
import net.flexengine.view.Texture;

/**
 *
 * @author Luann Athayde
 */
public class Shoot extends GameComponent {

    private Texture shootTexture;
    private float shootSpeed = 1.0f;
    private float maxSpeed = 30.0f;
    private float increaseSpeed = 1.0f;

    public Shoot() {
        super("Shoot", 0, 0);
    }

    @Override
    public void init(SceneBundle bundle) {
        shootTexture = TextureController.getInstance().getTexture("plasma.png");
    }

    @Override
    public void render(Graphics2D g) {
        super.render(g);
        if (shootTexture != null) {
            shootTexture.render(g, getX(), getY());
        }
    }

    @Override
    public void update() {
        shootSpeed += increaseSpeed;
        shootSpeed = shootSpeed > maxSpeed ? maxSpeed : shootSpeed;
        addY((int) -shootSpeed);
        if (getY() <= 0) {
            setVisible(false);
        }
    }

    public Texture getShootTexture() {
        return shootTexture;
    }

    public void setShootTexture(Texture shootTexture) {
        this.shootTexture = shootTexture;
    }

    public float getShootSpeed() {
        return shootSpeed;
    }

    public void setShootSpeed(float shootSpeed) {
        this.shootSpeed = shootSpeed;
    }

    public float getMaxSpeed() {
        return maxSpeed;
    }

    public void setMaxSpeed(float maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    public float getIncreaseSpeed() {
        return increaseSpeed;
    }

    public void setIncreaseSpeed(float increaseSpeed) {
        this.increaseSpeed = increaseSpeed;
    }
    
}
