/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.flexengine.inertiagame;


import net.flexengine.controller.FlexEngine;
import net.flexengine.controller.SceneController;
import net.flexengine.inertiagame.scene.InertiaGameScene;

/**
 * @author Luann Athayde
 */
public class InertiaGame {

    public static void main(String... args) {
        FlexEngine.getInstance().start();
        SceneController.getInstance().setNextScene(new InertiaGameScene());
    }

}
