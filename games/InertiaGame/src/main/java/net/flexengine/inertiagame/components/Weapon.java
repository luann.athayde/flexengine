/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.flexengine.inertiagame.components;

import net.flexengine.model.components.GameComponent;

/**
 *
 * @author Luann Athayde
 */
public class Weapon extends GameComponent {

    int needCharge;
    int charge;
    int shootID = 1;

    public Weapon() {
        super("Weapon", 0, 0);
        needCharge = 30;
        charge = 1;
    }

    public Shoot fire() {
        charge++;
        if (charge >= needCharge) {
            charge = 0;
            Shoot s = new Shoot();
            s.setId(shootID);
            shootID++;
            return s;
        }
        return null;
    }

    public int getNeedCharge() {
        return needCharge;
    }

    public void setNeedCharge(int needCharge) {
        this.needCharge = needCharge;
    }

    public int getCharge() {
        return charge;
    }

    public void setCharge(int charge) {
        this.charge = charge;
    }

    public int getShootID() {
        return shootID;
    }

    public void setShootID(int shootID) {
        this.shootID = shootID;
    }

}
