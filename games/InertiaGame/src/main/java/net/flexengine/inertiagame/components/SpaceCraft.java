/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.flexengine.inertiagame.components;

import net.flexengine.controller.InputController;
import net.flexengine.controller.TextureController;
import net.flexengine.model.components.GameComponent;
import net.flexengine.model.math.Vector2D;
import net.flexengine.model.scene.SceneBundle;
import net.flexengine.view.Texture;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Luann Athayde
 */
public class SpaceCraft extends GameComponent {

    private Texture spaceCraftTexture;

    private Weapon primaryWeapon1;
    private Weapon primaryWeapon2;

    private List<Shoot> shoots;

    private float shield = 100f;

    public SpaceCraft() {
        super("SpaceCraft", 0, 0);
        int width = (getWinSize().width / 10) * 3;
        shoots = new ArrayList<>();
        visible = true;
        primaryWeapon1 = new Weapon();
        primaryWeapon2 = new Weapon();
        int tmpHeight = (width * 797) / 532;
        spaceCraftTexture = TextureController.getInstance().getTexture("alienspaceship.png").scale(width, tmpHeight);
        setSize(spaceCraftTexture.getWidth(), spaceCraftTexture.getWidth());
        x = getWinSize().width / 2 - getWidth() / 2;
        y = getWinSize().height / 2 - getHeight() / 2;
        // ---
    }

    @Override
    public void render(Graphics2D g) {
        super.render(g);
        if (spaceCraftTexture != null) {
            spaceCraftTexture.render(g, x, y);
        }
        // --
        g.setColor(Color.WHITE);
        int tmpY = 40;
        g.drawString("Speed = [" + speed.getX() + "," + speed.getY() + "]", 10, tmpY);
        tmpY += 15;
        g.drawString("Position = [" + getX() + "," + getY() + "]", 10, tmpY);
        tmpY += 15;
        g.drawString("ColidTop = " + colidTop, 10, tmpY);
        tmpY += 15;
        g.drawString("ColidDown = " + colidDown, 10, tmpY);
        tmpY += 15;
        g.drawString("ColidLeft = " + colidLeft, 10, tmpY);
        tmpY += 15;
        g.drawString("ColidRight = " + colidRight, 10, tmpY);
        tmpY += 15;
        g.drawString("Shoots = " + shoots.size(), 10, tmpY);
        tmpY += 15;
        g.drawString("Shield = " + shield, 10, tmpY);
        // --
        shoots.stream().forEach((shoot) -> {
            shoot.render(g);
        });
    }

    private static final float BASIC_SPEED = 2.0f;
    private static final float INERTIA_GREEP = 0.50f;
    private static final float MAX_SPEED = 16.0f;
    private final Vector2D speed = new Vector2D();

    private volatile boolean colidTop, colidDown, colidLeft, colidRight;

    @Override
    public void update() {
        super.update();
        if (InputController.getInstance().isKeyPressed(KeyEvent.VK_LEFT)) {
            speed.addX(-BASIC_SPEED);
        }
        if (InputController.getInstance().isKeyPressed(KeyEvent.VK_RIGHT)) {
            speed.addX(BASIC_SPEED);
        }
        if (InputController.getInstance().isKeyPressed(KeyEvent.VK_UP)) {
            speed.addY(BASIC_SPEED);
        }
        if (InputController.getInstance().isKeyPressed(KeyEvent.VK_DOWN)) {
            speed.addY(-BASIC_SPEED);
        }
        // -- shields
        if (InputController.getInstance().isKeyPressed(KeyEvent.VK_C)) {
            shield -= 0.5f;
            if (shield <= 0f) {
                shield = 0;
            }
        }
        if (InputController.getInstance().isKeyPressed(KeyEvent.VK_V)) {
            shield += 0.5f;
            if (shield >= 100f) {
                shield = 100f;
            }
        }
        // -- inercia basica
        if (speed.getX() > 0) {
            speed.addX(-INERTIA_GREEP);
        }
        if (speed.getX() < 0) {
            speed.addX(INERTIA_GREEP);
        }
        if (speed.getY() > 0) {
            speed.addY(INERTIA_GREEP);
        }
        if (speed.getY() < 0) {
            speed.addY(-INERTIA_GREEP);
        }
        // -- limite de velocidade
        if (speed.getPointB().getX() > MAX_SPEED) {
            speed.getPointB().setX(MAX_SPEED);
        }
        if (speed.getPointB().getY() > MAX_SPEED) {
            speed.getPointB().setY(MAX_SPEED);
        }
        if (speed.getPointB().getX() < -MAX_SPEED) {
            speed.getPointB().setX(-MAX_SPEED);
        }
        if (speed.getPointB().getY() < -MAX_SPEED) {
            speed.getPointB().setY(-MAX_SPEED);
        }
        // -- move a nave
        x += speed.getX();
        y += speed.getY();
        // -- colisao da borda
        //colidTop = colidsWithTopWindowBorder(speed);
        //colidDown = colidsWithDownWindowBorder(speed);
        //colidLeft = colidsWithLeftWindowBorder(speed);
        //colidRight = colidsWithRightWindowBorder(speed);
        // -- shoot
        if (InputController.getInstance().isKeyPressed(KeyEvent.VK_Z)) {
            Shoot shoot1 = primaryWeapon1.fire();
            Shoot shoot2 = primaryWeapon2.fire();
            int tmpX = getX() + getWidth() / 2;
            int tmpY = getY() - 5;
            int moveX = (getWidth() * 210) / 532;
            if (shoot1 != null) {
                shoot1.setPosition(tmpX - moveX - 4, tmpY + 30);
                shoot1.init(new SceneBundle());
                shoots.add(shoot1);
            }
            if (shoot2 != null) {
                shoot2.setPosition(tmpX + moveX - 4, tmpY + 30);
                shoot2.init(new SceneBundle());
                shoots.add(shoot2);
            }
        }
        List<Shoot> removes = new ArrayList<>();
        for (int i = 0; i < shoots.size(); i++) {
            Shoot s = shoots.get(i);
            s.update();
            if (s.isVisible()) {
                removes.add(s);
            }
        }
        shoots.removeAll(removes);
        // --
    }

    public float getShieldValue() {
        return shield;
    }

}
