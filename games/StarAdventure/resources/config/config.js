
var oldLoad = window.onload;
window.onload = function (ev) {
    if (oldLoad) {
        oldLoad();
    }
    var comp = document.querySelector('.tabs').children[0];
    loadTab(comp, 1);
};

var tabs = [
    {id: 1, url: 'tabs/tab1.html'},
    {id: 2, url: 'tabs/tab2.html'},
    {id: 3, url: 'tabs/tab3.html'},
    {id: 4, url: 'tabs/tab4.html'}
];

function loadTab(comp, index) {
    if (index < 1 || index > tabs.length) {
        return;
    }
    var tabData = tabs[index - 1];
    $("#tab-content").load(tabData.url);
    setActive(comp);
}

function setActive( comp ) {
    var actives = document.querySelectorAll('.active');
    if (actives && actives.length > 0) {
        actives.forEach( function (active) {
            active.classList.remove('active');
        });
    }
    if (comp) {
        comp.classList.add('active');
    }
}