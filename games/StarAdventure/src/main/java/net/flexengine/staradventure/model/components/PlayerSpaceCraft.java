/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.flexengine.staradventure.model.components;

import net.flexengine.controller.InputController;
import net.flexengine.controller.SoundController;
import net.flexengine.controller.TextureController;
import net.flexengine.model.scene.SceneBundle;
import net.flexengine.utils.AsyncTask;
import net.flexengine.view.GameWindow;
import net.flexengine.view.Texture;
import net.java.games.input.Controller;

import java.awt.*;
import java.awt.event.KeyEvent;

/**
 * @author Luann Athayde
 */
public class PlayerSpaceCraft extends BasicSpaceCraft {

    protected Texture shootTexture;
    protected int needCharge;
    protected int shootCharge;
    protected int passiveRecover;

    protected double inertiaPollData;
    protected double keyboardPollDataX;
    protected double keyboardPollDataY;

    protected Controller joystick;
    protected InputController.JoystickButtons joystickButtons;
    protected float axisXData = 0;
    protected float axisYData = 0;

    protected int level;
    protected double exp;

    public PlayerSpaceCraft() {
        super();
        texture = TextureController.getInstance().getTexture("spacecraft.png");
        texture = texture.scale(texture.getWidth() / 4, texture.getHeight() / 4);
        shootTexture = TextureController.getInstance().getTexture("plasma_001.png");
        x = GameWindow.getInstance().getWidth() / 2 - texture.getWidth() / 2;
        y = GameWindow.getInstance().getHeight() - 200;
        basicSpeedX = 2;
        basicSpeedY = 2;
        maxHP = hp = 100;
        needCharge = 50;
        passiveRecover = 0;
        shootCharge = needCharge;
        keyboardPollDataX = 0d;
        keyboardPollDataY = 0d;
        inertiaPollData = 0.10d;
        // ---
        level = 1;
        exp = 0d;
    }

    @Override
    public void init(SceneBundle bundle) {
        super.init(bundle);
        try {
            joystick = InputController.getInstance().getJoystick();
            joystickButtons = InputController.getInstance().getJoystickButtons();
        } catch (Exception e) {
        }
    }

    @Override
    public void update() {
        super.update();
        // ---
        borderCollision();
        // ---
        if (InputController.getInstance().getControlType() == InputController.CONTROLLER_KEYBOARD) {

            if (InputController.getInstance().isKeyPressed(KeyEvent.VK_UP)) {
                this.keyboardPollDataY = -1.0f;
            }
            if (InputController.getInstance().isKeyPressed(KeyEvent.VK_DOWN)) {
                this.keyboardPollDataY = 1.0f;
            }
            if (InputController.getInstance().isKeyPressed(KeyEvent.VK_LEFT)) {
                this.keyboardPollDataX = -1.0f;
            }
            if (InputController.getInstance().isKeyPressed(KeyEvent.VK_RIGHT)) {
                this.keyboardPollDataX = 1.0f;
            }
            setBasicSpeedX((int) (getKeyboardPollDataX() * 2.5d));
            setBasicSpeedY((int) (getKeyboardPollDataY() * 2.5d));

            if (InputController.getInstance().isKeyPressed(KeyEvent.VK_Z)) {
                chargeShoot();
            }

        } else if (InputController.getInstance().getControlType() == InputController.CONTROLLER_JOYSTICK) {

            if (joystick.poll()) {
                axisXData = joystickButtons.getAxisX().getPollData();
                axisYData = joystickButtons.getAxisY().getPollData();
                setBasicSpeedX((int) (axisXData * 5.0f));
                setBasicSpeedY((int) (axisYData * 5.0f));
                float buttonXData = joystickButtons.getButtonA().getPollData();
                if (buttonXData > 0.0f) {
                    chargeShoot();
                }
            }

        }
        // ---
        // Passive HP recover...
        passiveRecover++;
        if (passiveRecover == 50) {
            setHp(getHp() + (level / 25) + 1);
            passiveRecover = 0;
        }
    }

    public double getKeyboardPollDataX() {
        if (keyboardPollDataX > 0d) {
            keyboardPollDataX -= inertiaPollData;
        } else {
            keyboardPollDataX += inertiaPollData;
        }
        return keyboardPollDataX;
    }

    public double getKeyboardPollDataY() {
        if (keyboardPollDataY > 0d) {
            keyboardPollDataY -= inertiaPollData;
        } else {
            keyboardPollDataY += inertiaPollData;
        }
        return keyboardPollDataY;
    }

    public void chargeShoot() {
        shootCharge++;
        if (shootCharge >= needCharge) {
            shootCharge = 0;
            shot();
        }
    }

    @Override
    public void shot() {
        Shot shot = new Shot(s -> {
            if (s.getY() < -s.getHeight() && s.isVisible()) {
                s.setVisible(false);
            }
        });
        shot.setTexture(shootTexture);
        shot.setSpeedY(-4f);
        shot.setMaxSpeed(-2.0f);
        shot.setBasicSpeed(-0.01f);
        shot.setPosition(x + texture.getWidth() / 2, y);
        shots.add(shot);
        // ---
        SoundController.getInstance().play(
                SoundController.getInstance().get("plasma_gun.wav")
        );
    }

    @Override
    public void render(Graphics2D g) {
        super.render(g);
    }

    public int getShootCharge() {
        return shootCharge;
    }

    @Override
    public void receiveDamage(int damage) {
        super.receiveDamage(damage);
        InputController.getInstance().rumbleJoystick(0.5f, 1000);
    }

    public int getNeedCharge() {
        return needCharge;
    }

    public void setNeedCharge(int needCharge) {
        this.needCharge = needCharge;
    }

    public void decreaseNeedCharge() {
        if (level % 5 == 0) {
            needCharge -= 1;
            needCharge = needCharge < 7 ? 7 : needCharge;
        }
    }

    public int getLevel() {
        return level;
    }

    public double getExp() {
        return exp;
    }

    public void addExp(double value) {
        this.exp += value;
        if (exp >= 1000) {
            this.decreaseNeedCharge();
            level++;
            double diff = exp - 1000d;
            while (diff > 0) {
                level++;
                diff = diff - 1000d;
                this.decreaseNeedCharge();
            }
            exp = diff < 0 ? -diff : 0d;
            // ---
            this.maxHP = (level * 5) + 100;
            recoverHp();
            // ---
            if (level >= 15 && level < 30) {
                shootTexture = TextureController.getInstance().getTexture("plasma_002.png");
            } else if (level >= 30 && level < 40) {
                shootTexture = TextureController.getInstance().getTexture("plasma_003.png");
            } else if (level >= 40 && level < 50) {
                shootTexture = TextureController.getInstance().getTexture("plasma_004.png");
            } else if (level >= 50) {
                shootTexture = TextureController.getInstance().getTexture("plasma_005.png");
            }
            // ---
        }
    }

    private void recoverHp() {
        AsyncTask.newInstance().run(() -> {
            try {
                int regen = (int) ((getMaxHP() - getHp()) * 0.10d);
                int totalRegen = 0;
                while ( totalRegen < regen && getHp() < getMaxHP()  ) {
                    setHp(getHp() + 1);
                    totalRegen++;
                    Thread.sleep(200);
                }
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        });
    }

    @Override
    public int getWidth() {
        return texture != null ? texture.getWidth() : size.width;
    }

    @Override
    public int getHeight() {
        return texture != null ? texture.getHeight() : size.height;
    }

}
