package net.flexengine.staradventure.model.components;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import net.flexengine.controller.FlexEngine;
import net.flexengine.controller.FontController;
import net.flexengine.model.components.Button;
import net.flexengine.model.components.ComponentStyle;
import net.flexengine.model.components.GameComponent;
import net.flexengine.model.components.GamePanel;
import net.flexengine.model.config.Priority;
import net.flexengine.staradventure.StarAdventure;
import net.flexengine.utils.StylesUtil;
import net.flexengine.view.GameWindow;

import java.awt.*;
import java.awt.event.ActionListener;

/**
 *
 */
@Data
@Builder
@AllArgsConstructor
public class ExitConfirmationPanel extends GamePanel {

    // ---
    private static final int BUTTON_WIDTH = 300;
    private static final int BUTTON_HEIGHT = 40;
    private static final Font buttonFont = FontController.getFont(FontController.getDefaultFont(), 1, 16);

    private final ComponentStyle style = StarAdventure.DEFAULT_STYLE;

    private Button buttonBack;
    private Button buttonConfirm;

    public ExitConfirmationPanel() {
        super("ExitConfirmationPanel", 0, 0, GameWindow.getInstance().getViewport());
        setPriority(Priority.HIGH_PRIORITY);
        setBackgroundColor(Color.DARK_GRAY);
        setVisible(false);
        setTransparence(0.9f);
        setDraggable(false);
        buttonBack = new Button("Voltar");
        buttonConfirm = new Button("Confirmar");

        applyStyle(buttonBack);
        applyStyle(buttonConfirm);

        buttonBack.addEventListener((ActionListener) e -> {
            setVisible(false);
        });
        buttonConfirm.addEventListener((ActionListener) e -> {
            FlexEngine.getInstance().stop();
        });

        add(buttonBack);
        add(buttonConfirm);
    }

    private void applyStyle(Button button) {
        button.setSize(BUTTON_WIDTH, BUTTON_HEIGHT);
        button.setPosition(
                GameComponent.SCREEN_HORIZONTAL_CENTER_ALIGN,
                GameComponent.SCREEN_VERTICAL_CENTER_ALIGN
        );
        button.setRoundRect(Button.ROUND_RECT_NONE);
        button.setFont(buttonFont);
        StylesUtil.aplyStyle(button, style);
    }

    @Override
    public void update() {
        setSize(GameWindow.getInstance().getViewport());
        applyStyle(buttonBack);
        applyStyle(buttonConfirm);
        buttonBack.setPosition(
                GameComponent.SCREEN_HORIZONTAL_CENTER_ALIGN,
                GameComponent.SCREEN_VERTICAL_CENTER_ALIGN,
                0, -buttonBack.getHeight() / 2
        );
        buttonConfirm.setPosition(
                GameComponent.SCREEN_HORIZONTAL_CENTER_ALIGN,
                GameComponent.SCREEN_VERTICAL_CENTER_ALIGN,
                0, buttonBack.getHeight() / 2
        );
        super.update();
    }

    // -------------------------------------------------------------------------
    @Override
    public boolean equals(Object o) {
        return super.equals(o);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

}
