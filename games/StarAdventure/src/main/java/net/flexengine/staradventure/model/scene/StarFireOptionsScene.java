package net.flexengine.staradventure.model.scene;

import net.flexengine.controller.InputController;
import net.flexengine.controller.SceneController;
import net.flexengine.model.scene.GameScene;

import java.awt.event.KeyEvent;

/**
 * @author Luann Athayde
 */
public class StarFireOptionsScene extends GameScene {

    @Override
    public void loadResources() {
        super.loadResources();
    }

    @Override
    public synchronized void update() {
        super.update();

        if (InputController.getInstance().isKeyPressed(KeyEvent.VK_ESCAPE)) {
            SceneController.getInstance().setNextScene(new StarFireGameMenuScene()).changeScene();
        }

    }

}
