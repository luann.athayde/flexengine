/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.flexengine.staradventure.controller;

import net.flexengine.controller.ResourceController;
import net.flexengine.controller.SoundController;
import net.flexengine.staradventure.model.components.BasicEnemyAI;
import net.flexengine.staradventure.model.components.EnemySpaceCraft;
import net.flexengine.staradventure.model.components.PlayerSpaceCraft;
import net.flexengine.staradventure.model.config.Stage;
import net.flexengine.view.GameWindow;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

/**
 * @author siga
 */
public class StageController {

    private static final HashMap<Long, Stage> stages = new HashMap<>();

    private final int enymeHP = 200;
    private final DecimalFormat    df = new DecimalFormat("000");
    private final PlayerSpaceCraft player;

    public StageController(PlayerSpaceCraft player) {
        this.player = player;
    }

    public Stage loadStage(long level) {
        Stage stage = new Stage(level);
        stage.setName("stage" + df.format(level));
        int id = 1;
        List<EnemySpaceCraft> enemies = new ArrayList<>();
        try {
            Scanner sc = new Scanner(
                ResourceController.getResourceAsStream("stages/" + stage.getName() + ".dat")
            );

            String sound = sc.nextLine();
            stage.setStageSound(
                    SoundController.getInstance().get(sound)
            );

            int xSteep = GameWindow.getInstance().getWidth() / 20;
            int ySteep = GameWindow.getInstance().getHeight() * 2;
            int tmpY = 0;
            while (sc.hasNextLine()) {
                String line = sc.nextLine();
                int tmpX = 0;
                for (int i = 0; i < line.length(); i++) {
                    char item = line.charAt(i);
                    EnemySpaceCraft enemy = null;
                    switch (item) {
                        case 'A':
                            enemy = new EnemySpaceCraft(enymeHP, enymeHP, new BasicEnemyAI(player));
                            enemy.setBounds(0, 0, 100, 100);
                            break;
                        case 'B':
                            enemy = new EnemySpaceCraft(enymeHP, enymeHP, new BasicEnemyAI(player));
                            enemy.setTextureName("spacecraft_001.png");
                            enemy.setBounds(0, 0, 100, 100);
                            break;
                        case 'C':
                            enemy = new EnemySpaceCraft(enymeHP, enymeHP, new BasicEnemyAI(player));
                            enemy.setTextureName("spacecraft_002.png");
                            enemy.setBounds(0, 0, 100, 100);
                            break;
                        case 'D':
                            enemy = new EnemySpaceCraft(enymeHP, enymeHP, new BasicEnemyAI(player));
                            enemy.setTextureName("spacecraft_003.png");
                            enemy.setBounds(0, 0, 100, 100);
                            break;
                        case 'E':
                            enemy = new EnemySpaceCraft(enymeHP, enymeHP, new BasicEnemyAI(player));
                            enemy.setTextureName("spacecraft_004.png");
                            enemy.setBounds(0, 0, 100, 100);
                            break;
                        case 'F':
                            enemy = new EnemySpaceCraft(enymeHP, enymeHP, new BasicEnemyAI(player));
                            enemy.setTextureName("spacecraft_005.png");
                            enemy.setBounds(0, 0, 100, 100);
                            break;
                        case 'G':
                            enemy = new EnemySpaceCraft(enymeHP, enymeHP, new BasicEnemyAI(player));
                            enemy.setTextureName("spacecraft_006.png");
                            enemy.setBounds(0, 0, 100, 100);
                            break;
                        case 'H':
                            enemy = new EnemySpaceCraft(enymeHP, enymeHP, new BasicEnemyAI(player));
                            enemy.setTextureName("spacecraft_007.png");
                            enemy.setBounds(0, 0, 100, 100);
                            break;
                        case 'I':
                            enemy = new EnemySpaceCraft(enymeHP, enymeHP, new BasicEnemyAI(player));
                            enemy.setTextureName("spacecraft_008.png");
                            enemy.setBounds(0, 0, 100, 100);
                            break;
                        case 'J':
                            enemy = new EnemySpaceCraft(enymeHP, enymeHP, new BasicEnemyAI(player));
                            enemy.setTextureName("spacecraft_009.png");
                            enemy.setBounds(0, 0, 100, 100);
                            break;
                        // ---
                        case 'X':
                            enemy = new EnemySpaceCraft(enymeHP*2, enymeHP*2, new BasicEnemyAI(player));
                            enemy.setTextureName("spacecraft_blue.png");
                            enemy.setBounds(0, 0, 150, 150);
                            break;
                        case 'Y':
                            enemy = new EnemySpaceCraft(enymeHP*3, enymeHP*3, new BasicEnemyAI(player));
                            enemy.setTextureName("spacecraft_red.png");
                            enemy.setBounds(0, 0, 180, 180);
                            break;
                        case 'Z':
                            enemy = new EnemySpaceCraft(enymeHP*4, enymeHP*4, new BasicEnemyAI(player));
                            enemy.setTextureName("spacecraft_black.png");
                            enemy.setBounds(0, 0, 210, 210);
                            break;
                    }
                    if (enemy != null) {
                        enemy.setId(id);
                        enemy.setX(tmpX);
                        enemy.setY(tmpY);
                        id++;
                        enemies.add(enemy);
                    }
                    tmpX += xSteep;
                }
                // --
                tmpY -= ySteep;
            }
        } catch (Exception e) {
            enemies.clear();
            System.err.println("StageContoller - loadStage(): " + e);
        }
        // ---
        stage.setEnemies(enemies);
        return stage;
    }

    public Stage getStage(long level) {
        if (!stages.containsKey(level)) {
            stages.put(level, loadStage(level));
        }
        return stages.get(level);
    }

}
