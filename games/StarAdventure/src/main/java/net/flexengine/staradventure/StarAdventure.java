/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.flexengine.staradventure;

import net.flexengine.FlexEngineBox;
import net.flexengine.controller.FlexEngine;
import net.flexengine.controller.FontController;
import net.flexengine.controller.SceneController;
import net.flexengine.controller.TextureController;
import net.flexengine.controller.arguments.ArgumentsProcessor;
import net.flexengine.model.components.ComponentStyle;
import net.flexengine.staradventure.model.scene.StarFireGameMenuScene;
import net.flexengine.utils.StylesUtil;
import net.flexengine.view.GameWindow;
import net.flexengine.view.Texture;

import java.util.List;

/**
 * @author Luann Athayde
 */
public class StarAdventure {

    public static final String APP_NAME = "Star Adventure";
    public static final String DEFAULT_FONT = "Harabara";

    public static final ComponentStyle DEFAULT_STYLE = StylesUtil.STYLE_FALL_WINTER_ALT;

    public static void main(String... args) {
        // ---
        FontController.setDefaultFont(DEFAULT_FONT);
        DEFAULT_STYLE.setAlpha(0.3f);
        ArgumentsProcessor.getInstance().addProcessor(
                "--rates", StarAdventure::defineRates
        );
        FlexEngineBox.main(args);
        // ---
        GameWindow.getInstance().setIconImage(
                TextureController.getInstance().getTexture(
                        "spacecraft.png", "./resources/textures/spacecraft.png"
                )
        );
        FlexEngine.getInstance().setGameName(APP_NAME);
        Texture texture = TextureController.getInstance()
                .getTexture("loadscreen.jpg", "./resources/textures/loadscreen.jpg");
        SceneController.getInstance().getLoadingScene().setBackground(texture);
        // ---
        SceneController.getInstance().setNextScene(new StarFireGameMenuScene());
        // ---
        FlexEngine engine = FlexEngine.getInstance();
        engine.setCloseOnScape(false);
//        engine.getEngineHandler().setDesireFps(120);
//        engine.getEngineHandler().setDesireUps(250);
        engine.start();
        // ---
    }

    public static void defineRates(List<String> values) {
        values.stream().findFirst().ifPresent( value -> {
            System.setProperty("rates", value);
        });
    }

}
