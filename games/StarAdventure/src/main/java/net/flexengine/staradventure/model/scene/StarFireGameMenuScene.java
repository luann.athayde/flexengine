/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.flexengine.staradventure.model.scene;

import lombok.extern.slf4j.Slf4j;
import net.flexengine.controller.*;
import net.flexengine.model.components.Button;
import net.flexengine.model.components.ComponentStyle;
import net.flexengine.model.components.GameComponent;
import net.flexengine.model.config.Configuration;
import net.flexengine.model.cursor.NavyCursor;
import net.flexengine.model.scene.GameScene;
import net.flexengine.model.scene.LoadScene;
import net.flexengine.model.scene.SceneBundle;
import net.flexengine.model.sound.Sound;
import net.flexengine.staradventure.StarAdventure;
import net.flexengine.staradventure.model.components.ExitConfirmationPanel;
import net.flexengine.utils.StylesUtil;
import net.java.games.input.Controller;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.util.Objects;

/**
 * @author Luann Athayde
 */
@Slf4j
public class StarFireGameMenuScene extends GameScene {

    private static final float JOYSTICK_MIN_ACTION_VALUE = 0.985f;

    private ExitConfirmationPanel exitConfirmationPanel;

    private Button buttonSingleplayer;
    private Button buttonMultiplayer;
    private Button buttonOptions;
    private Button buttonRanking;
    private Button buttonExit;

    private int joystickCurrentButton = 1;
    private int joystickCount = 0;
    private Controller joystick;
    private InputController.JoystickButtons joystickButtons;
    // ---
    private static final int BUTTON_WIDTH = 350;
    private static final int BUTTON_HEIGHT = 80;
    private static final Font fontButton = FontController.getFont(FontController.getDefaultFont(), Font.BOLD, 30);
    private static final ComponentStyle style = StarAdventure.DEFAULT_STYLE;

    @Override
    public void loadResourcesOnce() {
        LoadScene loadScene = (LoadScene) SceneController.getInstance().getLoadingScene();
        getGlobalBundle().putExtra(LoadScene.GLOBAL_LOADING_KEY, "Creating cursor");
        getGlobalBundle().putExtra(LoadScene.GLOBAL_PROGRESS_KEY, 0d);
        CursorController.getInstance().setGameCursor(new NavyCursor());
        getGlobalBundle().putExtra(LoadScene.GLOBAL_PROGRESS_KEY, 100d);
        // sleep(1000);
        // ---
        getGlobalBundle().putExtra(LoadScene.GLOBAL_LOADING_KEY, "Loading textures");
        getGlobalBundle().putExtra(LoadScene.GLOBAL_PROGRESS_KEY, 0d);
        TextureController.getInstance().loadTextures();
        // ---
        getGlobalBundle().putExtra(LoadScene.GLOBAL_LOADING_KEY, "Loading sounds and musics");
        getGlobalBundle().putExtra(LoadScene.GLOBAL_PROGRESS_KEY, 0d);
        SoundController.getInstance().loadSounds("", loadScene.getProgress());
        // ---
        loadScene.getProgress().setMaxValue(100d);
        getGlobalBundle().putExtra(LoadScene.GLOBAL_LOADING_KEY, "Verifying game configuration");
        getGlobalBundle().putExtra(LoadScene.GLOBAL_PROGRESS_KEY, 0d);
        // ---
        Configuration config = FlexEngine.getInstance().getConfig();
        getGlobalBundle().putExtra(LoadScene.GLOBAL_PROGRESS_KEY, 10d);
        // sleep(1000);
        if (Objects.nonNull(config)) {
            getGlobalBundle().putExtra(LoadScene.GLOBAL_PROGRESS_KEY, 80d);
        }
        // sleep(1000);
        getGlobalBundle().putExtra(LoadScene.GLOBAL_PROGRESS_KEY, 100d);
        // sleep(1000);
    }

    @Override
    public void loadResources() {
        SoundController.getInstance().stopAll();
        setBackground(TextureController.getInstance().getTexture("menu_002.jpg"));
        getGlobalBundle().putExtra(LoadScene.GLOBAL_PROGRESS_KEY, 0d);
        Sound soundMenuScene = SoundController.getInstance().get("Frostellar.mp3");
        getGlobalBundle().putExtra(LoadScene.GLOBAL_LOADING_KEY, "Playing menu music");
        // sleep(1000);
        SoundController.getInstance().play(soundMenuScene, true);
        getGlobalBundle().putExtra(LoadScene.GLOBAL_PROGRESS_KEY, 50d);
        // ---
        try {
            // sleep(1000);
            getGlobalBundle().putExtra(LoadScene.GLOBAL_LOADING_KEY, "Loading joysticks");
            joystick = InputController.getInstance().getJoystick();
            joystickButtons = InputController.getInstance().getJoystickButtons();
            // sleep(1000);
            getGlobalBundle().putExtra(LoadScene.GLOBAL_PROGRESS_KEY, 50d);
        } catch (Exception e) {
            getGlobalBundle().putExtra(LoadScene.GLOBAL_PROGRESS_KEY, 30d);
        }
        // sleep(1000);
        getGlobalBundle().putExtra(LoadScene.GLOBAL_LOADING_KEY, "Loading player profile");
        getGlobalBundle().putExtra(LoadScene.GLOBAL_PROGRESS_KEY, 50d);
        // sleep(1000);
        InputController.getInstance().releaseAll();
        getGlobalBundle().putExtra(LoadScene.GLOBAL_PROGRESS_KEY, 100d);
        // sleep(1000);
    }

    @Override
    public void initComponents(SceneBundle bundle) {
        getGlobalBundle().putExtra(LoadScene.GLOBAL_LOADING_KEY, "Creating components...");
        getGlobalBundle().putExtra(LoadScene.GLOBAL_PROGRESS_KEY, 0d);
        // sleep(1000);
        // --
        exitConfirmationPanel = new ExitConfirmationPanel();
        buttonSingleplayer = new Button();
        buttonMultiplayer = new Button();
        buttonOptions = new Button();
        buttonRanking = new Button();
        buttonExit = new Button();
        getGlobalBundle().putExtra(LoadScene.GLOBAL_PROGRESS_KEY, 30d);
        // sleep(1000);
        // --
        buttonSingleplayer.addEventListener((ActionListener) (ActionEvent e) -> {
            SceneController.getInstance().setNextScene(new StarFireGameScene());
            SceneController.getInstance().changeScene();
        });
        buttonMultiplayer.addEventListener((ActionListener) (ActionEvent e) -> {
            SceneController.getInstance().setNextScene(new StarFireMultiplayerScene());
            SceneController.getInstance().changeScene();
        });
        buttonOptions.addEventListener((ActionListener) (ActionEvent e) -> {
            SceneController.getInstance().setNextScene(new StarFireOptionsScene());
            SceneController.getInstance().changeScene();
        });
        buttonRanking.addEventListener((ActionListener) (ActionEvent e) -> {
            SceneController.getInstance().setNextScene(new StarFireRankingScene());
            SceneController.getInstance().changeScene();
        });
        buttonExit.addEventListener((ActionListener) (ActionEvent e) -> {
            exitConfirmationPanel.setVisible(true);
        });
        getGlobalBundle().putExtra(LoadScene.GLOBAL_PROGRESS_KEY, 35d);
        // sleep(1000);
        // ---
        aplyStyles();
        getGlobalBundle().putExtra(LoadScene.GLOBAL_PROGRESS_KEY, 60d);
        // sleep(1000);

        add(exitConfirmationPanel);
        add(buttonSingleplayer);
        add(buttonMultiplayer);
        add(buttonOptions);
        add(buttonRanking);
        add(buttonExit);
        getGlobalBundle().putExtra(LoadScene.GLOBAL_PROGRESS_KEY, 80d);
        // sleep(1000);

        getGlobalBundle().putExtra(LoadScene.GLOBAL_PROGRESS_KEY, 100d);
        // sleep(1000);
    }

    private void aplyStyles() {
        applyButtonStyle(buttonSingleplayer);
        applyButtonStyle(buttonMultiplayer);
        applyButtonStyle(buttonOptions);
        applyButtonStyle(buttonRanking);
        applyButtonStyle(buttonExit);
    }

    private void applyButtonStyle(Button button) {
        button.setSize(BUTTON_WIDTH, BUTTON_HEIGHT);
        button.setFont(fontButton);
        button.setRoundRect(Button.ROUND_RECT_NONE);
        StylesUtil.aplyStyle(button, style);
    }

    private void updateButtons() {
        buttonSingleplayer.setPosition(
                GameComponent.SCREEN_HORIZONTAL_LEFT_ALIGN, GameComponent.SCREEN_VERTICAL_CENTER_ALIGN,
                150, -BUTTON_HEIGHT * 2
        );
        buttonMultiplayer.setPosition(
                GameComponent.SCREEN_HORIZONTAL_LEFT_ALIGN, GameComponent.SCREEN_VERTICAL_CENTER_ALIGN,
                150, -BUTTON_HEIGHT
        );
        buttonOptions.setPosition(
                GameComponent.SCREEN_HORIZONTAL_LEFT_ALIGN, GameComponent.SCREEN_VERTICAL_CENTER_ALIGN,
                150, 0
        );
        buttonRanking.setPosition(
                GameComponent.SCREEN_HORIZONTAL_LEFT_ALIGN, GameComponent.SCREEN_VERTICAL_CENTER_ALIGN,
                150, BUTTON_HEIGHT
        );
        buttonExit.setPosition(
                GameComponent.SCREEN_HORIZONTAL_LEFT_ALIGN, GameComponent.SCREEN_VERTICAL_CENTER_ALIGN,
                150, BUTTON_HEIGHT * 2
        );
        buttonSingleplayer.setLabel(getLanguage().getValue("staradventure.singleplayer"));
        buttonMultiplayer.setLabel(getLanguage().getValue("staradventure.multiplayer"));
        buttonOptions.setLabel(getLanguage().getValue("staradventure.options"));
        buttonRanking.setLabel(getLanguage().getValue("staradventure.ranking"));
        buttonExit.setLabel(getLanguage().getValue("staradventure.exit"));
    }

    @Override
    public synchronized void update() {
        aplyStyles();
        updateButtons();
        super.update();

        if (InputController.getInstance().isKeyPressed(KeyEvent.VK_Z)) {
            InputController.getInstance().releaseKey(KeyEvent.VK_Z);
            style.setAlpha(style.getAlpha() + 0.1f);
            log.info("style = {}", style);
        } else if (InputController.getInstance().isKeyPressed(KeyEvent.VK_X)) {
            InputController.getInstance().releaseKey(KeyEvent.VK_X);
            style.setAlpha(style.getAlpha() - 0.1f);
            log.info("style = {}", style);
        }
        // ---
        if (!exitConfirmationPanel.isVisible()) {

            if (InputController.getInstance().getControlType() == InputController.CONTROLLER_JOYSTICK) {

                if (joystick.poll()) {
                    // ---
                    float joystickAxisYData = joystickButtons.getAxisY().getPollData();
                    float joystickPovButton = joystickButtons.getPov().getPollData();
                    int direction = 0;

                    // ---
                    if (joystickAxisYData >= JOYSTICK_MIN_ACTION_VALUE ||
                            joystickAxisYData <= -JOYSTICK_MIN_ACTION_VALUE ||
                            joystickPovButton == InputController.JoystickButtons.POV_UP ||
                            joystickPovButton == InputController.JoystickButtons.POV_DOWN) {
                        joystickCount++;
                        if (joystickAxisYData > 0.5f ||
                                joystickPovButton == InputController.JoystickButtons.POV_DOWN) {
                            direction = 1;
                        } else {
                            direction = -1;
                        }
                    }
                    // ---
                    if (joystickCount >= EngineHandler.getInstance().getDesireUps() / 10) {
                        if (direction > 0) {
                            joystickCurrentButton++;
                        } else {
                            joystickCurrentButton--;
                        }
                        joystickCount = 0;
                        InputController.getInstance().rumbleJoystick(0.5f, 250);
                    }
                    // ---
                    float joystickActionButton = joystickButtons.getButtonA().getPollData();
                    if (joystickActionButton >= JOYSTICK_MIN_ACTION_VALUE) {
                        Button currentButton = getCurrentButton();
                        if (currentButton != null) {
                            currentButton.doClick();
                        }
                    }
                }
                if (joystickCurrentButton > 5) {
                    joystickCurrentButton = 1;
                } else if (joystickCurrentButton < 1) {
                    joystickCurrentButton = 5;
                }

                Button currentButton = getCurrentButton();
                if (Objects.nonNull(currentButton)) {
                    currentButton.setBackgroundColor(style.getSecondaryBackground());
                    currentButton.setForegroundColor(style.getSecondaryForeground());
                    currentButton.setLabel("> " + currentButton.getLabel() + " <");
                }

            }

        }
    }

    @Override
    public synchronized void render(Graphics2D g) {
        super.render(g);
    }

    protected Button getCurrentButton() {
        switch (joystickCurrentButton) {
            case 1:
                return buttonSingleplayer;
            case 2:
                return buttonMultiplayer;
            case 3:
                return buttonOptions;
            case 4:
                return buttonRanking;
            case 5:
                return buttonExit;
        }
        return null;
    }

    @Override
    public void init(SceneBundle bundle) {
        super.init(bundle);
        // ---
    }

    @Override
    public void end(SceneBundle bundle) {
        super.end(bundle);
        SoundController.getInstance().stopAll();
    }

}
