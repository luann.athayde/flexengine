/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.flexengine.staradventure.model.components;

import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import lombok.Data;
import net.flexengine.model.components.GameComponent;
import net.flexengine.model.scene.SceneBundle;
import net.flexengine.view.Texture;

/**
 * @author Luann Athayde
 */
@Data
public class BasicSpaceCraft extends GameComponent {

    protected transient List<Shot> shots;
    protected transient Texture texture;

    protected int basicSpeedX;
    protected int speedX;
    protected int basicSpeedY;
    protected int speedY;
    protected int maxSpeedX;
    protected int maxSpeedY;
    protected int maxHP;
    protected int hp;

    public BasicSpaceCraft() {
        super("SpaceCraft", 0, 0);
        speedX = 0;
        speedY = 0;
        basicSpeedX = 0;
        basicSpeedY = 0;
        maxSpeedX = 15;
        maxSpeedY = 15;
        maxHP = hp = 100;
    }

    @Override
    public void init(SceneBundle bundle) {
        this.shots = new ArrayList<>();
    }

    @Override
    public void update() {
        speedX += basicSpeedX;
        speedY += basicSpeedY;
        // --
        speedX = Math.min(speedX, maxSpeedX);
        speedY = Math.min(speedY, maxSpeedY);
        speedX = Math.max(speedX, -maxSpeedX);
        speedY = Math.max(speedY, -maxSpeedY);
        if (speedX > 0) {
            speedX--;
        } else if (speedX < 0) {
            speedX++;
        }
        if (speedY > 0) {
            speedY--;
        } else if (speedY < 0) {
            speedY++;
        }
        x += speedX;
        y += speedY;
        // --
        if (Objects.nonNull(shots)) {
            List<Shot> removes = new ArrayList<>();
            for (Shot currentShot : shots) {
                currentShot.update();
                if (Boolean.FALSE.equals(currentShot.isVisible())) {
                    removes.add(currentShot);
                }
            }
            shots.removeAll(removes);
        } else {
            shots = new ArrayList<>();
        }
    }

    public void shot() {
    }

    @Override
    public void render(Graphics2D g) {
        if (Objects.nonNull(texture)) {
            texture.render(g, x, y);
        }
        // --
        if (Objects.nonNull(shots)) {
            shots.stream()
                    .filter(Shot::isVisible)
                    .forEach(s -> s.render(g));
        } else {
            shots = new ArrayList<>();
        }
        // --
    }

    public void invertSpeedX() {
        basicSpeedX = -basicSpeedX;
    }

    public void invertSpeedY() {
        basicSpeedY = -basicSpeedY;
    }

    public void setHp(int hp) {
        this.hp = Math.max(hp, 0);
        this.hp = Math.min(this.hp, this.maxHP);
    }

    public void receiveDamage(int damage) {
        hp = hp - damage;
        hp = Math.max(hp, 0);
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "[" + x + "," + y + ", hp=" + hp + ", maxHP=" + maxHP + "]";
    }

}
