/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.flexengine.staradventure.model.scene;

import net.flexengine.controller.FontController;
import net.flexengine.controller.SceneController;
import net.flexengine.controller.TextureController;
import net.flexengine.model.components.Button;
import net.flexengine.model.components.GamePanel;
import net.flexengine.model.components.Label;
import net.flexengine.model.scene.GameScene;
import net.flexengine.model.scene.SceneBundle;
import net.flexengine.staradventure.StarAdventure;
import net.flexengine.utils.StylesUtil;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * @author Luann Athayde
 */
public class StarFireMultiplayerScene extends GameScene {

    protected Label labelTitle;
    protected Label labelInfo;
    protected GamePanel panelClient;
    protected GamePanel panelServer;
    protected Button buttonBack;

    protected int labelTitleHeight = 100;
    protected int labelInfoHeight = 45;
    protected int buttonBackHeight = 45;

    @Override
    public void loadResources() {
        setBackground(TextureController.getInstance().getTexture("loadscreen.jpg"));
        // ---
    }

    @Override
    public void initComponents(SceneBundle bundle) {
        super.initComponents(bundle);
        labelTitle = new Label("- StarAdventure -");
        labelTitle.setBorder(Color.WHITE, 1);
        labelTitle.setFont(new Font("Calibri", 1, 32));
        labelTitle.setHorizontalTextAlign(Label.HORIZONTAL_ALIGN_CENTER);
        labelTitle.setForegroundColor(Color.WHITE);
        labelTitle.setBackgroundColor(new Color(0, 7, 14));
        labelTitle.setTransparence(0.65f);
        // ---
        initClientPanel();
        initServerPanel();
        // ---
        labelInfo = new Label("Informações:");
        labelInfo.setBorder(Color.WHITE, 1);
        labelInfo.setFont(FontController.getFont(FontController.getDefaultFont(), 0, 20));
        labelInfo.setHorizontalTextAlign(Label.HORIZONTAL_ALIGN_LEFT);
        labelInfo.setForegroundColor(Color.WHITE);
        labelInfo.setBackgroundColor(new Color(0, 7, 14));
        labelInfo.setTransparence(0.65f);
        // ---
        buttonBack = new Button("Voltar");
        buttonBack.setFont(FontController.getFont(FontController.getDefaultFont(), 0, 20));
        StylesUtil.aplyStyle(buttonBack, StarAdventure.DEFAULT_STYLE);
        buttonBack.setRoundRect(Button.ROUND_RECT_NONE);
        buttonBack.addEventListener((ActionListener) (ActionEvent e) -> {
            SceneController.getInstance().setNextScene(new StarFireGameMenuScene());
            SceneController.getInstance().changeScene();
        });
        // ---
        add(labelTitle);
        add(panelClient);
        add(panelServer);
        add(labelInfo);
        add(buttonBack);
        // ---
        getGlobalBundle().putExtra("progress", 80d);
    }

    @Override
    public void init(SceneBundle bundle) {
        super.init(bundle);
        update();
        getGlobalBundle().putExtra("progress", 100d);
    }

    private void initClientPanel() {
        panelClient = new GamePanel("panelClient", 0, 0, new Dimension(0, 0));
        panelClient.setCloseable(false);
        panelClient.addTopDecoration("- Entrar em um servidor...");
        panelClient.setBackgroundColor(new Color(0, 7, 14));
        panelClient.setTransparence(0.65f);
    }

    private void initServerPanel() {
        panelServer = new GamePanel("panelServer", 0, 0, new Dimension(0, 0));
        panelServer.setCloseable(false);
        panelServer.addTopDecoration("- Criar servidor...");
        panelServer.setBackgroundColor(new Color(0, 7, 14));
        panelServer.setTransparence(0.65f);
    }

    private Label createStyledLabel(String text) {
        Label label = new Label(text);
        label.setFont(new Font("Calibri", 1, 16));
        label.setHorizontalTextAlign(Label.HORIZONTAL_ALIGN_RIGHT);
        label.setForegroundColor(Color.WHITE);
        label.setTransparence(0.5f);
        label.setBounds(0, 0, 140, 32);
        // --
        label.setBorder(Color.WHITE, 0.01f);
        return label;
    }

    @Override
    public synchronized void update() {
        int spaceWidth = 60;
        int spaceHeight1 = 40, spaceHeight2 = 15;
        int panelWidth = (getViewportWidth() - (spaceWidth * 2) - spaceHeight2) / 2;
        int labelWidth = (panelWidth * 2) + spaceHeight2;
        int tmpX = spaceWidth, tmpY = spaceHeight1;
        // --
        labelTitle.setBounds(tmpX, tmpY, labelWidth, labelTitleHeight);
        tmpY += labelTitle.getHeight() + spaceHeight2;
        // -- panels...
        int panelHeight = getViewportHeight() - (spaceHeight1 + labelTitleHeight + spaceHeight2
                + spaceHeight2 + labelInfoHeight + spaceHeight2 + buttonBackHeight + spaceHeight1);
        panelClient.setBounds(tmpX, tmpY, panelWidth, panelHeight);
        panelServer.setBounds(tmpX + panelWidth + spaceHeight2, tmpY, panelWidth, panelHeight);
        // --
        tmpY += panelClient.getHeight() + spaceHeight2;
        labelInfo.setBounds(tmpX, tmpY, labelWidth, labelInfoHeight);
        tmpY += labelInfo.getHeight() + spaceHeight2;
        buttonBack.setBounds(getViewportWidth() - 300 - spaceWidth, tmpY, 300, buttonBackHeight);
        // --
        super.update();
    }

}
