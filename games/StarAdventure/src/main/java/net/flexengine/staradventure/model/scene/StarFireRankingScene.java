/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.flexengine.staradventure.model.scene;

import net.flexengine.controller.FlexEngine;
import net.flexengine.controller.InputController;
import net.flexengine.controller.SceneController;
import net.flexengine.controller.TextureController;
import net.flexengine.model.scene.GameScene;
import net.flexengine.model.scene.SceneBundle;
import net.flexengine.view.Texture;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author Luann Athayde
 */
public class StarFireRankingScene extends GameScene {

    private Integer currentShip = 0;
    private Integer flicker = 10;
    private Integer flicketCount = 0;
    private Map<Integer, List<Texture>> ships = new HashMap<>();

    @Override
    public void loadResources() {
        // setBackground(TextureController.getInstance().getTexture("loadscreen.jpg"));
        // ---
    }

    @Override
    public void initComponents(SceneBundle bundle) {
        super.initComponents(bundle);

        Texture shipsTexture = TextureController.getInstance().getTexture("ships.png");
        int gridWidth = 256;
        int[] gridY = {
                0,256,512,768,
                1024,1280,1536,1792,
                2048,2304,2560,2944
        };
        int[] gridHeight = {
                256,256,256,256,
                256,256,256,256,
                256,256,384,384
        };

        for (int i = 0; i < 12; i++) {
            ships.put(i,
                    Stream.of(
                            shipsTexture.getSubTexture(
                                    0, gridY[i],
                                    gridWidth, gridHeight[i]),
                            shipsTexture.getSubTexture(
                                    gridWidth, gridY[i],
                                    gridWidth, gridHeight[i])
                    ).collect(Collectors.toList())
            );
        }

        flicketCount = 0;
        flicker = FlexEngine.getInstance().getEngineHandler().getDesireUps() / 40;
        // ---
        getGlobalBundle().putExtra("progress", 80d);
    }

    @Override
    public void init(SceneBundle bundle) {
        super.init(bundle);
        getGlobalBundle().putExtra("progress", 100d);
        // ---
    }

    @Override
    public synchronized void update() {
        super.update();

        if (InputController.getInstance().isKeyPressed(KeyEvent.VK_ESCAPE)) {
            InputController.getInstance().releaseKey(KeyEvent.VK_ESCAPE);
            SceneController.getInstance().setNextScene(new StarFireGameMenuScene()).changeScene();
        }

        if (InputController.getInstance().isKeyPressed(KeyEvent.VK_A)) {
            InputController.getInstance().releaseKey(KeyEvent.VK_A);
            currentShip++;
            if( currentShip >= 12 ) {
                currentShip = 0;
            }
        }
        if (InputController.getInstance().isKeyPressed(KeyEvent.VK_S)) {
            InputController.getInstance().releaseKey(KeyEvent.VK_S);
            currentShip--;
            if( currentShip < 0 ) {
                currentShip = 11;
            }
        }

        if( flicketCount >= flicker ) {
            flicketCount = 0;
        }
        flicketCount++;

        // ---
        if (InputController.getInstance().isKeyPressed(KeyEvent.VK_UP)) {
            InputController.getInstance().releaseKey(KeyEvent.VK_UP);
            flicker++;
            if( flicker >= 60 ) {
                currentShip = 60;
            }
        }
        if (InputController.getInstance().isKeyPressed(KeyEvent.VK_DOWN)) {
            InputController.getInstance().releaseKey(KeyEvent.VK_DOWN);
            flicker--;
            if( flicker <= 1 ) {
                flicker = 1;
            }
        }
    }

    @Override
    public synchronized void render(Graphics2D g) {
        super.render(g);

        g.setColor(Color.WHITE);
        g.drawString("Flicker Rate "+flicker, 10, 40);

        Texture ship = ships.get(currentShip).get(0);
        Texture engine = ships.get(currentShip).get(1);

        if (flicketCount == flicker) {
            engine.render(g, 300, 300);
        }
        ship.render(g, 300, 300);

    }

}
