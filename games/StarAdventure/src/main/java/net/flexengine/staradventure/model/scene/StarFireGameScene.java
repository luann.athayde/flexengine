/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.flexengine.staradventure.model.scene;

import lombok.extern.slf4j.Slf4j;
import net.flexengine.controller.*;
import net.flexengine.model.components.Button;
import net.flexengine.model.components.Label;
import net.flexengine.model.components.ProgressBar;
import net.flexengine.model.scene.GameScene;
import net.flexengine.model.scene.LoadScene;
import net.flexengine.model.scene.SceneBundle;
import net.flexengine.staradventure.controller.StageController;
import net.flexengine.staradventure.model.components.BasicEnemyAI;
import net.flexengine.staradventure.model.components.EnemySpaceCraft;
import net.flexengine.staradventure.model.components.PlayerSpaceCraft;
import net.flexengine.staradventure.model.config.Stage;

import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Objects;

/**
 * @author Luann Athayde
 */
@Slf4j
public class StarFireGameScene extends GameScene {

    Label labelRates;
    Label labelHP;
    Label labelLevel;
    Label labelAtkInfo;
    Label labelPoints;
    Label labelStage;
    Label controllerType;
    PlayerSpaceCraft player;
    ProgressBar progressBarHP;
    StageController stageController;
    ProgressBar progressBarExperience;

    Stage stage;
    boolean loadingStage;
    int points;
    double rate;

    // ---
    // Options menu...
    boolean showOptions;
    Button buttonContinue;
    Button buttonGoBack;
    Button buttonOptions;
    Button buttonClose;

    @Override
    public void loadResources() {
        // ---
        setBackground(TextureController.getInstance().getTexture("background_stage001.jpg"));
        getGlobalBundle().putExtra(LoadScene.GLOBAL_PROGRESS_KEY, 10d);
    }

    @Override
    public void initComponents(SceneBundle bundle) {
        super.initComponents(bundle);
        getGlobalBundle().putExtra(LoadScene.GLOBAL_PROGRESS_KEY, 15d);

        labelRates = new Label("Rates: 100%");
        player = new PlayerSpaceCraft();
        progressBarHP = new ProgressBar(100, 100);
        labelLevel = new Label("Level: 1");
        labelHP = new Label("HP:");
        labelAtkInfo = new Label("ATK/s: 8 | DPS: 30 ~ 50");
        labelPoints = new Label("Points: #");
        labelStage = new Label("Stage: 001");
        controllerType = new Label("Controller: Keyboard");
        stageController = new StageController(player);
        progressBarExperience = new ProgressBar(0, 1000);
        buttonContinue = new Button("Continuar");
        buttonGoBack = new Button("Voltar");
        buttonOptions = new Button("Opções");
        buttonClose = new Button("Sair");
        getGlobalBundle().putExtra(LoadScene.GLOBAL_PROGRESS_KEY, 20d);

        progressBarExperience.setColorBackground(new Color(14, 115, 159));
        progressBarExperience.setColorProgress(new Color(13, 34, 68));
        progressBarExperience.setColorBorder(Color.LIGHT_GRAY);
        progressBarExperience.setColorStringInfo(Color.WHITE);
        progressBarExperience.setInfoPattern("0.0/0.0");
        progressBarExperience.setFontInfo(FontController.getFont(FontController.getDefaultFont(), 1, 11));
        getGlobalBundle().putExtra(LoadScene.GLOBAL_PROGRESS_KEY, 25d);

        applyOptionsButtonStyle(buttonGoBack);
        applyOptionsButtonStyle(buttonOptions);
        applyOptionsButtonStyle(buttonClose);
        applyOptionsButtonStyle(buttonContinue);
        getGlobalBundle().putExtra(LoadScene.GLOBAL_PROGRESS_KEY, 30d);

        buttonContinue.addEventListener((ActionListener) (e) -> {
            showOptions = false;
        });
        buttonGoBack.addEventListener((ActionListener) (e) -> {
            SceneController.getInstance().setNextScene(new StarFireGameMenuScene()).changeScene();
        });
        buttonClose.addEventListener((ActionListener) (e) -> {
            FlexEngine.getInstance().stop();
        });
        getGlobalBundle().putExtra(LoadScene.GLOBAL_PROGRESS_KEY, 35d);

    }

    @Override
    public void init(SceneBundle bundle) {
        getGlobalBundle().putExtra(LoadScene.GLOBAL_PROGRESS_KEY, 40d);

        labelRates.setHorizontalTextAlign(Label.HORIZONTAL_ALIGN_RIGHT);
        labelRates.setSize(200, 30);
        labelRates.setPosition(
                Label.SCREEN_HORIZONTAL_RIGHT_ALIGN, Label.SCREEN_VERTICAL_TOP_ALIGN,
                0, 0
        );

        int tmpX = 5;
        int tmpY = 5;
        int tmpHeight = 18;
        labelHP.setBounds(tmpX, tmpY, 35, tmpHeight);
        progressBarHP.setBounds(tmpX + 35, tmpY, 195, tmpHeight - 4);
        progressBarHP.setColorProgress(new Color(20, 20, 60));
        progressBarHP.setColorBackground(Color.BLACK);
        progressBarHP.setColorStringInfo(Color.WHITE);
        progressBarHP.setInfoPattern("0.0/0.0");
        tmpY += tmpHeight;
        labelLevel.setBounds(tmpX, tmpY, 230, tmpHeight);
        tmpY += tmpHeight;
        // --
        labelAtkInfo.setBounds(tmpX, tmpY, 230, tmpHeight);
        tmpY += tmpHeight;
        // --
        labelPoints.setBounds(tmpX, tmpY, 230, tmpHeight);
        tmpY += tmpHeight;
        // --
        labelStage.setBounds(tmpX, tmpY, 230, tmpHeight);
        tmpY += tmpHeight;
        // --
        controllerType.setBounds(tmpX, tmpY, 230, tmpHeight);
        // --
        points = 0;
        try {
            rate = Double.parseDouble(System.getProperty("rates"));
        } catch (Exception e) {
            log.warn("Fail to define rate: {}", e, e);
            rate = 1.0d;
        }
        getGlobalBundle().putExtra(LoadScene.GLOBAL_PROGRESS_KEY, 50d);
        // ---
        add(labelRates);
        add(player);
        add(labelHP);
        add(progressBarHP);
        add(labelLevel);
        add(labelAtkInfo);
        add(labelPoints);
        add(labelStage);
        add(controllerType);
        add(progressBarExperience);
        getGlobalBundle().putExtra(LoadScene.GLOBAL_PROGRESS_KEY, 60d);
        getGlobalBundle().putExtra(LoadScene.GLOBAL_PROGRESS_KEY, 80d);
        getGlobalBundle().putExtra(LoadScene.GLOBAL_LOADING_KEY, "Initalizing stage");
        // ---
        loadingStage = false;
        initStage();
        super.init(bundle);
        getGlobalBundle().putExtra(LoadScene.GLOBAL_PROGRESS_KEY, 100d);
    }

    // ---
    int buttonWidth = 320;
    int buttonHeight = 42;
    Font buttonFont = FontController.getFont(FontController.getDefaultFont(), Font.BOLD, 18);
    Color buttonBgColor = new Color(0, 7, 14);
    Color buttonFocusColor = new Color(0, 48, 112);
    Color buttonColor = new Color(245, 245, 245);

    private void applyOptionsButtonStyle(Button button) {
        button.setSize(buttonWidth, buttonHeight);
        button.setFont(buttonFont);
        button.setBackgroundColor(buttonBgColor);
        button.setBackgroundFocusColor(buttonFocusColor);
        button.setForegroundColor(buttonColor);
        button.setForegroundFocusColor(buttonColor);
        button.setRoundRect(Button.ROUND_RECT_NONE);
        button.setAlpha(0.8f);
    }

    @Override
    public synchronized void update() {
        // --
        if (InputController.getInstance().isKeyPressed(KeyEvent.VK_BACK_SPACE)) {
            InputController.getInstance().releaseKey(KeyEvent.VK_BACK_SPACE);
            SceneController.getInstance().setNextScene(new StarFireGameMenuScene()).changeScene();
        }
        // --
        if (InputController.getInstance().isKeyPressed(KeyEvent.VK_ESCAPE)) {
            InputController.getInstance().releaseKey(KeyEvent.VK_ESCAPE);
            showOptions = !showOptions;
        }
        if (showOptions) {
            int tmpX = getViewportWidth() / 2 - buttonContinue.getWidth() / 2;
            int tmpY = getViewportHeight() / 2 - buttonContinue.getHeight() * 2;
            buttonContinue.setPosition(tmpX, tmpY);
            tmpY += buttonContinue.getHeight();
            buttonGoBack.setPosition(tmpX, tmpY);
            tmpY += buttonContinue.getHeight();
            buttonOptions.setPosition(tmpX, tmpY);
            tmpY += buttonContinue.getHeight();
            buttonClose.setPosition(tmpX, tmpY);
            // --
            buttonContinue.update();
            buttonGoBack.update();
            buttonOptions.update();
            buttonClose.update();
            return;
        }
        // --
        labelRates.setText("Rates: " + (rate * 100d) + "%");
        labelRates.setPosition(
                Label.SCREEN_HORIZONTAL_RIGHT_ALIGN, Label.SCREEN_VERTICAL_TOP_ALIGN,
                0, 0
        );
        // --
        progressBarHP.setMaxValue(player.getMaxHP());
        progressBarHP.setValue(player.getHp());
        labelLevel.setText("Level: " + player.getLevel());
        labelPoints.setText("Points: " + points);
        labelStage.setText("Stage: " + stage.getLevel());
        int dps = (1000 / EngineHandler.getInstance().getDesireUps()) * player.getNeedCharge();
        dps = 1000 / dps;
        int dpsMin = dps * BasicEnemyAI.BASIC_DAMAGE;
        int dpsMax = dps * BasicEnemyAI.BASIC_DAMAGE + BasicEnemyAI.BASIC_DAMAGE / 2;
        labelAtkInfo.setText("ATK/s: " + dps + " | DPS: " + dpsMin + " ~ " + dpsMax);
        // --
        controllerType.setText("Controller: " + InputController.getInstance().getControlTypeInfo());
        // --
        progressBarExperience.setMaxValue(1000f);
        progressBarExperience.setValue(player.getExp());
        progressBarExperience.setBounds(
                200, getViewportHeight() - 32,
                getViewportWidth() - 400, 14);
        // ---
        super.update();
        // ---
        ArrayList<EnemySpaceCraft> removes = new ArrayList<>();
        getItems(EnemySpaceCraft.class).stream()
                .filter(item -> item.getHp() <= 0)
                .forEach(item -> {
                    item.setVisible(false);
                    points += rate;
                    player.addExp(item.getMaxHP() * rate);
                    removes.add(item);
                });
        getItems().removeAll(removes);
        // ---
        boolean noEnemies = getItems(EnemySpaceCraft.class).isEmpty();
        if (noEnemies && !loadingStage) {
            initStage();
        }
    }

    @Override
    public synchronized void render(Graphics2D g) {
        super.render(g);
        if (showOptions) {
            Graphics2D g2 = (Graphics2D) g.create();
            g2.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.6f));
            g2.setColor(Color.WHITE);
            g2.fillRect(0, 0, getViewportWidth(), getViewportHeight());
            // --
            buttonContinue.render(g);
            buttonGoBack.render(g);
            buttonOptions.render(g);
            buttonClose.render(g);
        }
    }

    protected void initStage() {
        loadingStage = true;
        // ---
        long currentLevel = Objects.nonNull(stage) ? stage.getLevel() : 0;
        stage = stageController.getStage(currentLevel + 1);
        if (Objects.isNull(stage.getEnemies()) || stage.getEnemies().isEmpty()) {
            clear();
            SceneController.getInstance()
                    .setNextScene(new StarFireGameMenuScene())
                    .changeScene();
        } else {
            SoundController.getInstance().stopAll();
            // ---
            SceneBundle bundle = new SceneBundle();
            log.info("Starting [" + stage.getName() + "]...");
            java.util.List<EnemySpaceCraft> enemies = stage.getEnemies();
            enemies.forEach(e -> e.init(bundle));
            getItems().addAll(enemies);
            // ---
            log.info("Playing scene music: " + stage.getStageSound());
            SoundController.getInstance().play(
                    stage.getStageSound(),
                    true
            );
        }
        // ---
        loadingStage = false;
    }

    @Override
    public void end(SceneBundle bundle) {
        super.end(bundle);
        SoundController.getInstance().stopAll();
    }

}
