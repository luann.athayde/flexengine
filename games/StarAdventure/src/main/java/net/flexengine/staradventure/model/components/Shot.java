/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.flexengine.staradventure.model.components;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;

import lombok.Data;
import net.flexengine.controller.TextureController;
import net.flexengine.model.components.GameComponent;
import net.flexengine.model.scene.SceneBundle;
import net.flexengine.view.Texture;

/**
 * @author Luann Athayde
 */
@Data
public class Shot extends GameComponent {

    private float basicSpeed;
    private float maxSpeed;
    private float speedY;
    private Color color;

    private Texture texture;

    private ShootPath path;

    public Shot() {
        this(null);
    }

    public Shot(ShootPath path) {
        super("Shoot", 0, 0, new Dimension(4, 16));
        this.maxSpeed = 5f;
        this.basicSpeed = 0.1f;
        this.speedY = 0f;
        this.color = Color.CYAN;
        this.path = path;
        this.texture = null;
    }

    @Override
    public void init(SceneBundle bundle) {
        this.texture = TextureController.getInstance().getTexture("plasma_001.png");
    }

    @Override
    public void update() {
        speedY += basicSpeed;
        speedY = Math.min(speedY, maxSpeed);
        y += (int) speedY;
        if (path != null) {
            path.update(this);
        }
    }

    @Override
    public void render(Graphics2D g) {
        if (texture != null) {
            texture.render(g, x, y);
        }
    }

    public interface ShootPath {

        void update(Shot s);

    }

}
