/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.flexengine.staradventure.model.components;

import net.flexengine.model.components.GameComponent;

/**
 * @author Luann Athayde
 */
public class BasicEnemyAI implements BasicAI {

    public static final int BASIC_DAMAGE = 20;

    private final PlayerSpaceCraft player;
    private int needCharge;
    private int shootCharge;

    private float pollDataX;

    public BasicEnemyAI(PlayerSpaceCraft player) {
        this.player = player;
        this.needCharge = 10;
        this.shootCharge = 5;
    }

    @Override
    public void update(BasicSpaceCraft bsc) {
        EnemySpaceCraft e = (EnemySpaceCraft) bsc;
        // --
        if (e.getY() >= 80) {
            e.setBasicSpeedY(0);
            int playerCenterX = player.getX() + player.getWidth() / 2;
            int enemyCenterX = e.getX() + e.getWidth() / 2;
            if (enemyCenterX < playerCenterX && e.getRand().nextInt(9999) + 1 >= (10000-e.getMaxHP())) {
                if (pollDataX == 0f)
                    pollDataX = 2.0f;
            } else if (enemyCenterX > playerCenterX && e.getRand().nextInt(9999) + 1 >= (10000-e.getMaxHP())) {
                if (pollDataX == 0f)
                    pollDataX = -2.0f;
            } else {
                pollDataX = 0.0f;
            }
            e.setBasicSpeedX((int) (getPollDataX() * 2));
            // --
            if ( enemyCenterX >= player.getX() && enemyCenterX <= player.getX() + player.getWidth()
                    && e.getRand().nextInt(9999) + 1 >= (10000-e.getMaxHP())) {
                shootCharge++;
                if (shootCharge == needCharge) {
                    shootCharge = 0;
                    e.shot();
                }
            }
            // ---
            // Random shoot
            if (e.getRand().nextInt(9999) + 1 >= (10000-e.getMaxHP()) ) {
                shootCharge++;
                if (shootCharge == needCharge) {
                    shootCharge = 0;
                    e.shot();
                }
            }
        }
        // --
        // colisions
        for (int i = 0; i < player.getShots().size(); i++) {
            Shot s = player.getShots().get(i);
            if (e.collide(s, GameComponent.RECTANGLE_COLISION)) {
                int dmg = BASIC_DAMAGE + e.getRand().nextInt(BASIC_DAMAGE / 2);
                e.receiveDamage(dmg);
                s.setVisible(false);
                player.getShots().remove(i);
            }
        }
        // --
        for (int i = 0; i < e.getShots().size(); i++) {
            Shot s = e.getShots().get(i);
            if (player.collide(s, GameComponent.RECTANGLE_COLISION)) {
                int dmg = BASIC_DAMAGE + e.getRand().nextInt(BASIC_DAMAGE / 2);
                player.receiveDamage(dmg);
                e.getShots().remove(i);
            }
        }
    }

    public float getPollDataX() {
        if (pollDataX > 0) {
            pollDataX -= -0.25f;
        } else if (pollDataX < 0) {
            pollDataX += 0.25f;
        }
        return pollDataX;
    }

    public int getNeedCharge() {
        return needCharge;
    }

    public void setNeedCharge(int needCharge) {
        this.needCharge = needCharge;
    }

    public int getShootCharge() {
        return shootCharge;
    }

    public void setShootCharge(int shootCharge) {
        this.shootCharge = shootCharge;
    }

}
