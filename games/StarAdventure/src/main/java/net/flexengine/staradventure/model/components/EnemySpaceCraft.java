/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.flexengine.staradventure.model.components;

import java.awt.Color;
import java.awt.Graphics2D;
import java.util.Objects;
import java.util.Random;

import net.flexengine.controller.Renderable;
import net.flexengine.controller.TextureController;
import net.flexengine.model.components.ProgressBar;
import net.flexengine.model.scene.SceneBundle;
import net.flexengine.view.GameWindow;

/**
 * @author Luann Athayde
 */
public class EnemySpaceCraft extends BasicSpaceCraft {

    private static final Random rand = new Random();

    private ProgressBar enemyHpBar;
    private BasicAI enemyAI;
    private Renderable renderInfo;
    private String textureName;

    public EnemySpaceCraft() {
        this(100, 100, null, null);
    }

    public EnemySpaceCraft(BasicAI ai, Renderable renderInfo) {
        this(100, 100, ai, renderInfo);
    }

    public EnemySpaceCraft(int HP, int maxHP, BasicAI ai) {
        this(HP, maxHP, ai, null);
    }

    public EnemySpaceCraft(int HP, int maxHP, Renderable renderInfo) {
        this(HP, maxHP, null, renderInfo);
    }

    public EnemySpaceCraft(int HP, int maxHP, BasicAI ai, Renderable renderInfo) {
        this.x = 0;
        this.y = -1000;
        this.basicSpeedX = 0;
        this.basicSpeedY = 2;
        this.speedX = 0;
        this.speedY = 0;
        this.enemyAI = ai;
        this.renderInfo = renderInfo;
        this.hp = HP;
        this.maxHP = maxHP;
        this.maxSpeedX = 7;
        this.maxSpeedY = 5;
        this.textureName = "spacecraft_red.png";
        // --
        enemyHpBar = new ProgressBar();
        enemyHpBar.setColorProgress(new Color(20, 60, 20));
        enemyHpBar.setColorBackground(Color.BLACK);
        enemyHpBar.setColorStringInfo(Color.WHITE);
        enemyHpBar.setInfoPattern("0.0/0.0");
        enemyHpBar.setRoundRect(10, 10);
    }

    @Override
    public void init(SceneBundle bundle) {
        super.init(bundle);
        texture = TextureController.getInstance().getTexture(textureName);
        if (Objects.nonNull(texture)) {
            texture = texture.rotate180();
            int newWidth = (int) (getResizedWidth());
            int newHeight = (int) (getResizedHeight());
            texture = texture.scale(newWidth, newHeight);
        }
    }

    private float getResizedWidth() {
        return (float) texture.getWidth() * ((float) size.width / (float) texture.getWidth());
    }

    private float getResizedHeight() {
        return (float) texture.getHeight() * ((float) size.height / (float) texture.getHeight());
    }

    @Override
    public void update() {
        super.update();
        enemyHpBar.setBounds(x, y - 14, getWidth(), 14);
        enemyHpBar.setMaxValue(maxHP);
        enemyHpBar.setValue(hp);
        enemyHpBar.update();
        // --
        if (Objects.nonNull(enemyAI)) {
            enemyAI.update(this);
        }
    }

    @Override
    public void render(Graphics2D g) {
        super.render(g);
        // --
        if (Objects.nonNull(renderInfo)) {
            renderInfo.render(g);
        }
        // --
        enemyHpBar.render(g);
        // --
    }

    @Override
    public void shot() {
        if (Objects.nonNull(texture)) {
            Shot shot = new Shot(s -> {
                if (s.getY() > GameWindow.getInstance().getHeight() && s.isVisible()) {
                    s.setVisible(false);
                }
            });
            shot.setSpeedY(1f);
            shot.setColor(Color.RED);
            shot.setPosition(x + texture.getWidth() / 2, y + texture.getHeight());
            shot.init(null);
            shot.setTexture(shot.getTexture().rotate180());
            shots.add(shot);
        }
    }

    public Random getRand() {
        return rand;
    }

    public ProgressBar getEnemyHpBar() {
        return enemyHpBar;
    }

    public String getTextureName() {
        return textureName;
    }

    public void setTextureName(String textureName) {
        this.textureName = textureName;
    }

}
