package net.flexengine.staradventure.model.config;

import lombok.Data;
import net.flexengine.model.sound.Sound;
import net.flexengine.staradventure.model.components.EnemySpaceCraft;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Data
public class Stage implements Serializable {

    private List<EnemySpaceCraft> enemies = new ArrayList<>();
    private long level = 1;
    private String name = "stage001";
    private Sound stageSound = null;

    public Stage(long level) {
        this.level = level;
    }

    public List<EnemySpaceCraft> getEnemies() {
        return enemies.stream().map( e -> (EnemySpaceCraft)e.clone() ).collect(Collectors.toList());
    }

}
