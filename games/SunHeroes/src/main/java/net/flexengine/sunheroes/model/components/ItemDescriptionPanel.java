/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.flexengine.sunheroes.model.components;

import java.awt.Color;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import net.flexengine.model.components.GamePanel;
import net.flexengine.model.scene.SceneBundle;

/**
 *
 * @author Luann Athayde
 */
@Data
@Builder
@AllArgsConstructor
public class ItemDescriptionPanel extends GamePanel {

    private String currentDescription;

    public ItemDescriptionPanel() {
        super("ItemDescriptionPanel", 0, 0, null);
    }

    @Override
    public void init(SceneBundle bundle) {
        super.init(bundle);
        setSize(500, 300);
        setPosition(SCREEN_HORIZONTAL_CENTER_ALIGN, SCREEN_VERTICAL_CENTER_ALIGN);
        setDraggable(true);
        setDragArea(0, getWidth(), 500, 32);
        setRoundRect(0, 0);
        setBorderColor(Color.WHITE);
        setBorderSize(1);
        setBackgroundColor(Color.WHITE);
        setTransparence(0.5f);
        // --
        addTopDecoration("Descrição");
        setVisible(false);
    }

    // -------------------------------------------------------------------------
    @Override
    public boolean equals(Object o) {
        return super.equals(o);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

}
