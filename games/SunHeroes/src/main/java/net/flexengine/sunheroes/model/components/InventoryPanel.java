/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.flexengine.sunheroes.model.components;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import net.flexengine.controller.FontController;
import net.flexengine.controller.InputController;
import net.flexengine.controller.LanguageController;
import net.flexengine.controller.TextureController;
import net.flexengine.model.components.*;
import net.flexengine.model.components.Button;
import net.flexengine.model.components.Label;
import net.flexengine.model.scene.SceneBundle;
import net.flexengine.sunheroes.service.ItemService;
import net.flexengine.view.Texture;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.font.FontRenderContext;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * @author Luann Athayde
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class InventoryPanel extends GamePanel {

    public static final int ITEM_ADD = 1;
    public static final int ITEM_DEL = 2;
    public static final int ITEM_MAX = 3;
    public static final int ITEM_NOTFOUND = 4;
    public static final int INVENTORY_FULL = -1;
    public static final int NUM_MAX_ITEM = 999;
    private static final String[] ORDER_NAMES = {"Por nome", "Por quantidade", "Por tipo/quant."};
    private static final String NO_ORDER = "Não Ordenado";

    private final Font amountFont = FontController.getFont(FontController.getDefaultFont(), 1, 12);
    private final DecimalFormat basicNumberFormat = new DecimalFormat("0");
    private final Color amountColor = new Color(0, 54, 98);
    private final List<ItemInventory> itens = new ArrayList<>();
    // --
    private final Color itemNameColor = new Color(255, 255, 255);
    private final Color itemNameBorderColor = new Color(255, 255, 255);
    private final Color itemNameBgColor = new Color(0, 0, 0);
    private final Font itemNameFont = FontController.getFont("Arial", 1, 11);
    private final transient FontRenderContext frc = new FontRenderContext(
            null, true, false
    );

    private int reoderCase;
    private int numColunas;
    private int numLinhas;
    private int maxItens;
    private Texture boxTexture;
    private Texture boxTextureOver;
    private Button buttonReorderItens;
    private ItemDescriptionPanel descPanel;
    private ItemInventory highlightItem;

    public InventoryPanel(int numColunas, int numLinhas, ItemDescriptionPanel descPanel) {
        super("Inventory", 0, 0, new Dimension());
        this.numColunas = numColunas;
        this.numLinhas = numLinhas;
        maxItens = numLinhas * numColunas;
        boxTexture = TextureController.getInstance().getTexture("item_box.png");
        boxTextureOver = TextureController.getInstance().getTexture("item_box_over.png");
        // --
        buttonReorderItens = new Button(NO_ORDER);
        buttonReorderItens.setFont(FontController.getFont("Arial", 1, 10));
        buttonReorderItens.setAlpha(0.5f);
        buttonReorderItens.setBorder(Color.BLACK, 1);
        buttonReorderItens.setRoundRectSize(0, 0);
        buttonReorderItens.addEventListener((ActionListener) (ActionEvent e) -> {
            switch (reoderCase) {
                default:
                case 0:
                    // -- nome
                    Collections.sort(itens, (ItemInventory o1, ItemInventory o2)
                            -> o1.getItem().getName().compareToIgnoreCase(o2.getItem().getName()));
                    break;
                case 1:
                    // -- quantidade
                    Collections.sort(itens, Comparator.comparingInt(ItemInventory::getAmount));
                    break;
                case 2:
                    // -- tipo
                    Collections.sort(itens, (ItemInventory o1, ItemInventory o2)
                            -> o1.getItem().getType().compareTo(o2.getItem().getType())
                            | (Integer.compare(o1.getAmount(), o2.getAmount()))
                    );
                    break;
            }
            buttonReorderItens.setLabel(ORDER_NAMES[reoderCase]);
            reoderCase++;
            if (reoderCase > 2) {
                reoderCase = 0;
            }
        });

        this.descPanel = descPanel;
    }

    @Override
    public void init(SceneBundle bundle) {
        setBorderSize(1);
        setBorderColor(Color.WHITE);
        setBackgroundColor(new Color(210, 210, 210));
        setTransparence(0.5f);
        setSize(numColunas * 55d + 10d, numLinhas * 42d + 40d + 30d);
        setRoundRect(0, 0);
        setDragArea(0d, 0d, getWidth() - 32d, 32d);
        buttonReorderItens.setBounds(getWidth() - 160, getHeight() - 30, 150, 24);
        // --
        add(buttonReorderItens);
        addTopDecoration(LanguageController.getDefaultLanguage().getValue("sunheroes.widget.inventory.title"));
        super.init(bundle);
    }


    @Override
    public void update() {
        super.update();
        int tmpY = getY() + 40;
        int box = 0;
        for (int i = 0; i < numLinhas; i++) {
            int tmpX = getX() + 8;
            for (int j = 0; j < numColunas; j++) {
                if (!itens.isEmpty() && box < itens.size()) {
                    ItemInventory inv = itens.get(box);
                    if (mouseX >= tmpX && mouseY >= tmpY
                            && mouseX <= tmpX + 50 && mouseY <= tmpY + 37 && !DRAGGING_INFO.dragging) {
                        inv.setMouseOver(true);
                        if (InputController.getInstance().isMousePressed(MouseEvent.BUTTON1)
                                && highlightItem == null && isVisible()) {
                            highlightItem = inv;
                            break;
                        } else if (InputController.getInstance().isMouseClicked(MouseEvent.BUTTON3) && isVisible()) {
                            InputController.getInstance().releaseMouseButton(MouseEvent.BUTTON3);
                            inv.setClicked(true);
                            DRAGGING_INFO.dragging = false;
                            DRAGGING_INFO.panelID = -1;
                        }
                    } else {
                        inv.setMouseOver(false);
                    }
                }
                tmpX += 55;
                box++;
            }
            tmpY += 42;
        }
        // --
        for (ItemInventory inv : itens) {
            if (inv.isClicked()) {
                inv.setClicked(false);
                showDescPanel(inv.getItem());
                break;
            }
        }
        // -- clear hightlight item
        if (!InputController.getInstance().isMousePressed(MouseEvent.BUTTON1)) {
            if (highlightItem != null) {
                DRAGGING_INFO.dragging = false;
                DRAGGING_INFO.panelID = -1;
            }
            highlightItem = null;
        }
        // -- disable panels dragging
        if (highlightItem != null) {
            DRAGGING_INFO.dragging = true;
            DRAGGING_INFO.panelID = -1;
        }
    }

    @Override
    public void render(Graphics2D g) {
        super.render(g);
        if (isVisible()) {
            // -- itens...
            int tmpY = getY() + 40;
            int box = 0;
            for (int i = 0; i < numLinhas; i++) {
                int tmpX = getX() + 8;
                for (int j = 0; j < numColunas; j++) {
                    if (!itens.isEmpty() && box < itens.size()) {
                        ItemInventory inv = itens.get(box);
                        if (inv.isMouseOver()) {
                            boxTextureOver.render(g, tmpX, tmpY);
                        } else if (boxTexture != null) {
                            boxTexture.render(g, tmpX, tmpY);
                        }
                        Item item = inv.getItem();
                        if (item != null) {
                            item.getTexture().render(g, tmpX + 10, tmpY + 3);
                            g.setColor(amountColor);
                            g.setFont(amountFont);
                            String amountStr = basicNumberFormat.format(inv.getAmount());
                            g.drawString(amountStr, tmpX + 26, tmpY + 35);
                        }
                        // --
                    } else if (boxTexture != null) {
                        boxTexture.render(g, tmpX, tmpY);
                    }
                    tmpX += 55;
                    box++;
                }
                tmpY += 42;
            }
            // --
            box = 0;
            for (int i = 0; i < numLinhas; i++) {
                for (int j = 0; j < numColunas; j++) {
                    if (!itens.isEmpty() && box < itens.size()) {
                        ItemInventory inv = itens.get(box);
                        Item item = inv.getItem();
                        if (item != null && inv.isMouseOver() && highlightItem == null) {
                            drawName(g, item.getCompleteName());
                        } else if (highlightItem != null) {
                            highlightItem.getItem().getTexture().render(g, mouseX - 20, mouseY - 15);
                            drawHighlightAmount(g, String.valueOf(highlightItem.getAmount()));
                        }
                        // --
                    }
                    box++;
                }
            }
        }
    }

    private void drawName(Graphics2D g, String name) {
        Label labelName = new Label();
        labelName.setText(name);
        labelName.setBorder(itemNameBorderColor, 1);
        labelName.setBackgroundColor(itemNameBgColor);
        labelName.setForegroundColor(itemNameColor);
        int width = (int) itemNameFont.getStringBounds(name, frc).getWidth() + 30;
        labelName.setBounds(mouseX - width / 2, mouseY + 20, width, 24);
        labelName.setFont(itemNameFont);
        labelName.setHorizontalTextAlign(HorizontalTextAlign.HORIZONTAL_ALIGN_CENTER);
        labelName.setRoundRectWidth(20);
        labelName.setRoundRectHeight(20);
        labelName.setTransparence(0.75f);
        labelName.render(g);
    }

    private void drawHighlightAmount(Graphics2D g, String amountText) {
        Label labelName = new Label();
        labelName.setText(amountText);
        labelName.setBorder(itemNameBorderColor, 1);
        labelName.setBackgroundColor(itemNameBgColor);
        labelName.setForegroundColor(itemNameColor);
        int width = (int) itemNameFont.getStringBounds(amountText, frc).getWidth() + 30;
        labelName.setBounds(mouseX - width / 2, mouseY + 20, width, 24);
        labelName.setFont(itemNameFont);
        labelName.setHorizontalTextAlign(HorizontalTextAlign.HORIZONTAL_ALIGN_CENTER);
        labelName.setRoundRectWidth(20);
        labelName.setRoundRectHeight(20);
        labelName.setTransparence(0.75f);
        labelName.render(g);
    }

    public void showDescPanel(Item item) {
        if (item != null && descPanel != null) {
            descPanel.setPosition(
                    GameComponent.SCREEN_HORIZONTAL_CENTER_ALIGN,
                    GameComponent.SCREEN_VERTICAL_CENTER_ALIGN);
            descPanel.getLabelTopTitle().setText("  - " + item.getCompleteName());
            descPanel.setVisible(true);
        }
    }

    public int addItem(int itemID, int amount) {
        amount = amount > NUM_MAX_ITEM ? NUM_MAX_ITEM : amount;
        for (ItemInventory tmp : itens) {
            if (tmp.getItem().getId() == itemID) {
                if (tmp.getAmount() < NUM_MAX_ITEM) {
                    tmp.addAmount(amount);
                    if (tmp.getAmount() > NUM_MAX_ITEM) {
                        tmp.setAmount(NUM_MAX_ITEM);
                    }
                    return ITEM_ADD;
                } else {
                    return ITEM_MAX;
                }
            }
        }
        if (itens.size() >= maxItens) {
            return INVENTORY_FULL;
        } else {
            Item newItem = ItemService.buscarItem(itemID);
            if (newItem != null) {
                ItemInventory inv = new ItemInventory(newItem, amount);
                itens.add(inv);
                buttonReorderItens.setLabel(NO_ORDER);
                reoderCase = 0;
                return ITEM_ADD;
            } else {
                return ITEM_NOTFOUND;
            }
        }
    }

    public ItemInventory getItem(int itemID, int amount) {
        List<ItemInventory> removes = new ArrayList<>();
        ItemInventory tmp = null;
        for (ItemInventory inv : itens) {
            if (inv.getItem().getId() == itemID && inv.getAmount() >= amount) {
                tmp = new ItemInventory(inv.getItem(), amount);
                // --
                inv.setAmount(inv.getAmount() - amount);
                if (inv.getAmount() <= 0) {
                    removes.add(inv);
                }
                break;
            }
        }
        // --
        itens.removeAll(removes);
        return tmp;
    }

    public int delItem(int itemID, int amount) {
        List<ItemInventory> removes = new ArrayList<>();
        for (ItemInventory inv : itens) {
            if (inv.getItem().getId() == itemID) {
                inv.setAmount(inv.getAmount() - amount);
                if (inv.getAmount() <= 0) {
                    removes.add(inv);
                }
                break;
            }
        }
        // --
        if (!removes.isEmpty()) {
            buttonReorderItens.setLabel(NO_ORDER);
            reoderCase = 0;
        }
        itens.removeAll(removes);
        return ITEM_DEL;
    }

    // -------------------------------------------------------------------------
    @Override
    public boolean equals(Object o) {
        return super.equals(o);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

}
