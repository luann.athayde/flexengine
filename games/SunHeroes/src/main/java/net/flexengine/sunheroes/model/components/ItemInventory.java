/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.flexengine.sunheroes.model.components;

import javax.persistence.Transient;
import net.flexengine.model.database.EntityModel;

/**
 *
 * @author Luann Athayde
 */
public class ItemInventory extends EntityModel {

    private Item item;
    private int amount;
    
    @Transient
    private boolean mouseOver;
    @Transient
    private boolean clicked;
    
    public ItemInventory(Item item, int amount) {
        this.item = item;
        this.amount = amount;
        this.mouseOver = false;
    }

    public boolean isMouseOver() {
        return mouseOver;
    }

    public void setMouseOver(boolean mouseOver) {
        this.mouseOver = mouseOver;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public void addAmount(int amount) {
        this.amount += amount;
    }

    public boolean isClicked() {
        return clicked;
    }

    public void setClicked(boolean clicked) {
        this.clicked = clicked;
    }
    
}
