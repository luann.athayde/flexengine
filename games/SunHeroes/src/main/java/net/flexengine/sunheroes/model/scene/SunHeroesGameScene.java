/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.flexengine.sunheroes.model.scene;

import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.util.Objects;

import net.flexengine.sunheroes.utils.SunHeroesConsoleListener;
import net.flexengine.sunheroes.model.components.InventoryPanel;
import net.flexengine.sunheroes.model.components.ItemDescriptionPanel;
import net.flexengine.sunheroes.service.ItemService;
import net.flexengine.controller.CursorController;
import net.flexengine.controller.InputController;
import net.flexengine.model.config.Priority;
import net.flexengine.controller.SoundController;
import net.flexengine.controller.TextureController;
import net.flexengine.model.components.GameConsole;
import net.flexengine.model.scene.GameScene;
import net.flexengine.model.scene.SceneBundle;
import net.flexengine.model.sound.Sound;
import net.flexengine.view.Texture;

/**
 *
 * @author luann
 */
public class SunHeroesGameScene extends GameScene {

    private Sound menuMusic;

    private InventoryPanel inventory;
    private ItemDescriptionPanel descPanel;
    private GameConsole console;

    private Texture selectedItem;

    public SunHeroesGameScene() {
        super("SunHeroesGameScene");
    }

    @Override
    public void loadResources() {
        // -- load textures...
        TextureController.getInstance().loadTextures();
        setBackground("background001.jpg");
        // -- load sounds and musics...
        SoundController.getInstance().loadSounds();
        // -- load itens...
        ItemService.loadItens();
        // --show game cursor
        CursorController.getInstance().showGameCursor();
    }

    @Override
    public void initComponents(SceneBundle bundle) {
        descPanel = new ItemDescriptionPanel();
        inventory = new InventoryPanel(6, 4, descPanel);
        inventory.setPosition(10,10);
        console = new GameConsole();
        console.setPriority(Priority.HIGH_PRIORITY);
        console.setVisible(true);
        console.setPosition(
                GameConsole.SCREEN_HORIZONTAL_RIGHT_ALIGN,
                10, -10, 0);
        console.addConsoleListener(new SunHeroesConsoleListener(inventory));
        add(inventory);
        add(console);
        add(descPanel);
    }

    @Override
    public void init(SceneBundle bundle) {
        super.init(bundle);
        menuMusic = SoundController.getInstance().get("virus_launchpad");
        SoundController.getInstance().play(menuMusic, true);
    }

    @Override
    public synchronized void render(Graphics2D g) {
        super.render(g);
        // draw selected item
        if (Objects.nonNull(selectedItem) ) {
            int tmpX = (int) (InputController.getInstance().getMouseX() - selectedItem.getWidth() / 2);
            int tmpY = (int) (InputController.getInstance().getMouseY() - selectedItem.getHeight() / 2);
            selectedItem.render(g, tmpX, tmpY);
        }
    }

    @Override
    public synchronized void update() {
        super.update();
        if (InputController.getInstance().isControlAndKeyPressed(KeyEvent.VK_I)) {
            inventory.setVisible(!inventory.isVisible());
            InputController.getInstance().releaseKey(KeyEvent.VK_I);
        }
        if (InputController.getInstance().isMouseClicked(MouseEvent.BUTTON1)
                && Objects.nonNull(inventory.getHighlightItem())) {
            selectedItem = inventory.getHighlightItem().getItem().getTexture();
        } else if (inventory.getHighlightItem() == null) {
            selectedItem = null;
        }
    }

}
