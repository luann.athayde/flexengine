/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.flexengine.sunheroes.service;

import net.flexengine.sunheroes.model.components.Item;
import net.flexengine.sunheroes.model.components.ItemType;
import net.flexengine.controller.ResourceController;
import net.flexengine.controller.SoundController;
import net.flexengine.controller.TextureController;
import net.flexengine.view.Texture;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Logger;

/**
 * @author luann
 */
public class ItemService {

    private static Logger LOG = Logger.getLogger("ItemService");
    private static List<Item> ITEMS;

    static {
        ITEMS = new ArrayList<>();
    }

    public static void loadItens() {
        try {
            // ID, sprite_name, Item Name, slots, atk, def, buy, sell, type, { script }
            InputStream itemInputStream = ResourceController.getResourceAsStream("item_db.txt");
            try (Scanner sc = new Scanner(itemInputStream, "UTF-8")) {
                while (sc.hasNextLine()) {
                    String line = sc.nextLine();
                    if (line.startsWith("--") || line.startsWith("//")) {
                        continue;
                    }
                    Item item = new Item();
                    String data[] = line.split(",", 10);
                    item.setId(Integer.valueOf(data[0].trim()));
                    item.setSpriteName(data[1].trim());
                    item.setName(data[2].trim());
                    item.setSlots(Integer.valueOf(data[3].trim()));
                    item.setAtk(Double.valueOf(data[4].trim()));
                    item.setDef(Double.valueOf(data[5].trim()));
                    item.setBuy(Double.valueOf(data[6].trim()));
                    item.setSell(Double.valueOf(data[7].trim()));
                    // --
                    Integer typeInt = Integer.valueOf(data[8].trim());
                    item.setType(ItemType.values()[typeInt]);
                    // --
                    item.setScript(data[9].trim());
                    // --
                    Texture itemTexture = TextureController.getInstance().getTexture(data[1].trim() + ".png");
                    if (itemTexture != null) {
                        itemTexture = itemTexture.scale(30, 30);
                        item.setTexture(itemTexture);
                    }
                    // --
                    ITEMS.add(item);
                }
            }
            // --
            // ID#DESCRIPTION#
            InputStream itemDescInputStream = ResourceController.getResourceAsStream("item_desc.txt");
            try (Scanner sc = new Scanner(itemDescInputStream, "UTF-8")) {
                Item item = null;
                String desc = "";
                while (sc.hasNextLine()) {
                    String line = sc.nextLine();
                    if (line.startsWith("--") || line.startsWith("//")) {
                        continue;
                    }
                    if (line.endsWith("#") && !line.equalsIgnoreCase("#")) {
                        if (item != null && !desc.isEmpty()) {
                            desc = desc.endsWith("[n]") ? desc.substring(0, desc.length() - 3) : desc;
                            item.setDescription(desc);
                        }
                        item = buscarItem(Integer.valueOf(line.replace("#", "")));
                        desc = "";
                    } else if (!line.equalsIgnoreCase("#")) {
                        desc += line + "[n]";
                    }
                }
                if (item != null && !desc.isEmpty()) {
                    desc = desc.endsWith("[n]") ? desc.substring(0, desc.length() - 3) : desc;
                    item.setDescription(desc);
                }
            }
        } catch (Exception e) {
            LOG.severe("ItemService::loadItens(): Falha ao carregar itens!\n\tERROR = " + e);
        }
    }

    public static Item buscarItem(int itemID) {
        for (Item tmp : ITEMS) {
            if (tmp.getId() == itemID) {
                return tmp;
            }
        }
        return null;
    }

    public static void main(String... args) {
        TextureController.getInstance().loadTextures();
        SoundController.getInstance().loadSounds();
        ItemService.loadItens();
        ITEMS.stream().forEach((item) -> {
            System.out.println(item);
        });
    }

}
