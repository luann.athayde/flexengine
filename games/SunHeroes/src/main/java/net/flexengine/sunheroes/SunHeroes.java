/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.flexengine.sunheroes;

import net.flexengine.FlexEngineBox;
import net.flexengine.sunheroes.model.scene.SunHeroesGameScene;
import net.flexengine.controller.FlexEngine;
import net.flexengine.controller.ResourceController;
import net.flexengine.controller.SceneController;
import net.flexengine.controller.TextureController;
import net.flexengine.view.Texture;

/**
 * @author luann
 */
public class SunHeroes {

    public static void main(String... args) {
        // -- load scene...
        // Change basic scene
        FlexEngineBox.main(args);
        SceneController.getInstance().setNextScene(new SunHeroesGameScene());
        // --
        Texture loadingTexture = TextureController.getInstance().getTexture(
                "background001.jpg",
                ResourceController.getLocale("textures/background001.jpg"));
        SceneController.getInstance().getLoadingScene().setBackground(loadingTexture);
        // --
        FlexEngine.getInstance().start();
    }

}
