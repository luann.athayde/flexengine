/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.flexengine.sunheroes.model.components;

/**
 *
 * @author Luann Athayde
 */
public enum ItemType {

    USABLE,     // 0
    WEAPON,     // 1
    SHIELD,     // 2
    ARMOR,      // 3
    HEADER,     // 4
    ACCESSORY,  // 5
    ETC;        // 6
    
}
