/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.flexengine.sunheroes.model.components;

import javax.persistence.Transient;
import net.flexengine.model.database.EntityModel;
import net.flexengine.view.Texture;

/**
 *
 * @author Luann Athayde
 */
public class Item extends EntityModel {

    // ID, sprite_name, Item Name, slots, atk, def, buy, sell, type, { script }
    private String name;
    private String spriteName;
    private int slots;
    private double atk;
    private double def;
    private double buy;
    private double sell;
    private ItemType type;
    private String script;

    @Transient
    private transient String description;
    @Transient
    private transient Texture texture;

    public Item() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSpriteName() {
        return spriteName;
    }

    public void setSpriteName(String spriteName) {
        this.spriteName = spriteName;
    }

    public int getSlots() {
        return slots;
    }

    public void setSlots(int slots) {
        this.slots = slots;
    }

    public double getAtk() {
        return atk;
    }

    public void setAtk(double atk) {
        this.atk = atk;
    }

    public double getDef() {
        return def;
    }

    public void setDef(double def) {
        this.def = def;
    }

    public double getBuy() {
        return buy;
    }

    public void setBuy(double buy) {
        this.buy = buy;
    }

    public double getSell() {
        return sell;
    }

    public void setSell(double sell) {
        this.sell = sell;
    }

    public ItemType getType() {
        return type;
    }

    public void setType(ItemType type) {
        this.type = type;
    }

    public String getScript() {
        return script;
    }

    public void setScript(String script) {
        this.script = script;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Texture getTexture() {
        return texture;
    }

    public void setTexture(Texture texture) {
        this.texture = texture;
    }
    
    public String getCompleteName() {
        String tmpName = name;
        if( slots > 0 ) {
            tmpName += " ["+slots+"]";
        }
        return tmpName;
    }

    @Override
    public String toString() {
        return "Item{id="+ getId() + ", name=" + name + ", spriteName=" + spriteName + 
                ", slots=" + slots + ", atk=" + atk + ", def=" + def + ", buy=" + buy + 
                ", sell=" + sell + ", type=" + type + ", script=" + script + 
                ", description=" + description + ", texture=" + texture + '}';
    }
    
}
