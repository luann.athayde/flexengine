/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.flexengine.sunheroes.utils;

import net.flexengine.sunheroes.model.components.InventoryPanel;
import net.flexengine.controller.FlexEngine;
import net.flexengine.model.components.GameConsole;


/**
 * @author Luann Athayde
 */
public class SunHeroesConsoleListener implements GameConsole.ConsoleListener {

    private final InventoryPanel inventory;

    public SunHeroesConsoleListener(InventoryPanel inventory) {
        this.inventory = inventory;
    }

    @Override
    public void execute(GameConsole console, String command) {
        try {
            command = command.trim();
            if (command.startsWith("additem")) {
                String parts[] = command.split(" ");
                Integer itemID = Integer.valueOf(parts[1]);
                Integer amount = Integer.valueOf(parts[2]);
                switch (inventory.addItem(itemID, amount)) {
                    case InventoryPanel.ITEM_ADD:
                        console.getTextFieldConsole().setText("Adiconado ao inventario!");
                        break;
                    case InventoryPanel.ITEM_MAX:
                        console.getTextFieldConsole().setText("Máximo para o item alcançado!");
                        break;
                    case InventoryPanel.ITEM_NOTFOUND:
                        console.getTextFieldConsole().setText("Item não encontrado!");
                        break;
                    case InventoryPanel.INVENTORY_FULL:
                        console.getTextFieldConsole().setText("Invetário está cheio!");
                        break;
                }
            } else if (command.startsWith("delitem")) {
                String parts[] = command.split(" ");
                Integer itemID = Integer.valueOf(parts[1]);
                Integer amount = Integer.valueOf(parts[2]);
                inventory.delItem(itemID, amount);
                console.getTextFieldConsole().setText("Removido do inventario!");
            } else if (command.startsWith("hide") || command.startsWith("close")) {
                console.setVisible(false);
            } else if (command.startsWith("exit")) {
                FlexEngine.getInstance().stop();
            } else {
                String text = "Comando não encontrado!";
                console.getTextFieldConsole().setText(text);
            }
        } catch (Exception e) {
            String text = "Falha ao executar o comando!\n\tERROR = " + e;
            console.getTextFieldConsole().setText(text);
        }
    }

}
