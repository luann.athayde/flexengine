/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.flexengine.jumper;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.util.Random;
import net.flexengine.controller.InputController;
import net.flexengine.model.components.GameComponent;
import net.flexengine.view.GameWindow;

/**
 *
 * @author Luann Athayde
 */
public class JumperGuy extends GameComponent {

    private float speedX;
    private float speedY;

    private int forca;
    private int potencia;

    private Random rand;

    public static final float GRAVITY = 9.8f;

    public JumperGuy() {
        super("JumperGuy", 300, 300);
        speedX = 0;
        speedY = 0;
        forca = 3;
        potencia = 3;
        rand = new Random();
    }

    @Override
    public void render(Graphics2D g) {
        g.setColor(Color.WHITE);
        g.drawRect(x, y, 10, 50);
    }

    private void rolarDados() {
        int sucessos = 0;
        for (int i = 0; i < forca + potencia; i++) {
            int dado = rand.nextInt(10) + 1;
            if (dado >= 3) {
                sucessos++;
            }
        }
        speedY -= (sucessos * 0.6);
    }

    @Override
    public void update() {
        x += speedX;
        y += (int) (speedY + GRAVITY);
        y = y + 50 >= GameWindow.getInstance().getViewportHeight() ? GameWindow.getInstance().getViewportHeight() - 50 : y;
        speedY++;
        speedY = speedY > 0 ? 0 : speedY;
        if (InputController.getInstance().isKeyPressed(KeyEvent.VK_Z)) {
            rolarDados();
        }
    }

}
