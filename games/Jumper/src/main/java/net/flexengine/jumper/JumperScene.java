/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.flexengine.jumper;

import java.awt.Graphics2D;
import net.flexengine.model.scene.GameScene;
import net.flexengine.model.scene.SceneBundle;

/**
 *
 * @author Luann Athayde
 */
public class JumperScene extends GameScene {

    public JumperScene() {
        super("JumperScene");
    }

    @Override
    public void initComponents(SceneBundle bundle) {
        add(new JumperGuy());
    }
    
    @Override
    public synchronized void render(Graphics2D g) {
        super.render(g);
    }

    @Override
    public synchronized void update() {
        super.update();
    }
    
}
