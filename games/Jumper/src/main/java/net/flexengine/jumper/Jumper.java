/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.flexengine.jumper;

import net.flexengine.controller.FlexEngine;
import net.flexengine.controller.SceneController;
import net.flexengine.view.GameWindow;

/**
 *
 * @author Luann Athayde
 */
public class Jumper {
    
    public static void main(String ... args) {
        GameWindow.getInstance().setSize(1400, 900);
        FlexEngine.getInstance().setDebugOnScreen(true);
        FlexEngine.getInstance().start();
        SceneController.getInstance().setNextScene(new JumperScene());
    }
    
}
