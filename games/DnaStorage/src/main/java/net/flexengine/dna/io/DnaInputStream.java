package net.flexengine.dna.io;

import java.io.ByteArrayInputStream;

/**
 *
 */
public class DnaInputStream extends ByteArrayInputStream implements Dna {

    public DnaInputStream(byte[] data) {
        super(data);
    }

    /**
     * Read the first 2 pairs available.
     * Aways only and only 4 bytes readed
     * @return
     */
    public String readPairs() {
        StringBuilder dna = new StringBuilder();
        while( available() > 0 ) {
           dna.append( decode( (byte)read() ) );
        }
        return dna.toString();
    }

    public String decode(byte pairs) {
        StringBuilder strPairs = new StringBuilder();

        for (int x = 7; x >= 0; x -= 2) {

            int b1 = (pairs >> x) & 1;
            int b2 = (pairs >> x - 1) & 1;

            strPairs.append(
                    Integer.toBinaryString(b1) +
                            Integer.toBinaryString(b2) + "-"
            );

        }

        return decodePairs(strPairs.toString());
    }

    public String decodePairs(String binaryPairs) {
        StringBuilder pairs = new StringBuilder();
        String data[] = binaryPairs.split("-");
        for (String part : data) {
            switch (part) {
                case "00":
                    pairs.append(Adenine);
                    break;
                case "01":
                    pairs.append(Thymine);
                    break;
                case "10":
                    pairs.append(Cytosine);
                    break;
                case "11":
                    pairs.append(Guanine);
                    break;
            }
        }

        return pairs.toString();
    }

}
