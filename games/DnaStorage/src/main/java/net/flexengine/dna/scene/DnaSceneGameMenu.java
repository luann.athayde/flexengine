package net.flexengine.dna.scene;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import net.flexengine.controller.CursorController;
import net.flexengine.controller.FontController;
import net.flexengine.controller.SoundController;
import net.flexengine.controller.TextureController;
import net.flexengine.utils.StylesUtil;
import net.flexengine.model.components.Button;
import net.flexengine.model.components.GameComponent;
import net.flexengine.model.scene.GameScene;
import net.flexengine.model.scene.LoadScene;
import net.flexengine.model.scene.SceneBundle;

import java.awt.*;
import java.awt.event.ActionListener;

/**
 *
 */
@Data
@Slf4j
public class DnaSceneGameMenu extends GameScene {

    private Font   defaultButtonFont;
    private Button buttonGenerateDNA;

    // ---
    public DnaSceneGameMenu() {
        super("DnaSceneGameMenu");
    }

    @Override
    public void loadResourcesOnce() {

        TextureController.getInstance().loadTextures();

        SoundController.getInstance().loadSounds();

        setBackground(TextureController.getInstance().getTexture("dna_bg_004.jpg"));

        getGlobalBundle().putExtra(LoadScene.GLOBAL_PROGRESS_KEY, 100d);
        sleep(5000);
    }

    @Override
    public void loadResources() {
        super.loadResources();
        CursorController.getInstance().showGameCursor();
    }

    @Override
    public void initComponents(SceneBundle bundle) {

        defaultButtonFont = FontController.getFont(FontController.getDefaultFont(), Font.BOLD, 32);

        buttonGenerateDNA = new Button("Generate DNA");
        buttonGenerateDNA.setBounds(
                GameComponent.SCREEN_HORIZONTAL_CENTER_ALIGN, GameComponent.SCREEN_VERTICAL_CENTER_ALIGN,
                320, 90
        );
        buttonGenerateDNA.setFont(defaultButtonFont);
        buttonGenerateDNA.setRoundRect(Button.ROUND_RECT_ALL);
        buttonGenerateDNA.setRoundRectSize(8, 8);

        StylesUtil.aplyStyle(buttonGenerateDNA, StylesUtil.STYLE_SOLARIN_ALT);

        add(buttonGenerateDNA);

        buttonGenerateDNA.addEventListener((ActionListener) e -> {
            try {
                buttonGenerateDNA.setVisible(false);
                log.info("Generating DNA...");
                Thread.sleep(10000);
                log.info("DNA generation finished!");
                buttonGenerateDNA.setVisible(true);
            } catch (Exception ex) {
                log.error("Fail to generate DNA...", ex);
            }
        });
    }


    @Override
    public synchronized void update() {
        super.update();

        buttonGenerateDNA.setBounds(
                GameComponent.SCREEN_HORIZONTAL_CENTER_ALIGN, GameComponent.SCREEN_VERTICAL_CENTER_ALIGN,
                320, 90
        );
        buttonGenerateDNA.setFont(defaultButtonFont);

    }

}
