/**
 *
 */
package net.flexengine.dna.io;

/**
 *
 */
public interface Dna {

    String Adenine = "A";  // 00
    String Thymine = "T";  // 01
    String Cytosine = "C"; // 10
    String Guanine = "G";  // 11

}
