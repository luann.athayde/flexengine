package net.flexengine.dna;

import net.flexengine.controller.*;
import net.flexengine.controller.arguments.ArgumentsProcessor;
import net.flexengine.dna.io.Dna;
import net.flexengine.dna.scene.DnaSceneGameMenu;
import net.flexengine.model.scene.InfinityProgressLoadScene;
import net.flexengine.view.GameWindow;
import net.flexengine.view.Texture;

/**
 *
 */
public class DnaStorage implements Dna {

    public static final String APP_NAME = "DNA Storage";
    public static final String DEFAULT_FONT = "Coldiac Free";
    public static final String APP_ICON = "dna_storage_icon.png";
    public static final String LOAD_BG_NAME = "dna_bg_002.jpg";

    public static void main(String... args) {
        // ---
        // Basic setup...
        FontController.setDefaultFont(DEFAULT_FONT);
        ArgumentsProcessor.getInstance().process(args);
        GameWindow.getInstance().setName(APP_NAME);
        GameWindow.getInstance().setIconImage(
                TextureController.getInstance().getTexture(
                        APP_ICON,
                        ResourceController.getTexturePathFor("icons/%s", APP_ICON)
                )
        );
        // Change basic scene
        SceneController.getInstance().setNextScene(new DnaSceneGameMenu());
        // --
        // Loading scene config...
        Texture loadingTexture = TextureController.getInstance().getTexture(
                LOAD_BG_NAME,
                ResourceController.getTexturePathFor("bg/%s", LOAD_BG_NAME)
        );
        SceneController.getInstance().setLoadingScene(new InfinityProgressLoadScene());
        SceneController.getInstance().getLoadingScene().setBackground(loadingTexture);
        // --
        FlexEngine.getInstance().start();
    }

}
