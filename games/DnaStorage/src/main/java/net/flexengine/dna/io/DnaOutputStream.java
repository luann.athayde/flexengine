package net.flexengine.dna.io;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class DnaOutputStream extends ByteArrayOutputStream implements Dna {

    public DnaOutputStream() {
        super(1024);
    }

    /**
     * Write pairs 2 pairs into stream...
     *
     * @param pairs
     * @throws IOException
     */
    public void writePairs(String pairs) throws IOException {
        write(
            encode(pairs)
        );
    }

    public byte[] encode(String multiPairs) {
        try {
            byte[] bPairs = new byte[multiPairs.length() / 4];

            for (int i = 0, first=0; i < bPairs.length; i++, first+=4) {

                String pairs = multiPairs.substring(first, first+4);

                StringBuilder binaryPairs = new StringBuilder();
                for (char c : pairs.toCharArray()) {
                    switch (c) {
                        case 'A':
                            binaryPairs.append("00");
                            break;
                        case 'T':
                            binaryPairs.append("01");
                            break;
                        case 'C':
                            binaryPairs.append("10");
                            break;
                        case 'G':
                            binaryPairs.append("11");
                            break;
                    }
                }

                bPairs[i] = (byte) (
                        Integer.valueOf(binaryPairs.toString(), 2).intValue()
                );

            }

            return bPairs;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

}
