package net.flexengine.holons;

import net.flexengine.FlexEngineBox;
import net.flexengine.controller.FlexEngine;
import net.flexengine.controller.ResourceController;
import net.flexengine.controller.SceneController;
import net.flexengine.controller.TextureController;
import net.flexengine.holons.scene.HolonsMenuScene;
import net.flexengine.view.GameWindow;

/**
 *
 */
public class Holons {

    public static void main(String... args) {
        FlexEngineBox.main(args);
        // ---
        GameWindow.getInstance().setName("Holons");
        // ---
        FlexEngine engine = FlexEngine.getInstance();
        engine.setCloseOnScape(true);
        engine.start();
        // ---
        SceneController.getInstance().getLoadingScene().setBackground(
                TextureController.getInstance().getTexture(
                        "loading_001.jpg",
                        ResourceController.getTexturePathFor("loading_001.jpg")
                )
        );
        SceneController.getInstance().setNextScene(new HolonsMenuScene());
    }

}
