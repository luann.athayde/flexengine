package net.flexengine.holons.scene;

import net.flexengine.controller.LanguageController;
import net.flexengine.controller.SoundController;
import net.flexengine.controller.SpriteController;
import net.flexengine.controller.TextureController;
import net.flexengine.model.scene.GameScene;
import net.flexengine.model.scene.SceneBundle;

import java.awt.geom.Path2D;

/**
 *
 */
public class HolonsMenuScene extends GameScene {

    Path2D path = new Path2D.Float();
    int x = 0;

    @Override
    public void loadResourcesOnce() {
        getGlobalBundle().putExtra("progress", 5d);

        LanguageController.loadLanguages();
        getGlobalBundle().putExtra("progress", 20d);

        LanguageController.setDefaultLanguageName(getConfig().getLanguage());
        getGlobalBundle().putExtra("progress", 30d);

        TextureController.getInstance().loadTextures();
        getGlobalBundle().putExtra("progress", 40d);

        SoundController.getInstance().loadSounds();
        getGlobalBundle().putExtra("progress", 50d);

        SpriteController.getInstance().loadSprites();
        getGlobalBundle().putExtra("progress", 70d);
    }

    @Override
    public void initComponents(SceneBundle bundle) {
        super.initComponents(bundle);
        getGlobalBundle().putExtra("progress", 90d);

        path = new Path2D.Float();
        path.moveTo(0,0);

        add(path);
    }

    @Override
    public synchronized void update() {
        super.update();

    }

    @Override
    public void init(SceneBundle bundle) {
        super.init(bundle);

        getGlobalBundle().putExtra("progress", 100d);
        sleep(2500);

    }

}
