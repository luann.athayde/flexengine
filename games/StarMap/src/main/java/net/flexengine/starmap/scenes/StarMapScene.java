/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.flexengine.starmap.scenes;

import net.flexengine.model.scene.GameScene;
import net.flexengine.model.scene.SceneBundle;

import java.awt.*;

/**
 * @author Luann Athayde
 */
public class StarMapScene extends GameScene {

    @Override
    public void initComponents(SceneBundle bundle) {
        super.initComponents(bundle);
    }

    @Override
    public void loadResources() {
    }

    @Override
    public synchronized void render(Graphics2D g) {
        super.render(g);
    }

    @Override
    public void update() {
        super.update();
    }

}
