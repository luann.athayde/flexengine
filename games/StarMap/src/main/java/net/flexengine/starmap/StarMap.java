/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.flexengine.starmap;

import net.flexengine.starmap.scenes.StarMapScene;
import net.flexengine.controller.EngineHandler;
import net.flexengine.controller.FlexEngine;
import net.flexengine.controller.SceneController;
import net.flexengine.view.GameWindow;

/**
 *
 * @author Luann Athayde
 */
public class StarMap {
    
    public static void main(String ... args) {
        GameWindow.getInstance().setTitle("StarMap v1.0");
        SceneController.getInstance().setNextScene(new StarMapScene());
        EngineHandler.getInstance().setDesireFps(200);
        EngineHandler.getInstance().setDesireUps(200);
        FlexEngine.getInstance().start();
    }
    
}
