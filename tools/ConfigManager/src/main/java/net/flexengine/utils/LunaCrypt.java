package net.flexengine.utils;

import java.nio.charset.StandardCharsets;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Arrays;
import java.util.Objects;

public class LunaCrypt {

    private static final NumberFormat BASIC_FORMAT = new DecimalFormat("0000");

    public char[] encrypt(String value) {

        int cryptBase = 23;

        char[] data = value.toCharArray();
        char[] buffer = new char[data.length];

        for (int i = 0, j = data.length-1; i < value.length(); i++, j--) {
            buffer[i] = (char)(data[j] * cryptBase);
        }

        System.out.printf("%s\n", previewData(data));
        System.out.printf("%s\n", previewData(buffer));

        return buffer;
    }

    private String previewData(char[] data) {
        StringBuilder sb = new StringBuilder();
        for( char b : data ) {
            sb.append("[").append(BASIC_FORMAT.format(b)).append("]");
        }
        return sb.toString();
    }

    public static void main(String... args) {
        LunaCrypt lunaCrypt = new LunaCrypt();

        lunaCrypt.encrypt("Luann Ramos Athayde");

    }

}
