package net.flexengine.controller.server;

import java.net.Socket;

public class UpdateServer {

    public FlexEngineServer server;

    public void init() {
        server = FlexEngineServer.getInstance();
        server.initialize(8080, 128);
        server.setListener(sock -> new UpdateSession(sock));
        server.start();
    }

    public class UpdateSession extends FlexEngineSession {

        public UpdateSession(Socket sock) {
            super(sock);
        }

        @Override
        public void messageReceived(FlexMessage msg) {
            System.out.println("["+getSessionID()+"] >>> "+msg);
            if( 1d == msg.getAction() ) {
                StringBuilder sb = new StringBuilder();
                sb.append("1\tpackage.txt\tFLD");
                sb.append("2\tupdateServer.jar\tFLD");
                getStream().write( new FlexMessage(1d, sb.toString()) );
            }
        }

    }

    private static final UpdateServer instance = new UpdateServer();
    public static UpdateServer getInstance() {
        return instance;
    }

    public static void main(String ... args) {

        UpdateServer.getInstance().init();

    }

}
