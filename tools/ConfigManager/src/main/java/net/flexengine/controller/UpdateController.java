package net.flexengine.controller;

import net.flexengine.controller.server.FlexEngineSession;
import net.flexengine.controller.server.FlexMessage;

public class UpdateController {

    public void update() {
        checkForUpdates();
        downloadUpdates();
        applyUpdates();
        rebootClient();
    }

    protected void checkForUpdates() {
        FlexEngineSession session = new FlexEngineSession() {
            @Override
            public void messageReceived(FlexMessage msg) {
                System.out.println("checkForUpdates(): "+msg);
            }
        };
        if( session.connect("localhost", 8080) ) {
            session.start();
            session.getStream().write(
                    new FlexMessage(1d, "checkUpdates")
            );
        }
    }

    protected void downloadUpdates() {

    }

    protected void applyUpdates() {

    }

    protected void rebootClient() {

    }

    public static void main(String ... args) {
        new UpdateController().update();
    }

}
