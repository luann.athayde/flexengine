<configuration>
   <resolution>
      <width>1600</width>
      <height>900</height>
      <bitDepth>32</bitDepth>
      <refreshRate>60</refreshRate>
   </resolution>
   <fullScreen>false</fullScreen>
   <antialising>false</antialising>
   <textAntialising>true</textAntialising>
   <language>English</language>
   <resourcesPath>./resources/</resourcesPath>
   <sound>true</sound>
   <generalVolume>100.0</generalVolume>
   <musicVolume>100.0</musicVolume>
   <effectVolume>100.0</effectVolume>
   <channels>32</channels>
   <host>localhost</host>
   <port>13000</port>
   <maxConnections>32000</maxConnections>
   <confirmOnClose>true</confirmOnClose>
</configuration>