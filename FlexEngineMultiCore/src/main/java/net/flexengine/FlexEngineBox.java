/**
 *
 */
package net.flexengine;

import lombok.extern.slf4j.Slf4j;
import net.flexengine.controller.*;
import net.flexengine.controller.arguments.ArgumentsProcessor;
import net.flexengine.model.scene.SandBoxScene;
import net.flexengine.view.GameWindow;

import java.util.Objects;

/**
 * @author Luann Athayde
 */
@Slf4j
public class FlexEngineBox {

    public static void main(String... args) {
        if (Objects.isNull(args) || args.length == 0) {
            showUsage();
        } else {
            LanguageController.loadLanguages();
            // ---
            ArgumentsProcessor.getInstance().addProcessor("--sandbox", (values) -> {
                TextureController.getInstance().loadTextures();
                SceneController.getInstance().setNextScene(new SandBoxScene());
            });
            ArgumentsProcessor.getInstance().addProcessor("--name", (values) -> {
                GameWindow.getInstance().setName(values.get(0));
            });
            ArgumentsProcessor.getInstance().addProcessor("--log", (values) -> {
                EngineHandler.getInstance().setShowInfo(true);
            });
            ArgumentsProcessor.getInstance().addProcessor("--start", (values) -> {
                FlexEngine.getInstance().start();
            });
            // ---
            // Process genery args now...
            ArgumentsProcessor.getInstance().process(args);
        }
    }

    private static void showUsage() {
        showLine(">-------------------------------> FlexEngineBOX <------------------------------<");
        showLine("| Arguments:                                                                   |");
        showLine("|    --setup         Open the engine's setup.                                  |");
        showLine("|    --name          Game name to aply on the engine.                          |");
        showLine("|    --sandbox       Start the engine with the sandBox option.                 |");
        showLine("|    --fps           Define the desired FPS of the engine.                     |");
        showLine("|    --ups           Define the desired UPS of the engine.                     |");
        showLine("|    --start         Start the basic engine.                                   |");
        showLine("|                                                                              |");
        showLine("|    --server [n] [n]                                                          |");
        showLine("|        -port: Define the value of the port to the server.                    |");
        showLine("|        -maxUsers: Define the max numbers of users.                           |");
        showLine(">------------------------------------------------------------------------------<");
    }

    private static void showLine(String line) {
        log.info(line);
    }

}
