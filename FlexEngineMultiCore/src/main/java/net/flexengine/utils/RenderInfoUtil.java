package net.flexengine.utils;

import lombok.extern.slf4j.Slf4j;
import net.flexengine.controller.EngineHandler;
import net.flexengine.controller.RenderController;
import net.flexengine.view.GameWindow;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

@Slf4j
public class RenderInfoUtil {

    private static final JFrame WINDOW_SHOW_INFO = new JFrame("ShowTimeInfoWindow");
    private static boolean windowShowTimeVisible = false;

    public static void openShowTimeInfoWindow() {
        if (windowShowTimeVisible) {
            log.info("ShowTimeInfoWindow already open!");
            return;
        }
        WINDOW_SHOW_INFO.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        WINDOW_SHOW_INFO.setLayout(new BorderLayout());
        WINDOW_SHOW_INFO.setSize(480, 320);
        WINDOW_SHOW_INFO.setLocationRelativeTo(GameWindow.getInstance());

        final JTextArea textAreaInfo = new JTextArea();
        textAreaInfo.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 14));
        textAreaInfo.setBackground(new Color(40, 40, 40));
        textAreaInfo.setForeground(new Color(230, 230, 230));
        textAreaInfo.setSelectedTextColor(new Color(250, 250, 250));
        textAreaInfo.setLineWrap(true);
        textAreaInfo.setTabSize(4);
        textAreaInfo.setText("Loading info...");

        WINDOW_SHOW_INFO.add(textAreaInfo, "Center");

        WINDOW_SHOW_INFO.setVisible(true);
        windowShowTimeVisible = true;
        log.info("ShowTimeInfoWindow opened!");

        AsyncTask.newInstance().run(() -> {

            final StringBuilder sb = new StringBuilder();

            ArrayList<Long> showTimes = RenderController.getInstance().getShowTimes();
            long currentTotalTime = showTimes.stream().reduce(Long::sum).orElse(1L);
            double avarageTime = 0d;
            if (EngineHandler.getInstance().getTotalFps() > 0) {
                avarageTime = (double) currentTotalTime / EngineHandler.getInstance().getTotalFps();
            }

            sb.append("---------------------------------------------------------").append("\r\n");
            sb.append("Current total time: ").append(currentTotalTime).append("\r\n");
            sb.append("Current avarage time: ").append(
                    String.format("%04.05f", avarageTime)
            ).append("\r\n");
            sb.append("---------------------------------------------------------").append("\r\n");

            if (showTimes.size() > 10) {
                log.info("showTimes size = {}", showTimes.size());
                ArrayList<Long> lastTimes = new ArrayList<>(
                        showTimes.subList(showTimes.size() - 10, showTimes.size() - 1)
                );
                lastTimes.forEach(time -> {
                    sb.append(time).append("ms").append("\r\n");
                });
                sb.append("---------------------------------------------------------").append("\r\n");
                if (showTimes.size() > 1024 * 10) {
                    showTimes.clear();
                    showTimes.addAll(lastTimes);
                }
            }

            textAreaInfo.setText(sb.toString());

        }, true);

    }

    public static void closeAll() {
        WINDOW_SHOW_INFO.dispose();
    }

    public static void main(String... args) {
        RenderInfoUtil.openShowTimeInfoWindow();
    }

}
