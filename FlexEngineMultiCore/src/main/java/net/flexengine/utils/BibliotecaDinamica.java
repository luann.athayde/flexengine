package net.flexengine.utils;

import lombok.extern.slf4j.Slf4j;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Luann Athayde
 * @version 1.0
 * @sice 17/05/2019
 */
@Slf4j
public class BibliotecaDinamica {

    private static final Map<String, Boolean> bibliotecasCarregadas = new HashMap<>();

    public static boolean carregarBiblioteca(String nome, String md5) {
        return carregarBiblioteca(nome, md5, false, false);
    }

    public static boolean carregarBiblioteca(String nome, String md5, boolean apagarArquivoTemporario, boolean recarregar) {
        try {
            log.info("Carregando biblioteca [" + nome + "]...");
            if (!recarregar && bibliotecasCarregadas.containsKey(nome)) {
                log.info("Biblioteca [" + nome + "] já está carregada...");
                return bibliotecasCarregadas.get(nome);
            }
            // ---
            BufferedInputStream streamBiblioteca = new BufferedInputStream(
                    ClassLoader.getSystemResourceAsStream(nome),
                    1024 // 1KB
            );
            // ---
            int tamanhoBiblioteca = streamBiblioteca.available();
            byte[] dados = new byte[tamanhoBiblioteca];
            int dadosCarregados = streamBiblioteca.read(dados, 0, tamanhoBiblioteca);
            streamBiblioteca.close();
            if (dadosCarregados != tamanhoBiblioteca) {
                log.error("Falha ao carregar biblioteca! Tamanho: " + tamanhoBiblioteca + " | Total carregado: " + dadosCarregados);
                return false;
            }

            String hash = HashUtil.calcularHashMd5(dados);
            if (md5 != null && !md5.isEmpty() && !hash.equals(md5)) {
                log.error("Arquivo de biblioteca corrompido!");
                return false;
            }

            String prefixoAplicacao = System.getProperty("appName");
            if (prefixoAplicacao == null || prefixoAplicacao.isEmpty()) {
                prefixoAplicacao = "app";
            }

            File bibliotecaTemporaria = File.createTempFile(prefixoAplicacao + "_", "_" + nome);
            bibliotecaTemporaria.deleteOnExit();
            Files.copy(
                    new ByteArrayInputStream(dados),
                    bibliotecaTemporaria.getAbsoluteFile().toPath(),
                    java.nio.file.StandardCopyOption.REPLACE_EXISTING
            );

            String caminhoBiblioteca = bibliotecaTemporaria.getAbsolutePath();
            log.info("Biblioteca temporaria [" + nome + "]: " + caminhoBiblioteca);
            System.load(caminhoBiblioteca);

            log.info("Biblioteca [" + nome + "] carregada...");
            if (apagarArquivoTemporario) {
                log.info("Arquivo temporario apagado: " + bibliotecaTemporaria.delete());
            }
            bibliotecasCarregadas.put(nome, true);
        } catch (Exception e) {
            bibliotecasCarregadas.put(nome, true);
            log.error("Falha ao carregar biblioteca dinamica [" + nome + "]!", e);
            return false;
        }
        return true;
    }

    public static Map<String, Boolean> getBibliotecasCarregadas() {
        Map<String, Boolean> copia = new HashMap<>();
        for (String key : bibliotecasCarregadas.keySet()) {
            copia.put(key, bibliotecasCarregadas.get(key));
        }
        return copia;
    }

}
