package net.flexengine.utils;

import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Slf4j
public class HashUtil {

    public static final String EMPTY = "";

    public static final String SHA_1 = "SHA-1";
    public static final String SHA_256 = "SHA-256";
    public static final String SHA_512 = "SHA-512";
    public static final String MD5 = "MD5";

    private HashUtil() {
        throw new IllegalStateException("Classe Utilitaria.");
    }

    public static String calcularHash(String dados, String algoritimo) {
        return calcularHash(dados.getBytes(StandardCharsets.UTF_8), algoritimo);
    }

    public static String calcularHash(byte[] dados, String algoritimo) {
        try {
            // ---
            MessageDigest md = MessageDigest.getInstance(algoritimo);
            md.update(dados);
            // ---
            return byteArrayToHex(md.digest()).toUpperCase();
        } catch (Exception e) {
            log.error("Falha ao calcular HASH[{}]!", algoritimo, e);
        }
        return EMPTY;
    }

    public static String calcularHash(File arquivo, String algoritmo) {
        try {
            // ---
            byte[] buffer = new byte[StreamUtil.BUFFER_2MB];
            MessageDigest md = MessageDigest.getInstance(algoritmo);
            try (FileInputStream fIn = new FileInputStream(arquivo)) {
                int read;
                while ((read = fIn.read(buffer, 0, buffer.length)) != -1) {
                    md.update(buffer, 0, read);
                }
            }
            // ---
            return byteArrayToHex(md.digest()).toUpperCase();
        } catch (Exception e) {
            log.error("Falha ao calcular HASH[{}]!", algoritmo, e);
        }
        return EMPTY;
    }

    public static String byteArrayToHex(Stream<Byte> value) {
        return value.map(b -> String.format("%02x", b))
                .collect(Collectors.joining());
    }

    public static String byteArrayToHex(byte[] value) {
        StringBuilder sb = new StringBuilder(value.length * 2);
        for (byte b : value) {
            sb.append(String.format("%02x", b));
        }
        return sb.toString();
    }

    public static String calcularHashMd5(byte[] dados) {
        return calcularHash(dados, MD5);
    }

    public static String calcularHashSha1(byte[] dados) {
        return calcularHash(dados, SHA_1);
    }

    public static String calcularHashSha256(byte[] dados) {
        return calcularHash(dados, SHA_256);
    }

    public static String calcularHashSha512(byte[] dados) {
        return calcularHash(dados, SHA_512);
    }

    public static String calcularHashMd5(File arquivo) {
        return calcularHash(arquivo, MD5);
    }

    public static String calcularHashSha1(File arquivo) {
        return calcularHash(arquivo, SHA_1);
    }

    public static String calcularHashSha256(File arquivo) {
        return calcularHash(arquivo, SHA_256);
    }

    public static String calcularHashSha512(File arquivo) {
        return calcularHash(arquivo, SHA_512);
    }

    public static String calcularHashSha1(String caminhoArquivo) {
        return calcularHash(new File(caminhoArquivo), SHA_1);
    }

    private static byte[] carregarArquivo(String caminhoArquivo) {
        byte[] buffer = null;
        try (InputStream inputStream = new FileInputStream(caminhoArquivo)) {
            buffer = new byte[inputStream.available()];
            int read = inputStream.read(buffer);
            if (read != buffer.length) {
                return null;
            }
        } catch (IOException e) {
            log.error("Ocorreu um erro ao procurar arquivo = {}", caminhoArquivo, e);
        }
        return buffer;
    }

}
