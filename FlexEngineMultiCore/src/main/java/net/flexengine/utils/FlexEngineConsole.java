/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.flexengine.utils;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.io.InputStream;
import java.io.PrintStream;
import java.util.Objects;
import java.util.Scanner;

/**
 * @author Luann R. Athayde
 * @version 1.0
 * @since 1.0
 */
@Data
@Slf4j
public abstract class FlexEngineConsole implements Runnable {

    private Thread tFlexEngineConsole;
    private String consoleUser;
    private boolean waiting;
    private Scanner sc;

    private InputStream in;
    private PrintStream out;

    public FlexEngineConsole(String consoleUser) {
        this(consoleUser, System.in, System.out);
    }

    public FlexEngineConsole(String consoleUser, InputStream in, PrintStream out) {
        this.consoleUser = consoleUser;
        this.waiting = false;
        this.sc = null;
        this.in = in;
        this.out = out;
    }

    public void waitForCommands() {
        if (!waiting) {
            tFlexEngineConsole = null;
            tFlexEngineConsole = new Thread(this, this.getClass().getSimpleName());
            tFlexEngineConsole.start();
        }
    }

    @Override
    public void run() {
        try {
            waiting = true;
            sc = new Scanner(in);
            while (waiting) {
                out.printf("%s@%s:~$: ", consoleUser, getClass().getSimpleName());
                execute(sc.nextLine());
            }
        } catch (Exception e) {
            log.error("run(): {}", e, e);
        }
    }

    public void stopWaiting() {
        try {
            waiting = false;
            if (Objects.nonNull(tFlexEngineConsole)) {
                tFlexEngineConsole.interrupt();
            }
            tFlexEngineConsole = null;
            if (Objects.nonNull(sc)) {
                sc.close();
            }
            sc = null;
        } catch (Exception e) {
            log.error("Fail to stop console!", e);
        }
    }

    public abstract void execute(String cmd);

}
