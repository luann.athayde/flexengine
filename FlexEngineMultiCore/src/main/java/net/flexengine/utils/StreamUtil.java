package net.flexengine.utils;

import lombok.extern.slf4j.Slf4j;

import java.io.*;
import java.util.Optional;

/**
 * Classe utilitaria para obter copia de streams.
 */
@Slf4j
public class StreamUtil {

    public static final String TMP_FILE_PREFIX = "file_";

    public static final int DEFAULT_BUFFER_SIZE = 100 * 1024; // 100KB
    public static final int BUFFER_1MB = 1024 * 1024; // 2MB
    public static final int BUFFER_2MB = 1024 * 1024 * 2; // 2MB
    public static final int BUFFER_5MB = 1024 * 1024 * 5; // 5MB
    public static final int BUFFER_8MB = 1024 * 1024 * 8; // 8MB
    public static final int BUFFER_10MB = 1024 * 1024 * 10; // 10MB

    private StreamUtil() {
        throw new IllegalStateException("StreamUtil - Classe utilitaria.");
    }

    /**
     * @param is
     * @return
     */
    public static InputStream executarOuResetar(InputStream is) {
        try {
            is.reset();
        } catch (IOException e) {
            log.error("Falha ao reiniciar stream!", e);
        }
        return is;
    }

    /**
     * @param inputStream
     * @return
     */
    public static Optional<InputStream> obterCopia(InputStream inputStream) {
        try {
            long fileSize = 0;
            File tmpFile = File.createTempFile(TMP_FILE_PREFIX, ".dat");
            tmpFile.deleteOnExit();
            try (FileOutputStream tmpFileStream = new FileOutputStream(tmpFile)) {
                int read;
                byte[] buffer = new byte[DEFAULT_BUFFER_SIZE];
                do {
                    read = inputStream.read(buffer, 0, buffer.length);
                    if (read > 0) {
                        fileSize += read;
                        tmpFileStream.write(buffer, 0, read);
                    }
                } while (read > 0 && fileSize < Integer.MAX_VALUE);
                tmpFileStream.flush();
            }
            // ---
            return Optional.of(
                    new BufferedInputStream(new FileInputStream(tmpFile), DEFAULT_BUFFER_SIZE)
            );
            // ---
        } catch (IOException e) {
            log.warn("Falha ao copiar stream de dados!", e);
            return Optional.empty();
        }
    }

}
