package net.flexengine.utils;

import java.util.concurrent.atomic.AtomicBoolean;

/**
 *
 */
public class AsyncTask {

    private final Long id;
    private final String name;
    private final AtomicBoolean running;

    private AsyncTask(Long id, String name) {
        this.id = id;
        this.name = name;
        this.running = new AtomicBoolean(Boolean.FALSE);
    }

    public String getName() {
        return name;
    }

    public Long getId() {
        return id;
    }

    public synchronized Boolean getRunning() {
        return running.get();
    }

    public synchronized void setRunning(Boolean running) {
        this.running.set(running);
    }

    public AsyncTask run(Runnable runnable) {
        return this.run(runnable, false, 1);
    }

    public AsyncTask run(Runnable runnable, boolean continuous) {
        return this.run(runnable, continuous, 1);
    }

    public AsyncTask run(Runnable runnable, boolean continuous, int ups) {
        Thread asyncThread = new Thread(ASYNC_TASK_GROUP, () -> {
            synchronized (running) {

                do {

                    try {
                        long initTime = System.currentTimeMillis();

                        runnable.run();

                        if (continuous) {
                            long sleepTime = 1000 / ups;
                            long diffTime = System.currentTimeMillis() - initTime;
                            sleepTime = sleepTime - diffTime;
                            if (sleepTime > 5)
                                running.wait(sleepTime);
                            else
                                running.wait(5);
                        }

                    } catch (InterruptedException | IllegalMonitorStateException e) {
                        Thread.yield();
                    }

                } while (running.get() && continuous);

            }
        }, getName());
        asyncThread.setDaemon(true);
        asyncThread.start();
        // ---
        return this;
    }

    // AsyncTask
    private static long ASYNC_TASK_ID = 0L;
    private static final ThreadGroup ASYNC_TASK_GROUP = new ThreadGroup("asyncTaskGroup");

    public static AsyncTask newInstance() {
        return newInstance(++ASYNC_TASK_ID);
    }

    public static AsyncTask newInstance(Long id) {
        AsyncTask task = new AsyncTask(id, "asyncTask-" + id);
        task.setRunning(Boolean.TRUE);
        return task;
    }

}
