package net.flexengine.utils;

import net.flexengine.model.components.Button;
import net.flexengine.model.components.ComponentStyle;

import java.awt.*;

public class StylesUtil {

    public static final ComponentStyle STYLE_FALL_WINTER;
    public static final ComponentStyle STYLE_FALL_WINTER_ALT;

    public static final ComponentStyle STYLE_SOLARIN;
    public static final ComponentStyle STYLE_SOLARIN_ALT;

    static {
        STYLE_FALL_WINTER = ComponentStyle.builder()
                .primaryForeground(Color.decode("#5d5c61"))
                .secondaryForeground(Color.decode("#938e94"))
                .primaryBackground(Color.decode("#7395ae"))
                .secondaryBackground(Color.decode("#557a95"))
                .terciary(Color.decode("#b0a295"))
                .alpha(0.8f)
                .build();

        STYLE_FALL_WINTER_ALT = ComponentStyle.builder()
                .primaryForeground(Color.decode("#938e94"))
                .secondaryForeground(Color.decode("#5d5c61"))
                .primaryBackground(Color.decode("#557a95"))
                .secondaryBackground(Color.decode("#7395ae"))
                .terciary(Color.decode("#b0a295"))
                .alpha(0.8f)
                .build();

        STYLE_SOLARIN = ComponentStyle.builder()
                .primaryForeground(Color.decode("#2c3531"))
                .secondaryForeground(Color.decode("#116466"))
                .primaryBackground(Color.decode("#d9b08c"))
                .secondaryBackground(Color.decode("#ffcb9a"))
                .terciary(Color.decode("#d1e8e2"))
                .alpha(0.8f)
                .build();

        STYLE_SOLARIN_ALT = ComponentStyle.builder()
                .primaryForeground(Color.decode("#ffcb9a"))
                .secondaryForeground(Color.decode("#d9b08c"))
                .primaryBackground(Color.decode("#2c3531"))
                .secondaryBackground(Color.decode("#116466"))
                .terciary(Color.decode("#d1e8e2"))
                .alpha(0.8f)
                .build();
    }

    private StylesUtil() {
        throw new IllegalStateException("Util class.");
    }

    public static void aplyStyle(Button button, ComponentStyle style) {

        button.setForegroundColor(style.getPrimaryForeground());
        button.setForegroundFocusColor(style.getSecondaryForeground());

        button.setBackgroundColor(style.getPrimaryBackground());
        button.setBackgroundFocusColor(style.getSecondaryBackground());

        float alpha = style.getAlpha();
        alpha = Math.max( alpha, 0.0f );
        alpha = Math.min( alpha, 1.0f );
        button.setAlpha( alpha );
    }

}
