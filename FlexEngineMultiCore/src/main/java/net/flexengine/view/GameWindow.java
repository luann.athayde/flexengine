/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.flexengine.view;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import net.flexengine.controller.ConfigurationController;
import net.flexengine.controller.InputController;
import net.flexengine.controller.RenderController;
import net.flexengine.model.config.FlexDisplayMode;

import javax.swing.*;
import java.awt.*;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Stream;

/**
 * @author Luann R. Athayde
 * @version 1.0
 * @since 1.0
 */
@Data
@Slf4j
@Builder
@AllArgsConstructor
public class GameWindow extends JFrame {

    public static final int DEFAULT_WINDOW_ID = 0x00001000;

    protected int windowID;
    protected Insets insets;
    protected boolean fullscreen;

    protected GameWindow() {
        this(new Insets(0, 0, 0, 0), false);
    }

    protected GameWindow(Insets insets, boolean fullscreen) {
        super("FlexEngine");
        this.windowID = DEFAULT_WINDOW_ID;
        this.insets = insets;
        this.fullscreen = fullscreen;
        this.init();
    }

    protected void init() {
        removeAll();
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        setBackground(Color.BLACK);
        setFocusTraversalKeysEnabled(false);
        setFocusCycleRoot(false);
        addKeyListener(InputController.getInstance());
        addMouseListener(InputController.getInstance());
        addMouseMotionListener(InputController.getInstance());
        addMouseWheelListener(InputController.getInstance());
        addWindowListener(InputController.getInstance());
    }

    @Override
    public String getName() {
        return super.getName();
    }

    @Override
    public String getTitle() {
        return super.getTitle();
    }

    @Override
    public void setName(String name) {
        super.setTitle(name);
        super.setName(name);
    }

    @Override
    public void setTitle(String name) {
        this.setName(name);
    }

    @Override
    public void setSize(int width, int height) {
        super.setSize(width, height);
        setLocationRelativeTo(null);
    }

    public void setPreferredSize(int width, int height) {
        if (width > 0 && height > 0) {
            this.setPreferredSize(new Dimension(width, height));
        }
    }

    @Override
    public void setPreferredSize(Dimension preferredSize) {
        super.setPreferredSize(preferredSize);
        setLocationRelativeTo(null);
    }

    public void setIconImage(Texture texture) {
        if (Objects.nonNull(texture)) {
            this.setIconImage(texture.getImage());
        }
    }

    public void setFullscreen(boolean fullscreen) {
        this.fullscreen = fullscreen;
        dispose();
        setUndecorated(this.fullscreen);
        if (fullscreen) {
            GraphicsDevice gd = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
            Optional<DisplayMode> betterDM = findBetterDisplayMode(gd);
            // ---
            if (gd.isFullScreenSupported()) {
                log.info("Changing to fullscreen...");
                gd.setFullScreenWindow(this);
                DisplayMode dm = betterDM.orElse(new DisplayMode(1280, 720, -1, 60));
                if (gd.isDisplayChangeSupported()) {
                    FlexDisplayMode fDM = ConfigurationController.getInstance()
                            .getDefaultConfiguration().getResolution();
                    log.info("Creating DisplayMode using custom settings = {}", fDM);
                    dm = new DisplayMode(fDM.getWidth(), fDM.getHeight(), fDM.getBitDepth(), fDM.getRefreshRate());
                }
                log.info("Changing display mode = {}", dm);
                gd.setDisplayMode(dm);
            } else {
                this.fullscreen = false;
                setUndecorated(false);
                setLocationRelativeTo(null);
                setVisible(true);
            }
            // ---
        } else {
            setLocationRelativeTo(null);
            setVisible(true);
        }
        // ---
        init();
    }

    public Optional<DisplayMode> findBetterDisplayMode(GraphicsDevice gd) {
        return Stream.of(gd.getDisplayModes())
                .filter(dm -> dm.getBitDepth() >= 24 && dm.getRefreshRate() == 60 &&
                        dm.getWidth() >= 1280 && dm.getHeight() >= 720)
                .min((dm1, dm2) -> {
                    if (dm2.getWidth() > dm1.getWidth() || dm2.getHeight() > dm1.getHeight() ||
                            dm2.getBitDepth() > dm1.getBitDepth() ||
                            dm2.getRefreshRate() > dm1.getRefreshRate()) {
                        return 1;
                    } else if (dm2.getWidth() < dm1.getWidth() || dm2.getHeight() < dm1.getHeight() ||
                            dm2.getBitDepth() < dm1.getBitDepth() ||
                            dm2.getRefreshRate() < dm1.getRefreshRate()) {
                        return -1;
                    }
                    return 0;
                });
    }

    @Override
    public Insets getInsets() {
        Insets windowInsets = super.getInsets();
        if (Objects.nonNull(windowInsets)) {
            return windowInsets;
        }
        return insets;
    }

    public Dimension getViewport() {
        return new Dimension(
                getViewportWidth(),
                getViewportHeight()
        );
    }

    public int getViewportWidth() {
        return RenderController.getInstance().getViewportWidth();
    }

    public int getViewportHeight() {
        return RenderController.getInstance().getViewportHeight();
    }

    /**
     * Take a screen host of the view game...
     */
    public static void captureScreenShot() {
        RenderController.getInstance().captureScreenshot();
    }

    @Override
    public boolean equals(Object o) {
        if (Objects.nonNull(o) && o instanceof GameWindow) {
            GameWindow that = (GameWindow) o;
            return windowID == that.windowID;
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(windowID);
    }

    public String toString() {
        return getClass().getSimpleName() + "[name=" + getName() + ", ID=" + getWindowID() + "]";
    }

    private static GameWindow instance = null;

    public static GameWindow getInstance() {
        if (Objects.isNull(instance)) {
            instance = new GameWindow();
        }
        return instance;
    }

}
