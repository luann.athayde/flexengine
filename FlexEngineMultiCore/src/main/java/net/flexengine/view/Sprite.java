/**
 * Sprite class...
 */
package net.flexengine.view;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import net.flexengine.controller.Finalizable;
import net.flexengine.controller.Initializable;
import net.flexengine.controller.Renderable;
import net.flexengine.controller.Updatable;
import net.flexengine.model.scene.SceneBundle;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;
import org.simpleframework.xml.Transient;

import java.awt.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Luann R. Athayde
 * @version 1.0
 * @since 1.0
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Root(name = "sprite")
public class Sprite implements Updatable, Renderable, Initializable, Finalizable {

    @ElementList(name = "frames", empty = false)
    private final List<SpriteFrame> frames = new ArrayList<>();
    @Element(name = "visible", required = false)
    private boolean visible;

    @Transient
    private int frameIndex;
    @Transient
    private int x;
    @Transient
    private int y;
    @Transient
    private double plusFrameTime;

    @Override
    public void init(SceneBundle bundle) {
        Double increaseFrameTime = bundle.getExtra("increaseFrameTime");
        if (increaseFrameTime != null) {
            plusFrameTime = increaseFrameTime;
        } else {
            plusFrameTime = 1d;
        }
    }

    @Override
    public void update() {
        SpriteFrame spriteFrame = frames.get(frameIndex);
        spriteFrame.currentFrameTime += plusFrameTime;
        if (spriteFrame.currentFrameTime >= spriteFrame.frameTime) {
            spriteFrame.currentFrameTime = 0d;
            frameIndex++;
        }
        if (frameIndex >= frames.size()) {
            frameIndex = 0;
        }
    }

    @Override
    public void render(Graphics2D graphics) {
        if (visible && frameIndex >= 0 && frameIndex < frames.size()) {
            SpriteFrame spriteFrame = frames.get(frameIndex);
            spriteFrame.getTexture().render(graphics, x, y);
        }
    }

    public void render(Graphics2D graphics, int x, int y) {
        this.x = x;
        this.y = y;
        render(graphics);
    }

    @Override
    public void end(SceneBundle bundle) {

    }

    @Data
    @Root(name = "spriteFrame")
    public static class SpriteFrame implements Serializable {
        @Element
        protected Texture texture;
        @Element
        protected Double frameTime;
        @Transient
        protected Double currentFrameTime;
    }

}
