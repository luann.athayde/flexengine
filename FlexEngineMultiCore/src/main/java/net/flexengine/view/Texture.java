/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.flexengine.view;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;
import org.simpleframework.xml.Transient;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.Serializable;
import java.util.Objects;

/**
 * @author Luann R. Athayde
 * @version 1.0
 * @since 1.0
 */
@Data
@Slf4j
@Builder
@AllArgsConstructor
@Root(name = "texture")
public class Texture implements Serializable {

    @Attribute
    protected String name;
    @Attribute
    protected String file;
    @Transient
    protected BufferedImage image;
    @Transient
    protected boolean loaded;

    public Texture() {
        this.name = "Texture";
        this.file = null;
        this.image = new BufferedImage(
                GameWindow.getInstance().getViewportWidth(),
                GameWindow.getInstance().getViewportHeight(),
                BufferedImage.TYPE_INT_ARGB
        );
        this.loaded = false;
    }

    public Texture(String name, int width, int height) {
        this.name = name;
        this.file = null;
        this.image = new BufferedImage(
                width > 0 ? width : 1,
                height > 0 ? height : 1,
                BufferedImage.TYPE_INT_ARGB
        );
        this.loaded = true;
    }

    public Texture(String name, String file) {
        this.name = name;
        this.file = file;
        this.image = null;
        this.loaded = false;
        load();
    }

    public Texture(String name, InputStream stream) {
        this.name = name;
        this.file = null;
        try {
            image = ImageIO.read(stream);
            loaded = true;
        } catch (Exception e) {
            loaded = false;
        }
    }

    public final void load() {
        if (!loaded) {
            loadImage();
        }
    }

    public void loadImage() {
        try {
            FileInputStream stream = new FileInputStream(file);
            image = ImageIO.read(stream);
            loaded = true;
        } catch (Exception e) {
            loaded = false;
        }
    }

    public int getWidth() {
        if (Objects.nonNull(image)) {
            return image.getWidth();
        }
        return 0;
    }

    public int getHeight() {
        if (Objects.nonNull(image)) {
            return image.getHeight();
        }
        return 0;
    }

    public void render(Graphics2D graphics, int x, int y) {
        render(graphics, x, y, GameWindow.getInstance());
    }

    public void render(Graphics2D graphics, int x, int y, ImageObserver observer) {
        if (isLoaded()) {
            graphics.drawImage(image, x, y, observer);
        }
    }

    public Texture rotate180() {
        return rotate(180);
    }

    public Texture rotate90() {
        return rotate(90);
    }

    public Texture rotate(double angle) {
        int width = angle % 90 == 0 ? image.getWidth(null) : image.getHeight(null);
        int height = angle % 90 == 0 ? image.getHeight(null) : image.getWidth(null);
        BufferedImage rotatedImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
        // --
        Graphics2D gRotate = (Graphics2D) rotatedImage.getGraphics();
        // --
        AffineTransform at = new AffineTransform();
        at.rotate(angle * Math.PI / angle);
        gRotate.setTransform(at);
        gRotate.translate(-image.getWidth(null), -image.getHeight(null));
        gRotate.drawImage(image, 0, 0, null);
        gRotate.dispose();
        // --
        Texture rotatedTexture = new Texture();
        rotatedTexture.name = this.name;
        rotatedTexture.file = null;
        rotatedTexture.loaded = true;
        rotatedTexture.image = rotatedImage;
        return rotatedTexture;
    }

    public Texture scale(int width, int height) {
        Image tmp = image.getScaledInstance(width, height, BufferedImage.SCALE_SMOOTH);
        BufferedImage scaledImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
        Graphics g = scaledImage.getGraphics();
        g.drawImage(tmp, 0, 0, null);
        // --
        Texture scaledTexture = new Texture();
        scaledTexture.name = "(scaled)" + this.name;
        scaledTexture.file = null;
        scaledTexture.loaded = true;
        scaledTexture.image = scaledImage;
        // --
        return scaledTexture;
    }

    public Texture getSubTexture(int x, int y, int width, int height) {
        try {
            Texture subTexture = new Texture();
            subTexture.name = this.name;
            subTexture.file = null;
            subTexture.image = image.getSubimage(x, y, width, height);
            return subTexture;
        } catch (Exception e) {
            log.error("Fail to get subtexture!", e);
        }
        return this;
    }

    @Override
    public int hashCode() {
        int hash = Objects.hashCode(this.image);
        hash += 13 * (this.loaded ? 1 : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (Objects.nonNull(obj) && obj instanceof Texture) {
            Texture other = (Texture) obj;
            return other.hashCode() == this.hashCode();
        }
        return false;
    }

    @Override
    public String toString() {
        if (Objects.nonNull(file)) {
            return name + "[" + file + ", loaded=" + loaded + "]@" + hashCode();
        }
        return name + "[loaded=" + loaded + "]@" + hashCode();
    }

}
