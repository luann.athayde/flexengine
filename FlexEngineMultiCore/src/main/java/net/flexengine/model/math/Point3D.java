/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.flexengine.model.math;

/**
 * @author Luann R. Athayde
 * @version 1.0
 * @since 1.0
 */
public class Point3D {
    
    private double x;
    private double y;
    private double z;

    public Point3D() {
        this(0f,0f,0f);
    }

    public Point3D(Point3D p) {
        if( p != null ) {
            this.x = p.x;
            this.y = p.y;
            this.z = p.z;
        }
        else {
            this.x = 0;
            this.y = 0;
            this.z = 0;
        }
    }

    public Point3D(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }
    
    public void move(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public void setLocation(Point3D p) {
        if( p == null ) return;
        x = p.x;
        y = p.y;
        z = p.z;
    }
    public void setLocation(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }
    public Point3D getLocation() {
        return new Point3D(x, y, z);
    }

    public void translate(double dx, double dy) {
        x += dx;
        y += dy;
    }
    
    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double getZ() {
        return z;
    }
    
    public double distanceSq(double x1, double y1, double z1, double x2, double y2, double z2) {
        x1 -= x2;
        y1 -= y2;
        z1 -= z2;
        return (x1 * x1 + y1 * y1 + z1 * z1);
    }
    public double distanceSq(double x, double y, double z) {
        return distanceSq(this.x, this.y, this.z, x, y, z);
    }

    public double distance(double x1, double y1, double z1, double x2, double y2, double z2) {
        x1 -= x2;
        y1 -= y2;
        z1 -= z2;
        return (double) Math.sqrt(x1 * x1 + y1 * y1 + z1 * z1);
    }
    
    public double distance(Point3D other) {
        return distance(other.x, other.y, other.z);
    }
    
    public double distance(double x, double y, double z) {
        return distance(this.x, this.y, this.z, x, y, z);
    }
    
    @Override
    protected Point3D clone() {
        return new Point3D(x, y, z);
    }
    
    @Override
    public boolean equals(Object obj) {
        if( obj != null && obj instanceof Point3D ) {
            Point3D tmp = (Point3D) obj;
            if( tmp.x == this.x && tmp.y == this.y && tmp.z == this.z )
                return true;
        }
        return false;
    }

    public boolean equals(Point3D p) {
        if( p != null ) {
            if(this.x == p.x && this.y == p.y && this.z == p.z) {
                return true;
            }
        }
        return false;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 23 * hash + (int) (Double.doubleToLongBits(this.x) ^ (Double.doubleToLongBits(this.x) >>> 32));
        hash = 23 * hash + (int) (Double.doubleToLongBits(this.y) ^ (Double.doubleToLongBits(this.y) >>> 32));
        hash = 23 * hash + (int) (Double.doubleToLongBits(this.z) ^ (Double.doubleToLongBits(this.z) >>> 32));
        return hash;
    }
    
    @Override
    public String toString() {
        return "Point3D("+x+","+y+","+z+")";
    }
}
