/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.flexengine.model.math;

/**
 * @author Luann R. Athayde
 * @version 1.0
 * @since 1.0
 */
public class Sphere extends Form3D {
    
    private double radius;

    public Sphere() {
        this("Sphere", 0f, 0f, 0f, 0f);
    }

    public Sphere(String name) {
        this(name, 0f, 0f, 0f, 0f);
    }

    public Sphere(String name, double x, double y, double z, double radius) {
        super(name, x, y, z);
        this.radius = radius;
    }
    
    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }
    
    @Override
    public double getArea() {
        return (double)(4 * Math.PI * radius * radius);
    }
    
    @Override
    public double getVolume() {
        return (double)(4/3 * Math.PI * radius * radius * radius);
    }

    @Override
    public String toString() {
        return getName()+"{" + "x=" + getX() + ", y=" + getY() + ", z=" + getZ() + ", radius=" + radius + '}';
    }
    
}
