/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.flexengine.model.math;

/**
 * @author Luann R. Athayde
 * @version 1.0
 * @since 1.0
 */
public abstract class Form3D extends Form {
    
    private Point3D location;
    
    public Form3D() {
        this("Form3D", 0d, 0d, 0d);
    }

    public Form3D(String name) {
        this(name, 0d, 0d, 0d);
    }

    public Form3D(String name, double x, double y, double z) {
        this(name, new Point3D(x, y, z));
    }

    public Form3D(String name, Point3D location) {
        super(name);
        this.location = location;
    }
    
    public Point3D getLocation() {
        return location;
    }

    public void setLocation(Point3D location) {
        this.location = location;
    }

    public double getX() {
        return getLocation().getX();
    }

    public double getY() {
        return getLocation().getY();
    }
    
    public double getZ() {
        return getLocation().getZ();
    }
    
    public abstract double getArea();
    public abstract double getVolume();
    
}
