/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.flexengine.model.math;

/**
 * @author Luann R. Athayde
 * @version 1.0
 * @since 1.0
 */
public class Vector3D {
    
    private Point3D pointA;
    private Point3D pointB;

    public Vector3D() {
        this(new Point3D(0, 0, 0),new Point3D(0, 0, 0));
    }

    public Vector3D(Point3D pointA, Point3D pointB) {
        this.pointA = pointA;
        this.pointB = pointB;
    }

    public Point3D getPointA() {
        return pointA;
    }

    public Point3D getPointB() {
        return pointB;
    }

    public void setPointA(Point3D pointA) {
        this.pointA = pointA;
    }

    public void setPointB(Point3D pointB) {
        this.pointB = pointB;
    }

    @Override
    public String toString() {
        return "Vector3D{"+pointA+","+pointB+"}";
    }
    
}
