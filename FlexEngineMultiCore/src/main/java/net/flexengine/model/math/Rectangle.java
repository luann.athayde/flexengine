/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.flexengine.model.math;

/**
 * @author Luann R. Athayde
 * @version 1.0
 * @since 1.0
 */
public class Rectangle extends Form2D {
    
    private double width;
    private double height;

    public Rectangle() {
        this("Rectangle", 0f, 0f, 0f, 0f);
    }
    public Rectangle(String name) {
        this(name, 0f, 0f, 0f, 0f);
    }
    public Rectangle(String name, double x, double y, double width, double height) {
        super(name, x, y);
        this.width = width;
        this.height = height;
    }
    
    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    @Override
    public double getArea() {
        return (width * height);
    }
    
    @Override
    public String toString() {
        return getName()+"{" + "x=" + getX() + ", y=" + getY() + ", width=" + width + ", height=" + height + '}';
    }   
    
}
