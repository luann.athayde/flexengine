/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.flexengine.model.math;

/**
 * @author Luann R. Athayde
 * @version 1.0
 * @since 1.0
 */
public class Cube extends Form3D {
    
    private double size;

    public Cube() {
        this("Cube", 0d, 0d, 0d, 0d);
    }

    public Cube(String nome) {
        this(nome, 0d, 0d, 0d, 0d);
    }

    public Cube(String name, double x, double y, double z, double size) {
        super(name, x, y, z);
        this.size = size;
    }

    public double getSize() {
        return size;
    }

    public void setSize(double size) {
        this.size = size;
    }
    
    @Override
    public double getArea() {
        return 6f * (size*size);
    }
    
    @Override
    public double getVolume() {
        return (size * size * size);
    }

    @Override
    public String toString() {
        return getName()+"{" + "x=" + getX() + ", y=" + getY() + ", z=" + getZ() + ", size=" + size + '}';
    }
    
}
