/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.flexengine.model.math;

/**
 * @author Luann R. Athayde
 * @version 1.0
 * @since 1.0
 */
public class Cylinder extends Form3D {
    
    private double radius;
    private double height;

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    @Override
    public double getVolume() {
        return (double)(Math.PI * radius * radius * height);
    }
    @Override
    public double getArea() {
        return getTotalArea();
    }
    
    public double getBaseArea() {
        return (double) (Math.PI * radius * radius);
    }
    public double getSideArea() {
        return (double) (2f * Math.PI * radius * height);
    }
    /**
     * Same that getArea...
     * @return 
     */
    public double getTotalArea() {
        return (2f*getBaseArea()+getSideArea());
    }
    
    
    @Override
    public String toString() {
        return getName()+"{" + "x=" + 
                getX() + ", y=" + getY() + ", z=" + getZ() + ", radius=" + radius + ", height=" + height + '}';
    }   
    
}
