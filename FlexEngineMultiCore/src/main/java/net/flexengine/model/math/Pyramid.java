/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.flexengine.model.math;

/**
 * @author Luann R. Athayde
 * @version 1.0
 * @since 1.0
 */
public class Pyramid extends Form3D {
    
    private Floor3D     base;

    public Pyramid(String name, double x, double y, double z, Floor3D base) {
        super(name, x, y, z);
        this.base = base;
    }
    public Pyramid(String name, Floor3D base) {
        this(name, 0f, 0f, 0f, base);
    }
    public Pyramid(Floor3D base) {
        this("Pyramid", base);
    }
    
    public Floor3D getBase() {
        return base;
    }

    public void setBase(Floor3D base) {
        this.base = base;
    }
    
    public double getHeight() {
        Point3D center = base.getCenter();
        double h = getY() - center.getY();
        return Math.abs(h);
    }
    
    @Override
    public double getVolume() {
        double v = base.getArea() * getHeight() / 3;
        return v;
    }

    @Override
    public double getArea() {
        return 0f;
    }
    
}
