/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.flexengine.model.math;

/**
 * @author Luann R. Athayde
 * @version 1.0
 * @since 1.0
 */
public class Floor3D extends Form3D {
    
    private double   width;
    private double   depth;

    public Floor3D() {
        this("Floor3D");
    }

    public Floor3D(String nome) {
        this(nome, 0f, 0f, 0f, 100f, 100f);
    }

    public Floor3D(String name, double x, double y, double z, double width, double depth) {
        super(name, x, y, z);
        this.width = width;
        this.depth = depth;
    }
    
    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getDepth() {
        return depth;
    }

    public void setDepth(double depth) {
        this.depth = depth;
    }
    
    @Override
    public double getArea() {
        return width*depth;
    }

    @Override
    public double getVolume() {
        return 0f;
    }
    
    public Point3D getCenter() {
        return new Point3D(getX(), getY(), getZ());
    }
    
    public Point3D getPointA() {
        return new Point3D(getX()-width/2, getY(), getZ()+depth/2);
    }
    public Point3D getPointB() {
        return new Point3D(getX()+width/2, getY(), getZ()+depth/2);
    }
    public Point3D getPointC() {
        return new Point3D(getX()-width/2, getY(), getZ()-depth/2);
    }
    public Point3D getPointD() {
        return new Point3D(getX()+width/2, getY(), getZ()-depth/2);
    }
    
    public Point3D[] getPoints() {
        Point3D points[] = new Point3D[4];
        points[0] = getPointA();
        points[1] = getPointB();
        points[2] = getPointC();
        points[3] = getPointD();
        return points;
    }

    @Override
    public String toString() {
        return getName()+"{" + "origin= [" + getX() + ","+getY()+","+getZ()+"], width=" + width + ", depth=" + depth + '}';
    }
    
}
