/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.flexengine.model.math;

/**
 * @author Luann R. Athayde
 * @version 1.0
 * @since 1.0
 */
public class Triangle extends Form2D {
    
    private Point2D pointB;
    private Point2D pointC;

    public Triangle() {
        this("Triangle", new Point2D(0, 0), new Point2D(0, 0), new Point2D(0, 0));
    }

    public Triangle(String name) {
        this(name, new Point2D(0, 0), new Point2D(0, 0), new Point2D(0, 0));
    }

    public Triangle(String name, Point2D pointA, Point2D pointB, Point2D pointC) {
        super(name, pointA.getX(), pointA.getY());
        this.pointB = pointB;
        this.pointC = pointC;
    }
    
    public void setPointA(Point2D p) {
        this.setLocation(p);
    }
    public void setPointB(Point2D p) {
        pointB = p;
    }
    public void setPointC(Point2D p) {
        pointC = p;
    }

    public Point2D getPointA() {
        return new Point2D(getX(), getY());
    }
    
    public Point2D getPointB() {
        return pointB;
    }

    public Point2D getPointC() {
        return pointC;
    }
    
    @Override
    public double getArea() {
        double h = getPointA().distance(new Vector2D(getPointB(), getPointC()));
        return getPointB().distance(getPointC()) * h;
    }

    @Override
    public String toString() {
        return getName()+"{points="+getPointA()+","+getPointB()+","+getPointC()+"}";
    }
    
}
