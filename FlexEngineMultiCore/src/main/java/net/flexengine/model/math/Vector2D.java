/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.flexengine.model.math;

/**
 * @author Luann R. Athayde
 * @version 1.0
 * @since 1.0
 */
public class Vector2D {
    
    private Point2D pointA;
    private Point2D pointB;

    public Vector2D() {
        this(0f,0f,0f,0f);
    }

    public Vector2D(Point2D pointA, Point2D pointB) {
        this.pointA = pointA;
        this.pointB = pointB;
    }
    
    public Vector2D(double ax, double ay, double bx, double by) {
        this.pointA = new Point2D(ax, ay);
        this.pointB = new Point2D(bx, by);
    }
    
    public void invertX() {
        pointB.move(-pointB.getX(), pointB.getY());
    }
    public void invertY() {
        pointB.move(pointB.getX(), -pointB.getY());
    }
    public void invert() {
        pointB.move(-pointB.getX(), -pointB.getY());
    }
    
    public double getLength() {
        double dx = pointA.getX()-pointB.getX();
        double dy = pointA.getX()-pointB.getX();
        return (double) Math.sqrt(dx * dx + dy * dy);
    }
    
    public void normalize() {
        double l = getLength();
        pointA.setLocation(pointA.getX()/l, pointA.getY()/l);
    }
    
    public void scale(double s) {
        pointA.setLocation(pointA.getX()*s, pointA.getY()*s);
    }
    
    public void scale(double dx, double dy) {
        pointA.setLocation(pointA.getX()*dx, pointA.getY()*dy);
    }
    
    public void translate(double dx, double dy) {
        pointA.setLocation(pointA.getX()*+dx, pointA.getY()*+dy);
    }
    
    public void add(Vector2D v) {
        pointB.setLocation(pointB.getX()+v.getX(), pointB.getY()+v.getY());
    }
    
    public void addX(double x) {
        pointB.setLocation(pointB.getX()+x, pointB.getY());
    }
    public void addY(double y) {
        pointB.setLocation(pointB.getX(), pointB.getY()+y);
    }

    public void setX(double x) {
        pointA.setLocation(x, pointA.getY());
    }

    public void setY(double y) {
        pointA.setLocation(pointA.getX(), y);
    }
    
    public void set(double x,double y) {
        pointA.setLocation(x, y);
    }

    public Point2D getPointA() {
        return pointA;
    }

    public Point2D getPointB() {
        return pointB;
    }

    public void setPointA(Point2D pointA) {
        this.pointA = pointA;
    }

    public void setPointB(Point2D pointB) {
        this.pointB = pointB;
    }
    
    public double getX() {
        return pointB.getX() - pointA.getX();
    }
    public double getY() {
        return pointA.getY() - pointB.getY();
    }
    
    public double dotProduct(Vector2D other) {
        return dotProduct(this, other);
    }
    public Vector2D crossProduct(Vector2D other) {
        return crossProduct(this, other);
    }
    
    public static double dotProduct(Vector2D v1, Vector2D v2) {
        return v1.getX()*v2.getX() + v1.getY()*v2.getY();
    }
    
    public static Vector2D crossProduct(Vector2D v1, Vector2D v2) {
        return new Vector2D(
                v1.getPointA().getX()*v2.getPointA().getX(),
                v1.getPointA().getY()*v2.getPointA().getY(),
                v1.getPointB().getX()*v2.getPointB().getX(),
                v1.getPointB().getY()*v2.getPointB().getY() );
    }
    
    public double[] straightCoefficients() {
        double matrizD = getPointA().getX();
        double matrizE = getPointA().getY();
        double matrizG = getPointB().getX();
        double matrizH = getPointB().getY();
        // -- 
        double a = (matrizE) - (matrizH);
        double b = (matrizG) - (matrizD);
        double c = (matrizD * matrizH) - (matrizE * matrizG);
        if(c<0) {
            a *= -1;
            b *= -1;
            c *= -1;
        }
        return new double[] {a,b,c};
    }
    
    public String straightEquation() {
        double e[] = straightCoefficients();
        return "("+e[0]+"x) + ("+e[1]+"y) + ("+e[2]+") = 0";
    }
    
    @Override
    public String toString() {
        return "Vector2D("+pointA+","+pointB+")";
    }
}
