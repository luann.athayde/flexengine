/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.flexengine.model.math;

/**
 * @author Luann R. Athayde
 * @version 1.0
 * @since 1.0
 */
public class Circle extends Form2D {

    private double radius;

    public Circle(double x, double y, double radius) {
        super("Circle", x, y);
        this.radius = radius;
    }
    public Circle() {
        this(0,0,0);
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public double getDiameter() {
        return radius * 2;
    }
    
    @Override
    public double getArea() {
        return (double)(Math.PI * (radius*radius));
    }
    
    @Override
    public String toString() {
        return getName()+"{" + "x=" + getX() + ", y=" + getY() + ", radius=" + radius + '}';
    }
    
}
