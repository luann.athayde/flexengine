/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.flexengine.model.math;

/**
 * @author Luann R. Athayde
 * @version 1.0
 * @since 1.0
 */
public class Point2D extends Form {

    protected volatile double x;
    protected volatile double y;

    public Point2D(double x, double y) {
        super("Point2D");
        this.x = x;
        this.y = y;
    }

    public Point2D(Point2D p) {
        super("Point2D");
        if (p != null) {
            this.x = p.x;
            this.y = p.y;
        } else {
            this.x = 0;
            this.y = 0;
        }
    }

    public Point2D() {
        this(0f, 0f);
    }

    public void setLocation(Point2D p) {
        if (p == null) {
            return;
        }
        x = p.x;
        y = p.y;
    }

    public Point2D setLocation(double x, double y) {
        this.x = x;
        this.y = y;
        return this;
    }

    public Point2D getLocation() {
        return new Point2D(x, y);
    }

    public Point2D translate(double dx, double dy) {
        x += dx;
        y += dy;
        return this;
    }

    public Point2D move(double x, double y) {
        this.x = x;
        this.y = y;
        return this;
    }

    public double getX() {
        return this.x;
    }

    public double getY() {
        return this.y;
    }

    public void setX(double x) {
        this.x = x;
    }

    public void setY(double y) {
        this.y = y;
    }

    public void addX(double x) {
        this.x += x;
    }

    public void addY(double y) {
        this.y += y;
    }

    public <T extends Number> T getX(Class<T> c) {
        try {
            if (c == Integer.class) {
                return c.cast((int) x);
            }
            if (c == Float.class) {
                return c.cast((float) x);
            }
            return c.cast(x);
        } catch (Exception e) {}
        return null;
    }
    
    public <T extends Number> T getY(Class<T> c) {
        try {
            if (c == Integer.class) {
                return c.cast((int) y);
            }
            if (c == Float.class) {
                return c.cast((float) y);
            }
            return c.cast(y);
        } catch (Exception e) {}
        return null;
    }
    
    public double distanceSq(double x1, double y1, double x2, double y2) {
        x1 -= x2;
        y1 -= y2;
        return (x1 * x1 + y1 * y1);
    }

    public double distanceSq(double x, double y) {
        return distanceSq(this.x, this.y, x, y);
    }

    public double distance(double x1, double y1, double x2, double y2) {
        x1 -= x2;
        y1 -= y2;
        return (double) Math.sqrt(x1 * x1 + y1 * y1);
    }

    public double distance(double x, double y) {
        return distance(this.x, this.y, x, y);
    }

    public double distance(Point2D p) {
        return distance(p.getX(), p.getY());
    }

    /**
     * Gives the distance from this point to straight (v)...
     *
     * @param v
     * @return
     */
    public double distance(Vector2D v) {
        double coefficients[] = v.straightCoefficients();
        return (double) (Math.abs(coefficients[0] * getX() + coefficients[1] * getY() + coefficients[2])
                / Math.sqrt(Math.pow(coefficients[0], 2) + Math.pow(coefficients[1], 2)));
    }

    /**
     * Calcule the distance from [a] to [b]...
     *
     * @param a
     * @param b
     * @return
     */
    public double distance(Point2D a, Point2D b) {
        return distance(a.getX(), a.getY(), b.getX(), b.getY());
    }

    @Override
    protected Point2D clone() {
        return new Point2D(x, y);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj != null && obj instanceof Point2D) {
            Point2D tmp = (Point2D) obj;
            if (tmp.x == this.x && tmp.y == y) {
                return true;
            }
        }
        return false;
    }

    public boolean equals(Point2D p) {
        if (p != null) {
            if (this.x == p.x && this.y == p.y) {
                return true;
            }
        }
        return false;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 17 * hash + (int) (Double.doubleToLongBits(this.x) ^ (Double.doubleToLongBits(this.x) >>> 32));
        hash = 17 * hash + (int) (Double.doubleToLongBits(this.y) ^ (Double.doubleToLongBits(this.y) >>> 32));
        return hash;
    }

    @Override
    public String toString() {
        return getName() + "(" + x + "," + y + ")";
    }

}
