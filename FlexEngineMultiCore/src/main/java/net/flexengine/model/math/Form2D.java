/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.flexengine.model.math;

/**
 * @author Luann R. Athayde
 * @version 1.0
 * @since 1.0
 */
public abstract class Form2D extends Form {

    private Point2D location;

    public Form2D() {
        this("Form2D", 0, 0);
    }

    public Form2D(String name) {
        this(name, 0, 0);
    }

    public Form2D(String name, double x, double y) {
        this(name, new Point2D(x, y));
    }

    public Form2D(String name, Point2D location) {
        super(name);
        if (location != null) {
            this.location = location;
        } else {
            this.location = new Point2D();
        }
    }

    public Point2D getLocation() {
        return location;
    }

    public void setLocation(Point2D location) {
        if (location != null) {
            this.location = location;
        } else {
            this.location = new Point2D();
        }
    }

    public void setLocation(double x, double y) {
        getLocation().setLocation(x, y);
    }

    public double getX() {
        return getLocation().getX();
    }

    public double getY() {
        return getLocation().getY();
    }

    public abstract double getArea();

}
