/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.flexengine.model.math;

/**
 * @author Luann R. Athayde
 * @version 1.0
 * @since 1.0
 */
public class Hexagon extends Form2D {

    private double     side;
    private Point2D[] points;
    private int       hx[], hy[];
    
    public Hexagon(double side, double x, double y) {
        super("Hexagon", x, y);
        this.side = side;
        this.points = new Point2D[6];
        calculeHexagon();
    }

    public Hexagon(double side, Point2D location) {
        super("Hexagon", location);
    }

    public Hexagon(double side) {
        this(side, 0,0);
    }
    
    private void calculeHexagon() {
        points[0] = new Point2D(getX()-side,getY());
        points[1] = new Point2D(getX()-side/2, (int) (getY()+side*(Math.sqrt(3)/2)));
        points[2] = new Point2D(getX()+side/2, (int) (getY()+side*(Math.sqrt(3)/2)));
        points[3] = new Point2D(getX()+side,getY());
        points[4] = new Point2D(getX()+side/2, (int) (getY()-side*(Math.sqrt(3)/2)));
        points[5] = new Point2D(getX()-side/2, (int) (getY()-side*(Math.sqrt(3)/2)));
        
        hx = new int[6];
        hy = new int[6];
        
        for (int i = 0; i<6; i++)
        {
            hx[i] = (int)points[i].getX();
            hy[i] = (int)points[i].getY();
        }
    }

    @Override
    public double getArea() {
        return 0f;
    }

    public double getSide() {
        return side;
    }

    public void setSide(double side) {
        if( side>0 ) {
            this.side = side;
            calculeHexagon();
        }
    }

    @Override
    public void setLocation(Point2D location) {
        super.setLocation(location); //To change body of generated methods, choose Tools | Templates.
        calculeHexagon();
    }
    
    public void setLocation(double x, double y) {
        this.getLocation().setLocation(x, y);
        calculeHexagon();
    }
    
    public int[] getXPoints() {
        return hx;
    }
    
    public int[] getYPoints() {
        return hy;
    }
    
}
