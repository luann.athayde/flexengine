/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.flexengine.model.map;

import net.flexengine.model.map.model.Tile;

/**
 *
 * @author luann
 */
public class GameMapLayerRenderer {

    private int id;
    private int zOrder;
    
    private TileRenderer[][] tiles;

    public GameMapLayerRenderer() {
    }

    public GameMapLayerRenderer(int id, int zOrder, TileRenderer[][] tiles) {
        this.id = id;
        this.zOrder = zOrder;
        this.tiles = tiles;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getzOrder() {
        return zOrder;
    }

    public void setzOrder(int zOrder) {
        this.zOrder = zOrder;
    }

    public TileRenderer[][] getTiles() {
        return tiles;
    }

    public void setTiles(TileRenderer[][] tiles) {
        this.tiles = tiles;
    }

    public void setTile(TileRenderer tRender) {
        this.tiles[tRender.getTile().getGridX()][tRender.getTile().getGridY()] = tRender;
    }
    
    public void setTile(Tile tile, int gridX, int gridY) {
        this.tiles[gridX][gridY] = new TileRenderer(tile);
    }
    
    public TileRenderer getTile(int gridX, int gridY) {
        return tiles[gridX][gridY];
    }
    
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 83 * hash + this.id;
        hash = 83 * hash + this.zOrder;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final GameMapLayerRenderer other = (GameMapLayerRenderer) obj;
        if (this.id != other.id) {
            return false;
        }
        return this.zOrder == other.zOrder;
    }

    @Override
    public String toString() {
        return "GameMapLayerRenderer{" + "id=" + id + ", zOrder=" + zOrder + '}';
    }

}
