/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.flexengine.model.map;

/**
 *
 * @author Luann Athayde
 */
public class InvalidGameMapException extends RuntimeException {

    public InvalidGameMapException(Class root, Throwable cause) {
        super(root.getSimpleName()+"(): Invalid GameMap!", cause);
    }
    
}
