/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.flexengine.model.map.model;

import java.util.ArrayList;
import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;

/**
 *
 * @author luann
 */
public class Tile implements Comparable<Tile> {

    public static final int TILE_WIDTH = 40;
    public static final int TILE_HEIGHT = 40;

    @Attribute(required = true)
    protected String name;

    @Attribute(required = true)
    protected int gridX;

    @Attribute(required = true)
    protected int gridY;

    @Attribute(required = true)
    protected boolean visible;

    @Element(required = false)
    protected Object item;
    
    @Attribute(name = "global_item", required = false)
    private String globalItem;

    @ElementList(required = false, type = TileEvent.class)
    protected ArrayList<TileEvent> events;

    public Tile() {
        this(0, 0);
    }

    public Tile(int gridX, int gridY) {
        this.name = "Tile";
        this.gridX = gridX;
        this.gridY = gridY;
        this.visible = true;
        this.globalItem = null;
        this.item = null;
        this.events = null;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public int getGridX() {
        return gridX;
    }

    public void setGridX(int gridX) {
        this.gridX = gridX;
    }

    public int getGridY() {
        return gridY;
    }

    public void setGridY(int gridY) {
        this.gridY = gridY;
    }

    public ArrayList<TileEvent> getEvents() {
        if (events == null) {
            events = new ArrayList<>();
        }
        return events;
    }

    public void setEvents(ArrayList<TileEvent> events) {
        this.events = events;
    }

    public Object getItem() {
        return item;
    }

    public void setItem(Object item) {
        this.item = item;
    }

    public String getGlobalItem() {
        return globalItem;
    }

    public void setGlobalItem(String globalItem) {
        this.globalItem = globalItem;
    }

    public void setGrid(int gridX, int gridY) {
        setGridX(gridX);
        setGridY(gridY);
    }

    @Override
    public String toString() {
        return "Tile{" + "name=" + name + '}';
    }

    @Override
    public int compareTo(Tile t) {
        if (getGridY() != t.getGridY()) {
            return getGridY() - t.getGridY();
        }
        return getGridX() - t.getGridX();
    }

}
