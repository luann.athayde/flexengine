/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.flexengine.model.map;

import java.awt.Graphics2D;

import net.flexengine.model.map.model.Tile;
import net.flexengine.model.config.Priority;
import net.flexengine.controller.Renderable;
import net.flexengine.controller.Updatable;
import net.flexengine.model.math.Point2D;
import net.flexengine.view.Texture;

/**
 * @author Luann R. Athayde
 * @version 1.0
 * @since 1.0
 */
public class TileRenderer implements Renderable, Updatable {

    private final Point2D location;
    private Tile tile;

    public TileRenderer(Tile tile) {
        this.tile = tile;
        this.location = new Point2D();
    }

    @Override
    public void render(Graphics2D graphics) {
        int x = location.getX(Integer.class);
        int y = location.getY(Integer.class);
        Object item = tile.getItem();
        if (item != null && item.getClass().isAssignableFrom(Renderable.class)) {
            Renderable r = (Renderable) item;
            //graphics.draw(r, x, y);
            // TODO!
        } else if (item != null && item.getClass().isAssignableFrom(Texture.class)) {
            Texture t = (Texture) item;
            //graphics.draw(t, x, y);
            // TODO!
        }
    }

    @Override
    public void update() {
        Object item = tile.getItem();
        if (item != null && item.getClass().isAssignableFrom(Updatable.class)) {
            Updatable u = (Updatable) item;
            u.update();
        }
    }

    public Point2D getLocation() {
        return location;
    }

    public void setLocation(float x, float y) {
        this.location.setLocation(x, y);
    }

    public Priority getPriority() {
        return Priority.NORMAL_PRIORITY;
    }

    public Tile getTile() {
        return tile;
    }

    public void setTile(Tile tile) {
        this.tile = tile;
    }

    public void setVisible(boolean visible) {
        tile.setVisible(visible);
    }

    @Override
    public boolean isVisible() {
        return tile.isVisible();
    }

    @Override
    public String toString() {
        return "T[" + tile.getGridY() + "," + tile.getGridX() + "]";
    }

}
