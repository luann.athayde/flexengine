/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.flexengine.model.map.model;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;

/**
 *
 * @author Luann Athayde
 */
@Root(name = "game_map_load_texture")
public class GameMapTexture {

    @Attribute(name = "load_texture_name", required = true)
    protected String  loadTextureName;
    @Attribute(name = "load_texture_file", required = true)
    protected String  loadTextureFile;
    @Attribute(name = "scaled_width", required = false)
    protected Integer scaledWith;
    @Attribute(name = "scaled_height", required = false)
    protected Integer scaledHeight;
    
    public GameMapTexture() {
        this(null,null,null,null);
    }

    public GameMapTexture(String loadTextureName, String loadTextureFile) {
        this(loadTextureName,loadTextureFile,null,null);
    }

    public GameMapTexture(String loadTextureName, String loadTextureFile, Integer scaledWith, Integer scaledHeight) {
        this.loadTextureName = loadTextureName;
        this.loadTextureFile = loadTextureFile;
        this.scaledWith = scaledWith;
        this.scaledHeight = scaledHeight;
    }
    
    public String getLoadTextureName() {
        return loadTextureName;
    }

    public void setLoadTextureName(String loadTextureName) {
        this.loadTextureName = loadTextureName;
    }

    public String getLoadTextureFile() {
        return loadTextureFile;
    }

    public void setLoadTextureFile(String loadTextureFile) {
        this.loadTextureFile = loadTextureFile;
    }

    public Integer getScaledWith() {
        return scaledWith;
    }

    public void setScaledWith(Integer scaledWith) {
        this.scaledWith = scaledWith;
    }

    public Integer getScaledHeight() {
        return scaledHeight;
    }

    public void setScaledHeight(Integer scaledHeight) {
        this.scaledHeight = scaledHeight;
    }
    
}
