/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.flexengine.model.map.model;

import java.util.ArrayList;
import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

/**
 *
 * @author luann
 */
@Root(name = "map_layer")
public class GameMapLayer {

    @Attribute(required = true)
    protected int id;

    @Attribute(required = true)
    protected int zOrder;

    @ElementList(required = false, type = Tile.class, inline = true)
    protected ArrayList<Tile> tiles;

    public GameMapLayer() {
        this(1, 0, null);
    }

    public GameMapLayer(int id, int zOrder, ArrayList<Tile> tiles) {
        this.id = id;
        this.zOrder = zOrder;
        this.tiles = tiles;
    }

    public int getzOrder() {
        return zOrder;
    }

    public void setzOrder(int zOrder) {
        this.zOrder = zOrder;
    }

    public ArrayList<Tile> getTiles() {
        return tiles;
    }

    public void setTiles(ArrayList<Tile> tiles) {
        this.tiles = tiles;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

}
