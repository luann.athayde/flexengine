/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.flexengine.model.map.model;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 *
 * @author Luann Athayde
 */
@Root(name = "game_map_item")
public class GameMapItem {
    
    @Attribute(name = "name")
    protected String name;
    @Attribute(name = "type")
    protected String type;
    @Element(name = "item")
    private Object item;

    public GameMapItem() {
    }

    public GameMapItem(String name, String type, Object item) {
        this.name = name;
        this.type = type;
        this.item = item;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Object getItem() {
        return item;
    }

    public void setItem(Object item) {
        this.item = item;
    }
    
}
