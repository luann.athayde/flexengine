/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.flexengine.model.map.model;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import net.flexengine.model.components.GameComponent;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 *
 * @author luann
 */
@Root(name = "tile_event")
public class TileEvent {

    @Element(name = "when", required = true)
    protected EventType when;

    @Element(name = "script", required = false, data = true)
    protected String script;

    public TileEvent() {
        this(ON_ALL, null);
    }

    public TileEvent(EventType when, String script) {
        this.when = when;
        this.script = script;
    }

    /**
     * Execute the script...
     *
     * @param script
     * @return
     */
    public int execute(String script) {
        return -1;
    }

    public void onEnter(GameComponent gc) {
        if (when == ON_ENTER || when == ON_ALL) {
            execute(script);
        }
    }

    public void onExit(GameComponent gc) {
        if (when == ON_EXIT || when == ON_ALL) {
            execute(script);
        }
    }

    public void onClick() {
        if (when == ON_CLICK || when == ON_ALL) {
            execute(script);
        }
    }

    public void onBlur() {
        if (when == ON_BLUR || when == ON_ALL) {
            execute(script);
        }
    }

    public void onEvent(GameComponent gc) {
        onEnter(gc);
        onExit(gc);
        onClick();
        onBlur();
    }

    public String getScript() {
        return script;
    }

    public void setScript(String script) {
        this.script = script;
    }

    public EventType getWhen() {
        return when;
    }

    public void setWhen(EventType when) {
        this.when = when;
    }

    @Override
    public String toString() {
        return "TileEvent{" + "script=" + script + '}';
    }

    public static final EventType ON_ALL = createEventType(0, "ON_ALL");
    public static final EventType ON_ENTER = createEventType(1, "ON_ENTER");
    public static final EventType ON_EXIT = createEventType(2, "ON_EXIT");
    public static final EventType ON_CLICK = createEventType(3, "ON_CLICK");
    public static final EventType ON_BLUR = createEventType(4, "ON_BLUR");

    private static EventType createEventType(int id, String name) {
        try {
            Constructor<EventType> evtConst = EventType.class.getDeclaredConstructor(int.class, String.class);
            evtConst.setAccessible(true);
            return evtConst.newInstance(id, name);
        } catch (NoSuchMethodException | SecurityException | InstantiationException | 
                IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
        }
        return null;
    }

}
