/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.flexengine.model.map.model;

import java.util.ArrayList;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

/**
 *
 * @author luann
 */
@Root(name = "game_map")
public class GameMap {

    public static final int MIN_MAP_WIDTH = 64;
    public static final int MIN_MAP_HEIGHT = 64;
    public static final int MAX_MAP_WIDTH = 512;
    public static final int MAX_MAP_HEIGHT = 512;
    public static final int DEFAULT_MAP_WIDTH = 128;
    public static final int DEFAULT_MAP_HEIGHT = 128;
    public static final int SCROLL_SPEED[] = {10, 9, 8, 7, 6, 5, 4, 3, 2, 1};
    public static final int MIN_SCROLL_SPEED = 1;
    public static final int MAX_SCROLL_SPEED = 10;

    @Element(required = true)
    protected int id;
    @Element(required = true)
    protected String name;
    @Element(required = true)
    protected String author;
    @Element(required = false, data = true)
    protected String description;
    @Element(required = false, name = "load_texture")
    protected GameMapTexture loadTexture;
    @Element(required = true)
    protected boolean drawGrid;
    @Element(required = true, name = "map_width")
    protected int mapWidth;
    @Element(required = true, name = "map_height")
    protected int mapHeight;
    @Element(required = true, name = "mouse_scroll_enable")
    protected boolean mouseScrollEnable;
    @Element(required = true, name = "mouse_scroll_speed")
    protected int mouseScrollSpeed;
    @Element(required = true, name = "left_key_scroll_enable")
    protected boolean leftKeyScrollEnable;
    @Element(required = true, name = "right_key_scroll_enable")
    protected boolean rightKeyScrollEnable;
    @Element(required = true, name = "top_key_scroll_enable")
    protected boolean topKeyScrollEnable;
    @Element(required = true, name = "down_key_scroll_enable")
    protected boolean downKeyScrollEnable;
    @Element(required = true, name = "key_scroll_speed")
    protected int keyScrollSpeed;
    
    @Element(required = false, name = "sounds")
    protected GameMapSounds sounds;
    
    @ElementList(required = false, name = "global_itens")
    protected ArrayList<GameMapItem> globalItens;

    @ElementList(required = false, type = GameMapLayer.class)
    protected ArrayList<GameMapLayer> layers;
    
    public GameMap() {
        this.id = 1000;
        this.name = null;
        this.author = null;
        this.description = null;
        this.loadTexture = null;
        this.drawGrid = true;
        this.mapWidth = DEFAULT_MAP_WIDTH;
        this.mapHeight = DEFAULT_MAP_HEIGHT;
        this.mouseScrollEnable = true;
        this.mouseScrollSpeed = 10;
        this.leftKeyScrollEnable = true;
        this.rightKeyScrollEnable = true;
        this.topKeyScrollEnable = true;
        this.downKeyScrollEnable = true;
        this.keyScrollSpeed = 3;
        this.layers = new ArrayList<>();
        this.globalItens = new ArrayList<>();
        this.sounds = new GameMapSounds();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public GameMapTexture getLoadTexture() {
        return loadTexture;
    }

    public void setLoadTexture(GameMapTexture loadTexture) {
        this.loadTexture = loadTexture;
    }

    public int getMapWidth() {
        return mapWidth;
    }

    public void setMapWidth(int mapWidth) {
        if (mapWidth >= MIN_MAP_WIDTH && mapWidth <= MAX_MAP_WIDTH) {
            this.mapWidth = mapWidth;
        }
    }

    public int getMapHeight() {
        return mapHeight;
    }

    public void setMapHeight(int mapHeight) {
        if (mapHeight >= MIN_MAP_HEIGHT && mapHeight <= MAX_MAP_HEIGHT) {
            this.mapHeight = mapHeight;
        }
    }

    public boolean isMouseScrollEnable() {
        return mouseScrollEnable;
    }

    public void setMouseScrollEnable(boolean mouseScrollEnable) {
        this.mouseScrollEnable = mouseScrollEnable;
    }

    public int getMouseScrollSpeed() {
        return mouseScrollSpeed;
    }

    public void setMouseScrollSpeed(int mouseScrollSpeed) {
        if (mouseScrollSpeed >= MIN_SCROLL_SPEED && mouseScrollSpeed <= MAX_SCROLL_SPEED) {
            this.mouseScrollSpeed = mouseScrollSpeed;
        }
    }

    public boolean isLeftKeyScrollEnable() {
        return leftKeyScrollEnable;
    }

    public void setLeftKeyScrollEnable(boolean leftKeyScrollEnable) {
        this.leftKeyScrollEnable = leftKeyScrollEnable;
    }

    public boolean isRightKeyScrollEnable() {
        return rightKeyScrollEnable;
    }

    public void setRightKeyScrollEnable(boolean rightKeyScrollEnable) {
        this.rightKeyScrollEnable = rightKeyScrollEnable;
    }

    public boolean isTopKeyScrollEnable() {
        return topKeyScrollEnable;
    }

    public void setTopKeyScrollEnable(boolean topKeyScrollEnable) {
        this.topKeyScrollEnable = topKeyScrollEnable;
    }

    public boolean isDownKeyScrollEnable() {
        return downKeyScrollEnable;
    }

    public void setDownKeyScrollEnable(boolean downKeyScrollEnable) {
        this.downKeyScrollEnable = downKeyScrollEnable;
    }

    public int getKeyScrollSpeed() {
        return keyScrollSpeed;
    }

    public void setKeyScrollSpeed(int keyScrollSpeed) {
        if (keyScrollSpeed >= MIN_SCROLL_SPEED && keyScrollSpeed <= MAX_SCROLL_SPEED) {
            this.keyScrollSpeed = keyScrollSpeed;
        }
    }

    public ArrayList<GameMapLayer> getLayers() {
        return layers;
    }
    
    public int getTotalLayers() {
        if( layers!=null && !layers.isEmpty() )
            return layers.size();
        return 0;
    }
    
    public void setLayers(ArrayList<GameMapLayer> layers) {
        this.layers = layers;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public boolean isDrawGrid() {
        return drawGrid;
    }

    public void setDrawGrid(boolean drawGrid) {
        this.drawGrid = drawGrid;
    }

    public GameMapSounds getSounds() {
        return sounds;
    }

    public void setSounds(GameMapSounds sounds) {
        this.sounds = sounds;
    }

    public ArrayList<GameMapItem> getGlobalItens() {
        return globalItens;
    }

    public void setGlobalItens(ArrayList<GameMapItem> globalItens) {
        this.globalItens = globalItens;
    }

    @Override
    public String toString() {
        return "GameMap{" + "name=" + name + ", description=" + description
                + ", loadTexture=" + loadTexture + ", mapWidth=" + mapWidth
                + ", mapHeight=" + mapHeight + ", mouseScrollEnable=" + mouseScrollEnable
                + ", mouseScrollSpeed=" + mouseScrollSpeed + ", leftKeyScrollEnable=" + leftKeyScrollEnable
                + ", rightKeyScrollEnable=" + rightKeyScrollEnable + ", topKeyScrollEnable=" + topKeyScrollEnable
                + ", downKeyScrollEnable=" + downKeyScrollEnable
                + ", keyScrollSpeed=" + keyScrollSpeed + '}';
    }

}
