/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.flexengine.model.map.model;

import java.util.ArrayList;
import java.util.Collection;
import net.flexengine.model.sound.Sound;
import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

/**
 *
 * @author luann
 */
@Root(name = "sounds")
public class GameMapSounds {

    public static final int PLAY_NORMAL_ORDER = 1;
    public static final int PLAY_INVERTED_ORDER = 2;
    public static final int PLAY_NAME_ORDER = 3;

    @Attribute(required = true)
    protected boolean continous;
    @Attribute(required = true)
    protected int playOrder;
    @ElementList(required = false, type = Sound.class, inline = true)
    protected ArrayList<Sound> sounds;

    public GameMapSounds() {
        this(true, PLAY_NORMAL_ORDER, null);
    }

    public GameMapSounds(boolean continous, int playOrder, ArrayList<Sound> sounds) {
        this.continous = continous;
        this.playOrder = playOrder;
        this.sounds = sounds;
    }

    public boolean isContinous() {
        return continous;
    }

    public void setContinous(boolean continous) {
        this.continous = continous;
    }

    public int getPlayOrder() {
        return playOrder;
    }

    public void setPlayOrder(int playOrder) {
        if (playOrder >= PLAY_NORMAL_ORDER && playOrder <= PLAY_NAME_ORDER) {
            this.playOrder = playOrder;
        }
    }

    public boolean addSound(Sound s) {
        if (sounds == null) {
            sounds = new ArrayList<>();
        }
        return sounds.add(s);
    }

    public Sound getSound(int index) {
        return sounds.get(index);
    }

    public boolean removeSound(Sound s) {
        boolean rm = sounds.remove(s);
        if (sounds.isEmpty()) {
            sounds = null;
        }
        return rm;
    }

    public Sound removeSound(int index) {
        Sound s = sounds.remove(index);
        if (sounds.isEmpty()) {
            sounds = null;
        }
        return s;
    }

    public boolean contains(Sound s) {
        return sounds.contains(s);
    }

    public boolean addAll(Collection<? extends Sound> snds) {
        if (sounds == null) {
            sounds = new ArrayList<>();
        }
        return sounds.addAll(snds);
    }

    public boolean removeAll(Collection<? extends Sound> snds) {
        boolean rm = sounds.removeAll(snds);
        if (sounds.isEmpty()) {
            sounds = null;
        }
        return rm;
    }
    
    public void setSound(int index, Sound sound) {
        sounds.set(index, sound);
    }

    public int size() {
        if (sounds != null) {
            return sounds.size();
        } else {
            return 0;
        }
    }

}
