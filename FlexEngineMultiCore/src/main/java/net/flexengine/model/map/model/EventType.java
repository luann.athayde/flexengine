/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.flexengine.model.map.model;

import java.util.Objects;
import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;

/**
 *
 * @author luann
 */
@Root
public final class EventType {

    @Attribute
    private int id;
    @Attribute
    private String name;

    private EventType() {
        this(0,"ON_ALL");
    }

    private EventType(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + Objects.hashCode(this.name);
        hash = 29 * hash + this.id;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final EventType other = (EventType) obj;
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        return this.id == other.id;
    }

    @Override
    public String toString() {
        return "EventType{" + "id=" + id + ", name=" + name + '}';
    }
    
}
