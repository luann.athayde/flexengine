/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.flexengine.model.map;

import net.flexengine.controller.*;
import net.flexengine.model.config.Priority;
import net.flexengine.model.map.model.GameMap;
import net.flexengine.model.map.model.GameMapLayer;
import net.flexengine.model.map.model.Tile;
import net.flexengine.model.scene.SceneBundle;
import net.flexengine.view.GameWindow;

import java.awt.*;
import java.awt.event.KeyEvent;

/**
 * @author Luann R. Athayde
 * @version 1.0
 * @since 1.0
 */
public class GameMapRenderer implements Renderable, Updatable, Initializable, Finalizable {

    protected GameMap gameMap;
    protected Insets insets;
    protected GameMapLayerRenderer[] viewPortLayers;
    protected int mouseScrollCount = 0;
    protected int keyScrollCount = 0;
    protected int viewPortX;
    protected int viewPortY;
    protected int viewPortWidth;
    protected int viewPortHeight;
    protected int viewScrollSize;
    protected boolean visible;

    public GameMapRenderer(GameMap gameMap) throws InvalidGameMapException {
        if (gameMap == null) {
            throw new InvalidGameMapException(getClass(), new Throwable("GameMap is null!"));
        }
        if (gameMap.getTotalLayers() == 0) {
            throw new InvalidGameMapException(getClass(), new Throwable("GameMap does not have layers!"));
        }
        this.gameMap = gameMap;
        this.viewPortLayers = new GameMapLayerRenderer[gameMap.getTotalLayers()];
        this.viewPortX = 0;
        this.viewPortY = 0;
        this.viewPortWidth = 0;
        this.viewPortHeight = 0;
        this.visible = true;
        this.viewScrollSize = 15;
        this.insets = null;
        // ---
    }

    @Override
    public void update() {
        // -- move view port...
        mouseScrollCount++;
        if (mouseScrollCount == gameMap.getMouseScrollSpeed() && gameMap.isMouseScrollEnable()) {
            if (InputController.getInstance().getMouseX() <= viewScrollSize + insets.left) {
                scrollLeft();
                updateViewPortTilesPosition();
            }
            if (InputController.getInstance().getMouseX() >= GameWindow.getInstance().getWidth() - (viewScrollSize + insets.right)) {
                scrollRight();
                updateViewPortTilesPosition();
            }
            if (InputController.getInstance().getMouseY() <= viewScrollSize + insets.top) {
                scrollTop();
                updateViewPortTilesPosition();
            }
            if (InputController.getInstance().getMouseY() >= GameWindow.getInstance().getHeight() - (viewScrollSize + insets.bottom)) {
                scrollDown();
                updateViewPortTilesPosition();
            }
            mouseScrollCount = 0;
        }
        // --
        keyScrollCount++;
        if (keyScrollCount == gameMap.getKeyScrollSpeed()) {
            if (InputController.getInstance().isKeyPressed(KeyEvent.VK_LEFT) && gameMap.isLeftKeyScrollEnable()) {
                scrollLeft();
                updateViewPortTilesPosition();
                //InputManager.releaseKey(InputManager.VK_LEFT);
            }
            if (InputController.getInstance().isKeyPressed(KeyEvent.VK_RIGHT) && gameMap.isRightKeyScrollEnable()) {
                scrollRight();
                updateViewPortTilesPosition();
                //InputManager.releaseKey(InputManager.VK_RIGHT);
            }
            if (InputController.getInstance().isKeyPressed(KeyEvent.VK_UP) && gameMap.isTopKeyScrollEnable()) {
                scrollTop();
                updateViewPortTilesPosition();
                //InputManager.releaseKey(InputManager.VK_UP);
            }
            if (InputController.getInstance().isKeyPressed(KeyEvent.VK_DOWN) && gameMap.isDownKeyScrollEnable()) {
                scrollDown();
                updateViewPortTilesPosition();
                //InputManager.releaseKey(InputManager.VK_DOWN);
            }
            keyScrollCount = 0;
        }
        // --
        // Debug use only...
        if (InputController.getInstance().isKeyPressed(KeyEvent.VK_Z)) {
            gameMap.setMouseScrollSpeed(gameMap.getMouseScrollSpeed() + 1);
            mouseScrollCount = 0;
            InputController.getInstance().releaseKey(KeyEvent.VK_Z);
        }
        if (InputController.getInstance().isKeyPressed(KeyEvent.VK_X)) {
            gameMap.setMouseScrollSpeed(gameMap.getMouseScrollSpeed() - 1);
            mouseScrollCount = 0;
            InputController.getInstance().releaseKey(KeyEvent.VK_X);
        }
        // -- Atualiza a tiles da VIEW_PORT
        for (int layer = 0; layer < viewPortLayers.length; layer++) {
            GameMapLayerRenderer layerRender = viewPortLayers[layer];
            for (int linha = 0; linha < gameMap.getMapHeight(); linha++) {
                for (int coluna = 0; coluna < gameMap.getMapWidth(); coluna++) {
                    TileRenderer tr = layerRender.getTile(linha, coluna);
                    if (tr != null) {
                        tr.update();
                    }
                }
            }
        }
    }

    @Override
    public void render(Graphics2D graphics) {
        // -- RENDERIZA AS TILES DA VIEW_PORT
        for (int layer = 0; layer < viewPortLayers.length; layer++) {
            GameMapLayerRenderer layerRender = viewPortLayers[layer];
            for (int linha = 0; linha < gameMap.getMapHeight(); linha++) {
                for (int coluna = 0; coluna < gameMap.getMapWidth(); coluna++) {
                    TileRenderer tr = layerRender.getTile(linha, coluna);
                    if (tr != null && tr.isVisible()) {
                        tr.render(graphics);
                    }
                }
            }
        }
        if (gameMap.isDrawGrid()) {
            int y = 0;
            for (int linha = 0; linha < gameMap.getMapHeight(); linha++) {
                int x = 0;
                for (int coluna = 0; coluna < gameMap.getMapWidth(); coluna++) {
                    graphics.setColor(Color.WHITE);
                    graphics.drawRect(x, y, Tile.TILE_WIDTH, Tile.TILE_HEIGHT);
                    x += Tile.TILE_WIDTH;
                }
                y += Tile.TILE_HEIGHT;
            }
        }
        // --
        drawAlphaBorder(graphics);
        // --
        graphics.setColor(Color.BLACK);
        graphics.drawRect(
                viewScrollSize + insets.left, viewScrollSize + insets.top,
                GameWindow.getInstance().getWidth() - insets.right - viewScrollSize * 2,
                GameWindow.getInstance().getHeight() - insets.bottom - insets.top - viewScrollSize * 2);
        // --
        graphics.setColor(Color.BLACK);
        graphics.fillRect(0, 0, 180, 70);
        graphics.setColor(Color.RED);
        graphics.drawRect(0, 0, 180, 70);
        graphics.drawString("ViewPort Size = (" + viewPortWidth + "," + viewPortHeight + ")", 10, 15);
        graphics.drawString("ViewPort Loc  = (" + viewPortX + "," + viewPortY + ")", 10, 30);
        graphics.drawString("MouseScroll Speed  = " + gameMap.getMouseScrollSpeed(), 10, 45);
        graphics.drawString("KeyScroll Speed  = " + gameMap.getKeyScrollSpeed(), 10, 60);
    }

    protected void drawAlphaBorder(Graphics2D graphics) {
        graphics.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.75f));
        graphics.setColor(Color.BLACK);
        int screenWidth = GameWindow.getInstance().getWidth();
        int screenHeight = GameWindow.getInstance().getHeight();
        // -- top
        graphics.fillRect(0, 0, screenWidth, viewScrollSize);
        // -- left
        graphics.fillRect(0, viewScrollSize, viewScrollSize, screenHeight);
        // -- right
        graphics.fillRect(screenWidth - viewScrollSize, viewScrollSize, viewScrollSize, screenHeight);
        // -- bottom
        graphics.fillRect(viewScrollSize, screenHeight - viewScrollSize, screenWidth - 2 * viewScrollSize, viewScrollSize);
        graphics.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 1f));
    }

    @Override
    public void init(SceneBundle bundle) {
        insets = GameWindow.getInstance().getInsets();
        updateViewPort();
    }

    @Override
    public void end(SceneBundle bundle) {
        viewPortLayers = null;
        System.gc();
    }

    private void updateViewPortTilesPosition() {
        for (int layer = 0; layer < gameMap.getTotalLayers(); layer++) {
            GameMapLayer mapLayer = gameMap.getLayers().get(layer);
            TileRenderer[][] tiles = viewPortLayers[layer].getTiles();
            float locX = 0;
            for (int gridX = viewPortX, vx = 0; gridX < viewPortX + viewPortWidth; gridX++, vx++) {
                float locY = 0;
                for (int gridY = viewPortY, vy = 0; gridY < viewPortY + viewPortHeight; gridY++, vy++) {
                    Tile tile = buscarTile(mapLayer, gridX, gridY);
                    if (tile != null) {
                        TileRenderer tr = new TileRenderer(tile);
                        tr.setLocation(locX, locY);
                        tiles[vx][vy] = tr;
                    } else {
                        tiles[vx][vy] = null;
                    }
                    // --
                    locY += Tile.TILE_HEIGHT;
                }
                locX += Tile.TILE_WIDTH;
            }
        }
    }

    protected void updateViewPort() {
        viewPortWidth = GameWindow.getInstance().getWidth() / Tile.TILE_WIDTH;          //
        viewPortHeight = (GameWindow.getInstance().getHeight()) / Tile.TILE_HEIGHT;     // 
        viewPortX = gameMap.getMapWidth() / 2 - viewPortWidth / 2;                      // 
        viewPortY = gameMap.getMapHeight() / 2 - viewPortHeight / 2;                    // 
        // ---
        // Create the VIEW_PORT_LAYERS...
        for (int layer = 0; layer < gameMap.getTotalLayers(); layer++) {
            GameMapLayer mapLayer = gameMap.getLayers().get(layer);
            TileRenderer[][] tiles = new TileRenderer[gameMap.getMapWidth()][gameMap.getMapHeight()];
            float locX = 0;
            for (int gridX = viewPortX, vx = 0; gridX < viewPortX + viewPortWidth; gridX++, vx++) {
                float locY = 0;
                for (int gridY = viewPortY, vy = 0; gridY < viewPortY + viewPortHeight; gridY++, vy++) {
                    Tile tile = buscarTile(mapLayer, gridX, gridY);
                    if (tile != null) {
                        TileRenderer tr = new TileRenderer(tile);
                        tr.setLocation(locX, locY);
                        tiles[vx][vy] = tr;
                    } else {
                        tiles[vx][vy] = null;
                    }
                    // --
                    locY += Tile.TILE_HEIGHT;
                }
                locX += Tile.TILE_WIDTH;
            }
            viewPortLayers[layer] = new GameMapLayerRenderer(mapLayer.getId(), mapLayer.getzOrder(), tiles);
        }
        // ---
    }

    public Tile buscarTile(GameMapLayer layer, int gridX, int gridY) {
        for (Tile tile : layer.getTiles()) {
            if (tile.getGridY() == gridY && tile.getGridX() == gridX) {
                return tile;
            }
        }
        return null;
    }

    public Priority getPriority() {
        return Priority.LOW_PRIORITY;
    }

    @Override
    public boolean isVisible() {
        return visible;
    }

    public void scrollTop() {
        viewPortY--;
        if (viewPortY < 0) {
            viewPortY = 0;
        }
    }

    public void scrollDown() {
        viewPortY++;
        if (viewPortY > gameMap.getMapHeight() - viewPortHeight) {
            viewPortY = gameMap.getMapHeight() - viewPortHeight;
        }
    }

    public void scrollLeft() {
        viewPortX--;
        if (viewPortX < 0) {
            viewPortX = 0;
        }
    }

    public void scrollRight() {
        viewPortX++;
        if (viewPortX > gameMap.getMapWidth() - viewPortWidth) {
            viewPortX = gameMap.getMapWidth() - viewPortWidth;
        }
    }

    public int getViewScrollSize() {
        return viewScrollSize;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public void setViewScrollSize(int viewScrollSize) {
        if (viewScrollSize >= 5 && viewScrollSize <= GameWindow.getInstance().getHeight() / 2) {
            this.viewScrollSize = viewScrollSize;
        }
    }

    public GameMap getGameMap() {
        return gameMap;
    }

}
