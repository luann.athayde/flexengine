/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.flexengine.model.cursor;

import net.flexengine.controller.CursorController;
import net.flexengine.model.config.Priority;
import net.flexengine.view.Texture;

import java.awt.Point;

/**
 * @author Luann R. Athayde
 * @version 1.0
 * @since 1.0
 */
public class GameCursor {

    protected String name;
    protected Texture texture;
    protected Point location;
    protected Point locOnScreen;
    protected boolean exclusiveMode;

    public GameCursor() {
        this(null, new Point(0, 0), new Point(0, 0));
    }

    public GameCursor(Texture texture, int x, int y, int xOnScren, int yOnScreen) {
        this(texture, new Point(x, y), new Point(xOnScren, yOnScreen));
    }

    public GameCursor(Texture texture, Point location, Point locOnScreen) {
        this.name = "GameCursor";
        this.texture = texture;
        this.location = location;
        this.locOnScreen = locOnScreen;
        this.exclusiveMode = false;
    }

    // -- Events...
    public void mouseClicked() {
    }

    public void mouseReleased() {
    }

    public void mousePressed() {
    }
    //--------------------------------------------------------------------------
    // Gets e sets...

    public Point getLocation() {
        return location;
    }

    public void setLocation(Point loc) {
        location.x = loc.x;
        location.y = loc.y;
    }

    public void setLocation(int x, int y) {
        location.move(x, y);
    }

    public void move(int x, int y) {
        location.move(x, y);
    }

    public Point getLocOnScreen() {
        return locOnScreen;
    }

    public void setLocOnScreen(Point locOnScreen) {
        this.locOnScreen.x = locOnScreen.x;
        this.locOnScreen.y = locOnScreen.y;
    }

    public void setLocOnScreen(int x, int y) {
        this.locOnScreen.x = x;
        this.locOnScreen.y = y;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Priority getPriority() {
        return Priority.NORMAL_PRIORITY;
    }

    public void setPriority(Priority p) {
    }

    public Texture getTexture() {
        return texture;
    }

    public void setTexture(Texture texture) {
        if (texture != this.texture) {
            this.texture = texture;
        }
    }

    public boolean isVisible() {
        return CursorController.getInstance().isShowGameCursor();
    }

    public void setVisible(boolean visible) {
        if (visible) {
            CursorController.getInstance().showGameCursor();
        } else {
            CursorController.getInstance().showBasicCursor();
        }
    }

    public boolean isExclusiveMode() {
        return exclusiveMode;
    }

    public void setExclusiveMode(boolean exclusiveMode) {
        this.exclusiveMode = exclusiveMode;
    }

    //--------------------------------------------------------------------------
    @Override
    public String toString() {
        return "GameCursor - " + name + " - " + texture + " - " + location;
    }
}
