package net.flexengine.model.cursor;

import net.flexengine.controller.InputController;
import net.flexengine.view.Texture;

import java.awt.*;

public class NavyCursor extends GameCursor {

    public NavyCursor() {
        super();
        // ---
        this.texture = new Texture("NavyCursor", 24, 24);
        Graphics2D g = texture.getImage().createGraphics();
        drawCursor(g);
        g.dispose();
        // ---
        int mouseX = InputController.getInstance().getMouseX();
        int mouseY = InputController.getInstance().getMouseY();
        this.location.setLocation(mouseX, mouseY);
    }

    private void drawCursor(Graphics2D g) {
        int[] cursorXPoints = new int[]{0, 20, 20, 10};
        int[] cursorYPoints = new int[]{0, 10, 20, 20};

        g.setColor(new Color(0, 30, 60));
        g.fillPolygon(cursorXPoints, cursorYPoints, 4);
        g.setColor(Color.WHITE);
        g.drawPolygon(cursorXPoints, cursorYPoints, 4);
    }

}
