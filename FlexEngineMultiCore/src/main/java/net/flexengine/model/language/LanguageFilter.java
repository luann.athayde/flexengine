/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.flexengine.model.language;

import java.io.File;
import java.io.FileFilter;

/**
 * @author Luann R. Athayde
 * @version 1.0
 * @since 1.0
 */
public final class LanguageFilter implements FileFilter {

    public static final String LANGUAGE_FILTER = ".lang";
    
    @Override
    public boolean accept(File pathname) {
        return pathname.getName().endsWith(LANGUAGE_FILTER);
    }
    
}
