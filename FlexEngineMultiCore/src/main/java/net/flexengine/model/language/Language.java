/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.flexengine.model.language;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Objects;

/**
 * @author Luann R. Athayde
 * @version 1.0
 * @since 1.0
 */
public class Language implements Serializable {

    private final String name;
    private final HashMap<String, String> dictionary;

    public Language(String name, HashMap<String, String> dictionary) {
        this.name = name;
        this.dictionary = dictionary;
    }

    public String getName() {
        return name;
    }

    public String getValue(String key) {
        String value = dictionary.get(key);
        return value!=null && !value.isEmpty() ? value : "["+key+"]";
    }

    @Override
    public String toString() {
        return name + " [" + dictionary.size() + "]";
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + Objects.hashCode(this.name);
        hash = 29 * hash + Objects.hashCode(this.dictionary);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Language other = (Language) obj;
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        return Objects.equals(this.dictionary, other.dictionary);
    }
    
}
