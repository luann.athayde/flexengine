/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.flexengine.model.database;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

/**
 * @author Luann R. Athayde
 * @version 1.0
 * @since 1.0
 */
@Data
@Slf4j
@MappedSuperclass
@AllArgsConstructor
public abstract class EntityModel implements Serializable, Cloneable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(insertable = false, updatable = false)
    protected int id;

    @Column
    @Temporal(TemporalType.TIMESTAMP)
    protected Date creationDate;

    @Column
    protected boolean enable;

    @Column
    @Temporal(TemporalType.TIMESTAMP)
    protected Date deleteDate;

    public EntityModel() {
        this.creationDate = new Date();
        this.enable = true;
        this.deleteDate = null;
    }

    @Override
    public Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException ce) {
            log.error("Fail to clone object...", ce);
        }
        return new Object();
    }

    @Override
    public int hashCode() {
        return 41 * 7 + id;
    }

    @Override
    public boolean equals(Object obj) {
        if (Objects.nonNull(obj) && obj instanceof EntityModel) {
            EntityModel that = (EntityModel) obj;
            return that.hashCode() == this.hashCode();
        }
        return false;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "{id=" + id + ", enable=" + enable + "}";
    }

}
