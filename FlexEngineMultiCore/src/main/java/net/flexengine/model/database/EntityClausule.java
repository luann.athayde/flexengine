/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.flexengine.model.database;

/**
 * @author Luann R. Athayde
 * @version 1.0
 * @since 1.0
 */
public class EntityClausule {
    
    private String field;
    private String operator;
    private String value;

    public EntityClausule(String field, String operator, String value) {
        this.field = field;
        this.operator = operator;
        this.value = value;
    }
    public EntityClausule(String field, String value) {
        this(field, "like", value);
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "EntityClausule{" + "field=" + field + ", operator=" + operator + ", value=" + value + '}';
    }
    
}
