/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.flexengine.model.game;

import java.io.File;
import java.io.FileFilter;


/**
 *
 * @author Luann Athayde
 */
public class FlexGameFileFilter implements FileFilter {

    public static final String FILE_GAME_EXTENSION = ".fml";
    
    @Override
    public boolean accept(File f) {
        return f!=null && f.getName().toLowerCase().endsWith(FILE_GAME_EXTENSION);
    }

    public String getDescription() {
        return "FlexML Game File";
    }
    
}
