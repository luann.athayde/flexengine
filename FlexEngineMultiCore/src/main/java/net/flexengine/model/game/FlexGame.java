/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.flexengine.model.game;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 *
 * @author Luann Athayde
 */
@Root(name = "game")
public class FlexGame {

    @Element
    private String name;

    @Element(name = "image_icon")
    private String imageIcon;

    @Element
    private String description;

    public FlexGame() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImageIcon() {
        return imageIcon;
    }

    public void setImageIcon(String imageIcon) {
        this.imageIcon = imageIcon;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "FlexGame{" + "name=" + name + ", imageIcon=" + imageIcon + ", description=" + description + '}';
    }

}
