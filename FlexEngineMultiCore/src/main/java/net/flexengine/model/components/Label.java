/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.flexengine.model.components;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.awt.*;
import java.awt.font.FontRenderContext;
import java.awt.geom.Rectangle2D;
import java.util.Objects;

/**
 * @author Luann R. Athayde
 * @version 1.0
 * @since 1.0
 */
@Data
@Builder
@AllArgsConstructor
public class Label extends GameComponent implements HorizontalTextAlign {

    private String text;
    private Font font;
    private Color foregroundColor;

    private Color backgroundColor;
    private Color borderColor;
    private float borderSize;

    private int leftSpace;
    private int rightSpace;

    private int roundRectWidth;
    private int roundRectHeight;

    private int horizontalTextAlign;

    private FontRenderContext fontContext;

    private float transparence;

    /**
     * Create a label with black text...
     */
    public Label() {
        this("");
    }

    /**
     * Create a label with a informed text...
     *
     * @param text
     */
    public Label(String text) {
        super("Label", 0, 0, new Dimension(0, 0));
        this.text = text;
        this.font = new Font("Arial", Font.PLAIN, 12);
        this.backgroundColor = null;
        this.foregroundColor = Color.WHITE;
        this.borderColor = null;
        this.borderSize = 0;
        this.roundRectWidth = 0;
        this.roundRectHeight = 0;
        this.fontContext = new FontRenderContext(null, true, true);
        this.horizontalTextAlign = HORIZONTAL_ALIGN_LEFT;
        this.transparence = 1f;
        this.leftSpace = 5;
        this.rightSpace = 5;
    }

    @Override
    public void render(Graphics2D g) {
        g.setFont(font);
        if (backgroundColor != null) {
            g.setColor(backgroundColor);
            g.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, transparence));
            g.fillRoundRect((int) x, (int) y, size.width, size.height, roundRectWidth, roundRectHeight);
            g.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 1.0f));
        }
        if (borderColor != null) {
            g.setColor(borderColor);
            g.setStroke(new BasicStroke(borderSize));
            g.drawRoundRect((int) x, (int) y, size.width, size.height, roundRectWidth, roundRectHeight);
            g.setStroke(new BasicStroke(1));
        }

        if (foregroundColor != null) {
            g.setColor(foregroundColor);
        } else {
            g.setColor(Color.BLACK);
        }
        int tmpX = (int) x;
        Rectangle2D stringBounds = font.getStringBounds(text, fontContext);
        switch (horizontalTextAlign) {
            case HORIZONTAL_ALIGN_LEFT:
                tmpX += roundRectWidth / 2;
                tmpX += leftSpace;
                break;
            case HORIZONTAL_ALIGN_CENTER:
                tmpX += size.width / 2 - stringBounds.getWidth() / 2;
                break;
            case HORIZONTAL_ALIGN_RIGHT:
                tmpX += (int) (size.width - stringBounds.getWidth()) - roundRectWidth / 2;
                tmpX -= rightSpace;
                break;
        }
        int tmpY = (int) (y + size.height / 2 - stringBounds.getCenterY());
        g.drawString(text, tmpX, tmpY);
    }

    public void setBorderSize(float borderSize) {
        if (borderSize > 0 && borderSize < 1000) {
            this.borderSize = borderSize;
        } else {
            this.borderSize = 1;
        }
    }

    public void setBorder(Color borderColor, float borderSize) {
        setBorderColor(borderColor);
        setBorderSize(borderSize);
    }

    public void setText(String text) {
        if (Objects.nonNull(text)) {
            this.text = text;
        } else {
            this.text = "";
        }
    }

    @Override
    public boolean equals(Object o) {
        return super.equals(o);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

}
