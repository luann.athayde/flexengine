/**
 *
 */
package net.flexengine.model.components;

import com.sun.management.OperatingSystemMXBean;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import net.flexengine.controller.*;
import net.flexengine.model.scene.GameScene;
import net.flexengine.utils.AsyncTask;
import net.flexengine.view.GameWindow;

import java.awt.*;
import java.lang.management.ManagementFactory;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 *
 */
@Data
@Builder
@AllArgsConstructor
public class DebugInfo extends GamePanel {

    private static final transient Color BG_COLOR = new Color(250, 250, 250);

    private final transient DecimalFormat basicFormat = new DecimalFormat(" #0.00");
    private final transient DecimalFormat loadFormat = new DecimalFormat("0.00%");
    private final transient Font debugFont = FontController.getFont("Monospaced", 0, 12);
    private final transient OperatingSystemMXBean osBean;

    private final transient AsyncTask debugInfoTask;
    // ---
    private String memoryInfo;
    private String sceneTimerInfo;
    private String systemInfo;
    private String osInfoInfo;

    public DebugInfo() {
        super("DebugInfo", 0, 0,
                new Dimension(GameWindow.getInstance().getWidth(), 60));
        osBean = ManagementFactory.getPlatformMXBean(OperatingSystemMXBean.class);
        memoryInfo = "";
        sceneTimerInfo = "";
        systemInfo = "";
        osInfoInfo = "";
        debugInfoTask = AsyncTask.newInstance();
        debugInfoTask.run(this::updateInformation, true, 2);
    }

    public void updateInformation() {
        memoryInfo = buildMemoryInfo();
        sceneTimerInfo = buildSceneInfo();
        systemInfo = buildSystemInfo();
        osInfoInfo = buildOsInfo();
    }

    @Override
    public void update() {
        size.width = GameWindow.getInstance().getViewportWidth();
    }

    @Override
    public void render(Graphics2D g2d) {
        try {
            // ---
            List<Object> sceneItems = new ArrayList<>(
                    SceneController.getInstance().getCurrentScene().getItems()
            );
            int tmpX = 0;
            int tmpY = 0;

            g2d.setFont(debugFont);
            g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.8f));
            g2d.setColor(BG_COLOR);
            g2d.fillRect(tmpX, tmpY, size.width, size.height);

            g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 1.0f));
            g2d.setColor(Color.BLACK);
            tmpX += 5;
            tmpY += 14;
            g2d.drawString(osInfoInfo, tmpX, tmpY);
            tmpY += 20;
            g2d.drawString(memoryInfo, tmpX, tmpY);
            tmpY += 20;
            g2d.drawString(sceneTimerInfo, tmpX, tmpY);
            tmpY += 20;
            g2d.drawString(systemInfo, tmpX, tmpY);
            tmpY += 20;
            g2d.drawString(EngineHandler.getInstance().getCurrentInfo(), tmpX, tmpY);
            tmpY += 20;
            g2d.drawString("Scene items: " + sceneItems.size(), tmpX, tmpY);
            // tmpY += 20;
            // g.drawString("Scene items: " + sceneItems, tmpX, tmpY);
            size.height = tmpY + 5;
        } catch (Exception e) {
        }
    }

    public void renderAvarageInfo(Graphics2D g2d) {
        try {
            g2d.setFont(debugFont);
            // ---
            g2d.setColor(calculateInfoColor());
            int viewportHeight = RenderController.getInstance().getViewportHeight();
            g2d.drawString(buildFpsInfo(), 4, viewportHeight - 40);
            g2d.drawString(buildUpsInfo(), 4, viewportHeight - 28);
            g2d.drawString(buildRenderTimeInfo(), 4, viewportHeight - 16);
            g2d.drawString(buildLogicTimeInfo(), 4, viewportHeight - 4);
        } catch (Exception e) {
        }
    }

    public String buildFpsInfo() {
        StringBuilder sb = new StringBuilder();
        sb.append("aFPS: ");
        sb.append(basicFormat.format(EngineHandler.getInstance().getAvarageFPS()));
        return sb.toString();
    }

    public String buildUpsInfo() {
        StringBuilder sb = new StringBuilder();
        sb.append("aUPS: ");
        sb.append(basicFormat.format(EngineHandler.getInstance().getAvarageUPS()));
        return sb.toString();
    }

    public String buildRenderTimeInfo() {
        StringBuilder sb = new StringBuilder();
        sb.append("renderTime: ");
        sb.append(RenderController.getInstance().getShowRenderTime());
        sb.append(" ms");
        return sb.toString();
    }

    public String buildLogicTimeInfo() {
        StringBuilder sb = new StringBuilder();
        sb.append("logicTime: ");
        sb.append(LogicController.getInstance().getShowLogicTime());
        sb.append(" ms");
        return sb.toString();
    }

    public Color calculateInfoColor() {
        if (GameWindow.getInstance().isBackgroundSet()) {
            Color bgColor = GameWindow.getInstance().getBackground();
            // ---
            // Counting the perceptive luminance - human eye favors green color...
            double luminance = (0.299 * bgColor.getRed() +
                    0.587 * bgColor.getGreen() +
                    0.114 * bgColor.getBlue()
            ) / 255;
            // ---
            int d = 0; // bright colors - black font
            if (luminance <= 0.5)
                d = 255; // dark colors - white font
            // --
            return new Color(d, d, d);
        }
        return Color.CYAN;
    }

    private String buildMemoryInfo() {
        StringBuilder sb = new StringBuilder();
        sb.append("Memory usage: ");
        double totalMemory = Runtime.getRuntime().totalMemory() / 1024d / 1024d;
        double freeMemory = Runtime.getRuntime().freeMemory() / 1024d / 1024d;
        double maxMemory = Runtime.getRuntime().maxMemory() / 1024d / 1024d;
        sb.append(basicFormat.format(totalMemory));
        sb.append("MB | ");
        sb.append(basicFormat.format(freeMemory));
        sb.append("MB | ");
        sb.append(basicFormat.format(maxMemory));
        sb.append("MB");
        return sb.toString();
    }

    private String buildSceneInfo() {
        StringBuilder sb = new StringBuilder();
        sb.append("Scene: ");
        GameScene scene = SceneController.getInstance().getCurrentScene();
        if (Objects.nonNull(scene)) {
            sb.append(scene.getName());
            sb.append(" | time: ");
            sb.append(scene.getTimer().getTime().toTimeString());
        }
        return sb.toString();
    }

    private String buildSystemInfo() {
        StringBuilder sb = new StringBuilder();
        sb.append("System load: ");
        try {
            sb.append(loadFormat.format(osBean.getSystemCpuLoad()));
            sb.append("/");
            sb.append(loadFormat.format(osBean.getProcessCpuLoad()));
        } catch (Exception e) {
        }
        return sb.toString();
    }

    private String buildOsInfo() {
        StringBuilder sb = new StringBuilder();
        sb.append("OS: ");
        sb.append(System.getProperty("os.name")).append(" ");
        sb.append(System.getProperty("os.arch")).append(" ");
        sb.append(System.getProperty("os.version"));
        return sb.toString();
    }

    // -------------------------------------------------------------------------
    @Override
    public boolean equals(Object o) {
        return super.equals(o);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

}