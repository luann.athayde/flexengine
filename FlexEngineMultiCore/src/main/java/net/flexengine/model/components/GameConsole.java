/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.flexengine.model.components;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import net.flexengine.controller.FontController;
import net.flexengine.controller.InputController;
import net.flexengine.model.config.Priority;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Luann R. Athayde
 * @version 1.0
 * @since 1.0
 */
@Data
@Builder
@AllArgsConstructor
public class GameConsole extends GamePanel {

    private final List<ConsoleListener> listeners = new ArrayList<>();
    private final TextField textFieldConsole = new TextField();
    private final TextField textFieldCommand = new TextField();

    protected Label topLabel;
    protected Button buttonClose;

    public GameConsole() {
        super("Console", 100, 100, new Dimension(480, 320));
        Color corBorda = new Color(160, 160, 180);
        Color corTexto = new Color(0, 200, 240);
        // -- basic settings...
        setPriority(Priority.HIGH_PRIORITY);
        setDragArea(0, 0, 448, 32);
        // -- Panel bar...
        topLabel = new Label("Console");
        topLabel.setBounds(0, 0, 448, 32);
        topLabel.setBorder(BASIC_BORDER_COLOR, 2);
        topLabel.setFont(FontController.getFont("Serif", 1, 16));
        topLabel.setBackgroundColor(Color.BLACK);
        topLabel.setForegroundColor(Color.WHITE);
        topLabel.setTransparence(0.5f);
        // --
        buttonClose = new Button("X");
        buttonClose.setBounds(448, 0, 32, 32);
        buttonClose.setRoundRect(Button.ROUND_RECT_NONE);
        buttonClose.setBorder(BASIC_BORDER_COLOR, 2);
        buttonClose.setFont(FontController.getFont("Serif", 1, 14));
        buttonClose.setBackgroundColor(Color.BLACK);
        buttonClose.setForegroundColor(Color.WHITE);
        buttonClose.setAlpha(0.5f);
        buttonClose.addEventListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                setVisible(false);
            }
        });

        textFieldConsole.setFont(FontController.getFont("Serif", 1, 14));
        textFieldConsole.setBackgroundColor(Color.BLACK);
        textFieldConsole.setBorderColor(corBorda);
        textFieldConsole.setBorderSize(2);
        textFieldConsole.setForegroundColor(corTexto);
        textFieldConsole.setCursorColor(corTexto);
        textFieldConsole.setBounds(0, 32, 480, 320 - 64);
        textFieldConsole.setSelected(false);
        textFieldConsole.setTransparence(0.5f);
        textFieldConsole.setText("");

        textFieldCommand.setFont(FontController.getFont("Serif", 1, 14));
        textFieldCommand.setBackgroundColor(Color.BLACK);
        textFieldCommand.setBorderColor(corBorda);
        textFieldCommand.setBorderSize(2);
        textFieldCommand.setForegroundColor(corTexto);
        textFieldCommand.setCursorColor(corTexto);
        textFieldCommand.setBounds(0, 320 - 32, 480, 32);
        textFieldCommand.setSelected(false);
        textFieldCommand.setTransparence(0.5f);
        textFieldCommand.setText("");

        add(topLabel);
        add(buttonClose);
        add(textFieldConsole);
        add(textFieldCommand);
        setVisible(false);

        textFieldCommand.addEventListener((ActionEvent e) -> {
            executeCommand();
        });

    }

    private void executeCommand() {
        listeners.forEach((listener) -> {
            listener.execute(this, textFieldCommand.getText());
        });
        textFieldCommand.setText("");
    }

    @Override
    public void update() {
        super.update();
        if (InputController.getInstance().isControlAndKeyPressed(KeyEvent.VK_C)) {
            setVisible(!isVisible());
            InputController.getInstance().releaseKey(KeyEvent.VK_C);
        }
    }

    public boolean addConsoleListener(ConsoleListener listener) {
        return listeners.add(listener);
    }

    public boolean removeConsoleListener(ConsoleListener listener) {
        return listeners.remove(listener);
    }

    /**
     * Console Listener (interface)
     */
    public interface ConsoleListener {

        /**
         * Execute a command typed on console.
         *
         * @param console
         * @param command
         */
        void execute(GameConsole console, String command);
    }

    // -------------------------------------------------------------------------
    @Override
    public boolean equals(Object o) {
        return super.equals(o);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

}
