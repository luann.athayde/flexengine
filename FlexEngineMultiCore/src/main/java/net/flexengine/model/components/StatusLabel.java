/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.flexengine.model.components;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author Luann R. Athayde
 * @version 1.0
 * @since 1.0
 */
@Data
@AllArgsConstructor
public class StatusLabel extends Label {

    private String basicText;

    private int count;
    private int dots;
    private boolean showDots;

    public StatusLabel() {
        this("");
    }

    public StatusLabel(String text) {
        super(text);
        basicText = text;
    }

    @Override
    public void update() {
        super.update();
        if (showDots) {
            count++;
            if (count >= 25) {
                dots++;
                if (dots == 4) {
                    dots = 0;
                }
                setText(basicText + getDots());
                count = 0;
            }
        } else {
            setText(basicText);
        }
    }

    private String getDots() {
        String strDots = "";
        for (int i = 0; i < dots; i++) {
            strDots += ".";
        }
        return strDots;
    }

    @Override
    public boolean equals(Object o) {
        return super.equals(o);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

}
