/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.flexengine.model.components;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import net.flexengine.controller.EngineHandler;
import net.flexengine.controller.FontController;
import net.flexengine.controller.InputController;
import org.apache.commons.lang3.StringUtils;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.font.FontRenderContext;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.Objects;

/**
 * @author Luann R. Athayde
 * @version 1.0
 * @since 1.0
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TextField extends GameComponent implements HorizontalTextAlign {

    private volatile int mouseX;
    private volatile int mouseY;

    private String text;
    private boolean selected;

    private int columns;

    private Font font;

    private Color backgroundColor;
    private Color foregroundColor;
    private Color cursorColor;

    private Color borderColor;
    private float borderSize;

    private int roundRectWidth;
    private int roundRectHeight;

    private int horizontalTextAlign;

    private int maxLenght;

    // -------------------------------------------------------------------------
    private boolean blink;
    private int blickCont;

    private boolean passwordField;
    // -------------------------------------------------------------------------
    private final java.util.List<ActionListener> listeners = new ArrayList<>();
    private int eventID;

    private float transparence;

    public TextField(String text) {
        this.text = text;
        this.passwordField = false;
        this.selected = false;
        this.columns = 0;
        this.font = FontController.getFont(FontController.getDefaultFont());
        this.backgroundColor = Color.LIGHT_GRAY;
        this.foregroundColor = Color.BLACK;
        this.cursorColor = Color.BLACK;
        this.borderColor = Color.WHITE;
        this.borderSize = 1;
        this.roundRectWidth = 0;
        this.roundRectHeight = 0;
        this.blink = false;
        this.blickCont = 0;
        this.size.setSize(100, 20);
        this.horizontalTextAlign = HORIZONTAL_ALIGN_LEFT;
        this.eventID = 0;
        this.maxLenght = -1;
        this.transparence = 1f;
    }

    @Override
    public void update() {
        if (blickCont >= EngineHandler.getInstance().getDesireUps() / 2) {
            blickCont = 0;
            blink = !blink;
        }
        blickCont++;
        // ---------------------------------------------------------------------
        mouseX = InputController.getInstance().getMouseX();
        mouseY = InputController.getInstance().getMouseY();
        // ---------------------------------------------------------------------
        if (InputController.getInstance().isMouseClicked(MouseEvent.BUTTON1)
                && mouseX > x && mouseX < x + size.width
                && mouseY > y && mouseY < y + size.height) {
            selected = true;
            InputController.getInstance().releaseMouseButton(MouseEvent.BUTTON1);
        } else if (InputController.getInstance().isMouseClicked(MouseEvent.BUTTON1)
                && (mouseX < x || mouseX > x + size.width
                || mouseY < y || mouseY > y + size.height)) {
            selected = false;
        }
        if (selected && visible) {
            if (InputController.getInstance().isKeyPressed(KeyEvent.VK_BACK_SPACE)) {
                if (text.length() > 0) {
                    text = text.substring(0, text.length() - 1);
                }
                InputController.getInstance().releaseKey(KeyEvent.VK_BACK_SPACE);
                InputController.getInstance().releaseLastKey();
            } else if (InputController.getInstance().isKeyPressed(KeyEvent.VK_ENTER)) {
                for (int i = 0; i < listeners.size(); i++) {
                    listeners.get(i).actionPerformed(new ActionEvent(this, eventID, text, System.currentTimeMillis(), -1));
                    eventID++;
                }
                InputController.getInstance().releaseKey(KeyEvent.VK_ENTER);
                InputController.getInstance().releaseLastKey();
            } else if (validLastKey()) {
                if (maxLenght > 0) {
                    if (text.length() < maxLenght) {
                        text += InputController.getInstance().getLastKey();
                    }
                } else {
                    text += InputController.getInstance().getLastKey();
                }
                InputController.getInstance().releaseLastKey();
            }
        }
    }

    protected boolean validLastKey() {
        return InputController.getInstance().getLastKey() > 0
                && InputController.getInstance().getLastKey() >= 32
                && InputController.getInstance().getLastKey() <= 126;
    }

    @Override
    public void render(Graphics2D g) {
        //super.render(g);
        var fontContext = new FontRenderContext(null, true, false);
        Rectangle2D r2d = new Rectangle(0, 0, size.width, size.height);
        StringBuilder textTmp = new StringBuilder();
        if (StringUtils.isNotBlank(text)) {
            textTmp = new StringBuilder(text);
            if (passwordField) {
                textTmp = new StringBuilder();
                textTmp.append("#".repeat(text.length()));
            }
            if (Objects.nonNull(font)) {
                r2d = font.getStringBounds(textTmp.toString(), fontContext);
            }
        }

        g.setFont(font);

        if (Objects.nonNull(backgroundColor)) {
            g.setColor(backgroundColor);
            g.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, transparence));
            g.fillRoundRect((int) x, (int) y, size.width, size.height, roundRectWidth, roundRectHeight);
            g.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 1.0f));
        }

        if (Objects.nonNull(borderColor)) {
            g.setColor(borderColor);
            Stroke s = g.getStroke();
            if (borderSize > 0) {
                g.setStroke(new BasicStroke(borderSize));
            }
            g.drawRoundRect(x, y, size.width, size.height, roundRectWidth, roundRectHeight);
            g.setStroke(s);
        }

        if (Objects.nonNull(foregroundColor)) {
            g.setColor(foregroundColor);
        } else {
            g.setColor(Color.BLACK);
        }
        //g.drawString(text,(int)x+roundRectLeft/2,(int)y+font.getSize()-2);
        float xTmp = x + 2f;
        switch (horizontalTextAlign) {
            case HORIZONTAL_ALIGN_LEFT:
                xTmp += roundRectWidth / 2;
                break;
            case HORIZONTAL_ALIGN_CENTER:
                xTmp += (size.width / 2) - (r2d.getWidth() / 2);
                break;
            case HORIZONTAL_ALIGN_RIGHT:
                xTmp += (float) (size.width - r2d.getWidth()) - roundRectWidth - 3;
                break;
        }
        float yTmp = y;
        yTmp += r2d.getHeight() / 2 + size.getHeight() / 2;
        g.drawString(textTmp.toString(), (int) xTmp + 2, (int) yTmp);

        if (Objects.nonNull(cursorColor)) {
            g.setColor(cursorColor);
        } else {
            g.setColor(Color.BLACK);
        }

        xTmp = x + 2f;
        if (blink && selected && horizontalTextAlign == HORIZONTAL_ALIGN_LEFT) {
            xTmp += r2d.getWidth() + 3f;
            float xTmp2 = xTmp;
            if (font.isItalic()) {
                xTmp += 8;
            }
            g.drawLine((int) xTmp + roundRectWidth / 2, (int) y + 3, (int) xTmp2 + roundRectWidth / 2, (int) y + size.height - 6);
            if (font.isBold()) {
                g.drawLine((int) xTmp + roundRectWidth / 2 + 1, (int) y + 3, (int) xTmp2 + roundRectWidth / 2 + 1, (int) y + size.height - 6);
            }
        } else if (blink && selected && horizontalTextAlign == HORIZONTAL_ALIGN_RIGHT) {
            xTmp += size.width - roundRectWidth;
            float xTmp2 = xTmp;
            if (font.isItalic()) {
                xTmp += 8;
            }
            g.drawLine((int) xTmp, (int) y + 3, (int) xTmp2, (int) y + size.height - 6);
            if (font.isBold()) {
                g.drawLine((int) xTmp + 1, (int) y + 3, (int) xTmp2 + 1, (int) y + size.height - 6);
            }
        } else if (blink && selected && horizontalTextAlign == HORIZONTAL_ALIGN_CENTER) {
            xTmp += size.width / 2f + r2d.getWidth() / 2f + 4f;
            float xTmp2 = xTmp;
            if (font.isItalic()) {
                xTmp += 8;
            }
            g.drawLine((int) xTmp, (int) y + 3, (int) xTmp2, (int) y + size.height - 6);
            if (font.isBold()) {
                g.drawLine((int) xTmp + 1, (int) y + 3, (int) xTmp2 + 1, (int) y + size.height - 6);
            }
        }

    }

    public void addEventListener(ActionListener event) {
        listeners.add(event);
    }

    public boolean removeListener(ActionListener event) {
        return listeners.remove(event);
    }

    public void clearListener() {
        listeners.clear();
    }

    public void setRoundRect(int roundRectLeft, int roundRectRight) {
        this.roundRectWidth = roundRectLeft;
        this.roundRectHeight = roundRectRight;
    }

    public void setText(String text) {
        if (text == null) {
            text = "";
        }
        this.text = text;
    }

    @Override
    public boolean equals(Object o) {
        return super.equals(o);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

}
