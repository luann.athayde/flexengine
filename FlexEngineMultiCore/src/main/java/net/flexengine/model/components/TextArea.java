/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.flexengine.model.components;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Luann R. Athayde
 * @version 1.0
 * @since 1.0
 */
@Data
@Builder
@AllArgsConstructor
public class TextArea extends GameComponent implements HorizontalTextAlign {

    private final List<String> wraps = new ArrayList<>();
    private int horizontalTextAlign;

    public TextArea() {
        horizontalTextAlign = HORIZONTAL_ALIGN_LEFT;
    }

    @Override
    public boolean equals(Object o) {
        return super.equals(o);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

}
