/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.flexengine.model.components;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import net.flexengine.controller.FontController;
import net.flexengine.controller.InputController;
import net.flexengine.controller.RenderController;
import net.flexengine.model.config.Priority;
import net.flexengine.model.scene.SceneBundle;
import net.flexengine.view.GameWindow;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @author Luann R. Athayde
 * @version 1.0
 * @since 1.0
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class GamePanel extends GameComponent {

    protected static final DraggPanelInfo DRAGGING_INFO = new DraggPanelInfo();
    protected static int globalPriority = 3;

    protected final transient List<GameComponent> components = new ArrayList<>();

    protected float transparence;
    protected Color backgroundColor;

    protected boolean border;
    protected Color borderColor;
    protected float borderSize;
    protected int roundRectWidth;
    protected int roundRectHeight;

    // -- Draggable options...
    private Label labelTopTitle;
    private Button buttonClose;

    protected int mouseX;
    protected int mouseY;
    protected boolean draggable;
    protected boolean dragging;
    protected Point dragInitPoint;
    protected Point dragPoint;
    protected Rectangle dragArea;

    protected boolean closeable;
    protected boolean showTopDecoration;

    public GamePanel(String name, int x, int y) {
        this(name, x, y, new Dimension(), true, Priority.NORMAL_PRIORITY);
    }

    public GamePanel(String name, int x, int y, Dimension size) {
        this(name, x, y, size, true, Priority.NORMAL_PRIORITY);
    }

    public GamePanel(String name, int x, int y, Dimension size, boolean visible, Priority priority) {
        this.name = name;
        normalizePosition(x, y);
        this.size = Objects.nonNull(size) ? size : new Dimension();
        this.visible = visible;
        this.priority = priority;
        // --
        transparence = 1f;
        backgroundColor = null;
        border = true;
        borderColor = BASIC_BORDER_COLOR;
        borderSize = 1;
        roundRectWidth = roundRectHeight = 0;
        // -- Draggable options...
        this.draggable = true;
        this.dragging = false;
        this.dragInitPoint = new Point(-1, -1);
        this.dragPoint = new Point(-1, -1);
        this.dragArea = new Rectangle(0, 0, getSize().width, getSize().height);
        this.closeable = true;
        this.showTopDecoration = false;
    }

    private void normalizePosition(int x, int y) {
        this.x = x;
        this.y = y;
        if (this.x > GameWindow.getInstance().getWidth() - getSize().getWidth()) {
            this.x = (int) (RenderController.getInstance().getViewportWidth() - getSize().getWidth());
        }
        if (this.y > GameWindow.getInstance().getHeight() - getSize().getHeight()) {
            this.y = (int) (RenderController.getInstance().getViewportHeight() - getSize().getHeight());
        }
    }

    @Override
    public void init(SceneBundle bundle) {
        for (int i = 0; i < components.size(); i++) {
            GameComponent gc = components.get(i);
            gc.init(bundle);
        }
    }

    @Override
    public void update() {
        if (showTopDecoration) {
            int labelTopTitleWidth = getWidth();
            if (closeable) {
                labelTopTitleWidth -= 32;
            }
            // --
            labelTopTitle.setBounds(0, 0, labelTopTitleWidth, 32);
            buttonClose.setBounds(getWidth() - 32, 0, 32, 32);
            //
            labelTopTitle.update();
            buttonClose.update();
        }
        //--
        mouseX = InputController.getInstance().getMouseX();
        mouseY = InputController.getInstance().getMouseY();
        components.stream().forEach((tmp) -> {
            int tmpX = tmp.getX();
            int tmpY = tmp.getY();
            tmp.setPosition(tmpX + x, tmpY + y);
            tmp.update();
            tmp.setPosition(tmpX, tmpY);
        });
        //--
        if (draggable && visible) {
            if (InputController.getInstance().isMousePressed(MouseEvent.BUTTON1)
                    && !dragging
                    && mouseX >= x && mouseX <= x + dragArea.width
                    && mouseY >= y && mouseY <= y + dragArea.height
                    && !DRAGGING_INFO.dragging && DRAGGING_INFO.panelID == -1) {
                dragInitPoint.move((int) x, (int) y);
                dragPoint.move(mouseX, mouseY);
                dragging = true;
                DRAGGING_INFO.dragging = true;
                DRAGGING_INFO.panelID = getId();
                setPriority(new Priority("CUSTOM_GAMEPANEL", globalPriority));
                globalPriority++;
            } else if (InputController.getInstance().isMousePressed(MouseEvent.BUTTON1) && !dragging) {
                dragInitPoint.setLocation(-1, -1);
                dragPoint.setLocation(-1, -1);
            } else if (!InputController.getInstance().isMousePressed(MouseEvent.BUTTON1)
                    && DRAGGING_INFO.panelID == getId()) {
                dragging = false;
                dragInitPoint.setLocation(0, 0);
                dragPoint.setLocation(0, 0);
                DRAGGING_INFO.dragging = false;
                DRAGGING_INFO.panelID = -1;
            }
            if (dragging && DRAGGING_INFO.dragging && DRAGGING_INFO.panelID == getId()) {
                x = dragInitPoint.x + (mouseX - dragPoint.x);
                y = dragInitPoint.y + (mouseY - dragPoint.y);
                Dimension winSize = GameWindow.getInstance().getSize();
                if (x <= 0) {
                    x = 0;
                } else if (x + size.width >= winSize.getWidth()) {
                    x = (int) (winSize.getWidth() - size.width);
                }
                if (y <= 0) {
                    y = 0;
                } else if (y + size.height >= winSize.getHeight()) {
                    y = (int) (winSize.getHeight() - size.height);
                }
            }
        }
    }

    @Override
    public void render(Graphics2D g) {
        Graphics2D gTmp = (Graphics2D) g.create();
        if (visible) {
            if (Objects.nonNull(backgroundColor)) {
                gTmp.setColor(backgroundColor);
                gTmp.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, transparence));
                gTmp.fillRoundRect(x, y, size.width, size.height, roundRectWidth, roundRectHeight);
                gTmp.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC));
            }
            components.stream().filter(GameComponent::isVisible)
                    .forEach(tmp -> drawComponent(tmp, g));
            drawBorder(gTmp);
        }
        //--
        if (showTopDecoration) {
            drawComponent(labelTopTitle, g);
            if (closeable) {
                drawComponent(buttonClose, g);
            }
        }
    }

    protected void drawComponent(GameComponent gc, Graphics2D g) {
        var tmpX = gc.getX();
        var tmpY = gc.getY();
        gc.setPosition(tmpX + x, tmpY + y);
        gc.render(g);
        gc.setPosition(tmpX, tmpY);
    }

    public void drawBorder(Graphics2D g) {
        if (border) {
            g.setColor(borderColor);
            g.setStroke(new BasicStroke(borderSize));
            g.drawRoundRect(x, y, size.width, size.height, roundRectWidth, roundRectHeight);
        }
    }

    @Override
    public void end(SceneBundle bundle) {
        components.forEach((gc) -> {
            gc.end(bundle);
        });
    }

    @Override
    public void setVisible(boolean visible) {
        this.visible = visible;
        components.forEach(gc -> gc.setVisible(visible));
    }

    @Override
    public Priority getPriority() {
        return priority;
    }

    @Override
    public void setPriority(Priority p) {
        this.priority = p;
    }

    public void add(GameComponent obj) {
        components.add(obj);
    }

    public GameComponent get(int index) {
        return components.get(index);
    }

    public GameComponent remove(int index) {
        return components.remove(index);
    }

    public boolean remove(GameComponent obj) {
        return components.remove(obj);
    }

    public boolean isEmpty() {
        return components.isEmpty();
    }

    public int size() {
        return components.size();
    }

    public void clear() {
        components.clear();
    }

    public List<GameComponent> getPanelComponents() {
        return components;
    }

    public List<GameComponent> getPanelComponents(Class<? extends GamePanel> clazz) {
        return getComponents().stream().filter(item ->
                item.getClass().getSimpleName().equalsIgnoreCase(clazz.getSimpleName())
        ).collect(Collectors.toList());
    }

    public void setDragArea(double x, double y, double width, double height) {
        if (Objects.isNull(dragArea)) {
            dragArea = new Rectangle();
        }
        dragArea.setBounds((int) x, (int) y, (int) width, (int) height);
    }

    public void setRoundRect(int roundRectHeight, int roundRectWidth) {
        setRoundRectHeight(roundRectHeight);
        setRoundRectWidth(roundRectWidth);
    }

    public void addTopDecoration(String title) {
        labelTopTitle = new Label("  - " + title);
        labelTopTitle.setBorder(BASIC_BORDER_COLOR, 1);
        labelTopTitle.setFont(FontController.getFont("Serif", 1, 16));
        labelTopTitle.setBackgroundColor(Color.BLACK);
        labelTopTitle.setForegroundColor(Color.WHITE);
        labelTopTitle.setTransparence(0.5f);
        // --
        buttonClose = new Button("X");
        buttonClose.setRoundRect(Button.ROUND_RECT_NONE);
        buttonClose.setBorder(BASIC_BORDER_COLOR, 1);
        buttonClose.setFont(FontController.getFont("Serif", 1, 14));
        buttonClose.setBackgroundColor(Color.BLACK);
        buttonClose.setForegroundColor(Color.WHITE);
        buttonClose.setAlpha(0.5f);
        buttonClose.addEventListener((ActionListener) (ActionEvent e) -> {
            setVisible(false);
        });
        // --
        showTopDecoration = true;
    }

    public static class DraggPanelInfo {
        public int panelID = -1;
        public boolean dragging = false;
    }

    // -------------------------------------------------------------------------
    @Override
    public boolean equals(Object o) {
        return super.equals(o);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

}
