/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.flexengine.model.components;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.awt.*;

/**
 * @author Luann R. Athayde
 * @version 1.0
 * @since 1.0
 */
@Data
@Builder
@AllArgsConstructor
public class Separator extends GameComponent {

    private Color color;

    public Separator() {
        super("Separator");
        color = Color.WHITE;
    }

    @Override
    public void render(Graphics2D g) {
        g.setColor(color);
        g.fillRect(getX(), getY(), getSize().width, getSize().height);
    }

    @Override
    public void update() {
    }

    // -------------------------------------------------------------------------
    @Override
    public boolean equals(Object o) {
        return super.equals(o);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

}
