/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.flexengine.model.components;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import net.flexengine.controller.FontController;
import net.flexengine.controller.InputController;
import net.flexengine.utils.AsyncTask;
import net.flexengine.view.Texture;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.font.FontRenderContext;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.EventListener;
import java.util.List;
import java.util.Objects;

/**
 * @author Luann R. Athayde
 * @version 1.0
 * @since 1.0
 */
@Data
@Builder
@AllArgsConstructor
public class Button extends GameComponent implements HorizontalTextAlign {

    public static final int ROUND_RECT_ALL = 0;
    public static final int ROUND_RECT_NONE = -1;
    public static final int ROUND_RECT_LEFT = -10;
    public static final int ROUND_RECT_RIGHT = -20;
    public static final int ROUND_RECT_TOP = -30;
    public static final int ROUND_RECT_BOTTOM = -40;

    // private final List<AWTEvent> events = new ArrayList<>();
    private final List<EventListener> listeners = new ArrayList<>();

    private volatile String label;
    private volatile Font font;
    private volatile float borderSize;
    private volatile int roundRectWidth;
    private volatile int roundRectHeight;
    private volatile float alpha;
    private volatile int horizontalTextAlign;

    private volatile Color foregroundColor;
    private volatile Color foregroundFocusColor;
    private volatile Color backgroundColor;
    private volatile Color backgroundFocusColor;
    private volatile Color borderColor;

    private volatile Color basicBackgroundColor;
    private volatile Color basicForegroundColor;

    private transient Texture icon;
    private FontRenderContext fontContext;

    private volatile int roundRect;

    private volatile boolean runningActions;
    private volatile boolean doClick;
    private volatile int mouseX;
    private volatile int mouseY;
    private volatile boolean onFocus;
    private volatile int eventID;

    public Button() {
        this("Button");
    }

    public Button(String label) {
        super("Button");
        this.label = label;
        this.font = FontController.getFont(FontController.getDefaultFont(), 0, 12);
        // --
        this.basicBackgroundColor = this.backgroundColor = Color.BLACK;
        this.backgroundFocusColor = Color.LIGHT_GRAY;
        // --
        this.basicForegroundColor = this.foregroundColor = Color.WHITE;
        this.foregroundFocusColor = Color.DARK_GRAY;
        // --
        this.borderColor = BASIC_BORDER_COLOR;
        this.borderSize = 0;
        this.roundRectWidth = 0;
        this.roundRectHeight = 0;
        this.alpha = 1f;
        this.size = new Dimension(100, 20);
        this.horizontalTextAlign = HORIZONTAL_ALIGN_CENTER;
        this.fontContext = new FontRenderContext(null, true, true);
        this.icon = null;
        this.roundRect = ROUND_RECT_ALL;
        // ---
    }

    @Override
    public void setSize(double width, double height) {
        super.setSize(width, height);
        if (Objects.nonNull(size) && size.height > 0) {
            font = FontController.getFont(
                    FontController.getDefaultFont(), 0, (12 * size.height) / 20
            );
        }
    }

    @Override
    public void setSize(Dimension size) {
        super.setSize(size);
        if (Objects.nonNull(size) && size.height > 0) {
            font = FontController.getFont(
                    FontController.getDefaultFont(), 0, (12 * size.height) / 20
            );
        }
    }

    @Override
    public void update() {
        mouseX = InputController.getInstance().getMouseX();
        mouseY = InputController.getInstance().getMouseY();
        if (enable && visible) {
            if (mouseX > x && mouseX < x + size.width && mouseY > y && mouseY < y + size.height) {
                onFocus = true;
                backgroundColor = backgroundFocusColor;
                foregroundColor = foregroundFocusColor;
            } else {
                onFocus = false;
                backgroundColor = basicBackgroundColor;
                foregroundColor = basicForegroundColor;
            }
        } else if (!enable) {
            onFocus = false;
        }
        // ---
        if (!runningActions && checkAction()) {
            runningActions = true;
            AsyncTask.newInstance().run(this::processEvent);
        }
    }

    public void processEvent() {
        InputController.getInstance().releaseMouseButton(MouseEvent.BUTTON1);
        doClick = false;
        for (int i = 0; i < listeners.size(); i++) {
            EventListener tmp = listeners.get(i);
            if (tmp instanceof ActionListener) {
                ActionListener aEvent = (ActionListener) tmp;
                aEvent.actionPerformed(
                        new ActionEvent(
                                this,
                                eventID,
                                "clicked",
                                0));
                eventID++;
            }
        }
        runningActions = false;
    }

    public boolean checkAction() {
        return doClick || (onFocus && enable && visible &&
                InputController.getInstance().isMouseClicked(MouseEvent.BUTTON1));
    }

    public void doClick() {
        doClick = true;
    }

    @Override
    public void render(Graphics2D g) {
        g.setFont(font);
        if (Objects.nonNull(backgroundColor)) {
            g.setColor(enable ? backgroundColor : backgroundColor.darker());
            g.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, alpha));
            if (roundRect != ROUND_RECT_NONE) {
                g.fillRoundRect(x, y, size.width, size.height, roundRectWidth, roundRectHeight);
            } else {
                g.fillRect(x, y, size.width, size.height);
            }
            g.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 1.0f));
            // --
            g.setColor(borderColor);
            switch (roundRect) {
                case ROUND_RECT_LEFT:
                    g.drawRect(
                            (x + roundRectWidth / 2), y,
                            size.width - roundRectWidth / 2, size.height
                    );
                    break;
                // --
                case ROUND_RECT_RIGHT:
                    g.drawRect(x, y, size.width - roundRectWidth / 2, size.height);
                    break;
                case ROUND_RECT_TOP:
                    g.drawRect(
                            x, y + roundRectHeight / 2,
                            size.width, size.height - roundRectHeight / 2
                    );
                    break;
                case ROUND_RECT_BOTTOM:
                    g.drawRect(x, y, size.width, size.height - roundRectHeight / 2);
                    break;
            }
        }
        if (borderColor != null) {
            g.setColor(borderColor);
            Stroke basicStroke = g.getStroke();
            if (borderSize > 0) {
                g.setStroke(new BasicStroke(borderSize));
            }
            if (roundRect != ROUND_RECT_NONE) {
                g.drawRoundRect(x, y, size.width, size.height, roundRectWidth, roundRectHeight);
            } else {
                g.drawRect(x, y, size.width, size.height);
            }
            g.setStroke(basicStroke);

            if (roundRect != ROUND_RECT_NONE) {
                g.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, alpha));
                switch (roundRect) {
                    case ROUND_RECT_LEFT:
                        g.setColor(backgroundColor);
                        g.fillRect(
                                (x + roundRectWidth / 2), y + 1,
                                size.width - roundRectWidth / 2, size.height - 1);
                        break;
                    case ROUND_RECT_RIGHT:
                        g.setColor(backgroundColor);
                        g.fillRect(
                                x + 1, y + 1,
                                size.width - roundRectWidth / 2, size.height - 1);
                        break;
                    case ROUND_RECT_TOP:
                        g.setColor(backgroundColor);
                        g.fillRect(
                                x + 1, y + roundRectHeight / 2,
                                size.width - 1, size.height - roundRectHeight / 2);
                        break;
                    case ROUND_RECT_BOTTOM:
                        g.setColor(backgroundColor);
                        g.fillRect(
                                x + 1, y + 1,
                                size.width - 1, size.height - roundRectHeight / 2
                        );
                        break;
                }
                g.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 1.0f));
            }
        }

        double tmpX = x, tmpY = y;

        if (icon != null) {
            tmpX += (size.width / 2d - icon.getWidth() / 2d);
            tmpY += icon.getHeight() / 2d;
            icon.render(g, (int) tmpX, (int) tmpY);
            tmpX = x;
            tmpY = y + icon.getHeight() / 2d;
        }

        if (Objects.nonNull(label) && !label.isEmpty()) {
            if (foregroundColor != null) {
                g.setColor(foregroundColor);
            } else {
                g.setColor(Color.BLACK);
            }
            Rectangle2D textBounds = font.getStringBounds(label, fontContext);
            switch (horizontalTextAlign) {
                case HORIZONTAL_ALIGN_LEFT:
                    tmpX += roundRectWidth / 2d;
                    break;
                case HORIZONTAL_ALIGN_CENTER:
                    tmpX += size.width / 2d - textBounds.getWidth() / 2d;
                    break;
                case HORIZONTAL_ALIGN_RIGHT:
                    tmpX += (size.width - textBounds.getWidth()) - roundRectWidth - 3d;
                    break;
                default:
                    tmpX = x;
                    break;
            }
            tmpY += (size.height / 2d + textBounds.getHeight() / 2d - 3d);
            g.setFont(font);
            g.drawString(label, (int) tmpX, (int) tmpY);
        }
    }

    public void addEventListener(EventListener event) {
        listeners.add(event);
    }

    public boolean removeListener(EventListener event) {
        return listeners.remove(event);
    }

    public void setBorder(Color borderColor, float borderSize) {
        setBorderColor(borderColor);
        setBorderSize(borderSize);
    }

    public void setRoundRectSize(int roundRectWidth, int roundRectHeight) {
        setRoundRectWidth(roundRectWidth);
        setRoundRectHeight(roundRectHeight);
    }

    public void setBackgroundColor(Color backgroundColor) {
        this.basicBackgroundColor = this.backgroundColor = backgroundColor;
    }

    public void setForegroundColor(Color foregroundColor) {
        this.basicForegroundColor = this.foregroundColor = foregroundColor;
    }

    // -------------------------------------------------------------------------
    @Override
    public boolean equals(Object o) {
        return super.equals(o);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

}
