/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.flexengine.model.components;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import net.flexengine.controller.*;
import net.flexengine.model.config.Priority;
import net.flexengine.model.database.EntityModel;
import net.flexengine.model.scene.SceneBundle;
import net.flexengine.view.GameWindow;
import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.HashSet;
import java.util.Objects;

/**
 * @author Luann R. Athayde
 * @version 1.0
 * @since 1.0
 */
@Data
@Root
@MappedSuperclass
@AllArgsConstructor
public class GameComponent extends EntityModel
        implements Updatable, Renderable, Initializable, Finalizable {

    public static final int SCREEN_HORIZONTAL_LEFT_ALIGN = -10;
    public static final int SCREEN_HORIZONTAL_CENTER_ALIGN = -20;
    public static final int SCREEN_HORIZONTAL_RIGHT_ALIGN = -30;
    public static final int SCREEN_VERTICAL_TOP_ALIGN = -40;
    public static final int SCREEN_VERTICAL_CENTER_ALIGN = -50;
    public static final int SCREEN_VERTICAL_BOTTOM_ALIGN = -60;

    public static final int RECTANGLE_COLISION = 0x00A;
    public static final int PIXEL_COLISION = 0x00B;
    public static final int OVAL_COLISION = 0x00C;
    public static final Color BASIC_BORDER_COLOR = new Color(160, 160, 180);

    @Attribute
    @Column
    protected String name;

    @Transient
    protected transient int x;

    @Transient
    protected transient int y;

    @Transient
    protected Dimension size;

    @Transient
    protected transient boolean visible;
    @Transient
    protected transient Priority priority;
    @Transient
    protected transient int hAlign;
    @Transient
    protected transient int vAlign;

    public GameComponent(String name, Priority p, int x, int y, Dimension size) {
        this.name = name;
        this.priority = p;
        this.x = x;
        this.y = y;
        this.size = size;
        this.visible = true;
        Insets insets = GameWindow.getInstance().getInsets();
        if (insets != null) {
            this.x += insets.left;
            this.y += insets.top;
        }
        hAlign = vAlign = -1;
    }

    public GameComponent(String name, int x, int y, Dimension size) {
        this(name, Priority.NORMAL_PRIORITY, x, y, size);
    }

    public GameComponent(String name, int x, int y) {
        this(name, Priority.NORMAL_PRIORITY, x, y, new Dimension());
    }

    public GameComponent(String name) {
        this(name, 0, 0);
    }

    public GameComponent() {
        this("GameComponent", 0, 0);
    }

    @Override
    public void init(SceneBundle bundle) {
    }

    @Override
    public void update() {
        // -- do nothing...
    }

    @Override
    public void render(Graphics2D g) {
    }

    @Override
    public void end(SceneBundle bundle) {
    }

    public int getWidth() {
        return size.width;
    }

    public int getHeight() {
        return size.height;
    }

    public Dimension getWinSize() {
        return GameWindow.getInstance().getSize();
    }

    public void addX(int x) {
        this.x += x;
    }

    public void addY(int y) {
        this.y += y;
    }

    public void setSize(Dimension size) {
        if (Objects.nonNull(size)) {
            getSize().setSize(size.width, size.height);
        } else {
            this.size = new Dimension(0, 0);
        }
    }

    public void setSize(double width, double height) {
        if (width >= 0.1 && width <= 1.0 && height >= 0.1 && height <= 1.0) {
            getSize().setSize(
                    GameWindow.getInstance().getWidth() * width,
                    GameWindow.getInstance().getHeight() * height
            );
        } else {
            getSize().setSize(width, height);
        }
    }

    public Dimension getSize() {
        if (Objects.isNull(size)) {
            size = new Dimension();
        }
        return size;
    }

    public synchronized GameComponent setPosition(int hAlign, int vAlign) {
        this.hAlign = hAlign;
        this.vAlign = vAlign;
        int bWidth = RenderController.getInstance().getViewportWidth();
        int bHeight = RenderController.getInstance().getViewportHeight();
        switch (hAlign) {
            case SCREEN_HORIZONTAL_LEFT_ALIGN:
                x = 0;
                break;
            case SCREEN_HORIZONTAL_CENTER_ALIGN:
                x = bWidth / 2 - size.width / 2;
                break;
            case SCREEN_HORIZONTAL_RIGHT_ALIGN:
                x = bWidth - size.width;
                break;
            default:
                x = hAlign;
                break;
        }
        switch (vAlign) {
            case SCREEN_VERTICAL_TOP_ALIGN:
                y = 0;
                break;
            case SCREEN_VERTICAL_CENTER_ALIGN:
                y = bHeight / 2 - size.height / 2;
                break;
            case SCREEN_VERTICAL_BOTTOM_ALIGN:
                y = bHeight - size.height;
                break;
            default:
                y = vAlign;
                break;
        }
        return this;
    }

    public synchronized GameComponent setPosition(int hAling, int vAlign, int moveX, int moveY) {
        setPosition(hAling, vAlign);
        x += moveX;
        y += moveY;
        return this;
    }

    public synchronized GameComponent setBounds(int x, int y, int width, int height) {
        this.setSize(width, height);
        this.setPosition(x, y);
        return this;
    }

    public void borderCollision() {
        if (x <= 0) {
            x = 0;
        } else if (x + getWidth() >= RenderController.getInstance().getViewportWidth()) {
            x = RenderController.getInstance().getViewportWidth() - getWidth();
        }
        if (y <= 0) {
            y = 0;
        } else if (y + getHeight() >= RenderController.getInstance().getViewportHeight()) {
            y = RenderController.getInstance().getViewportHeight() - getHeight();
        }
    }

    protected boolean rectangleCollide(GameComponent other) {
        // left side
        if (other.getX() + other.getWidth() < getX()) {
            return false;
        }
        // top side
        if (other.getY() + other.getHeight() < getY()) {
            return false;
        }
        // right side
        if (other.getX() > getX() + getWidth()) {
            return false;
        }
        // bottom side
        if (other.getY() > getY() + getHeight()) {
            return false;
        }
        return true;
    }

    // returns a HashSet of strings that list all the pixels in an image that aren't transparent
    // the pixels contained in the HashSet follow the guideline of:
    // x,y where x is the absolute x position of the pixel and y is the absolute y position of the pixel
    protected HashSet<String> getMask(GameComponent gc) {
        HashSet<String> mask = new HashSet<>();
        PixelCollision pc = (PixelCollision) gc;
        BufferedImage image = pc.getImage();
        int pixel, a;
        for (int i = 0; i < image.getWidth(); i++) { // for every (x,y) component in the given box, 
            for (int j = 0; j < image.getHeight(); j++) {
                //  --
                pixel = image.getRGB(i, j); // get the RGB value of the pixel
                a = (pixel >> 24) & 0xff;
                // --
                if (a != 0) {  // if the alpha is not 0, it must be something other than transparent
                    mask.add((gc.getX() + i) + "," + (gc.getY() - j)); // add the absolute x and absolute y coordinates to our set
                }
            }
        }
        return mask;
    }

    // Returns true if there is a collision between object a and object b	
    protected boolean pixelCollide(GameComponent other) {
        // for more accuracy colision and for speed porpouses, check rectangle colision first...
        if (!rectangleCollide(other)) {
            return false;
        }
        // This method detects to see if the images overlap at all. If they do, collision is possible
        int ax1 = getX();
        int ay1 = getY();
        int ax2 = ax1 + getWidth();
        int ay2 = ay1 + getHeight();
        int bx1 = other.getX();
        int by1 = other.getY();
        int bx2 = bx1 + other.getWidth();
        int by2 = by1 + other.getHeight();

        if (by2 < ay1 || ay2 < by1 || bx2 < ax1 || ax2 < bx1) {
            return false; // Collision is impossible.
        } else {
            // Collision is possible.
            // get the masks for both images
            HashSet<String> maskPlayer1 = getMask(this);
            HashSet<String> maskPlayer2 = getMask(other);

            maskPlayer1.retainAll(maskPlayer2);  // Check to see if any pixels in maskPlayer2 are the same as those in maskPlayer1

            if (maskPlayer1.size() > 0) {  // if so, than there exists at least one pixel that is the same in both images, thus
                return true;
            }
        }
        return false;
    }

    public boolean collide(GameComponent other, int colisionType) {
        switch (colisionType) {
            case RECTANGLE_COLISION:
                return rectangleCollide(other);
            case PIXEL_COLISION:
                return pixelCollide(other);
            case OVAL_COLISION:
                return false;
        }
        return false;
    }

    // -------------------------------------------------------------------------
    @Override
    public boolean equals(Object o) {
        return super.equals(o);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public String toString() {
        int w = -1, h = -1;
        if (size != null) {
            w = size.width;
            h = size.height;
        }
        return getName() + "(" + getId() + ")" + "{[x=" + x + ",y=" + y + "][w=" + w + ",h=" + h + "][" + visible + "][" + priority + "]}";
    }

}
