/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.flexengine.model.components;

/**
 * @author Luann R. Athayde
 * @version 1.0
 * @since 1.0
 */
public interface HorizontalTextAlign {

    int HORIZONTAL_ALIGN_CENTER = 0;
    int HORIZONTAL_ALIGN_RIGHT = 1;
    int HORIZONTAL_ALIGN_LEFT = 2;

    void setHorizontalTextAlign(int horizontalTextAlign);

    int getHorizontalTextAlign();

}
