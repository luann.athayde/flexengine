/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.flexengine.model.components;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.awt.*;
import java.awt.geom.Rectangle2D;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Luann R. Athayde
 * @version 1.0
 * @since 1.0
 */
@Data
@Builder
@AllArgsConstructor
public class ProgressBar extends GameComponent {

    public static final int HORIZONAL_PROGRESS_BAR = 1;
    public static final int VERTICAL_PROGRESS_BAR = 2;

    protected double maxValue;
    protected double value;
    protected Font fontInfo;

    protected String infoPattern;
    protected boolean patternModified;
    protected List<DecimalFormat> dfParts;

    protected String info;

    protected Color colorBackground;
    protected Color colorProgress;
    protected Color colorBorder;
    protected Color colorStringInfo;
    protected int leading;
    protected int roundRectWidth;
    protected int roundRectHeight;
    protected int valueWidth;
    protected double percent;
    protected DecimalFormat df;

    public ProgressBar() {
        this("ProgressBar", 0, 0, 0, 100);
    }

    public ProgressBar(double value, double maxValue) {
        this("ProgressBar", 0, 0, value, maxValue);
    }

    public ProgressBar(String name, int x, int y, double value, double maxValue) {
        super(name, x, y);
        this.maxValue = maxValue;
        this.value = value;
        // --
        fontInfo = new Font("Calibri", Font.PLAIN, 9);
        infoPattern = "0.0%";
        dfParts = new ArrayList<>();
        colorBackground = new Color(200, 200, 200);
        colorProgress = new Color(210, 210, 240);
        colorBorder = Color.WHITE;
        colorStringInfo = Color.WHITE;
        df = new DecimalFormat(infoPattern);
        leading = HORIZONAL_PROGRESS_BAR;
        info = df.format(value);
        roundRectWidth = roundRectHeight = 0;
        patternModified = false;
    }

    @Override
    public void update() {
        valueWidth = (int) ( size.width * (value / maxValue) );
        valueWidth = Math.min(valueWidth, size.width);
        percent = value / maxValue;
        if (infoPattern != null) {
            if (patternModified && infoPattern.contains("/")) {
                dfParts.clear();
                String parts[] = infoPattern.split("/", 2);
                dfParts.add(new DecimalFormat(parts[0]));
                dfParts.add(new DecimalFormat(parts[1]));
                patternModified = false;
                applyCustomFormat();
            } else if (infoPattern.contains("/")) {
                applyCustomFormat();
            } else {
                info = df.format(percent);
            }
        } else {
            info = null;
        }
    }

    private void applyCustomFormat() {
        info = dfParts.get(0).format(value);
        info += "/";
        info += dfParts.get(1).format(maxValue);
    }

    @Override
    public void render(Graphics2D g) {
        if (leading == HORIZONAL_PROGRESS_BAR) {
            g.setColor(colorBackground);
            g.fillRoundRect(x, y, size.width, size.height, roundRectWidth, roundRectHeight);
            g.setColor(colorProgress);
            g.fillRoundRect(x, y, valueWidth, size.height, roundRectWidth, roundRectHeight);
            g.setColor(colorBorder);
            g.drawRoundRect(x, y, size.width, size.height, roundRectWidth, roundRectHeight);
            // --
            if (info != null) {
                Rectangle2D infoBounds = fontInfo.getStringBounds(info, g.getFontRenderContext());
                int tmpX = (int) (x + size.width / 2 - Math.floor(infoBounds.getWidth()) / 2);
                int tmpY = (int) (y + size.height - Math.floor(infoBounds.getHeight()) / 4);
                g.setColor(colorStringInfo);
                g.setFont(fontInfo);
                g.drawString(info, tmpX, tmpY);
            }
        } else if (leading == VERTICAL_PROGRESS_BAR) {
            g.setColor(colorBackground);
            g.fillRoundRect(x, y, size.height, size.width, roundRectWidth, roundRectHeight);
            g.setColor(colorProgress);
            g.fillRoundRect(x, y + (size.width - valueWidth), size.height, valueWidth, roundRectWidth, roundRectHeight);
            g.setColor(colorBorder);
            g.drawRoundRect(x, y, size.height, size.width, roundRectWidth, roundRectHeight);
            // --
            if (info != null) {
                Rectangle2D infoBounds = fontInfo.getStringBounds(info, g.getFontRenderContext());
                int tmpX = (int) (x + size.height / 2 - infoBounds.getCenterX());
                int tmpY = (int) (y + size.width / 2 - infoBounds.getCenterY());
                g.setColor(colorStringInfo);
                g.setFont(fontInfo);
                g.drawString(info, tmpX, tmpY);
            }
        }
    }

    public void setInfoPattern(String infoPattern) {
        this.infoPattern = infoPattern;
        this.patternModified = true;
        if (infoPattern != null) {
            if (!infoPattern.contains("/")) {
                df = new DecimalFormat(infoPattern);
            }
        }
    }

    public void setRoundRect(int roundRectWidth, int roundRectHeight) {
        setRoundRectWidth(roundRectWidth);
        setRoundRectHeight(roundRectHeight);
    }

    public void increment() {
        value++;
        value = value > maxValue ? maxValue : value;
    }

    // -------------------------------------------------------------------------
    @Override
    public boolean equals(Object o) {
        return super.equals(o);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

}
