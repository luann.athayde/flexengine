package net.flexengine.model.components;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.awt.*;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ComponentStyle implements Cloneable {

    private Color primaryForeground;
    private Color secondaryForeground;
    private Color primaryBackground;
    private Color secondaryBackground;
    private Color terciary;

    private float alpha;

    /**
     * Return a cloned component style with all its defined properties.
     *
     * @return ComponentStyle
     */
    @SuppressWarnings("all")
    @Override
    public ComponentStyle clone() {
        return ComponentStyle.builder()
                .primaryForeground(primaryForeground)
                .secondaryForeground(secondaryForeground)
                .primaryBackground(primaryBackground)
                .secondaryBackground(secondaryBackground)
                .alpha(alpha)
                .build();
    }

}
