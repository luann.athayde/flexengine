/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.flexengine.model.scene;

import lombok.extern.slf4j.Slf4j;
import net.flexengine.controller.*;
import net.flexengine.model.components.GameComponent;
import net.flexengine.model.components.GamePanel;
import net.flexengine.model.components.HorizontalTextAlign;
import net.flexengine.model.components.Label;
import net.flexengine.view.GameWindow;
import net.flexengine.view.Texture;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.font.FontRenderContext;
import java.awt.geom.Rectangle2D;
import java.io.InputStream;
import java.util.Objects;

/**
 * @author Luann R. Athayde
 * @version 1.0
 * @since 1.0
 */
@Slf4j
public final class EngineIntroScene extends GameScene {

    private int desiredUps;
    private boolean done;
    private float alpha;
    private float updateAlpha;
    private int show;

    private final GamePanel logo;
    private final Label labelCredits;
    private Texture texture;

    public EngineIntroScene() {
        super("EngineIntroScene");
        done = false;
        // --
        try {
            InputStream flexEngineLogo = ResourceController.getResourceAsStream("FlexEngine.png");
            TextureController.getInstance().getTexture("FlexEngine", flexEngineLogo);
            flexEngineLogo.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        // ---
        try {
            InputStream elementalWareLogo = ResourceController.getResourceAsStream("ElementalWare2.png");
            texture = TextureController.getInstance().getTexture("ElementalWare", elementalWareLogo);
            elementalWareLogo.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        // ---
        show = 0;
        alpha = 0.0f;
        updateAlpha = 0.008f;
        // ---
        logo = new GamePanel("Logo", 0, 0) {
            @Override
            public void render(Graphics2D g) {
                super.render(g);
                g.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, transparence));
                if (Objects.nonNull(texture)) {
                    texture.render(g, getX(), getY());
                }
                g.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 1.0f));
            }
        };
        logo.setBorder(false);
        logo.setBounds(
                GameComponent.SCREEN_HORIZONTAL_CENTER_ALIGN,
                GameComponent.SCREEN_VERTICAL_CENTER_ALIGN,
                texture.getWidth(), texture.getHeight());
        logo.setVisible(true);
        add(logo);
        // ---
        labelCredits = new Label(
                "Created by Luann R. Athayde 2013\u00A9 | All rights reserved."
        );
        labelCredits.setForegroundColor(Color.WHITE);
        labelCredits.setFont(FontController.getFont("Arial", 1, 12));
        labelCredits.setSize(640, 30);
        labelCredits.setHorizontalTextAlign(HorizontalTextAlign.HORIZONTAL_ALIGN_CENTER);
        labelCredits.setPosition(
                GameComponent.SCREEN_HORIZONTAL_CENTER_ALIGN,
                GameComponent.SCREEN_VERTICAL_BOTTOM_ALIGN);
        // ---
        add(labelCredits);
        getTimer().start();
    }

    @Override
    public void init(SceneBundle bundle) {
        super.init(bundle);
        desiredUps = EngineHandler.getInstance().getDesireUps();
        EngineHandler.getInstance().setDesireUps(60);
    }

    @Override
    public synchronized void update() {
        super.update();
        // ---
        labelCredits.setHorizontalTextAlign(HorizontalTextAlign.HORIZONTAL_ALIGN_CENTER);
        labelCredits.setPosition(
                GameComponent.SCREEN_HORIZONTAL_CENTER_ALIGN,
                GameComponent.SCREEN_VERTICAL_BOTTOM_ALIGN
        );
        // ---
        alpha += updateAlpha;
        if (alpha >= 1.0f) {
            alpha = 1.0f;
            updateAlpha = -updateAlpha;
        } else if (alpha <= 0) {
            alpha = 0.0f;
            updateAlpha = -updateAlpha;
            show++;
            texture = nextTexture();
        }
        logo.setTransparence(alpha);
        if (Objects.nonNull(texture)) {
            logo.setBounds(
                    GameComponent.SCREEN_HORIZONTAL_CENTER_ALIGN,
                    GameComponent.SCREEN_VERTICAL_CENTER_ALIGN,
                    texture.getWidth(), texture.getHeight()
            );
        }

        if (InputController.getInstance().isKeyPressed(KeyEvent.VK_ENTER) ||
                InputController.getInstance().isKeyPressed(KeyEvent.VK_SPACE)) {
            InputController.getInstance().releaseKey(KeyEvent.VK_ENTER);
            this.done = true;
            SceneController.getInstance().changeScene();
        }
    }

    public Texture nextTexture() {
        if (show == 1) {
            return TextureController.getInstance().getTexture("FlexEngine");
        } else if (show == 2) {
            return createAppNameTexture();
        } else if (show == 3) {
            updateAlpha = 0f;
        } else if (show == 300) {
            done = true;
            SceneController.getInstance().changeScene();
        }
        // ---
        return null;
    }

    protected Texture createAppNameTexture() {
        log.info("Creating texture for current APP...");
        FontRenderContext fontContext = new FontRenderContext(null, true, true);
        String[] parts = GameWindow.getInstance().getTitle().split(" ");
        int height = getViewportHeight();
        int fontSize = height / 10;
        Font basicFont = FontController.getFont(FontController.getDefaultFont(), Font.BOLD, fontSize);
        // ---
        Texture tmp = new Texture("AppLogo", getViewportWidth(), height);
        try {
            Graphics2D g = (Graphics2D) tmp.getImage().getGraphics();
            g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
            g.setFont(basicFont);
            g.setColor(new Color(240, 240, 240));

            Rectangle2D stringBounds = basicFont.getStringBounds("Logo", fontContext);
            int y = height / 2 - ((parts.length / 2) * ((int) stringBounds.getHeight()) / 2);
            for (String text : parts) {
                stringBounds = basicFont.getStringBounds(text, fontContext);
                int x = (getViewportWidth() / 2 - ((int) stringBounds.getWidth()) / 2);
                g.drawString(text, x, y);
                y += ((int) stringBounds.getHeight());
            }

            g.dispose();
        } catch (Exception e) {
            e.printStackTrace();
        }
        // ---
        return tmp;
    }

    @Override
    public synchronized void render(Graphics2D g) {
        super.render(g);
    }

    @Override
    public void end(SceneBundle bundle) {
        super.end(bundle);
        getTimer().stop();
        EngineHandler.getInstance().setDesireUps(desiredUps);
    }

    @Override
    public boolean isVisible() {
        return true;
    }

    public boolean isDone() {
        return done;
    }

}
