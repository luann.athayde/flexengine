/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.flexengine.model.scene;

import net.flexengine.controller.CursorController;
import net.flexengine.controller.FlexGameController;
import net.flexengine.controller.FontController;
import net.flexengine.model.components.Button;
import net.flexengine.model.components.Label;
import net.flexengine.model.components.Separator;
import net.flexengine.model.game.FlexGame;
import net.flexengine.view.GameWindow;

import java.awt.Color;

/**
 *
 * @author Luann Athayde
 */
public class SandBoxScene extends GameScene {

    private Label titulo;
    private Separator separator;

    public SandBoxScene() {
        this("SandBoxScene");
    }

    public SandBoxScene(String nome) {
        super(nome);
    }

    @Override
    public void initComponents(SceneBundle bundle) {
        GameWindow.getInstance().setBackground(new Color(150, 150, 150));
        GameWindow.getInstance().setTitle("FlexEngine - Sandbox Mode");
        titulo = new Label(getLanguage().getValue("sandbox.available_games"));
        titulo.setBounds(10, 10, 900, 80);
        titulo.setFont(FontController.getFont("Arial", 0, 50));
        // --
        separator = new Separator();
        separator.setBounds(10, 95, getViewportWidth() - 20, 2);
        separator.setColor(Color.BLACK);
        // --
        add(titulo);
        add(separator);
        //--
        int tmpX = 30;
        int tmpY = 120;
        int width = (getViewportWidth() - 150) / 4;
        int height = (getViewportHeight() - 150) / 5;
        for (FlexGame game : FlexGameController.getGames().values()) {
            Button gameItem = createGameButton(game, width, height);
            gameItem.setPosition(tmpX, tmpY);
            tmpX += width + 30;
            if (tmpX > getViewportWidth() - width) {
                tmpY += height + 30;
                tmpX = 30;
            }
            add(gameItem);
        }
        // -- show game cursor
        CursorController.getInstance().showGameCursor();
    }

    @Override
    public void init(SceneBundle bundle) {
        super.init(bundle);
    }

    private Button createGameButton(FlexGame game, int width, int height) {
        Button gameItem = new Button(game.getName());
        gameItem.setBounds(0, 0, width, height);
        gameItem.setFont(FontController.getFont("Arial", 1, 20));
        gameItem.setRoundRectSize(0, 0);
        gameItem.setBackgroundColor(Color.DARK_GRAY);
        gameItem.setBackgroundFocusColor(Color.LIGHT_GRAY);
        gameItem.setForegroundColor(Color.WHITE);
        gameItem.setForegroundFocusColor(Color.BLACK);
        gameItem.setHorizontalTextAlign(Button.HORIZONTAL_ALIGN_CENTER);
        gameItem.setBorder(Color.WHITE, 1);
        return gameItem;
    }

    @Override
    public void loadResources() {
        FlexGameController.loadGames();
    }

}
