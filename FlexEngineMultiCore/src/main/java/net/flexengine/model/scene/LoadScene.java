/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.flexengine.model.scene;

import net.flexengine.controller.EngineHandler;
import net.flexengine.controller.FontController;
import net.flexengine.model.components.GameComponent;
import net.flexengine.model.components.HorizontalTextAlign;
import net.flexengine.model.components.Label;
import net.flexengine.model.components.ProgressBar;
import net.flexengine.view.GameWindow;

import java.awt.*;
import java.util.Objects;

/**
 * @author Luann R. Athayde
 * @version 1.0
 * @since 1.0
 */
public class LoadScene extends GameScene {

    public static final String GLOBAL_PROGRESS_KEY = "global.loading.progress";
    public static final String GLOBAL_LOADING_KEY = "global.loading.info";

    protected String credits;
    protected Label labelLoading;
    protected Label labelCredits;
    protected ProgressBar progress;

    private int count;
    private int dots;
    private int countRange;

    public LoadScene() {
        super("LoadScene");
    }

    @Override
    public void initComponents(SceneBundle bundle) {
        // -- label loading...
        labelLoading = new Label();
        labelLoading.setFont(FontController.getFont(FontController.getDefaultFont(), 1, 32));
        this.add(labelLoading);
        // -- load bar widget
        progress = new ProgressBar();
        progress.setColorBackground(new Color(216, 216, 216));
        progress.setColorProgress(new Color(0, 30, 60));
        progress.setInfoPattern(null);
        progress.setRoundRect(4, 4);
        this.add(progress);
        // -- label loading...
        credits = "FlexEngine © 2019 | " + GameWindow.getInstance().getName() + " © 2019 ";
        labelCredits = new Label(credits);
        labelCredits.setHorizontalTextAlign(HorizontalTextAlign.HORIZONTAL_ALIGN_CENTER);
        labelCredits.setFont(FontController.getFont("Monospaced", 1, 10));
        this.add(labelCredits);
    }

    @Override
    public void init(SceneBundle bundle) {
        super.init(bundle);

        if (EngineHandler.getInstance().getDesireUps() >= 120) {
            countRange = 60;
        } else if (EngineHandler.getInstance().getDesireUps() >= 80) {
            countRange = 40;
        } else if (EngineHandler.getInstance().getDesireUps() >= 60) {
            countRange = 30;
        } else if (EngineHandler.getInstance().getDesireUps() >= 30) {
            countRange = 15;
        } else {
            countRange = 5;
        }

        if (Objects.nonNull(labelLoading)) {
            String basicLoadingText = getGlobalBundle().getExtra(GLOBAL_LOADING_KEY);
            if( Objects.isNull(basicLoadingText) ) {
                basicLoadingText = getLanguage().getValue(GLOBAL_LOADING_KEY);
            }
            labelLoading.setText(basicLoadingText);
        }
        if (Objects.nonNull(progress)) {
            progress.setValue(0d);
            progress.setMaxValue(100d);
        }
        if (Objects.nonNull(labelCredits)) {
            labelCredits.setText(credits);
        }
        updatePositions();

    }

    @Override
    public synchronized void update() {
        super.update();
        this.updatePositions();

        if (Objects.nonNull(labelLoading)) {
            count++;
            if (count == countRange) {
                dots++;
                count = 0;
            }
            // ---
            String textLoading = getGlobalBundle().getExtra(GLOBAL_LOADING_KEY);
            textLoading += getDots();
            labelLoading.setText(textLoading);
            // ---
            if (dots >= 3) {
                dots = 0;
            }
        }
        if (Objects.nonNull(progress)) {
            Double value = getGlobalBundle().getExtra(GLOBAL_PROGRESS_KEY);
            if (Objects.nonNull(value) && value >= 0.0d) {
                progress.setValue(value);
            }
        }
        if (Objects.nonNull(labelCredits)) {
            labelCredits.setText(credits);
            labelCredits.setSize(getViewportWidth(), 32);
            labelCredits.setPosition(
                    GameComponent.SCREEN_HORIZONTAL_CENTER_ALIGN,
                    GameComponent.SCREEN_VERTICAL_BOTTOM_ALIGN
            );
        }
    }

    protected void updatePositions() {
        if (Objects.nonNull(labelLoading)) {
            labelLoading.setSize(getViewportWidth() - 200d, 40d);
            labelLoading.setPosition(
                    GameComponent.SCREEN_HORIZONTAL_CENTER_ALIGN, GameComponent.SCREEN_VERTICAL_BOTTOM_ALIGN,
                    0, -120
            );
        }
        if (Objects.nonNull(progress)) {
            progress.setSize(getViewportWidth() - 200d, 8d);
            progress.setPosition(
                    GameComponent.SCREEN_HORIZONTAL_CENTER_ALIGN, GameComponent.SCREEN_VERTICAL_BOTTOM_ALIGN,
                    0, -110);
        }
        if (Objects.nonNull(labelCredits)) {
            labelCredits.setSize(getViewportWidth(), 32);
            labelCredits.setPosition(
                    GameComponent.SCREEN_HORIZONTAL_CENTER_ALIGN, GameComponent.SCREEN_VERTICAL_BOTTOM_ALIGN
            );
        }
    }

    protected String getDots() {
        StringBuilder strDots = new StringBuilder();
        for (int i = 1; i <= dots; i++) {
            strDots.append(".");
        }
        return strDots.toString();
    }

    public ProgressBar getProgress() {
        return progress;
    }

    @Override
    public boolean equals(Object o) {
        return super.equals(o);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

}
