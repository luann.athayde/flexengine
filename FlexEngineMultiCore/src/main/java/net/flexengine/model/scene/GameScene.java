/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.flexengine.model.scene;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import net.flexengine.controller.*;
import net.flexengine.model.components.Button;
import net.flexengine.model.components.GameComponent;
import net.flexengine.model.config.Configuration;
import net.flexengine.model.language.Language;
import net.flexengine.view.GameWindow;
import net.flexengine.view.Texture;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import java.awt.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * @author Luann R. Athayde
 * @version 1.0
 * @since 1.0
 */
@Data
@Slf4j
@Builder
@AllArgsConstructor
public class GameScene implements Updatable, Renderable, Initializable, Finalizable {

    private final ScriptEngineManager scriptEngineManager = new ScriptEngineManager();

    private String name;
    private List<Object> sceneItems;
    private GameWindow gameWindow;
    private TimeController timer;
    private boolean visible;

    private SceneBundle globalBundle;
    private Language language;

    private Texture background;
    private Texture backgroundStretched;
    private boolean stretchBackground;
    private int itemIDS;

    public GameScene() {
        this(null);
    }

    public GameScene(String name) {
        this.name = name;
        this.sceneItems = new ArrayList<>();
        this.gameWindow = GameWindow.getInstance();
        this.timer = TimeController.newInstance();
        this.language = LanguageController.getDefaultLanguage();
        // ---
        this.itemIDS = 1;
        this.visible = true;
        this.background = null;
        this.backgroundStretched = null;
        this.stretchBackground = true;
        // ---
        if (Objects.isNull(this.name)) {
            this.name = getClass().getSimpleName();
        }
    }

    // -------------------------------------------------------------------------
    public void add(Object obj) {
        if (obj instanceof Renderable || obj instanceof Updatable || obj instanceof Shape) {
            if (obj instanceof GameComponent) {
                GameComponent gc = (GameComponent) obj;
                gc.setId(itemIDS);
                itemIDS++;
            }
            sceneItems.add(obj);
        }
    }

    public Object get(int index) {
        return sceneItems.get(index);
    }

    public Object remove(int index) {
        return sceneItems.remove(index);
    }

    public boolean remove(Object obj) {
        return sceneItems.remove(obj);
    }

    public boolean isEmpty() {
        return sceneItems.isEmpty();
    }

    public int size() {
        return sceneItems.size();
    }

    public void clear() {
        clear(new SceneBundle());
    }

    protected void finalize(SceneBundle bundle) {
        for (Object item : sceneItems) {
            if (item instanceof Finalizable) {
                Finalizable fItem = (Finalizable) item;
                fItem.end(bundle);
            }
        }
    }

    public void clear(SceneBundle bundle) {
        finalize(bundle);
        sceneItems.clear();
    }

    public List<Object> getItems() {
        return sceneItems;
    }

    public <T> List<T> getItems(Class<? extends T> clazz) {
        List<T> list = new ArrayList<>();
        // ---
        for (Object sceneItem : sceneItems) {
            if (Objects.equals(clazz, sceneItem.getClass())) {
                list.add(clazz.cast(sceneItem));
            }
        }
        return list;
    }
    // -------------------------------------------------------------------------

    public void initComponents(SceneBundle bundle) {
        // --
    }

    @Override
    public void init(SceneBundle bundle) {
        // ---
        if (getName().equalsIgnoreCase("GameScene")) {
            setName(getClass().getSimpleName());
        }
        // ---
        for (int i = 0; i < sceneItems.size(); i++) {
            Object obj = sceneItems.get(i);
            if (obj instanceof Initializable) {
                Initializable item = (Initializable) obj;
                item.init(bundle);
            }
        }
        timer.start();
        // ---
        loadScript(bundle);
        runScript(bundle);
        updateBackgroundTexture();
    }

    protected void loadScript(SceneBundle bundle) {
        // ---
        try (InputStream in = ResourceController.getResourceAsStream(getName() + ".js")) {
            if (Objects.nonNull(in)) {
                log.info("Loading script for [" + getName() + "]...");
                bundle.putExtra("script", readData(in));
            }
        } catch (Exception e) {
            log.error(e.toString());
        }
        // ---
        if (Objects.isNull(bundle.getExtra("script"))) {
            File scriptFile = new File(ResourceController.getScriptPathFor(getName() + ".js"));
            if (scriptFile.exists() && scriptFile.canRead()) {
                try (InputStream in = new FileInputStream(scriptFile)) {
                    log.info("Loading script for [" + getName() + "]...");
                    bundle.putExtra("script", readData(in));
                } catch (Exception ex) {
                    log.error(ex.toString());
                }
            }
        }
        // ---
    }

    private String readData(InputStream in) throws IOException {
        int read, totalRead = 0;
        byte[] buffer = new byte[1024];
        StringBuilder data = new StringBuilder();
        while ((read = in.read(buffer, 0, buffer.length)) > 0) {
            totalRead += read;
            data.append(new String(buffer, 0, read, StandardCharsets.UTF_8));
            if (totalRead > 10240) {
                throw new RuntimeException("Script file to large! Max file size: 10KB ");
            }
        }
        return data.toString();
    }

    protected void runScript(SceneBundle bundle) {
        try {
            String script = bundle.getExtra("script");
            if (Objects.isNull(script) || script.isEmpty()) return;

            ScriptEngine engine = scriptEngineManager.getEngineByName("javascript");
            engine.put("flexEngine", FlexEngine.getInstance());
            engine.put("scene", this);
            engine.put("bundle", bundle);
            engine.put("log", log);

            engine.eval(script);

            Object result = engine.get("result");
            bundle.putExtra("scriptResult", result);

        } catch (Exception e) {
            log.error("Fail to run script for scene: {}", e, e);
        }
    }

    @Override
    public void end(SceneBundle bundle) {
        finalize(bundle);
        timer.stop();
    }

    @Override
    public synchronized void update() {
        // ---
        this.language = LanguageController.getDefaultLanguage();
        // ---
        sceneItems.sort((Object o1, Object o2) -> {
            if (o1 instanceof GameComponent) {
                GameComponent gc1 = (GameComponent) o1;
                if (o2 instanceof GameComponent) {
                    GameComponent gc2 = (GameComponent) o2;
                    if (Objects.nonNull(gc1.getPriority()) && Objects.nonNull(gc2.getPriority())) {
                        return gc1.getPriority().compareTo(gc2.getPriority());
                    }
                } else {
                    return 1;
                }
            }
            return 0;
        });
        // ---
        for (int i = 0; i < sceneItems.size(); i++) {
            Object item = sceneItems.get(i);
            if (item instanceof Updatable) {
                Updatable uItem = (Updatable) item;
                uItem.update();
            }
        }
        // ---
        updateBackgroundTexture();
        // ---
    }

    protected void updateBackgroundTexture() {
        if (stretchBackground && Objects.nonNull(background)) {
            if (backgroundStretched == null
                    || backgroundStretched.getWidth() != getViewportWidth()
                    || backgroundStretched.getHeight() != getViewportHeight()) {
                log.info("Resizing background to viewport ["
                        + getViewportWidth() + "," + getViewportHeight() + "]...");
                backgroundStretched = background.scale(getViewportWidth(), getViewportHeight());
            }
        }
    }

    @Override
    public synchronized void render(Graphics2D g) {
        if (Objects.nonNull(backgroundStretched)) {
            backgroundStretched.render(g, 0, 0);
        } else if (Objects.nonNull(background)) {
            background.render(g, 0, 0);
        }
        // --
        for (int i = 0; i < sceneItems.size(); i++) {
            Object item = sceneItems.get(i);
            if (item instanceof Renderable) {
                Renderable rItem = (Renderable) item;
                if (rItem.isVisible()) {
                    rItem.render(g);
                }
            } else if (item instanceof Shape) {
                g.draw((Shape) item);
            }
        }
        // ---
    }

    // ---
    public void loadResourcesOnce() {
    }

    public void loadResources() {
    }
    // ---

    public void sleep(long milis) {
        try {
            Thread.sleep(milis);
        } catch (Exception e) {
            log.warn("### Fail to sleep!", e);
            Thread.currentThread().interrupt();
        }
    }

    public void disableAllButtons() {
        for (Button button : getItems(Button.class)) {
            button.setEnable(false);
        }
    }

    public void enableAllButtons() {
        for (Button button : getItems(Button.class)) {
            button.setEnable(true);
        }
    }

    public void setBackground(Texture texture) {
        this.background = texture;
    }

    public void setBackground(String backgroundName) {
        this.background = TextureController.getInstance().getTexture(backgroundName);
    }

    public Language getLanguage() {
        if (Objects.isNull(language)) {
            language = LanguageController.getDefaultLanguage();
        }
        return language;
    }

    public int getViewportWidth() {
        return RenderController.getInstance().getViewportWidth();
    }

    public int getViewportHeight() {
        return RenderController.getInstance().getViewportHeight();
    }

    public Configuration getConfig() {
        return FlexEngine.getInstance().getConfig();
    }

    @Override
    public String toString() {
        return name;
    }

}
