package net.flexengine.model.scene;

import net.flexengine.view.Texture;

import java.awt.*;
import java.util.Objects;

/**
 *
 */
public class InfinityProgressLoadScene extends LoadScene {

    Color bgColor;
    Color lineColor;

    int moveX;
    int speedX;
    int x = 0;
    int y = 0;
    int width;
    int height;
    int round;
    int ovalSize;
    int ovalEffect;
    boolean showOvalEffect;
    int effectTime;
    int screenSpace;

    @Override
    public void initComponents(SceneBundle bundle) {
        clear();
        bgColor = new Color(10, 80, 120);
        lineColor = new Color(160, 160, 160);
        moveX = 0;
        speedX = 1;
        // ---
        width = getViewportWidth() / 100;
        if (width < 160) {
            width = 160;
        }
        height = round = 12;
        ovalSize = height * 2;
        ovalEffect = 0;
        screenSpace = 120;
    }

    @Override
    public void init(SceneBundle bundle) {
        super.init(bundle);
    }

    @Override
    public void setBackground(Texture backgroundTexture) {
        if (Objects.nonNull(backgroundTexture)) {
            // ---
            Texture bgTmp = new Texture(
                    backgroundTexture.getName(), backgroundTexture.getWidth(), backgroundTexture.getHeight()
            );
            Graphics2D g = bgTmp.getImage().createGraphics();
            g.clearRect(0, 0, bgTmp.getWidth(), bgTmp.getHeight());
            g.drawImage(backgroundTexture.getImage(), 0, 0, null);
            g.setColor(Color.BLACK);
            g.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.7f));
            g.fillRect(0, 0, bgTmp.getWidth(), bgTmp.getHeight());
            g.dispose();
            // ---
            super.setBackground(bgTmp);
        }
    }

    @Override
    public synchronized void render(Graphics2D gBuffer) {
        super.render(gBuffer);

        x = y = 0;

        gBuffer.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        gBuffer.setRenderingHint(RenderingHints.KEY_ALPHA_INTERPOLATION, RenderingHints.VALUE_ALPHA_INTERPOLATION_QUALITY);
        gBuffer.setRenderingHint(RenderingHints.KEY_COLOR_RENDERING, RenderingHints.VALUE_COLOR_RENDER_QUALITY);

        // ---
        // Basic bar...
        gBuffer.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.40f));
        gBuffer.setColor(bgColor);
        gBuffer.fillRoundRect(
                getViewportWidth() - width - screenSpace,
                getViewportHeight() - height - screenSpace,
                width, height,
                round, round);

        gBuffer.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.80f));
        gBuffer.setColor(lineColor);
        gBuffer.drawRoundRect(
                getViewportWidth() - width - screenSpace,
                getViewportHeight() - height - screenSpace,
                width, height,
                round, round);

        // ---
        // Bubble
        x += moveX;
        int tmpOvalWidth = ovalSize - ovalEffect;
        int tmpOvalHeight = ovalSize + ovalEffect;
        y -= tmpOvalHeight / 2 + tmpOvalHeight / 4;

        gBuffer.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.60f));
        gBuffer.setColor(bgColor);
        gBuffer.fillOval(
                getViewportWidth() - width - screenSpace + x,
                getViewportHeight() - screenSpace + y,
                tmpOvalWidth, tmpOvalHeight
        );

        gBuffer.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.90f));
        gBuffer.setColor(lineColor);
        gBuffer.drawOval(
                getViewportWidth() - width - screenSpace + x,
                getViewportHeight() - screenSpace + y,
                tmpOvalWidth, tmpOvalHeight
        );

        // ---
        gBuffer.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF);
        gBuffer.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 1f));
    }

    @Override
    public synchronized void update() {
        super.update();
        moveX += speedX;
        if (moveX > (width - ovalSize) || moveX <= 0) {
            speedX = -speedX;
        }

    }

}
