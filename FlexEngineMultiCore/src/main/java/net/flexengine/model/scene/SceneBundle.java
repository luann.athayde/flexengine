/**
 *
 */
package net.flexengine.model.scene;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Luann R. Athayde
 * @version 1.0
 * @since 1.0
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SceneBundle implements Serializable {

    protected final Map<String, Object> extras = new HashMap<>();
    protected String bundleName;

    public <T> SceneBundle putExtra(String key, T value) {
        extras.put(key, value);
        return this;
    }

    public <T> T getExtra(String key) {
        return (T) extras.get(key);
    }

    public void clear() {
        extras.clear();
    }

    @Override
    public String toString() {
        return "SceneBundle{" + "extras=" + extras + '}';
    }

}
