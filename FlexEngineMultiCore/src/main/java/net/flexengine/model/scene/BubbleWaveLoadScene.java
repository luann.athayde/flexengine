/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.flexengine.model.scene;

import net.flexengine.controller.*;
import net.flexengine.model.config.Priority;

import java.awt.*;

/**
 * @author Luann Athayde
 */
public class BubbleWaveLoadScene extends LoadScene {

    @Override
    public void initComponents(SceneBundle bundle) {
        add(new CircleWave(30));
        add(new CircleWave(50));
        add(new CircleWave(70));
    }

    class CircleWave implements Updatable, Renderable, Initializable, Finalizable {

        volatile int circleX, circleY;
        volatile int circleSize;
        volatile int sizeGap;
        volatile float alpha;

        public CircleWave(int circleSize) {
            this.circleSize = circleSize;
            this.circleX = getViewportWidth() / 2 - circleSize / 2;
            this.circleY = getViewportHeight() - circleSize / 2 - 100;
            this.sizeGap = 0;
            this.alpha = 1f;
        }

        @Override
        public void init(SceneBundle bundle) {
            if (EngineHandler.getInstance().getDesireUps() >= 120) {
                this.sizeGap = 1;
            } else if (EngineHandler.getInstance().getDesireUps() >= 60) {
                this.sizeGap = 2;
            } else if (EngineHandler.getInstance().getDesireUps() >= 30) {
                this.sizeGap = 3;
            } else if (EngineHandler.getInstance().getDesireUps() >= 15) {
                this.sizeGap = 4;
            } else if (EngineHandler.getInstance().getDesireUps() >= 1) {
                this.sizeGap = 5;
            }
        }

        @Override
        public void update() {
            circleX = getViewportWidth() / 2 - circleSize / 2;
            this.circleY = getViewportHeight() - circleSize / 2 - 100;
            // --
            circleSize += sizeGap;
            if (circleSize >= 100) {
                circleSize = 0;
                alpha = 1f;
            }
            alpha -= (sizeGap / 100f);
            alpha = alpha < 0 ? 0 : alpha;
        }

        @Override
        public void render(Graphics2D g) {
            g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

            g.setColor(Color.WHITE);
            g.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, alpha));
            g.drawOval(circleX, circleY, circleSize, circleSize);
            g.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 1f));

            g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF);
        }

        @Override
        public void end(SceneBundle bundle) {
        }

        public Priority getPriority() {
            return Priority.NORMAL_PRIORITY;
        }

        @Override
        public boolean isVisible() {
            return true;
        }

    }

}
