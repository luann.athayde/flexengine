/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.flexengine.model.config;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

import java.io.Serializable;

/**
 * @author Luann R. Athayde
 * @version 1.0
 * @since 1.0
 */
@Data
@Root
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class FlexDisplayMode implements Serializable {

    @Element
    private int width;
    @Element
    private int height;
    @Element
    private int bitDepth;
    @Element
    private int refreshRate;

    @Override
    public String toString() {
        return format(width, height, bitDepth, refreshRate);
    }

    public static String format(int width, int height, int bitDepth, int refreshRate) {
        String format = width + "x" + height;
        if (bitDepth > 0) {
            format += "x" + bitDepth;
        }
        format += " " + refreshRate + "Hz";
        return format;
    }

}
