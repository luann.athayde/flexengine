/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.flexengine.model.config;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import net.flexengine.controller.LanguageController;
import net.flexengine.controller.ResourceController;
import net.flexengine.controller.SoundController;
import net.flexengine.controller.server.FlexEngineServer;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

import java.io.Serializable;

/**
 * @author Luann R. Athayde
 * @version 1.0
 * @since 1.0
 */
@Data
@Root
@Builder
@AllArgsConstructor
public class Configuration implements Serializable {

    @Element
    private FlexDisplayMode resolution;
    @Element
    private boolean fullScreen;
    @Element
    private boolean antialising;
    @Element
    private boolean textAntialising;
    @Element
    private String language;
    @Element
    private String resourcesPath;
    @Element
    private boolean sound;
    @Element
    private float generalVolume;
    @Element
    private float musicVolume;
    @Element
    private float effectVolume;
    @Element
    private int channels;
    @Element
    private String host;
    @Element
    private int port;
    @Element
    private int maxConnections;
    @Element
    private boolean confirmOnClose;

    public Configuration() {
        this(new FlexDisplayMode(1280, 720, -1, 60),
                false,
                false, true,
                LanguageController.getDefaultLanguageName(),
                ResourceController.DEFAULT_PATH,
                SoundController.getInstance().isEnable(),
                100f, 100f, 100f, 32,
                "localhost",
                FlexEngineServer.DEFAULT_PORT,
                FlexEngineServer.MAX_USERS_CONNECTION,
                Boolean.TRUE
        );
    }

}
