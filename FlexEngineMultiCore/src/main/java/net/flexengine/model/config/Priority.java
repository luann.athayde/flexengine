/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.flexengine.model.config;

import lombok.Getter;

import java.io.Serializable;
import java.util.Objects;

/**
 * @author Luann R. Athayde
 * @version 1.0
 * @since 1.0
 */
@Getter
public class Priority implements Serializable, Comparable<Priority> {

    public static Priority CUSTOM_PRIORITY = new Priority("CUSTOM_PRIORITY", -1);
    public static Priority LOW_PRIORITY = new Priority("LOW_PRIORITY", 0);
    public static Priority NORMAL_PRIORITY = new Priority("NORMAL_PRIORITY", 1);
    public static Priority HIGH_PRIORITY = new Priority("HIGH_PRIORITY", 2);

    private final String name;
    private int level;

    public Priority(String name, int val) {
        this.name = name;
        this.level = val;
    }

    public Priority setLevel(int val) {
        if (this.level >= 3) {
            this.level = val;
        } else {
            this.level = -1;
        }
        return this;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.name);
        hash = 97 * hash + this.level;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Priority other = (Priority) obj;
        if (this.level != other.level) {
            return false;
        }
        return Objects.equals(this.name, other.name);
    }

    @Override
    public int compareTo(Priority o) {
        if (getLevel() < o.getLevel()) {
            return -1;
        }
        if (getLevel() > o.getLevel()) {
            return 1;
        }
        return 0;
    }

    @Override
    public String toString() {
        return getName() + "(" + getLevel() + ")";
    }


}
