/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.flexengine.model.time;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.flexengine.controller.TimeController;

/**
 * @author Luann R. Athayde
 * @version 1.0
 * @since 1.0
 */
@Data
@Slf4j
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DateTime {

    public long day;
    public long month;
    public long year;

    public long hour;
    public long minutes;
    public long seconds;

    public DateTime(long hour, long minute, long seconds) {
        this.day = 0;
        this.month = 0;
        this.year = 0;
        this.hour = hour;
        this.minutes = minute;
        this.seconds = seconds;
    }

    public void setHour(long hour) {
        if (hour > 24) {
            hour = 24;
        } else if (hour < 0) {
            hour = 0;
        }
        this.hour = hour;
    }

    public void setMinutes(long minutes) {
        if (minutes > 60) {
            minutes = 60;
        } else if (minutes < 0) {
            minutes = 0;
        }
        this.minutes = minutes;
    }

    public void setSeconds(long seconds) {
        if (seconds > 60) {
            seconds = 60;
        } else if (seconds < 0) {
            seconds = 0;
        }
        this.seconds = seconds;
    }

    public void setDay(long day) {
        if (day > 366) {
            day = 366;
        } else if (day < 0) {
            day = 0;
        }
        this.day = day;
    }

    public void setMonth(long month) {
        if (month > 12) {
            month = 12;
        } else if (month < 0) {
            month = 0;
        }
        this.month = month;
    }

    public void setYear(long year) {
        if (year < 0) {
            year = 0;
        }
        this.year = year;
    }

    /**
     * Receive the complete time in milliseconds...
     *
     * @param time current time
     */
    public DateTime setTime(double time) {
        double tmpSeconds = time / TimeController.MILI_SECOND;
        double tmpMinutes = tmpSeconds / 60;
        tmpSeconds = tmpSeconds % 60;
        double hours = tmpMinutes / 60;
        tmpMinutes = tmpMinutes % 60;
        double days = hours / 24;
        hours = hours % 24;
        double months = days / 30;
        days = days % 30;
        double years = months / 12;
        months = months % 12;
        setSeconds((long) tmpSeconds);
        setMinutes((long) tmpMinutes);
        setHour((long) hours);
        setDay((long) days);
        setMonth((long) months);
        setYear((long) years);
        return this;
    }

    public String toTimeString() {
        String h = "" + hour;
        if (hour < 10) {
            h = "0" + h;
        }
        String m = "" + minutes;
        if (minutes < 10) {
            m = "0" + m;
        }
        String s = "" + seconds;
        if (seconds < 10) {
            s = "0" + s;
        }
        return h + "h:" + m + "m:" + s + "s";
    }

    public String toDateString() {
        return year + "y " + month + "m " + day + "d";
    }

    @Override
    public String toString() {
        return toDateString() + " " + toTimeString();
    }

}
