/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.flexengine.model.sound;

/**
 * @author Luann R. Athayde
 * @version 1.0
 * @since 1.0
 */
public class MaxSimultaneousSoundException extends Exception {

    public MaxSimultaneousSoundException() {
    }

    public MaxSimultaneousSoundException(String message) {
        super(message);
    }
    
}
