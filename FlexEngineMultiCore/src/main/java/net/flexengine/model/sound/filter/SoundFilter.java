/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.flexengine.model.sound.filter;

import lombok.extern.slf4j.Slf4j;

/**
 * @author Luann R. Athayde
 * @version 1.0
 * @since 1.0
 */
@Slf4j
public abstract class SoundFilter {

    public abstract void filter(byte[] buffer, int offset, int lenght);

    public void reset() {
        // -- do nothing...
    }

    public int getRemainingSize() {
        return 0;
    }

    public void filter(byte[] samples) {
        filter(samples, 0, samples.length);
    }

    public static short getSample(byte[] samples, int position) {
        short ret = 0;
        try {
            ret = (short) (
                    ((samples[position + 1] & 0xff) << 8) |
                            (samples[position] & 0xff));
        } catch (Exception e) {
            log.error("Fail to get sound sample!", e);
        }
        return ret;
    }

    public static void setSample(byte[] buffer, int position, short sample) {
        try {
            buffer[position] = (byte) (sample & 0xff);
            buffer[position] = (byte) ((sample >> 8) & 0xff);
        } catch (Exception e) {
            log.error("Fail to set sound sample!", e);
        }
    }

}
