/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.flexengine.model.sound;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.simpleframework.xml.Root;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.UnsupportedAudioFileException;
import java.io.DataInputStream;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.Objects;

/**
 * @author Luann R. Athayde
 * @version 1.0
 * @since 1.0
 */
@Data
@Slf4j
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Root(name = "sound")
public class Sound implements Serializable {

    /**
     * Sound type unknown...
     */
    public static final int SOUND_TYPE_UNK = 0x0000;
    /**
     * Sound type OGG...
     */
    public static final int SOUND_TYPE_OGG = 0x000a;
    /**
     * Sound type WAV or WAVE...
     */
    public static final int SOUND_TYPE_WAV = 0x000b;
    /**
     * Sound type MIDI...
     */
    public static final int SOUND_TYPE_MID = 0x000c;
    /**
     * Sound type MP3...
     */
    public static final int SOUND_TYPE_MP3 = 0x000d;
    /**
     * Sound type M4A...
     */
    public static final int SOUND_TYPE_M4A = 0x000e;
    // ---
    private String soundName;
    private String soundPath;
    private File soundFile;
    private int soundType;
    // ---
    private transient byte[] samples;
    private transient AudioInputStream audioStream;
    private transient AudioFormat soundFormat;

    public Sound(String soundPath) {
        this(null, soundPath, false);
    }

    public Sound(String soundName, String soundPath) {
        this(soundName, soundPath, false);
    }

    public Sound(String soundName, String soundPath, boolean load) {
        this.soundPath = soundPath;
        this.soundName = soundName;
        if (Objects.isNull(soundName) && Objects.nonNull(soundPath)) {
            File fSound = new File(soundPath);
            this.soundName = fSound.getName();
        }
        if (load) {
            loadSound();
        }
    }

    public final void loadSound() {
        try {
            if (soundPath.endsWith(".ogg")) {
                soundType = SOUND_TYPE_OGG;
            } else if (soundPath.endsWith(".wav")) {
                soundType = SOUND_TYPE_WAV;
            } else if (soundPath.endsWith(".midi") || soundPath.endsWith(".mid")) {
                soundType = SOUND_TYPE_MID;
            } else if (soundPath.endsWith(".mp3")) {
                soundType = SOUND_TYPE_MP3;
            } else if (soundPath.endsWith(".m4a")) {
                soundType = SOUND_TYPE_M4A;
            } else {
                soundType = SOUND_TYPE_UNK;
            }
            // -- ./resources/
            this.soundFile = new File(soundPath);
            if (soundType != SOUND_TYPE_MP3
                    && soundType != SOUND_TYPE_OGG
                    && soundType != SOUND_TYPE_M4A) {
                // -- get sound stream...
                audioStream = AudioSystem.getAudioInputStream(this.soundFile);
                // -- get sound format...
                soundFormat = audioStream.getFormat();
                // -- get sound samples...
                samples = Sound.getSamples(audioStream, soundFormat);
            } else {
                audioStream = null;
                soundFormat = null;
                samples = null;
            }
        } catch (UnsupportedAudioFileException ue) {
            log.error(" - Unsupported audio file [" + this.soundPath + "]...\n"
                    + "\tException: " + ue);
        } catch (IOException ioe) {
            log.error(" - IO Exception in Sound constructor...\n"
                    + "\tException: " + ioe);
        } catch (NullPointerException npe) {
            log.error(" - NullPointer exception...\n"
                    + "\tException: " + npe);
        }
    }

    public String getSoundName() {
        return soundName;
    }

    public void setSoundName(String soundName) {
        this.soundName = soundName;
    }

    public File getSoundFile() {
        return soundFile;
    }

    public AudioInputStream getAudioStream() {
        return audioStream;
    }

    public AudioFormat getSoundFormat() {
        return soundFormat;
    }

    public byte[] getSamples() {
        return samples;
    }

    public int getSoundType() {
        return soundType;
    }

    public static String getTypeName(int soundType) {
        switch (soundType) {
            case SOUND_TYPE_MID:
                return "Midi";
            case SOUND_TYPE_MP3:
                return "mp3";
            case SOUND_TYPE_OGG:
                return "ogg";
            case SOUND_TYPE_WAV:
                return "wav";
            case SOUND_TYPE_UNK:
            default:
                return "Unknown";
        }
    }

    public static byte[] getSamples(AudioInputStream inStream, AudioFormat format) {
        try {
            int len = (int) (inStream.getFrameLength() * format.getFrameSize());
            byte[] nSamples = new byte[len];
            DataInputStream dataStream = new DataInputStream(inStream);
            dataStream.readFully(nSamples);
            return nSamples;
        } catch (Exception e) {
            log.error("Fail to get samples - " + e.toString());
        }
        return null;
    }

    @Override
    public String toString() {
        String sType = "0x" + Integer.toHexString(soundType).toUpperCase();
        return "Sound [" + soundName + "[" + soundPath + "], type = " + sType + ']';
    }

}
