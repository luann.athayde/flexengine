/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.flexengine.model.sound;

import javazoom.jl.decoder.JavaLayerException;
import javazoom.jl.player.advanced.AdvancedPlayer;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import javax.sound.sampled.*;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Objects;

/**
 * @author Luann R. Athayde
 * @version 1.0
 * @since 1.0
 */
@Data
@Slf4j
@Builder
@AllArgsConstructor
public class SoundPlayer {

    private boolean paused;
    private boolean playing;
    private AdvancedPlayer aPlayer;

    private Sound sound;
    private AudioFormat format;

    public SoundPlayer() {
        this(null);
    }

    public SoundPlayer(Sound sound) {
        this.paused = false;
        this.playing = false;
        this.aPlayer = null;
        try {
            this.sound = sound;
            this.format = sound.getSoundFormat();
        } catch (Exception e) {
            log.error(getClass().getSimpleName() + ": " + e);
        }
    }

    public synchronized void play(InputStream source) {
        //----------------------------------------------------------------------
        try {
            // ---
            // Create a source line to play...
            int bufferSize = format.getFrameSize() * Math.round(format.getSampleRate() / 10);
            byte[] buffer = new byte[bufferSize];
            DataLine.Info info = new DataLine.Info(SourceDataLine.class, format);
            try (SourceDataLine line = (SourceDataLine) AudioSystem.getLine(info)) {
                line.open(format, bufferSize);

                // -- start line...
                line.start();

                // copy data to line...
                playing = true;
                int numBytesRead = 0;
                while (numBytesRead != -1 && playing) {
                    numBytesRead = source.read(buffer, 0, buffer.length);
                    if (numBytesRead != -1) {
                        if (paused) {
                            wait();
                        }
                        line.write(buffer, 0, numBytesRead);
                    }
                }

                // wait until all data is played...
                line.drain();
                // stop...
                line.stop();
                // --
            }
            // ---
        }  catch (InterruptedException itre) {
            log.error("Critical error ocurred while playing sound: {}", itre, itre);
            Thread.currentThread().interrupt();
        } catch (Exception e) {
            try {
                aPlayer = new AdvancedPlayer(source);
                aPlayer.play();
            } catch (Exception ex) {
                log.error("play(): Error with try playing [" + sound + "]...", ex);
            }
        } finally {
            try {
                source.close();
            } catch (IOException ioe) {
                log.error("Fail to close sound stream!", ioe);
            }
        }
    }

    public synchronized void play() {
        paused = false;
        this.notifyAll();
        try {
            aPlayer.play();
        } catch (JavaLayerException ex) {
            log.error("play(): " + ex);
        }
    }

    public void stop() {
        playing = false;
        if (Objects.nonNull(aPlayer)) {
            aPlayer.close();
        }
    }

    public void pause() {
        paused = true;
        if (Objects.nonNull(aPlayer)) {
            aPlayer.stop();
        }
    }

}
