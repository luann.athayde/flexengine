/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.flexengine.controller;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.flexengine.view.Sprite;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * @author Luann R. Athayde
 * @version 1.0
 * @since 1.0
 */
@Data
@Slf4j
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SpriteController {

    protected final Map<String, Sprite> sprites = new HashMap<>();
    protected boolean loadOnStartup;

    public void loadSprites() {
        log.info("Loading sprites...");
    }

    private static SpriteController instance;

    public static SpriteController getInstance() {
        if (Objects.isNull(instance)) {
            instance = new SpriteController();
        }
        return instance;
    }

}
