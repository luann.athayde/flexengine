/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.flexengine.controller;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import net.flexengine.model.config.Configuration;
import org.simpleframework.xml.core.Persister;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Objects;
import java.util.ResourceBundle;

/**
 * @author Luann R. Athayde
 * @version 1.0
 * @since 1.0
 */
@Data
@Slf4j
@Builder
@AllArgsConstructor
public class ConfigurationController {

    public static final ResourceBundle CONFIG_BUNDLE = ResourceBundle.getBundle("configuration");
    public static final String CONFIG_FILE = "configuration.flex";

    private final Persister persister;
    private Configuration defaultConfiguration;

    public ConfigurationController() {
        persister = new Persister();
        defaultConfiguration = null;
    }

    public boolean saveConfiguration() {
        return saveConfiguration(defaultConfiguration);
    }

    public boolean saveConfiguration(Configuration config) {
        try {
            File resourcesFolder = new File(ResourceController.getCurrentPath());
            if (!resourcesFolder.exists()) {
                log.info("Creating [" + resourcesFolder + "] folder...");
                resourcesFolder.mkdir();
            }
            File configFile = new File(ResourceController.getLocale(CONFIG_FILE));
            persister.write(config, new FileOutputStream(configFile));
            return true;
        } catch (Exception e) {
            log.error("Failed to save configuration {} to file: {}", e, config, e);
        }
        return false;
    }

    public Configuration getDefaultConfiguration() {
        if( Objects.isNull(defaultConfiguration) ) {
            defaultConfiguration = loadConfiguration();
        }
        return defaultConfiguration;
    }

    public void setDefaultConfiguration(Configuration defaultConfiguration) {
        this.defaultConfiguration = defaultConfiguration;
    }

    private Configuration loadConfiguration() {
        try {
            log.debug("Loading configuration...");
            Configuration config = persister.read(
                    Configuration.class,
                    new FileInputStream(ResourceController.getLocale(CONFIG_FILE))
            );
            log.debug("Configuration loaded = " + config);
            return config;
        } catch (Exception e) {
            log.error("Failed to load configuration file: {}", e, e);
            defaultConfiguration = new Configuration();
            saveConfiguration();
            return loadConfiguration();
        }
    }

    private static ConfigurationController instance;

    public static ConfigurationController getInstance() {
        if (Objects.isNull(instance)) {
            instance = new ConfigurationController();
        }
        return instance;
    }

}
