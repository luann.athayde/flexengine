/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.flexengine.controller;

/**
 * @author Luann R. Athayde
 * @version 1.0
 * @since 1.0
 */
public interface Updatable {

    void update();

}
