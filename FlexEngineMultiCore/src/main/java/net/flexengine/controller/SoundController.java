/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.flexengine.controller;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import net.flexengine.model.components.ProgressBar;
import net.flexengine.model.sound.LoopByteArrayInputStream;
import net.flexengine.model.sound.MaxSimultaneousSoundException;
import net.flexengine.model.sound.Sound;
import net.flexengine.model.sound.SoundPlayer;
import net.flexengine.model.sound.filter.FilteredSoundStream;
import net.flexengine.model.sound.filter.SoundFilter;

import java.io.*;
import java.util.*;

/**
 * @author Luann R. Athayde
 * @version 1.0
 * @since 1.0
 */
@Data
@Slf4j
@Builder
@AllArgsConstructor
public class SoundController {

    public static final int DEFAULT_MAX_SIMULTANEOUS_SOUNDS = 1024;

    protected static final List<String> VALID_SOUND_FORMATS = Arrays.asList(
            "ogg", "wav", "wave", "mid",
            "midi", "mp3", "m4a");

    private final ArrayList<SoundPlayer> players;
    private final HashMap<String, Sound> soundsMap;

    private int maxSimultaneousSounds;
    private boolean enable;

    public SoundController() {
        maxSimultaneousSounds = DEFAULT_MAX_SIMULTANEOUS_SOUNDS;
        players = new ArrayList<>();
        soundsMap = new HashMap<>();
        enable = true;
    }

    private void recursiveLoad(File[] files, ProgressBar pBar) {
        if (Objects.nonNull(files)) {
            for (File tmpFile : files) {
                if (tmpFile.isDirectory()) {
                    File[] fTmp = tmpFile.listFiles();
                    recursiveLoad(fTmp, pBar);
                } else if (tmpFile.isFile() && isValidSound(tmpFile)) {
                    Sound sound = new Sound(tmpFile.getName(), tmpFile.getAbsolutePath(), true);
                    soundsMap.put(tmpFile.getName(), sound);
                    log.info("{}", sound);
                    if (Objects.nonNull(pBar)) {
                        pBar.increment();
                    }
                }
            }
        }
    }

    /**
     * Load all sounds in recursive mode conatis in ./resources/folder and
     * subfolders...
     *
     * @param folder
     * @param pBar
     */
    public void loadSounds(final String folder, ProgressBar pBar) {
        log.info("Loading all sounds{}{}", folder.isEmpty() ? "" : " from ["+folder+"]", "...");
        try {
            File fRes = new File(ResourceController.getLocale(folder));
            pBar.setMaxValue(soundsToLoad(folder));
            File[] fSounds = fRes.listFiles();
            recursiveLoad(fSounds, pBar);
        } catch (Exception e) {
            log.error("SoundController::loadSounds(" + folder + "): " + e);
            FlexEngine.getInstance().stop();
        }
    }

    /**
     * Load all sounds in recursive mode conatis in ./resources/folder and
     * subfolders...
     *
     * @param folder
     */
    public void loadSounds(final String folder) {
        loadSounds(folder, new ProgressBar());
    }

    /**
     * Load all sounds in recursive mode contains in ./resources/ and
     * subfolders...
     */
    public void loadSounds() {
        loadSounds("");
    }

    private long recursiveCount(File[] files) {
        long count = 0;
        if (Objects.nonNull(files)) {
            for (File file : files) {
                if (file.isFile()) {
                    if (isValidSound(file)) {
                        count++;
                    }
                } else {
                    count += recursiveCount(file.listFiles());
                }
            }
        }
        return count;
    }

    public long soundsToLoad(String folder) {
        try {
            File fRes = new File(ResourceController.getLocale(folder));
            return recursiveCount(fRes.listFiles());
        } catch (Exception e) {
            log.error("SoundController - soundsToLoad(): " + e);
        }
        return 0;
    }

    public long soundsToLoad() {
        return soundsToLoad("");
    }

    public int size() {
        return soundsMap.size();
    }

    public boolean isEmpty() {
        return soundsMap.isEmpty();
    }

    public boolean containsKey(String soundKey) {
        return soundsMap.containsKey(soundKey);
    }

    public boolean containsValue(Sound value) {
        return soundsMap.containsValue(value);
    }

    public Sound get(String soundKey) {
        return soundsMap.get(soundKey);
    }

    public Sound get(String soundName, String path) {
        return get(soundName, path, true);
    }

    public Sound get(String soundName, String soundPath, boolean load) {
        Sound ret = soundsMap.get(soundName);
        if (ret == null) {
            ret = new Sound(soundName, soundPath, load);
            this.put(soundName, ret);
        }
        return ret;
    }

    public Sound put(String soundKey, Sound sound) {
        return soundsMap.put(soundKey, sound);
    }

    public Sound remove(String soundKey) {
        return soundsMap.remove(soundKey);
    }

    public void clear() {
        soundsMap.clear();
    }

    public Collection<Sound> values() {
        return soundsMap.values();
    }

    public Set<String> getKeySet() {
        return soundsMap.keySet();
    }

    public synchronized SoundPlayer play(Sound sound, SoundFilter filter, boolean loop) {
        if (Objects.isNull(sound)) {
            log.error("play(): Sound can not be NULL...");
            return null;
        }
        if (Objects.equals(enable, false)) {
            log.warn("play(): The sound suport is disabled!");
            return null;
        }
        SoundPlayer player = new SoundPlayer(sound);
        try {
            if (players.size() >= maxSimultaneousSounds) {
                throw new MaxSimultaneousSoundException(
                        "Max simultaneos channels is alread used! Please stop sounds to do that..."
                );
            }
            InputStream is;
            if (sound.getSoundType() != Sound.SOUND_TYPE_MP3
                    && sound.getSoundType() != Sound.SOUND_TYPE_UNK
                    && sound.getSamples() != null) {
                if (Objects.nonNull(filter)) {
                    if (loop) {
                        is = new FilteredSoundStream(
                                new LoopByteArrayInputStream(sound.getSamples()),
                                filter
                        );
                    } else {
                        is = new FilteredSoundStream(
                                new ByteArrayInputStream(sound.getSamples()),
                                filter
                        );
                    }
                } else if (loop) {
                    is = new LoopByteArrayInputStream(sound.getSamples());
                } else {
                    is = new ByteArrayInputStream(sound.getSamples());
                }
            } else if (loop) {
                is = new LoopByteArrayInputStream(sound.getSoundFile());
            } else {
                is = new FileInputStream(sound.getSoundFile());
            }
            players.add(player);
            Thread tPlayer = new Thread(new SoundPlayerTask(player, is), "playSound");
            tPlayer.start();
        } catch (MaxSimultaneousSoundException | FileNotFoundException e) {
            log.error("Fail to play(" + sound + "," + filter + "," + loop + "): {} ", e, e);
            player = null;
        }
        // ---
        return player;
    }

    public synchronized SoundPlayer play(Sound sound, SoundFilter filter) {
        return play(sound, filter, false);
    }

    public synchronized SoundPlayer play(Sound sound, boolean loop) {
        return play(sound, null, loop);
    }

    public synchronized SoundPlayer play(Sound sound) {
        return play(sound, null, false);
    }

    public void pauseAll() {
        log.info("Pausing all sounds...");
        synchronized (players) {
            players.forEach(SoundPlayer::pause);
        }
    }

    public void stopAll() {
        log.info("Stoping all sounds...");
        synchronized (players) {
            players.forEach(SoundPlayer::stop);
        }
    }

    public void continueAll() {
        log.info("Continuing all sounds...");
        synchronized (players) {
            players.forEach(SoundPlayer::play);
        }
    }

    /**
     * @param sound
     * @param channel
     * @return
     */
    public SoundPlayer newSoundPlayer(Sound sound, int channel) {
        if (players.get(channel) != null) {
            SoundPlayer sp = players.get(channel);
            sp.stop();
        }
        SoundPlayer player = new SoundPlayer(sound);
        players.set(channel, player);
        return player;
    }

    public SoundPlayer newSoundPlayer(Sound sound) {
        return newSoundPlayer(sound, 0);
    }

    public SoundPlayer getSoundPlayer(int channel) {
        return players.get(channel);
    }

    public void setEnable(boolean enable) {
        this.enable = enable;
        if (!enable) {
            stopAll();
        }
    }

    public void setMaxSimultaneousSounds(int maxSimultaneousSounds) {
        this.maxSimultaneousSounds = maxSimultaneousSounds;
        if (this.maxSimultaneousSounds > DEFAULT_MAX_SIMULTANEOUS_SOUNDS) {
            this.maxSimultaneousSounds = DEFAULT_MAX_SIMULTANEOUS_SOUNDS;
        } else if (this.maxSimultaneousSounds < 0) {
            this.maxSimultaneousSounds = 2;
        }
    }

    // -----------------------------------------------------------------------------------------------------------------
    public static boolean isValidSound(File file) {
        try {
            String fileName = file.getName();
            if (fileName.contains(".")) {
                String ext = fileName.toLowerCase().split("\\.", 2)[1];
                return VALID_SOUND_FORMATS.contains(ext);
            }
        } catch (Exception e) {
            log.warn("Invalid sound detected: {}", e, e);
        }
        return false;
    }

    private static SoundController instance;

    public static SoundController getInstance() {
        if (Objects.isNull(instance)) {
            instance = new SoundController();
        }
        return instance;
    }

    /**
     * SoundPlayerTask
     */
    private class SoundPlayerTask implements Runnable {

        private final SoundPlayer player;
        private final InputStream is;

        public SoundPlayerTask(SoundPlayer player, InputStream is) {
            this.player = player;
            this.is = is;
        }

        @Override
        public void run() {
            player.play(is);
            synchronized (players) {
                if (players.contains(player)) {
                    players.remove(player);
                }
            }
        }
    }

}
