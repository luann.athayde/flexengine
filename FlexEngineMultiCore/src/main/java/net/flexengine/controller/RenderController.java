/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.flexengine.controller;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import net.flexengine.model.config.Configuration;
import net.flexengine.model.config.FlexDisplayMode;
import net.flexengine.model.scene.GameScene;
import net.flexengine.model.scene.LoadScene;
import net.flexengine.utils.AsyncTask;
import net.flexengine.utils.RenderInfoUtil;
import net.flexengine.view.DefaultGraphics2D;
import net.flexengine.view.GameWindow;
import net.flexengine.view.Texture;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;
import java.util.Objects;

/**
 * @author Luann Athayde
 */
@Data
@Slf4j
@Builder
@AllArgsConstructor
public class RenderController extends Thread {

    public static final int THREAD_RENDER_ID = 0x1000;
    private static final String PRINTSCREEN_FORMAT = "png";

    protected int renderID;
    protected Font fontPaused;
    protected Font fontDefault;

    protected boolean running;
    protected boolean captureScreenshot;

    protected GameWindow gameWindow;
    protected Image backBuffer;
    protected Graphics2D graphics;
    protected Configuration config;

    protected long renderTime;
    protected long showRenderTime;

    protected RenderController() {
        renderID = THREAD_RENDER_ID;
        fontDefault = FontController.getFont();
        fontPaused = FontController.getFont(FontController.getDefaultFont(), Font.BOLD, 18);
        backBuffer = new BufferedImage(800, 600, BufferedImage.TYPE_INT_ARGB);
        gameWindow = GameWindow.builder().build();
        graphics = new DefaultGraphics2D();
        config = new Configuration();
    }

    public void initRender() {
        gameWindow = GameWindow.getInstance();
        config = ConfigurationController.getInstance().getDefaultConfiguration();
        FlexDisplayMode flexDM = config.getResolution();
        gameWindow.setSize(flexDM.getWidth(), flexDM.getHeight());
        gameWindow.setVisible(true);
        CursorController.getInstance().centerCursor();
        createBufferStrategy();
        // ---
        if (EngineHandler.getInstance().isShowInfo()) {
            RenderInfoUtil.openShowTimeInfoWindow();
        }
    }

    protected void createBufferStrategy() {
        Insets insets = gameWindow.getInsets();
        backBuffer = new BufferedImage(
                gameWindow.getWidth() - insets.left - insets.right,
                gameWindow.getHeight() - insets.top - insets.bottom,
                BufferedImage.TYPE_INT_ARGB
        );
        graphics = (Graphics2D) backBuffer.getGraphics();
        clear(graphics);
    }

    protected void clear(Graphics2D g2d) {
        if (Objects.nonNull(g2d)) {
            g2d.setFont(fontDefault);
            g2d.setBackground(gameWindow.getBackground());
            g2d.clearRect(0, 0, getViewportWidth(), getViewportHeight());
        }
    }

    @Override
    public void run() {
        running = true;
        while (running) {
            renderTime = System.currentTimeMillis();
            if (checkBufferSize()) {
                createBufferStrategy();
            }
            // --- Set ultimate attributes...
            aplyRenderConfiguration();
            clear(graphics);
            renderCurrentGameScene(graphics);
            // --- Render the Game Cursor...
            if (CursorController.getInstance().isShowGameCursor()) {
                Point cursorLoc = CursorController.getInstance().getGameCursor().getLocation();
                Texture cursorTexture = CursorController.getInstance().getCursorTexture();
                if (Objects.nonNull(graphics) && Objects.nonNull(cursorTexture)) {
                    cursorTexture.render(graphics, cursorLoc.x, cursorLoc.y);
                }
            }
            // ---
            // draw avarage fps/ups
            if (Objects.nonNull(graphics) ) {
                if( FlexEngine.getInstance().isDebugOnScreen() ) {
                    FlexEngine.DEBUG_INFO.render(graphics);
                }
                if( FlexEngine.getInstance().isAvarageInfoOnScreen() ) {
                    FlexEngine.DEBUG_INFO.renderAvarageInfo(graphics);
                }
            }
            // ---
            if (show()) {
                EngineHandler.getInstance().incrementFps();
            }
            // ---
            if (captureScreenshot) {
                captureScreenshotAfterCompleteRender();
            }
            // ---
            showRenderTime = renderTime = System.currentTimeMillis() - renderTime;
            sleep();
        }
    }

    public void aplyRenderConfiguration() {
        config = ConfigurationController.getInstance().getDefaultConfiguration();
        if (config.isAntialising()) {
            graphics.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        } else {
            graphics.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF);
        }
        if (config.isTextAntialising()) {
            graphics.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        } else {
            graphics.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_OFF);
        }
    }

    public boolean renderCurrentGameScene(Graphics2D g2d) {
        GameScene scene = SceneController.getInstance().getCurrentScene();
        if (Objects.nonNull(scene) && Objects.nonNull(g2d)) {
            boolean paused = FlexEngine.getInstance().isPaused();
            boolean loadScene = scene instanceof LoadScene;
            if (!paused || loadScene) {
                scene.render(g2d);
                return true;
            } else {
                g2d.setColor(FlexEngine.DEBUG_INFO.calculateInfoColor());
                g2d.setFont(fontPaused);
                g2d.drawString("- Paused -",
                        getViewportWidth() / 2 - 50,
                        getViewportHeight() / 2 - 10
                );
            }
        }
        return false;
    }

    public void captureScreenshot() {
        captureScreenshot = true;
    }

    public int getViewportWidth() {
        return Objects.nonNull(backBuffer) ?
                backBuffer.getWidth(null) : GameWindow.getInstance().getWidth();
    }

    public int getViewportHeight() {
        return Objects.nonNull(backBuffer) ?
                backBuffer.getHeight(null) : GameWindow.getInstance().getHeight();
    }

    private final ArrayList<Long> showTimes = new ArrayList<>(1024 * 10);

    public boolean show() {
        if (Objects.nonNull(gameWindow) && gameWindow.isVisible()) {
            long initTime = System.currentTimeMillis();
            Insets insets = gameWindow.getInsets();
            Graphics gTmp = gameWindow.getGraphics();
            gTmp.drawImage(backBuffer, insets.left, insets.top, null);
            synchronized (showTimes) {
                showTimes.add(System.currentTimeMillis() - initTime);
            }
            return true;
        }
        return false;
    }

    private boolean checkBufferSize() {
        Insets insets = gameWindow.getInsets();
        int width = getViewportWidth() + insets.left + insets.right;
        int height = getViewportHeight() + insets.top + insets.bottom;
        return gameWindow.getWidth() != width
                || gameWindow.getHeight() != height;
    }

    private void sleep() {
        try {
            long sleepTime = EngineHandler.getInstance().calculeRenderSleepTime();
            if (sleepTime > 0) {
                Thread.sleep(sleepTime);
            }
        } catch (Exception e) {
            log.error("### Fail to sleep!", e);
            Thread.currentThread().interrupt();
        }
    }

    protected void captureScreenshotAfterCompleteRender() {
        // ---
        final BufferedImage printScreen = new BufferedImage(
                getViewportWidth(), getViewportHeight(),
                BufferedImage.TYPE_INT_RGB
        );
        Graphics2D g = printScreen.createGraphics();
        g.drawImage(backBuffer, 0, 0, null);
        g.dispose();
        // ---
        captureScreenshot = false;
        AsyncTask.newInstance().run(() -> {
            try {
                long initTime = System.currentTimeMillis();
                String screenShotName = String.format("./%s_%d.%s",
                        GameWindow.getInstance().getTitle(),
                        System.currentTimeMillis(),
                        PRINTSCREEN_FORMAT
                );
                log.info("Salving new ScreenShot = {}", screenShotName);
                if (ImageIO.write(printScreen, PRINTSCREEN_FORMAT, new File(screenShotName))) {
                    log.info("ScreenShot saved in {}ms!", (System.currentTimeMillis() - initTime));
                }
            } catch (Exception e) {
                log.error("Fail to save screenshot!", e);
            }
        });
    }

    @Override
    public boolean equals(Object o) {
        if (Objects.nonNull(o) && o instanceof RenderController) {
            RenderController that = (RenderController) o;
            return renderID == that.renderID;
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(renderID);
    }

    @Override
    public String toString() {
        return "RenderController{" +
                "renderID=" + renderID +
                '}';
    }

    private static RenderController instance;

    public static RenderController getInstance() {
        if (Objects.isNull(instance)) {
            instance = new RenderController();
        }
        return instance;
    }

}
