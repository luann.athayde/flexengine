package net.flexengine.controller.arguments;

import java.io.Serializable;
import java.util.List;

/**
 * @author luann
 */
public interface ProcessorListener extends Serializable {

    void process(List<String> values);

}
