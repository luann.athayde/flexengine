package net.flexengine.controller.arguments;

/**
 * Arguments Processor
 */

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import net.flexengine.controller.FlexEngine;
import net.flexengine.controller.SoundController;
import net.flexengine.controller.server.FlexEngineServer;
import net.flexengine.controller.server.ServerConsole;
import net.flexengine.view.GameWindow;

import java.util.*;

/**
 * @author Luann Athayde
 * @version 1.0.0
 * @since 1.0.0
 */
@Data
@Slf4j
@Builder
@AllArgsConstructor
public class ArgumentsProcessor {

    private Map<String, ProcessorListener> argumentsMapped;

    public ArgumentsProcessor() {
        argumentsMapped = new HashMap<>();
        argumentsMapped.put("--nosound", (values) -> {
            SoundController.getInstance().setEnable(false);
        });
        argumentsMapped.put("--nographics", (values) -> {
            throw new UnsupportedOperationException("No graphics context not implemented yet!");
        });
        argumentsMapped.put("--fullscreen", (values) -> {
            GameWindow.getInstance().setFullscreen(true);
        });
        argumentsMapped.put("--fps", (values) -> {
            Integer desiredFps = Integer.valueOf(values.get(0));
            FlexEngine.getInstance().getEngineHandler().setDesireFps(desiredFps);
        });
        argumentsMapped.put("--ups", (values) -> {
            Integer desiredUps = Integer.valueOf(values.get(0));
            FlexEngine.getInstance().getEngineHandler().setDesireFps(desiredUps);
        });
        argumentsMapped.put("--server", (values) -> {
            Integer port = Integer.valueOf( values.get(0) );
            Integer maxUsers = Integer.valueOf( values.get(1) );
            FlexEngineServer.getInstance().initialize(port, maxUsers);
        });
        argumentsMapped.put("--console", (values) -> {
            ServerConsole.getInstance().waitForCommands();
        });
    }

    public ArgumentsProcessor addProcessor(String argument, ProcessorListener listener) {
        argumentsMapped.put(argument, listener);
        return this;
    }

    public ArgumentsProcessor removeProcessor(String argument) {
        argumentsMapped.remove(argument);
        return this;
    }

    public void process(String... args) {
        log.info("Current arguments for processing...");
        argumentsMapped.keySet().forEach( key -> log.info("Argument -> {}", key) );
        Map<String, List<String>> argsMap = buildArgsMap(args);
        // --
        argsMap.keySet().forEach(key -> {
            List<String> values = argsMap.get(key);
            if (argumentsMapped.containsKey(key)) {
                try {
                    ProcessorListener listener = argumentsMapped.get(key);
                    listener.process(values);
                } catch (Exception e) {
                    log.warn("### Fail to process argument: {} {}", key, e);
                }
            }
        });
    }

    private Map<String, List<String>> buildArgsMap(String... args) {
        Map<String, List<String>> argsMap = new HashMap<>();
        for (int i = 0; i < args.length; i++) {
            String arg = args[i];
            if (arg.startsWith("-") || arg.startsWith("--")) {
                List<String> values = new ArrayList<>();
                // -- read possible values if has
                int indexValues = i + 1;
                for (; indexValues < args.length; indexValues++) {
                    String arg2 = args[indexValues];
                    if (arg2.startsWith("-") || arg2.startsWith("--")) {
                        break;
                    }
                    values.add(arg2);
                }
                i = indexValues - 1;
                argsMap.put(arg, values);
            }
        }
        return argsMap;
    }

    private static ArgumentsProcessor instance;

    public static ArgumentsProcessor getInstance() {
        if (Objects.isNull(instance)) {
            instance = new ArgumentsProcessor();
        }
        return instance;
    }

}
