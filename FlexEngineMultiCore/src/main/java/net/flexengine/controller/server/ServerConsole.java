/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.flexengine.controller.server;

import lombok.extern.slf4j.Slf4j;
import net.flexengine.utils.FlexEngineConsole;
import net.flexengine.view.GameWindow;

import java.util.Objects;

/**
 * @author Luann R. Athayde
 * @version 1.0
 * @since 1.0
 */
@Slf4j
public class ServerConsole extends FlexEngineConsole {

    public ServerConsole() {
        super("flexengine");
    }

    @Override
    public void execute(String cmd) {
        if (cmd.equalsIgnoreCase("stop")) {
            FlexEngineServer.getInstance().stop();
        } else if (cmd.equalsIgnoreCase("start")) {
            FlexEngineServer.getInstance().start();

        } else if (cmd.equalsIgnoreCase("init")) {
            FlexEngineServer.getInstance().initialize();
        } else if (cmd.equalsIgnoreCase("exit")) {
            FlexEngineServer.getInstance().stop();
            this.stopWaiting();
        } else if (cmd.startsWith("set port")) {
            cmd = cmd.replace("set port", "").trim();
            try {
                FlexEngineServer.getInstance().setServerPort(Integer.valueOf(cmd));
            } catch (NumberFormatException e) {
            }
        } else if (cmd.startsWith("set max")) {
            cmd = cmd.replace("set max", "").trim();
            try {
                FlexEngineServer.getInstance().setMaxUsers(Long.valueOf(cmd));
            } catch (NumberFormatException e) {
            }
        }  else if (cmd.startsWith("game close")) {
            try {
                GameWindow.getInstance().setVisible(false);
            } catch (NumberFormatException e) {
            }
        } else {
            log.warn("Command [" + cmd + "] not found!");
        }
    }

    private static ServerConsole instance;

    public static ServerConsole getInstance() {
        if (Objects.isNull(instance)) {
            instance = new ServerConsole();
        }
        return instance;
    }

}
