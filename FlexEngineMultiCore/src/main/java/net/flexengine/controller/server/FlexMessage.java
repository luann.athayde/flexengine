/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.flexengine.controller.server;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.text.DecimalFormat;

/**
 * @author Luann R. Athayde
 * @version 1.0
 * @since 1.0
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class FlexMessage implements Serializable, Cloneable {

    private static final DecimalFormat BASIC_FORMAT = new DecimalFormat("#0.000000");

    private long id;
    private double action;
    private Serializable message;

    public FlexMessage(double action, Serializable message) {
        this(-1, action, message);
    }

    @Override
    public FlexMessage clone() {
        return FlexMessage.builder()
                .id(this.id)
                .action(this.action)
                .message(this.message)
                .build();
    }

    @Override
    public String toString() {
        return "FlexMessage{" + "id=" + id + ", action=" + BASIC_FORMAT.format(action) + ", message=" + message + '}';
    }

}
