/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.flexengine.controller.server;

import java.net.Socket;

/**
 * @author Luann R. Athayde
 * @version 1.0
 * @since 1.0
 */
public interface ServerListener {

    /**
     * Obtain the session for the client connection...
     * @param sock
     * @return
     */
    FlexEngineSession getSession(Socket sock);
    
}
