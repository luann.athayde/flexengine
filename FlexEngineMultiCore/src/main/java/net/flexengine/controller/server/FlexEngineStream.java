/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.flexengine.controller.server;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

/**
 * @author Luann R. Athayde
 * @version 1.0
 * @since 1.0
 */
@Data
@Slf4j
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class FlexEngineStream {

    private static long msgID = 1;

    private ObjectInputStream in;
    private ObjectOutputStream out;
    private boolean autoFlush;

    public void flush() {
        try {
            out.flush();
        } catch (Exception e) {
            log.error("Fail to flux stream: {}", e, e);
        }
    }

    public boolean write(Serializable obj) {
        try {
            if (obj instanceof FlexMessage) {
                FlexMessage msg = (FlexMessage) obj;
                msg.setId(msgID);
                msgID++;
            }
            out.writeObject(obj);
            if (isAutoFlush()) {
                flush();
            }
            return true;
        } catch (Exception e) {
            log.error("Fail to write stream: {}", e, e);
            return false;
        }
    }

    public <T> T read() {
        try {
            return (T) in.readObject();
        } catch (IOException | ClassNotFoundException e) {
            log.error("Fail to read stream: {}", e, e);
        }
        return null;
    }

    public boolean close() {
        try {
            in.close();
            out.close();
            return true;
        } catch (IOException e) {
            return false;
        }
    }

}
