/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.flexengine.controller.server;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * @author Luann R. Athayde
 * @version 1.0
 * @since 1.0
 */
@Data
@Slf4j
public class FlexEngineServer implements Runnable {

    public static final int MAX_USERS_CONNECTION = 32000;
    public static final int DEFAULT_PORT = 13000;
    // -- yyyy mm dd
    public static final long SERVER_VERSION = 20131015;
    //--------------------------------------------------------------------------
    private Thread serverThread;
    private boolean running;
    //--------------------------------------------------------------------------
    private ServerSocket serverSocket;
    private Socket clientSock;
    private int serverPort;
    private boolean canStart;
    private long maxUsers;

    private final Map<Long, FlexEngineSession> sessions;

    //--------------------------------------------------------------------------
    private ServerListener listener;
    //--------------------------------------------------------------------------

    private FlexEngineServer() {
        serverThread = null;
        running = false;
        serverSocket = null;
        serverPort = DEFAULT_PORT;
        canStart = false;
        maxUsers = MAX_USERS_CONNECTION;
        sessions = new HashMap<>();
        listener = null;
    }

    public void setMaxUsers(long maxUsers) {
        if (maxUsers > 0 && maxUsers <= MAX_USERS_CONNECTION) {
            this.maxUsers = maxUsers;
        }
    }

    public void setServerPort(int serverPort) {
        if (serverPort > 0 && serverPort < 65535) {
            this.serverPort = serverPort;
        }
    }

    public boolean initialize() {
        return initialize(serverPort, maxUsers);
    }

    public boolean initialize(int port) {
        return initialize(port, maxUsers);
    }

    public boolean initialize(long maxUsers) {
        return initialize(serverPort, maxUsers);
    }

    public boolean initialize(int port, long maxUsers) {
        if (Objects.nonNull(serverSocket) && serverSocket.isBound()) {
            return false;
        }
        if (maxUsers <= 0 || maxUsers > MAX_USERS_CONNECTION) {
            maxUsers = MAX_USERS_CONNECTION;
        }
        try {
            log.info("initialize(): Initializing server on port = " + port + " with max users = " + maxUsers);
            this.serverPort = port;
            this.maxUsers = maxUsers;
            this.serverSocket = new ServerSocket(port);
            log.info("initialize(): Server socket created...");
            if (serverSocket.isClosed()) {
                log.warn("initialize(" + port + "," + maxUsers + "): Server socket was closed...");
                canStart = false;
                return false;
            }
            canStart = true;
        } catch (IOException e) {
            canStart = false;
            serverSocket = null;
            log.error("initialize(" + port + "," + maxUsers + "): " + e);
        }
        return canStart;
    }

    public void start() {
        if (canStart) {
            if (!running) {
                initialize();
                for (Long key : sessions.keySet()) {
                    FlexEngineSession session = sessions.get(key);
                    session.disconnect();
                }
                sessions.clear();
                if (Objects.nonNull(serverThread)) {
                    running = false;
                    serverThread.interrupt();
                    serverThread = null;
                }
                running = true;
                serverThread = new Thread(this, getClass().getSimpleName());
                serverThread.start();
            } else {
                log.warn("start(" + serverPort + "," + maxUsers + "): Server is already running...");
            }
        } else {
            log.warn("start(" + serverPort + "," + maxUsers + "): Server can not be started...\n\t"
                    + "Please, be sure that initialize() method has been called properly!");
        }
    }

    public void stop() {
        try {
            running = false;
            for (Long key : sessions.keySet()) {
                FlexEngineSession session = sessions.get(key);
                session.disconnect();
            }
            sessions.clear();
            if (serverSocket != null) {
                serverSocket.close();
            }
            serverSocket = null;
            serverThread = null;
            log.info("stop(): Server stopped...");
        } catch (IOException e) {
            log.warn("stop(): " + e);
        }
    }

    @Override
    public void run() {
        while (running) {
            try {
                log.info("run(): Waiting for connection...");
                clientSock = serverSocket.accept();
                log.info("run(): Connection received...");
                Thread createSession = new Thread(new CreateSession(clientSock), "CreateSession");
                createSession.setDaemon(true);
                createSession.start();
            } catch (IOException e) {
                log.error("run(): " + e);
            }
        }
        log.info("run(): Server finished...");
    }

    public FlexEngineSession getSession(long id) {
        return sessions.get(id);
    }

    // ---

    private static FlexEngineServer instance;

    public static FlexEngineServer getInstance() {
        if (Objects.isNull(instance)) {
            instance = new FlexEngineServer();
        }
        return instance;
    }

    private class CreateSession implements Runnable {

        private final Socket socket;

        public CreateSession(Socket socket) {
            this.socket = socket;
        }

        @Override
        public void run() {
            log.info("CreateSession - run(): Creating session...");
            try {
                if (sessions.size() < maxUsers && Objects.nonNull(listener)) {
                    FlexEngineSession session = listener.getSession(socket);
                    FlexEngineStream stream = session.getStream();
                    log.info("CreateSession - run(): Waiting for version...");
                    FlexMessage msg = stream.<FlexMessage>read();
                    log.info("CreateSession - run(): msg = " + msg);
                    if (msg.getAction() == FlexEngineServer.SERVER_VERSION && msg.getMessage().equals("version")) {
                        log.info("CreateSession - run(): VERSION OK!");
                        msg = new FlexMessage(FlexEngineServer.SERVER_VERSION, "VERSION_OK");
                        stream.write(msg);
                    } else {
                        msg = new FlexMessage(FlexEngineServer.SERVER_VERSION, "WRONG_VERSION");
                        log.info("CreateSession - run(): WRONG VERSION!");
                        stream.write(msg);
                        session.disconnect();
                    }
                    // -- session ID sanity CHECK!
                    if (session.getSessionID() <= 0) {
                        session.setSessionID(1);
                    }
                    while (true) {
                        if (sessions.containsKey(session.getSessionID())) {
                            session.setSessionID(session.getSessionID() + 1);
                        } else {
                            break;
                        }
                    }
                    sessions.put(session.getSessionID(), session);
                    session.start();

                    // -- listener for end session...
                    log.info("FlexEngineServer - Starting the session listener...");
                    log.info("FlexEngineServer - session running = " + session.isRunning());
                    while (session.isRunning()) {
                        sleep(1000);
                    }
                    log.info("FlexEngineServer - Removing session[" + sessions.remove(session.getSessionID()) + "]...");
                } else if (Objects.isNull(listener)) {
                    log.info("run(): Listener not found!");
                    FlexMessage msg = new FlexMessage(0, "NOT_HAVE_LISTENER");
                    FlexEngineStream stream = new FlexEngineStream(
                            new ObjectInputStream(socket.getInputStream()),
                            new ObjectOutputStream(socket.getOutputStream()),
                            true
                    );
                    // -- receive version
                    stream.read();
                    // -- send info
                    stream.write(msg);
                    stream.close();
                    socket.close();
                } else {
                    log.info("run(): The mas users has reached...");
                    FlexMessage msg = new FlexMessage(sessions.size(), "MAX_USERS_REACHED");
                    FlexEngineStream stream = new FlexEngineStream(
                            new ObjectInputStream(socket.getInputStream()),
                            new ObjectOutputStream(socket.getOutputStream()),
                            true
                    );
                    // -- receive version
                    stream.read();
                    // -- send info
                    stream.write(msg);
                    stream.close();
                    socket.close();
                }
                log.info("CreateSession - run(): Create session fineshed...");
            } catch (Exception e) {
                log.error(getClass() + " - run(): " + e);
            }
        }

        public void sleep(long milis) {
            try {
                Thread.sleep(milis);
            } catch (Exception e) {
                log.error("Fail to wait: {}", e, e);
                Thread.currentThread().interrupt();
            }
        }

    }

}
