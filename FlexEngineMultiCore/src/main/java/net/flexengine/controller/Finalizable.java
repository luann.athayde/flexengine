/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package net.flexengine.controller;

import net.flexengine.model.scene.SceneBundle;

/**
 * @author Luann R. Athayde
 * @version 1.0
 * @since 1.0
 */
public interface Finalizable {

    void end(SceneBundle bundle);

}
