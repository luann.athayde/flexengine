/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.flexengine.controller;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.util.Objects;

/**
 * @author Luann R. Athayde
 * @version 1.0
 * @since 1.0
 */
@Data
@Slf4j
@Builder
@AllArgsConstructor
public class EngineHandler implements Runnable {

    private Thread tEngineHandler;
    private boolean running;

    private boolean showInfo;
    private int fps;
    private int ups;
    private long totalFps;
    private long totalUps;

    private int desireUps;
    private int desireFps;

    private long startedTime;

    private String currentInfo;

    public EngineHandler() {
        tEngineHandler = new Thread(this, getClass().getSimpleName());
        running = false;
        // --
        desireFps = 120;
        desireUps = 120;
        // --
    }

    @Override
    public synchronized void run() {
        running = true;
        startedTime = System.currentTimeMillis();
        while (running) {
            try {
                wait(1000);
                if (running) {
                    currentInfo = buildInfo();
                    if( showInfo ) {
                        log.info( currentInfo );
                    }
                    resetFps();
                    resetUps();
                }
            } catch (InterruptedException e) {
                log.error("Fail on running engine handler!", e);
                Thread.currentThread().interrupt();
                running = false;
            } catch (Exception e) {
                log.error("Fail on running engine handler!", e);
            }
        }
    }

    private String buildInfo() {
        StringBuilder sb = new StringBuilder();
        sb.append("FPS: ");
        sb.append(fps);
        sb.append(" | UPS: ");
        sb.append(ups);
        sb.append(" | TotalFPS: ");
        sb.append(totalFps);
        sb.append(" | TotalUPS: ");
        sb.append(totalUps);
        sb.append(" | threads: ");
        sb.append(Thread.activeCount());
        return sb.toString();
    }

    public void start() {
        if (!running) {
            tEngineHandler.setDaemon(true);
            tEngineHandler.start();
        }
    }

    public void stop() {
        running = false;
    }

    public long calculeRenderSleepTime() {
        long needSleep = 1000L / desireFps;
        long renderTime = RenderController.getInstance().getRenderTime();
        if (renderTime > needSleep) {
            return 0;
        }
        return needSleep - renderTime;
    }

    public long calculeLogicSleepTime() {
        long needSleep = 1000L / desireUps;
        long logicTime = LogicController.getInstance().getLogicTime();
        if (logicTime > needSleep) {
            return 0;
        }
        return needSleep - logicTime;
    }

    public float getAvarageFPS() {
        if (getTimeRunning() > 0) {
            return (float)totalFps / getTimeRunning();
        }
        return 0f;
    }

    public float getAvarageUPS() {
        if (getTimeRunning() > 0) {
            return (float)totalUps / getTimeRunning();
        }
        return 0f;
    }

    /**
     * Time running in seconds...
     *
     * @return
     */
    public long getTimeRunning() {
        return (System.currentTimeMillis() - startedTime) / 1_000L;
    }

    public void incrementFps() {
        fps++;
        totalFps++;
    }

    public void resetFps() {
        fps = 0;
    }

    public void incrementUps() {
        ups++;
        totalUps++;
    }

    public void resetUps() {
        ups = 0;
    }

    private static EngineHandler instance;

    public static EngineHandler getInstance() {
        if (Objects.isNull(instance)) {
            instance = new EngineHandler();
        }
        return instance;
    }

}
