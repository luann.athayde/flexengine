/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.flexengine.controller;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.flexengine.model.components.ProgressBar;
import net.flexengine.view.Texture;

import java.io.File;
import java.io.FileFilter;
import java.io.InputStream;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author Luann R. Athayde
 * @version 1.0
 * @since 1.0
 */
@Data
@Slf4j
@Builder
@AllArgsConstructor
public class TextureController {

    protected static final List<String> IMAGE_TYPES = Stream.of(
            ".png", ".jpg", ".gif", ".jpeg", ".tiff", ".bmp"
    ).collect(Collectors.toList());

    protected final Map<String, Texture> textures = new HashMap<>();
    protected boolean loadOnStartup;

    protected TextureController() {
        this.loadOnStartup = false;
    }

    protected void fullRecursiveLoad(File[] files, ProgressBar pb) {
        if (Objects.nonNull(files)) {
            for (File file : files) {
                if (file.isDirectory()) {
                    File[] tmpFiles = file.listFiles(new ImagesFileFilter());
                    fullRecursiveLoad(tmpFiles, pb);
                } else {
                    getTexture(file.getName(), file.getPath());
                }
            }
        }
    }

    public void loadTextures(String path, ProgressBar pb) {
        try {
            File fPath = new File(ResourceController.getLocale(path));
            log.info("Loading all textures from [" + fPath.getPath() + "]...");
            File[] files = fPath.listFiles(new ImagesFileFilter());
            fullRecursiveLoad(files, pb);
            log.info("Textures loaded!");
        } catch (Exception e) {
            log.info("Failed to load textures. ERROR = " + e);
        }
    }

    public void loadTextures(String path) {
        loadTextures(path, new ProgressBar());
    }

    public void loadTextures(ProgressBar pb) {
        loadTextures("textures/", pb);
    }

    public void loadTextures() {
        loadTextures("textures/", new ProgressBar());
    }

    public Texture getTexture(String name) {
        return textures.get(name);
    }

    public Texture getTexture(String name, String file) {
        if (textures.containsKey(name)) {
            return textures.get(name);
        } else {
            File fTmp = new File(file);
            if (fTmp.exists() && fTmp.canRead()) {
                Texture texture = new Texture(name, file);
                if (texture.isLoaded()) {
                    log.info("Texture [" + texture + "] loaded...");
                    textures.put(name, texture);
                    return texture;
                }
            }
        }
        return null;
    }

    public Texture getTexture(String name, InputStream stream) {
        if (textures.containsKey(name)) {
            return textures.get(name);
        } else {
            Texture texture = new Texture(name, stream);
            if (texture.isLoaded()) {
                log.info("Texture [" + texture + "] loaded...");
                textures.put(name, texture);
                return texture;
            }
        }
        return null;
    }

    static class ImagesFileFilter implements FileFilter {

        @Override
        public boolean accept(File file) {
            if (IMAGE_TYPES.stream().anyMatch(type -> file.getName().endsWith(type))) {
                return true;
            }
            return file.isDirectory();
        }

    }

    private static TextureController instance;

    public static TextureController getInstance() {
        if (Objects.isNull(instance)) {
            instance = new TextureController();
        }
        return instance;
    }

}
