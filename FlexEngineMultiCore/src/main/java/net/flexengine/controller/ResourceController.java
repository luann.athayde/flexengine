/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.flexengine.controller;

import java.io.InputStream;

/**
 * @author Luann R. Athayde
 * @version 1.0
 * @since 1.0
 */
public class ResourceController {

    public static final String DEFAULT_PATH = "./resources/";
    public static final String LANGUAGES_PATH = "languages/";
    public static final String FONTS_PATH = "fonts/";
    public static final String CURSORS_PATH = "cursors/";
    public static final String TEXTURES_PATH = "textures/";
    public static final String SCRIPTS_PATH = "scripts/";

    private static String currentPath = DEFAULT_PATH;

    public static String getLocale(String pathName) {
        return currentPath + pathName;
    }

    public static String getFontPathFor(String font) {
        return currentPath + FONTS_PATH + font;
    }

    public static String getLanguagePathFor(String languageFile) {
        return currentPath + LANGUAGES_PATH + languageFile;
    }

    public static String getTexturePathFor(String basePath, String ... args) {
        return currentPath + TEXTURES_PATH + String.format(basePath, args);
    }

    public static String getCursorPathFor(String cursorImage) {
        return currentPath + CURSORS_PATH + cursorImage;
    }

    public static String getScriptPathFor(String scriptFile) {
        return currentPath + SCRIPTS_PATH + scriptFile;
    }

    public static InputStream getResourceAsStream(String name) {
        return ClassLoader.getSystemResourceAsStream(name);
    }

    public static void setCurrentPath(String currentPath) {
        ResourceController.currentPath = currentPath;
    }

    public static String getCurrentPath() {
        return currentPath;
    }

}
