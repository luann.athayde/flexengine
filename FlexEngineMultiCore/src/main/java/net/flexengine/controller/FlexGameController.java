/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.flexengine.controller;

import lombok.extern.slf4j.Slf4j;
import net.flexengine.model.game.FlexGame;
import net.flexengine.model.game.FlexGameFileFilter;
import org.simpleframework.xml.core.Persister;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * @author Luann Athayde
 */
@Slf4j
public class FlexGameController {

    private static final Map<String, FlexGame> FLEX_GAMES = new HashMap<>();

    private FlexGameController() {
        throw new IllegalStateException("### Use static methods!");
    }

    public static void loadGames() {
        File gamesFolder = new File(ResourceController.getLocale("games/"));
        if (gamesFolder.exists() && gamesFolder.canRead()) {
            Persister p = new Persister();
            File[] fileGames = gamesFolder.listFiles(new FlexGameFileFilter());
            if (Objects.nonNull(fileGames)) {
                for (File gameFile : fileGames) {
                    try {
                        FlexGame game = p.read(FlexGame.class, gameFile);
                        FlexGameController.FLEX_GAMES.put(game.getName(), game);
                    } catch (Exception e) {
                        log.warn("### Fail to load game: {}", gameFile.getAbsolutePath(), e);
                    }
                }
            }
        }
    }

    public static Map<String, FlexGame> getGames() {
        return FlexGameController.FLEX_GAMES;
    }

    public static FlexGame getGame(String name) {
        return FlexGameController.FLEX_GAMES.get(name);
    }

    public static FlexGame putGame(String name, FlexGame game) {
        return FlexGameController.FLEX_GAMES.put(name, game);
    }

}
