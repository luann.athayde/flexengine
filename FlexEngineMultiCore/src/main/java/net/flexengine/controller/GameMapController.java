/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.flexengine.controller;

import lombok.extern.slf4j.Slf4j;
import net.flexengine.model.map.model.*;
import net.flexengine.model.sound.Sound;
import org.simpleframework.xml.core.Persister;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;

/**
 * @author Luann R. Athayde
 * @version 1.0
 * @since 1.0
 */
@Slf4j
public class GameMapController {

    /**
     * Load a map using its name...
     *
     * @param mapName
     * @return
     */
    public static GameMap load(String mapName) {
        return load(mapName, false);
    }

    /**
     * Load a map using its name...
     *
     * @param mapName
     * @param compressed
     * @return GameMap
     */
    public static GameMap load(String mapName, boolean compressed) {
        try {
            Persister persister = new Persister();
            String file = "./" + mapName + (compressed ? ".zip" : ".xml");
            GameMap map = null;
            if (compressed) {
                ZipFile zipFile = new ZipFile(file);
                ZipEntry entry = zipFile.getEntry(mapName + ".xml");
                if (entry == null) {
                    throw new Exception("ZIP-FILE does'nt contains a MAP with name[" + mapName + "]!");
                }
                map = persister.read(GameMap.class, zipFile.getInputStream(entry));
            } else {
                FileInputStream inMap = new FileInputStream(file);
                map = persister.read(GameMap.class, inMap);
            }
            // -- load the texture used in loading state...
            if (map.getLoadTexture() != null) {
                // --
                // TODO!
            }
            // --------------------------------------------------------------------------------
            // -- Sort the layers of the map by zOrder
            Collections.sort(map.getLayers(), (GameMapLayer o1, GameMapLayer o2)
                    -> Integer.compare(o1.getzOrder(), o2.getzOrder()));
            // -- Sort tiles by GridX and GridY
            for (GameMapLayer layer : map.getLayers()) {
                Collections.sort(layer.getTiles());
            }

            // -- load sounds
            ArrayList<Sound> removes = new ArrayList<>();
            for (int i = 0; i < map.getSounds().size(); i++) {
                Sound s = map.getSounds().getSound(i);
                Sound loaded = SoundController.getInstance().get(s.getSoundName(), s.getSoundPath(), true);
                if (loaded == null) {
                    removes.add(s);
                } else {
                    map.getSounds().setSound(i, loaded);
                }
            }
            map.getSounds().removeAll(removes);

            // -- Load global itens...
            for (GameMapItem item : map.getGlobalItens()) {
                if (item.getItem().equals("GameMapTexture")) {
                    GameMapTexture mapTex = (GameMapTexture) item.getItem();
                    // TODO!
                }
            }

            // --
            return map;
        } catch (Exception e) {
            log.error("GameMapController::load() - " + e, e);
        }
        return null;
    }

    private static String getScaledTextureName(String name, int width, int height) {
        return name + "_scaledto_" + width + "_" + height + "";
    }

    /**
     * Save a map in a ZIP compressed file...
     *
     * @param map
     */
    public static void saveCompressed(GameMap map) {
        try {
            Persister persister = new Persister();
            try (ByteArrayOutputStream outStream = new ByteArrayOutputStream()) {
                persister.write(map, outStream, "UTF-8");
                try (ZipOutputStream zipOut = new ZipOutputStream(new FileOutputStream("./" + map.getName() + ".zip"))) {
                    zipOut.putNextEntry(new ZipEntry(map.getName() + ".xml"));
                    zipOut.write(outStream.toByteArray());
                    zipOut.closeEntry();
                }
            }
        } catch (Exception e) {
            log.error("GameMapController::saveCompressed() - " + e);
        }
    }

    /**
     * Save a map in a XML basic file...
     *
     * @param map
     */
    public static void save(GameMap map) {
        try {
            Persister persister = new Persister();
            FileOutputStream outFile = new FileOutputStream("./" + map.getName() + ".xml");
            persister.write(map, outFile, "UTF-8");
        } catch (Exception e) {
            log.error("GameMapController::save() - " + e);
        }
    }

    public static ArrayList<Tile> createTiles(int mapWidth, int mapHeight) {
        ArrayList<Tile> tiles = new ArrayList<>();
        for (int linha = 0; linha < mapHeight; linha++) {
            for (int coluna = 0; coluna < mapWidth; coluna++) {
                Tile t = new Tile(linha, coluna);
                t.setName("Tile[" + linha + "," + coluna + "]");
                tiles.add(t);
            }
        }
        return tiles;
    }

}
