/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.flexengine.controller.database;

import lombok.extern.slf4j.Slf4j;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.Objects;

/**
 * @author Luann R. Athayde
 * @version 1.0
 * @since 1.0
 */
@Slf4j
public class FlexEngineEntityManagerFactory {

    private static EntityManagerFactory ENTITY_MANAGER_FACTORY;
    private static EntityManager ENTITY_MANAGER = null;
    private static String PERSISTENCE_UNIT;

    private FlexEngineEntityManagerFactory() {
        throw new IllegalStateException("### Use FlexEngineEntityManagerFactory.init() instead!");
    }

    public static void init() {
        try {
            ENTITY_MANAGER_FACTORY = Persistence.createEntityManagerFactory(getPersistenceUnit());
            ENTITY_MANAGER = ENTITY_MANAGER_FACTORY.createEntityManager();
        } catch (Exception e) {
            ENTITY_MANAGER_FACTORY = null;
            ENTITY_MANAGER = null;
            log.error("### Fail to initialize entity manager!", e);
        }
    }

    public static String getPersistenceUnit() {
        return FlexEngineEntityManagerFactory.PERSISTENCE_UNIT;
    }

    public static void setPersistenceUnit(String unitName) {
        FlexEngineEntityManagerFactory.PERSISTENCE_UNIT = unitName;
    }

    public static EntityManager getDefaultEntityManager() {
        if (Objects.isNull(FlexEngineEntityManagerFactory.ENTITY_MANAGER)) {
            FlexEngineEntityManagerFactory.init();
        }
        return FlexEngineEntityManagerFactory.ENTITY_MANAGER;
    }

    public static EntityManager getEntityManager() {
        if (Objects.nonNull(FlexEngineEntityManagerFactory.ENTITY_MANAGER_FACTORY)) {
            return FlexEngineEntityManagerFactory.ENTITY_MANAGER_FACTORY.createEntityManager();
        }
        return null;
    }

}
