/**
 *
 */
package net.flexengine.controller;

import lombok.Builder;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.awt.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author Luann R. Athayde
 * @version 1.0
 * @since 1.0
 */
@Data
@Slf4j
@Builder
public class FontController {

    public static final String FONT_EXTENSION = ".ttf";

    private static String defaultFont;

    private static final String[] DEFAULT_FONTS = {
            "Arial", "Calibri", "Coldiac Free",
            "FreeMono", "FreeSans", "Harabara"
    };
    private static final List<Font> LOADED_FONTS;
    private static final List<String> FONTS;

    static {
        defaultFont = Font.SANS_SERIF;
        FONTS = new ArrayList<>();
        LOADED_FONTS = new ArrayList<>();
    }

    private FontController() {
        throw new IllegalStateException("FontController can't be instanciated.");
    }

    public static void loadEmbeddedFonts() {
        for (String embeddedFont : DEFAULT_FONTS) {
            try (InputStream fontStream = ResourceController.getResourceAsStream(
                    ResourceController.FONTS_PATH + embeddedFont + FONT_EXTENSION
            )) {
                if ( !containsFont(embeddedFont) && Objects.nonNull(fontStream)) {
                    boolean install = installFont(fontStream);
                    if (install) {
                        log.info("Embedded Font [" + embeddedFont + "] installed sussefully...");
                    }
                }
            } catch (Exception e) {
                log.error("Fail to install basic embedded font...", e);
            }
        }
    }

    public static void loadAllAvailableFonts() {
        try {
            reloadFontsList();
            loadEmbeddedFonts();
            File fontFolder = new File(ResourceController.getLocale(ResourceController.FONTS_PATH));
            File[] aFonts = fontFolder.listFiles((File dir, String name) ->
                    name.toLowerCase().endsWith(".ttf"));
            if (Objects.nonNull(aFonts)) {
                for (File font : aFonts) {
                    boolean install = installFont(font);
                    if (install) {
                        log.info("Font [" + font + "] installed sussefully...");
                    }
                }
            }
            // ---
            reloadFontsList();
        } catch (Exception e) {
            log.error("Fail to load FONTS...", e);
        }
    }

    private static void reloadFontsList() {
        FONTS.clear();
        FONTS.addAll(Stream.of(
                GraphicsEnvironment.getLocalGraphicsEnvironment().getAvailableFontFamilyNames()
        ).collect(Collectors.toList()));
    }

    public static boolean containsFont(String fonte) {
        return FONTS.stream().anyMatch(fonte::equalsIgnoreCase);
    }

    public static Font getFont() {
        return new Font(getDefaultFont(), Font.PLAIN, 12);
    }

    public static Font getFont(String fonte) {
        return new Font(fonte, Font.PLAIN, 12);
    }

    public static Font getFont(String fonte, int style) {
        return new Font(fonte, style, 12);
    }

    public static Font getFont(String fonte, int style, int size) {
        return new Font(fonte, style, size);
    }

    public static String getDefaultFont() {
        return defaultFont;
    }

    public static void setDefaultFont(String defaultFont) {
        FontController.defaultFont = defaultFont;
    }

    // ---
    // Installing methods for FONTS...
    public static boolean installFont(String file) {
        return installFont(new File(file));
    }

    public static boolean installFont(File file) {
        if (Objects.nonNull(file) && file.exists() && file.getName().toLowerCase().endsWith(FONT_EXTENSION)) {
            String foneName = file.getName().replace(FONT_EXTENSION, "");
            if (!containsFont(foneName)) {
                try (FileInputStream fIn = new FileInputStream(file)) {
                    return installFont(fIn);
                } catch (IOException ioe) {
                    log.error("Fail to install font...", ioe);
                }
            } else {
                log.warn("installFont({}): This font is allread available on system...", file);
            }
        } else {
            log.warn("installFont({}): Unsuportted font file...", file);
        }
        return false;
    }

    // ---
    public static boolean installFont(InputStream fontStream) throws IOException {
        try {
            return GraphicsEnvironment.getLocalGraphicsEnvironment().registerFont(
                    Font.createFont(Font.TRUETYPE_FONT, fontStream)
            );
        } catch (FontFormatException e) {
            log.error("Fail to install font...", e);
        }
        return false;
    }

}
