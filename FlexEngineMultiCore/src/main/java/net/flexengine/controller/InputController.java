/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.flexengine.controller;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import net.flexengine.utils.AsyncTask;
import net.flexengine.view.GameWindow;
import net.java.games.input.*;

import java.awt.event.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * @author Luann R. Athayde
 * @version 1.0
 * @since 1.0
 */
@Data
@Slf4j
@Builder
@AllArgsConstructor
public class InputController implements
        KeyListener, MouseListener, MouseWheelListener, MouseMotionListener, WindowListener {

    public static final int CONTROLLER_KEYBOARD = 0x00A1;
    public static final int CONTROLLER_JOYSTICK = 0x00A2;
    public static final String CONTROLLER_KEYBOARD_TYPE = "Keyboard";
    public static final String CONTROLLER_JOYSTICK_TYPE = "Joystick";

    protected int controlType;
    protected Map<Integer, Integer> keysEvents;
    protected Map<Integer, Integer> mouseEvents;
    protected int mouseX;
    protected int mouseY;
    protected boolean mouseOnWindow;
    protected char lastKey;
    protected ControllerEnvironment controllerEnvironment;
    protected Controller joystick;
    protected JoystickButtons joystickButtons;
    protected boolean rumbling;

    private InputController() {
        controlType = CONTROLLER_KEYBOARD;
        keysEvents = new HashMap<>();
        mouseEvents = new HashMap<>();
        initJoysticks();
    }

    public void initJoysticks() {
        try {
            boolean initJoysticksConfig = Boolean.parseBoolean(
                    ConfigurationController.CONFIG_BUNDLE.getString("initJoysticks")
            );
            cleanJoysticks();
            if (initJoysticksConfig) {
                log.info("JInput version: " + Version.getVersion());
                controllerEnvironment = ControllerEnvironment.getDefaultEnvironment();
                if (controllerEnvironment.isSupported()) {
                    log.info("Loading available controllers/joysticks...");
                    Controller[] cs = controllerEnvironment.getControllers();
                    for (Controller c : cs) {
                        if (c.getType() == Controller.Type.STICK || c.getType() == Controller.Type.GAMEPAD) {
                            joystick = c;
                            break;
                        }
                    }
                    if (Objects.nonNull(joystick)) {
                        log.info("joystick = " + joystick.getName());
                        log.info("joystick type = " + joystick.getType().toString());
                        for (Component c : joystick.getComponents()) {
                            log.info("component = " + c.getName() + " | Identifier: " + c.getIdentifier().getName());
                        }
                        for (Controller c : joystick.getControllers()) {
                            log.info("controller = " + c);
                        }
                        // ---
                        joystickButtons = new JoystickButtons();
                        findJoystickButtons(joystickButtons);
                        // ---
                        controlType = CONTROLLER_JOYSTICK;
                    }
                }
            }
        } catch (Throwable e) {
            log.error("Failed to load contollers.", e);
            cleanJoysticks();
        }
    }

    public void findJoystickButtons(JoystickButtons joystickButtons) {
        joystickButtons.buttonSelect = joystick.getComponent(Component.Identifier.Button._6);
        joystickButtons.buttonStart = joystick.getComponent(Component.Identifier.Button._7);
        // ---
        joystickButtons.buttonA = joystick.getComponent(Component.Identifier.Button._0);
        joystickButtons.buttonB = joystick.getComponent(Component.Identifier.Button._1);
        joystickButtons.buttonX = joystick.getComponent(Component.Identifier.Button._2);
        joystickButtons.buttonY = joystick.getComponent(Component.Identifier.Button._3);
        // ---
        joystickButtons.axisX = joystick.getComponent(Component.Identifier.Axis.X);
        joystickButtons.axisY = joystick.getComponent(Component.Identifier.Axis.Y);
        // ---
        Component POV = joystick.getComponent(Component.Identifier.Axis.POV);
        if (POV != null) {
            joystickButtons.pov = POV;
        } else {
            joystickButtons.buttonLeft = joystick.getComponent(Component.Identifier.Button.LEFT);
            joystickButtons.buttonRight = joystick.getComponent(Component.Identifier.Button.RIGHT);
            joystickButtons.buttonUp = joystick.getComponent(Component.Identifier.Button._8);
            joystickButtons.buttonDown = joystick.getComponent(Component.Identifier.Button._9);
        }
    }

    public void update() {
        // ---
        if (InputController.getInstance().isControlAndKeyPressed(KeyEvent.VK_J)) {
            log.info("Switching controller option...");
            InputController.getInstance().releaseKey(KeyEvent.VK_CONTROL);
            InputController.getInstance().releaseKey(KeyEvent.VK_J);
            if (Objects.isNull(joystick)) {
                initJoysticks();
            }
            if (Objects.nonNull(joystick)) {
                switch (controlType) {
                    case CONTROLLER_JOYSTICK:
                        controlType = CONTROLLER_KEYBOARD;
                        CursorController.getInstance().showGameCursor();
                        break;
                    default:
                    case CONTROLLER_KEYBOARD:
                        controlType = CONTROLLER_JOYSTICK;
                        rumbleJoystick(0.5f, 800);
                        CursorController.getInstance().hideGameCursor();
                        break;
                }
            }
            log.info("Controller option switched to: " + getControlTypeInfo());
        }
        // ---
        if (FlexEngine.getInstance().isCloseOnScape() &&
                InputController.getInstance().isKeyPressed(KeyEvent.VK_ESCAPE)) {
            InputController.getInstance().releaseKey(KeyEvent.VK_ESCAPE);
            FlexEngine.getInstance().stop();
        }
        if (InputController.getInstance().isControlAndKeyPressed(KeyEvent.VK_P)) {
            InputController.getInstance().releaseKey(KeyEvent.VK_CONTROL);
            InputController.getInstance().releaseKey(KeyEvent.VK_P);
            FlexEngine.getInstance().pause(!FlexEngine.getInstance().isPaused());
        }
        if (InputController.getInstance().isControlAndKeyPressed(KeyEvent.VK_P)) {
            InputController.getInstance().releaseKey(KeyEvent.VK_CONTROL);
            InputController.getInstance().releaseKey(KeyEvent.VK_P);
            FlexEngine.getInstance().pause(!FlexEngine.getInstance().isPaused());
        }
        if (InputController.getInstance().isAltAndKeyPressed(KeyEvent.VK_ENTER)) {
            InputController.getInstance().releaseKey(KeyEvent.VK_ALT);
            InputController.getInstance().releaseKey(KeyEvent.VK_ENTER);
            GameWindow.getInstance().setFullscreen(!GameWindow.getInstance().isFullscreen());
        }
        if (InputController.getInstance().isKeyPressed(KeyEvent.VK_F12)) {
            InputController.getInstance().releaseKey(KeyEvent.VK_F12);
            //-- Debug info...
            FlexEngine.getInstance().setDebugOnScreen(
                    !FlexEngine.getInstance().isDebugOnScreen()
            );
        }
        if (InputController.getInstance().isControlAndKeyPressed(KeyEvent.VK_F)) {
            InputController.getInstance().releaseKey(KeyEvent.VK_CONTROL);
            InputController.getInstance().releaseKey(KeyEvent.VK_F);
            //-- Debug info...
            FlexEngine.getInstance().setAvarageInfoOnScreen(
                    !FlexEngine.getInstance().isAvarageInfoOnScreen()
            );
        }
    }

    /**
     * Rumble main joystick with force (float) for the time (ms)
     *
     * @param force
     * @param time
     */
    public void rumbleJoystick(float force, long time) {
        AsyncTask.newInstance().run(() -> {
            try {
                if (Objects.nonNull(joystick) && controlType == CONTROLLER_JOYSTICK &&
                        force > 0.0f && !rumbling) {
                    rumbling = true;
                    Rumbler[] motores = joystick.getRumblers();
                    for (Rumbler r : motores) {
                        r.rumble(force);
                    }
                    // ---
                    try {
                        Thread.currentThread().sleep(time);
                    } catch (Exception e) {
                        log.warn("Fail to wait time for rumble joystick!", e);
                    }
                    // ---
                    motores = joystick.getRumblers();
                    for (Rumbler r : motores) {
                        r.rumble(0.0f);
                    }
                }
            } catch (Exception e) {
                log.warn("Fail to rumble joystick!", e);
            } finally {
                rumbling = false;
            }
        });
        // ---
    }

    private void cleanJoysticks() {
        controlType = CONTROLLER_KEYBOARD;
        joystick = null;
    }

    public String getControlTypeInfo() {
        return getControlType() == CONTROLLER_KEYBOARD ?
                CONTROLLER_KEYBOARD_TYPE : CONTROLLER_JOYSTICK_TYPE;
    }

    @Override
    public void keyTyped(KeyEvent e) {
        keysEvents.put(e.getKeyCode(), KeyEvent.KEY_TYPED);
        lastKey = e.getKeyChar();
    }

    @Override
    public void keyPressed(KeyEvent e) {
        keysEvents.put(e.getKeyCode(), KeyEvent.KEY_PRESSED);
        lastKey = e.getKeyChar();
    }

    @Override
    public void keyReleased(KeyEvent e) {
        if (KeyEvent.VK_PRINTSCREEN == e.getKeyCode()) {
            GameWindow.captureScreenShot();
        }
        keysEvents.put(e.getKeyCode(), KeyEvent.KEY_RELEASED);
        releaseLastKey();
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        mouseEvents.put(e.getButton(), MouseEvent.MOUSE_CLICKED);
    }

    @Override
    public void mousePressed(MouseEvent e) {
        mouseEvents.put(e.getButton(), MouseEvent.MOUSE_PRESSED);
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        mouseEvents.put(e.getButton(), MouseEvent.MOUSE_RELEASED);
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        mouseOnWindow = true;
    }

    @Override
    public void mouseExited(MouseEvent e) {
        mouseOnWindow = false;
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        updateMousePosition(e);
    }

    @Override
    public void mouseMoved(MouseEvent e) {
        updateMousePosition(e);
    }

    @Override
    public void mouseWheelMoved(MouseWheelEvent e) {
    }

    @Override
    public void windowOpened(WindowEvent e) {
    }

    @Override
    public void windowClosing(WindowEvent e) {
    }

    @Override
    public void windowClosed(WindowEvent e) {
    }

    @Override
    public void windowIconified(WindowEvent e) {
    }

    @Override
    public void windowDeiconified(WindowEvent e) {
    }

    @Override
    public void windowActivated(WindowEvent e) {
    }

    @Override
    public void windowDeactivated(WindowEvent e) {
    }

    private void updateMousePosition(MouseEvent e) {
        mouseX = e.getX() - GameWindow.getInstance().getInsets().left;
        mouseY = e.getY() - GameWindow.getInstance().getInsets().top;
        CursorController.getInstance().getGameCursor().setLocation(
                mouseX,
                mouseY
        );
    }

    public void releaseKey(int keyCode) {
        keysEvents.remove(keyCode);
    }

    public void releaseMouseButton(int mouseButton) {
        mouseEvents.remove(mouseButton);
    }

    public boolean isKeyPressed(int keyCode) {
        return keysEvents.get(keyCode) != null
                && keysEvents.get(keyCode) == KeyEvent.KEY_PRESSED;
    }

    public boolean isKeyTyped(int keyCode) {
        return keysEvents.get(keyCode) != null
                && keysEvents.get(keyCode) == KeyEvent.KEY_TYPED;
    }

    public boolean isKeyReleased(int keyCode) {
        return keysEvents.get(keyCode) == null
                || keysEvents.get(keyCode) == KeyEvent.KEY_RELEASED;
    }

    public boolean isControlAndKeyPressed(int keyCode) {
        return isKeyPressed(KeyEvent.VK_CONTROL) && isKeyPressed(keyCode);
    }

    public boolean isAltAndKeyPressed(int keyCode) {
        return isKeyPressed(KeyEvent.VK_ALT) && isKeyPressed(keyCode);
    }

    public boolean isAltGraphAndKeyPressed(int keyCode) {
        return isKeyPressed(KeyEvent.VK_ALT_GRAPH) && isKeyPressed(keyCode);
    }

    public boolean isShiftAndKeyPressed(int keyCode) {
        return isKeyPressed(KeyEvent.VK_SHIFT) && isKeyPressed(keyCode);
    }

    // -- mouse states
    public boolean isMouseClicked(int mouseButton) {
        return mouseEvents.get(mouseButton) != null
                && mouseEvents.get(mouseButton) == MouseEvent.MOUSE_CLICKED;
    }

    public boolean isMousePressed(int mouseButton) {
        return mouseEvents.get(mouseButton) != null
                && mouseEvents.get(mouseButton) == MouseEvent.MOUSE_PRESSED;
    }

    public boolean isMouseReleased(int mouseButton) {
        return mouseEvents.get(mouseButton) == null
                || mouseEvents.get(mouseButton) == MouseEvent.MOUSE_RELEASED;
    }

    public boolean isControlAndMouseClicked(int mouseButton) {
        return isKeyPressed(KeyEvent.VK_CONTROL) && isMouseClicked(mouseButton);
    }

    public boolean isAltAndMouseClicked(int mouseButton) {
        return isKeyPressed(KeyEvent.VK_ALT) && isMouseClicked(mouseButton);
    }

    public boolean isAltGraphAndMouseClicked(int mouseButton) {
        return isKeyPressed(KeyEvent.VK_ALT_GRAPH) && isMouseClicked(mouseButton);
    }

    public boolean isShiftAndMouseClicked(int mouseButton) {
        return isKeyPressed(KeyEvent.VK_SHIFT) && isMouseClicked(mouseButton);
    }

    public void releaseLastKey() {
        lastKey = (char) 0;
    }

    public void releaseAll() {
        releaseMouse();
        releaseKeys();
        releaseLastKey();
    }

    public void releaseKeys() {
        keysEvents.clear();
    }

    public void releaseMouse() {
        mouseEvents.clear();
    }

    @Data
    public static class JoystickButtons {
        // ---
        Component buttonSelect;
        Component buttonStart;
        Component buttonA;
        Component buttonB;
        Component buttonX;
        Component buttonY;
        Component axisX;
        Component axisY;
        // ---
        Component pov;
        Component buttonUp;
        Component buttonDown;
        Component buttonLeft;
        Component buttonRight;
        // ---
        public static final float POV_LEFT = 1.0f;
        public static final float POV_RIGHT = 0.5f;
        public static final float POV_UP = 0.25f;
        public static final float POV_DOWN = 0.75f;
        // ---
    }

    // ---
    private static InputController instance;

    public static InputController getInstance() {
        if (Objects.isNull(instance)) {
            instance = new InputController();
        }
        return instance;
    }

}
