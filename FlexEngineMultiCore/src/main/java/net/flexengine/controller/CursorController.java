/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.flexengine.controller;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import net.flexengine.model.cursor.GameCursor;
import net.flexengine.view.GameWindow;
import net.flexengine.view.Texture;

import javax.imageio.ImageIO;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.Objects;

/**
 * @author Luann R. Athayde
 * @version 1.0
 * @since 1.0
 */
@Data
@Slf4j
@Builder
@AllArgsConstructor
public class CursorController {

    private boolean showBasicCursor;
    private boolean showGameCursor;
    private boolean exclusiveMouse;
    private GameCursor gameCursor;
    private Cursor hideCursor;
    private Robot robot;

    public CursorController() {
        this("CursorDefault");
    }

    protected CursorController(String defaultCursor) {
        showBasicCursor = true;
        showGameCursor = false;
        exclusiveMouse = false;
        gameCursor = new GameCursor();
        hideCursor = Toolkit.getDefaultToolkit().createCustomCursor(
                Toolkit.getDefaultToolkit().getImage(""),
                new Point(0, 0), "hideCursor");
        try {
            Texture gameCursorTexture = TextureController.getInstance().getTexture(
                    String.format("%s.png", defaultCursor),
                    ResourceController.getLocale(String.format("textures/cursors/%s.png", defaultCursor))
            );
            gameCursor.setTexture(gameCursorTexture);
            robot = new Robot();
        } catch (AWTException e) {
            log.error("Fail to initialize basic cursor controller: {}", e, e);
            throw new IllegalStateException(e);
        }
    }

    public void hideBasicCursor() {
        GameWindow.getInstance().setCursor(hideCursor);
        showBasicCursor = false;
    }

    public void showBasicCursor() {
        if (isShowGameCursor()) {
            hideGameCursor();
        }
        GameWindow.getInstance().setCursor(Cursor.getDefaultCursor());
        showBasicCursor = true;
    }

    public void hideGameCursor() {
        showGameCursor = false;
    }

    public void showGameCursor() {
        if (isShowBasicCursor()) {
            hideBasicCursor();
        }
        showGameCursor = true;
    }

    public void setBasicCursor(String cursorImage, String name) {
        try {
            String imgPath = ResourceController.getCursorPathFor(cursorImage);
            Image img = ImageIO.read(new File(imgPath));
            Cursor cursor = Toolkit.getDefaultToolkit().createCustomCursor(img, getGameCursor().getLocation(), name);
            GameWindow.getInstance().setCursor(cursor);
        } catch (IOException | IndexOutOfBoundsException | HeadlessException e) {
            log.error("CursorController::setBasicCursor(): " + e);
        }
    }

    public void setGameCursor(String name, Texture img, Point loc) {
        // ---
        getGameCursor().setName(name);
        getGameCursor().setTexture(img);
        // ---
        if (Objects.nonNull(loc)) {
            getGameCursor().setLocation(loc);
            mouseMove(loc.x, loc.y);
        }
    }

    public void setGameCursor(GameCursor gameCursor) {
        if (Objects.nonNull(gameCursor) && Objects.nonNull(gameCursor.getTexture())) {
            this.gameCursor.setVisible(false);
            this.gameCursor = gameCursor;
            this.gameCursor.setVisible(true);
        }
    }

    public Texture getCursorTexture() {
        return getGameCursor().getTexture();
    }

    public void setCursorTexture(Texture texture) {
        getGameCursor().setTexture(texture);
    }

    public void setCursorTexture(String textureName) {
        getGameCursor().setTexture(TextureController.getInstance().getTexture(textureName));
    }

    public void setExclusiveMouse(boolean exclusiveMouse) {
        this.exclusiveMouse = exclusiveMouse;
        getGameCursor().setExclusiveMode(exclusiveMouse);
    }

    public void mouseMove(int x, int y) {
        try {
            robot.mouseMove(x, y);
        } catch (Exception e) {
            log.warn("mouseMove(): " + e.getMessage());
        }
    }

    public void centerCursor() {
        try {
            int centerX = GameWindow.getInstance().getX() + GameWindow.getInstance().getWidth() / 2;
            int centerY = GameWindow.getInstance().getY() + GameWindow.getInstance().getHeight() / 2;
            mouseMove(centerX, centerY);
            getGameCursor().move(centerX, centerY);
        } catch (Exception e) {
            log.warn("centerCursor(): " + e.getMessage());
        }
    }

    private static CursorController instance;

    public static CursorController getInstance() {
        if (Objects.isNull(instance)) {
            instance = new CursorController();
        }
        return instance;
    }

}
