/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.flexengine.controller;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import net.flexengine.model.scene.EngineIntroScene;
import net.flexengine.model.scene.GameScene;
import net.flexengine.model.scene.LoadScene;
import net.flexengine.model.scene.SceneBundle;
import net.flexengine.utils.AsyncTask;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * @author Luann R. Athayde
 * @version 1.0
 * @since 1.0
 */
@Data
@Slf4j
@Builder
@AllArgsConstructor
public class SceneController {

    private static final Map<GameScene, Boolean> loadedOnce = new HashMap<>();

    protected SceneBundle globalBundle;
    protected SceneBundle sceneBundle;

    protected GameScene currentScene;
    protected GameScene nextScene;
    protected GameScene loadingScene;

    protected boolean resourcesLoadedForLoadScene;

    public SceneController() {
        globalBundle = new SceneBundle();
        sceneBundle = new SceneBundle();
        currentScene = new EngineIntroScene();
        nextScene = new GameScene("BasicScene");
        loadingScene = new LoadScene();
        loadingScene.setGlobalBundle(globalBundle);
        resourcesLoadedForLoadScene = false;
        this.init();
    }

    public void init() {
        currentScene.init(this.sceneBundle);
    }

    public SceneController setNextScene(GameScene nextScene) {
        return setNextScene(nextScene, new SceneBundle());
    }

    public SceneController setNextScene(GameScene nextScene, SceneBundle bundle) {
        this.nextScene = nextScene;
        this.sceneBundle = bundle;
        if (Objects.nonNull(this.nextScene)) {
            this.nextScene.setGlobalBundle(globalBundle);
        }
        return this;
    }

    public void changeScene() {
        new SceneChangeTask().start();
    }

    public void setLoadingScene(GameScene loadingScene) {
        if (Objects.nonNull(loadingScene)) {
            this.loadingScene = loadingScene;
            this.loadingScene.setGlobalBundle(globalBundle);
            this.resourcesLoadedForLoadScene = false;
        }
    }

    /**
     * Scene change task class responsible for change the current scene for the next.
     */
    @Data
    class SceneChangeTask implements Runnable {

        private Thread tSceneChangeTask;

        public void start() {
            tSceneChangeTask = new Thread(this, "SceneChangeTask");
            tSceneChangeTask.setDaemon(true);
            tSceneChangeTask.start();
        }

        @Override
        public void run() {
            if (currentScene instanceof EngineIntroScene) {
                EngineIntroScene introScene = (EngineIntroScene) currentScene;
                if (!introScene.isDone()) {
                    return;
                }
            }
            // Current scene change to Loading scene...
            GameScene oldScene = currentScene;
            log.info("Changing to loading scene...");
            // -- load resources first
            if ( Boolean.FALSE.equals(resourcesLoadedForLoadScene) ) {
                log.info("Loading resources...");
                loadedOnce.computeIfAbsent(loadingScene, k -> {
                    loadingScene.loadResourcesOnce();
                    return Boolean.TRUE;
                });
                loadingScene.loadResources();
                log.info("Load completed...");
                log.info("Initalizing componentes for loading...");
                loadingScene.initComponents(sceneBundle);
                resourcesLoadedForLoadScene = true;
            }
            // ---
            loadingScene.init(sceneBundle);
            currentScene = loadingScene;
            // ---
            log.info("Changing scene to [" + nextScene + "]...");
            if ( Objects.nonNull(oldScene) ) {
                AsyncTask.newInstance().run( () -> oldScene.end(sceneBundle) );
            }
            // ---
            if (Objects.nonNull(nextScene)) {
                globalBundle.putExtra(LoadScene.GLOBAL_PROGRESS_KEY, 0d);
                log.info("Initializing next scene...");
                // load resources first...
                log.info("Loading resources for scene {}...", nextScene);
                loadedOnce.computeIfAbsent(nextScene, k -> {
                    nextScene.loadResourcesOnce();
                    return Boolean.TRUE;
                });
                nextScene.loadResources();
                log.info("Loading {} completed...", nextScene);
                // init components...
                nextScene.initComponents(sceneBundle);
                // init...
                nextScene.init(sceneBundle);
            }
            // ---
            // Finalize loading scene...
            loadingScene.end(sceneBundle);
            currentScene = nextScene;
            // ---
            log.info("Scene changed!");
        }
    }

    private static SceneController instance;

    public static SceneController getInstance() {
        if (Objects.isNull(instance)) {
            instance = new SceneController();
        }
        return instance;
    }

}
