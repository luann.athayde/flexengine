/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.flexengine.controller;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import net.flexengine.model.components.DebugInfo;
import net.flexengine.model.config.Configuration;
import net.flexengine.utils.AsyncTask;
import net.flexengine.utils.RenderInfoUtil;
import net.flexengine.view.GameWindow;
import org.slf4j.simple.SimpleLogger;

import javax.swing.*;
import java.util.Objects;

/**
 * @author Luann R. Athayde
 * @version 1.0
 * @since 1.0
 */
@Data
@Slf4j
@Builder
@AllArgsConstructor
public class FlexEngine {

    public static final DebugInfo DEBUG_INFO = new DebugInfo();

    protected String gameName;
    protected Configuration config;
    protected EngineHandler engineHandler;
    protected LogicController logicController;
    protected RenderController renderController;
    protected SceneController sceneController;
    protected GameWindow gameWindow;
    protected InputController inputController;
    protected boolean paused;
    protected boolean debugOnScreen;
    protected boolean avarageInfoOnScreen;
    protected boolean closeOnScape;

    public FlexEngine() {
        log.info("Creating engine...");
        config = ConfigurationController.getInstance().getDefaultConfiguration();
        engineHandler = EngineHandler.getInstance();
        logicController = LogicController.getInstance();
        renderController = RenderController.getInstance();
        sceneController = SceneController.getInstance();
        gameWindow = GameWindow.getInstance();
        inputController = InputController.getInstance();
        paused = false;
        debugOnScreen = false;
        closeOnScape = true;
        loadEngine();
        applyConfiguration();
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception e) {
            log.error("Fail to apply look and feel!", e);
        }
        log.info("Engine created...");
    }

    protected void loadEngine() {
        LanguageController.loadLanguages();
        FontController.loadAllAvailableFonts();
    }

    protected void applyConfiguration() {
        if (applyConfiguration(config)) {
            log.info("Configuration applied successfully...");
        } else {
            Configuration config = new Configuration();
            ConfigurationController.getInstance().setDefaultConfiguration(config);
            ConfigurationController.getInstance().saveConfiguration();
            applyConfiguration();
        }
    }

    public boolean applyConfiguration(Configuration config) {
        if ( Objects.nonNull(config) ) {
            CursorController.getInstance().hideBasicCursor();
            ConfigurationController.getInstance().setDefaultConfiguration(config);
            LanguageController.setDefaultLanguageName(config.getLanguage());
            SoundController.getInstance().setEnable(config.isSound());
            SoundController.getInstance().setMaxSimultaneousSounds(config.getChannels());
            // ---
            GameWindow.getInstance().setSize(
                    config.getResolution().getWidth(),
                    config.getResolution().getHeight());
            GameWindow.getInstance().setFullscreen(config.isFullScreen());
            // ---
            // Log configuration...
            // System.setProperty(org.slf4j.simple.SimpleLogger.DEFAULT_LOG_LEVEL_KEY, "Info");
            // ---
            return true;
        }
        return false;
    }

    public void start() {
        log.info("Starting engine...");
        logicController.setDaemon(true);
        renderController.setDaemon(true);
        renderController.initRender();
        // start controllers
        renderController.start();
        logicController.start();
        // --
        engineHandler.start();
        // --
        log.info("Engine started...");
    }

    public void stop() {
        try {
            if (getConfig().isConfirmOnClose()) {
                AsyncTask.newInstance().run(() -> {
                    String msg = LanguageController.getDefaultLanguage().getValue(
                            "flexengine.window.stop_message"
                    );
                    String title = LanguageController.getDefaultLanguage().getValue(
                            "flexengine.window.stop_title"
                    );
                    int value = JOptionPane.showConfirmDialog(
                            gameWindow, msg, title, JOptionPane.OK_CANCEL_OPTION, JOptionPane.WARNING_MESSAGE
                    );
                    if (JOptionPane.OK_OPTION == value) {
                        stopEngine();
                    }
                });
            } else {
                stopEngine();
            }
        } catch (Exception e) {
            log.error("Fail to stop engine/game: {}", e, e);
        }
    }

    protected void stopEngine() {
        // ---
        SoundController.getInstance().stopAll();
        // ---
        logicController.setRunning(false);
        renderController.setRunning(false);
        engineHandler.stop();
        gameWindow.dispose();
        RenderInfoUtil.closeAll();
        log.info("FlexEngine stopped...");
    }

    public boolean pause(boolean paused) {
        this.paused = paused;
        return this.paused;
    }

    public void setGameName(String gameName) {
        this.gameName = gameName;
        GameWindow.getInstance().setTitle(gameName);
    }

    public String getGameName() {
        String windowTitle = GameWindow.getInstance().getTitle();
        if( windowTitle.equalsIgnoreCase(gameName) ) {
            gameName = windowTitle;
        }
        return gameName;
    }

    private static FlexEngine instance;

    public static FlexEngine getInstance() {
        if (Objects.isNull(instance)) {
            instance = new FlexEngine();
        }
        return instance;
    }

}
