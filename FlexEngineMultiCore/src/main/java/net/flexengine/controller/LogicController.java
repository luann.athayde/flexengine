/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.flexengine.controller;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.flexengine.model.scene.GameScene;
import net.flexengine.model.scene.LoadScene;

import java.util.Objects;

/**
 * @author Luann Athayde
 */
@Data
@Slf4j
@Builder
@AllArgsConstructor
public class LogicController extends Thread {

    public static final int LOGIC_CONTROLLER_ID = 1000;

    private int threadID;
    private boolean running;
    private long logicTime;
    private long showLogicTime;

    public LogicController() {
        this.threadID = LOGIC_CONTROLLER_ID;
        this.running = false;
        this.logicTime = 0L;
        this.showLogicTime = 0L;
    }

    @Override
    public void run() {
        running = true;
        while (running) {
            logicTime = System.currentTimeMillis();
            try {
                InputController.getInstance().update();
                // ---
                GameScene scene = SceneController.getInstance().getCurrentScene();
                if (Objects.nonNull(scene)) {
                    boolean paused = FlexEngine.getInstance().isPaused();
                    boolean loadScene = scene instanceof LoadScene;
                    if (!paused || loadScene) {
                        scene.update();
                    }
                }
                // ---
                if (FlexEngine.getInstance().isDebugOnScreen()) {
                    FlexEngine.DEBUG_INFO.update();
                }
                // --
                InputController.getInstance().releaseMouse();
                EngineHandler.getInstance().incrementUps();
                // --
            } catch (Exception e) {
                log.error("run(): Fail on running...", e);
            }
            showLogicTime = logicTime = System.currentTimeMillis() - logicTime;
            // --
            sleep();
        }
    }

    private void sleep() {
        try {
            long sleepTime = EngineHandler.getInstance().calculeLogicSleepTime();
            if (sleepTime > 0) {
                Thread.sleep(sleepTime);
            }
        } catch (Exception e) {
            log.error("### Fail to sleep!", e);
            Thread.currentThread().interrupt();
        }
    }

    @Override
    public int hashCode() {
        return Objects.hash(threadID);
    }

    @Override
    public boolean equals(Object obj) {
        if (Objects.nonNull(obj) && obj instanceof LogicController) {
            LogicController other = (LogicController) obj;
            return getThreadID() == other.getThreadID();
        }
        return false;
    }

    @Override
    public String toString() {
        return "LogicController[" + getThreadID() + "]";
    }

    private static LogicController instance;

    public static LogicController getInstance() {
        if (Objects.isNull(instance)) {
            instance = new LogicController();
        }
        return instance;
    }

}
