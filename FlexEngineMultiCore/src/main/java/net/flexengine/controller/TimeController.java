/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.flexengine.controller;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.flexengine.model.time.DateTime;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * @author Luann R. Athayde
 * @version 1.0
 * @since 1.0
 */
@Data
@Slf4j
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TimeController implements Runnable {

    private static final List<TimeController> timers;
    private static TimeController instance;

    static {
        instance = null;
        timers = new ArrayList<>();
    }

    /**
     * 1 second = 1000 milis
     */
    public static final long MILI_SECOND = 1000L;
    /**
     * 1 second = 1000000000 nanos
     */
    public static final long NANO_SECOND = 1000000000L;
    /**
     * 1 Millisecond = 1000000 nanos
     */
    public static final long MILISECOND_IN_NANO = 1000000L;

    public static final long MIN_SLEEP_MILIS = 3L;
    public static final long MIN_SLEEP_NANOS = 3000000L;

    private Thread timerThread;
    private long totalTime;
    private boolean counting;
    private boolean paused;

    @Override
    public synchronized void run() {
        while (counting) {
            try {
                if (paused) {
                    this.wait();
                }
                long timeBefore = milisTime();
                this.wait(1000);
                long diff = milisTime() - timeBefore;
                totalTime += diff;
                // ---
            } catch (InterruptedException e) {
                log.info("Timer closed...");
                Thread.currentThread().interrupt();
            }
        }
        timers.remove(this);
    }

    public void reset() {
        this.totalTime = 0;
    }

    public synchronized boolean start() {
        if (counting) {
            return false;
        }
        totalTime = 0;
        counting = true;
        paused = false;
        timerThread = new Thread(this, getClass().getSimpleName());
        timers.add(this);
        timerThread.setDaemon(true);
        timerThread.start();
        return true;
    }

    public void stop() {
        counting = false;
        paused = false;
        if (timerThread != null) {
            timerThread.interrupt();
        }
        timerThread = null;
    }

    public synchronized void pause(boolean pause) {
        this.paused = pause;
        if (!paused) {
            this.notifyAll();
        }
    }

    public long getTotalSeconds() {
        return getTotalTime() / MILISECOND_IN_NANO;
    }

    public DateTime getTime() {
        return new DateTime().setTime(totalTime);
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "{" + "time=" + getTime() + ", paused=" + paused + '}';
    }

    public static long nanoTime() {
        return System.nanoTime();
    }

    public static long milisTime() {
        return System.currentTimeMillis();
    }

    public static long nanoToMilis(long nanos) {
        return (nanos / MILISECOND_IN_NANO);
    }

    public static long miliToNanos(long milis) {
        return milis * MILISECOND_IN_NANO;
    }

    public static Date getDate() {
        return new Date();
    }

    public static List<TimeController> getTimers() {
        return timers;
    }

    public static TimeController getInstance() {
        if (Objects.isNull(instance)) {
            instance = new TimeController();
        }
        return instance;
    }

    public static TimeController newInstance() {
        return TimeController.builder().build();
    }

}
