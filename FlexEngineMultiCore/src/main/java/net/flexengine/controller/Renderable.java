/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.flexengine.controller;

import java.awt.Graphics2D;

/**
 * @author Luann R. Athayde
 * @version 1.0
 * @since 1.0
 */
public interface Renderable {

    void render(Graphics2D graphics);
    boolean isVisible();
    
}
