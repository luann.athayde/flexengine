/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.flexengine.controller;

import lombok.Builder;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import net.flexengine.model.language.Language;
import net.flexengine.model.language.LanguageFilter;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Objects;
import java.util.Properties;

/**
 * @author Luann R. Athayde
 * @version 1.0
 * @since 1.0
 */
@Data
@Slf4j
@Builder
public class LanguageController {

    private static HashMap<String, Language> languages;
    private static String defaultLanguageName;

    static {
        defaultLanguageName = "English";
        languages = new HashMap<>();
    }

    protected LanguageController() {
        LanguageController.loadLanguages();
    }

    public static Language getLanguage(String name) {
        Language lang = languages.get(name);
        return lang != null ? lang : new Language("default", new HashMap<>());
    }

    public static Language getDefaultLanguage() {
        return getLanguage(defaultLanguageName);
    }

    public static void installLanguage(String name, Language lang) {
        languages.put(name, lang);
    }

    public static String getDefaultLanguageName() {
        return defaultLanguageName;
    }

    public static void setDefaultLanguageName(String defaultLanguageName) {
        LanguageController.defaultLanguageName = defaultLanguageName;
    }

    public static boolean containsLanguage(String languageName) {
        return languages.containsKey(languageName);
    }

    public static Language[] getLanguages() {
        try {
            if (languages.isEmpty()) {
                loadLanguages();
            }
            Language[] langs = new Language[languages.size()];
            int i = 0;
            for (String key : languages.keySet()) {
                langs[i] = languages.get(key);
                i++;
            }
            return langs;
        } catch (Exception e) {
            log.error("LanguageController::getLanguages(): " + e.getMessage());
        }
        return new Language[]{};
    }

    public static void loadLanguages(String path) {
        try {
            log.info("Loading languages...");
            languages.clear();
            File langFolder = new File(ResourceController.getLocale(path));
            File[] langFiles = langFolder.listFiles(new LanguageFilter());
            if (Objects.nonNull(langFiles) && langFiles.length > 0) {
                for (File langFile : langFiles) {
                    log.info("Language found [" + langFile + "]...");
                    Properties p = new Properties();
                    try (InputStreamReader reader =
                                 new InputStreamReader(new FileInputStream(langFile), StandardCharsets.UTF_8)) {
                        p.load(reader);
                    }
                    HashMap<String, String> dictionary = new HashMap<>();
                    p.stringPropertyNames().forEach(key -> dictionary.put(key, p.getProperty(key)));
                    String languageName = p.getProperty("language");
                    Language language = new Language(languageName, dictionary);
                    languages.put(languageName, language);
                }
            }
            log.info("Languages loaded!");
            log.info("Available languages = " + languages.keySet());
        } catch (Exception e) {
            log.error("Failed to load language from path [{}]!", path, e);
        }
    }

    public static void loadLanguages() {
        loadLanguages(ResourceController.LANGUAGES_PATH);
    }

}
