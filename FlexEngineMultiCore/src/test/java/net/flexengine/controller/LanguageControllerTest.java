package net.flexengine.controller;

import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

@Slf4j
@RunWith(MockitoJUnitRunner.class)
public class LanguageControllerTest {

    // @InjectMocks
    // private LogicController logicController;

    @Before
    public void configure() {


    }

    @Test
    public void mustSuccedWhenLoadingLanguages() {

        LanguageController.loadLanguages();

        Assert.assertNotNull( LanguageController.getDefaultLanguage() );

    }


}
